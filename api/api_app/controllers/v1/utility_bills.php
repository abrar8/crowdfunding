<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Utility_bills extends My_Controller {

    // constructor
    function __construct() {
        parent::__construct();

        $this->load->model("utility_bills_model");
    }

    // Get List of All available provider types
    public function getProviderTypes() {
        $data = $this->utility_bills_model->getProviderTypes();
        if ($data == NULL) {
            $response["error"] = true;
            $response["message"] = "No records founded.";
        } else {
            $response["error"] = false;
            $response["types"] = $data;
            $response["message"] = "List all provider types.";
        }
        $this->EchoResponse(200, $response);
    }

// End - getProviderTypes
    // getProviderList
    public function getProviderList() {

        $this->form_validation->set_rules('type', 'Type', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
        } else {

            $data = $this->utility_bills_model->getProviderList($this->input->post('type'));
            if ($data == NULL) {
                $response["error"] = true;
                $response["message"] = "No records founded.";
            } else {
                $response["error"] = false;
                $response["providers"] = $data;
                $response["message"] = "List all provider.";
            }
        }
        $this->EchoResponse(200, $response);
    }

    private function saveBill($provider_id, $customer_id) {

        // Varify provider_id
        $varify_provider_id = $this->utility_bills_model->varify_provider_id($provider_id);

        if ($varify_provider_id == true) {
            $data = array(
                'userId' => $this->token_user_id,
                'provider_id' => $provider_id,
                'customer_id' => $customer_id,
                'paid' => 0,
                'status' => 1
            );

            $insert = $this->utility_bills_model->saveBill($data);
            if ($insert == false) {
                return FALSE;
            } else {
                return $insert;
            }
        } else {
            return FALSE;
        }


        $this->EchoResponse(200, $response);
    }

    public function getMyBills() {

        $data = $this->utility_bills_model->getMyBills($this->token_user_id);
        if ($data == NULL) {
            $response["error"] = true;
            $response["message"] = "No bills were founded.";
        } else {
            $response["error"] = false;
            $response["types"] = $data;
            $response["message"] = "List all my payable bills.";
        }

        $this->EchoResponse(200, $response);
    }

    public function payBill() {

//        $this->form_validation->set_rules('bill_id', 'Bill ID', 'trim|required');
        $this->form_validation->set_rules('provider_id', 'Provider Id', 'trim|required');
        $this->form_validation->set_rules('customer_id', 'Customer Id', 'trim|required');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required');

        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {

            $bill_id = $this->saveBill($this->input->post('provider_id'), $this->input->post('customer_id'));
            if ($bill_id == FALSE) {
                $response["error"] = true;
                $response["message"] = "Sorry provider id is invalid.";
                $this->EchoResponse(200, $response);
            } else {

                $amount = $this->input->post('amount');

                $this->load->model('transactions_model');
                $userCr = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance

                if ($userCr['credit'] < $amount) { // Verify user available balance
                    $response["error"] = true;
                    $response["message"] = 'Insufficient balance. Your balance ' . $userCr['credit'];
                    $this->EchoResponse(200, $response);
                } else {

                    $varify_bill_id = $this->utility_bills_model->varify_bill_id($bill_id);
                    if ($varify_bill_id == true) {
                        $utility_bills_account_id = $this->utility_bills_model->get_utility_bills_account_id();

                        $transaction_track_code = rand(1, 99999);
                        $debit = array('userId' => $this->token_user_id,
                            'refId' => $utility_bills_account_id, //$bill_reference_number, 
                            'dr' => $amount,
                            'gps' => $this->input->post('gps'),
                            'devId' => $this->input->post('dev_id'),
                            'type' => 'TR',
                            'mode' => 'UB', // mode = UB for utility Bill transaction
                            'transaction_code' => $transaction_track_code,
                            'description' => $this->input->post('description')
                        );
                        $credit = array('userId' => $this->token_user_id,
                            'refId' => $this->token_user_id,
                            'cr' => $amount,
                            'gps' => $this->input->post('gps'),
                            'devId' => $this->input->post('dev_id'),
                            'type' => 'TR',
                            'mode' => 'UB', // mode = UB for utility Bill transaction
                            'transaction_code' => $transaction_track_code,
                            'description' => $this->input->post('description')
                        );

                        $transaction_data = $this->utility_bills_model->payBill($this->token_user_id, $debit, $credit);
                        if ($transaction_data) {
                            $upd_data = array(
                                'transaction_track_code' => $transaction_track_code,
                                'amount' => $amount,
                                'paid' => 1
                            );
                            // Update Bill
                            $this->utility_bills_model->update_bill($bill_id, $upd_data);
                            $currency_code = $this->transactions_model->my_currency_code($this->token_user_id);
                            // Response
                            $user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance
                            $response["error"] = false;
                            $response["transaction_id"] = $transaction_data->transId;
                            $response["transaction_track_code"] = strval($transaction_track_code);
                            $response["user_id"] = $transaction_data->userId;
                            $response["paid_amount"] = $amount;
                            $response["credit"] = strval(number_format($user['total_daily_contributions_credit'] + $user['credit'], 2)); //strval($user['credit']);
                            $response["debit"] = $transaction_data->dr;
                            $response["gps"] = $transaction_data->gps;
                            $response["dev_id"] = $transaction_data->devId;
                            $response["created"] = $transaction_data->created;
                            $response["currency_code"] = $currency_code;
                            $response["message"] = "You have charged $amount for utility bill.";
                            $this->EchoResponse(200, $response);
                        }
                    } else {
                        $response["error"] = true;
                        $response["message"] = "Sorry bill id is invalid.";
                        $this->EchoResponse(200, $response);
                    }
                }
            }
        }
    }

    // Paid Bills History
    public function myPaidBillsHistory() {
        $data = $this->utility_bills_model->myPaidBillsHistory($this->token_user_id);
        if ($data == NULL) {
            $response["error"] = true;
            $response["message"] = "No records founded.";
        } else {
            $response["error"] = false;
            $response["types"] = $data;
            $response["message"] = "List all provider types.";
        }
        $this->EchoResponse(200, $response);
    }

}

// End - Utility_bills Class
?>