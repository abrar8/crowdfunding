<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Crowdfund extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('crowdfund_model');
        $this->load->model('transactions_model');
        $this->load->helper('string');
    }
	

    public function create_Crowdfund() {        
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {			
			 ///////////////////////// Image Upload START //////////////////////
            $config['upload_path'] = UPLOADS_DIR.'crowdfund/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif|bmp';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            $config['overwrite'] = FALSE;
            $config['file_name'] = mt_rand().'.png';

            $this->load->library('upload', $config);			
            if (!$this->upload->do_upload('file_name')) {
                $response["error"] = true;
                $response["message"] = $this->upload->display_errors();
                $this->EchoResponse(200, $response);
            } else {
                $file = $this->upload->data();
                $image_name = $file['file_name']; // Original Image
				$title = $this->input->post('title');
				$description = $this->input->post('description');
				$reference_number = random_string('alnum', 12);			
				$data = array('userId'           => $this->token_user_id,
							  'title'            => addslashes($title),
							  'description'      => addslashes($description),
							  'image'            => $image_name,
							  'status'           => 1, // Status = 0 for pending Charity Approval Activation
							  'reference_number' => $reference_number); 
				$status = $this->crowdfund_model->create_crowdfund($data);
				if ($status) {                   
					$response["error"] = false;
					$response["message"] = "Crowdfund was successfully created.";
					$this->EchoResponse(200, $response);
				} else {
					$response["error"] = true;
					$response["message"] = 'Unable to save crowdfund record in database.';
					$this->EchoResponse(200, $response);
				}
			} // else
        } //else
    } // create_Crowdfund


    public function getCrowdfund() {

        $data = $this->crowdfund_model->getCrowdfund($this->token_user_id);
        if ($data == NULL) {
            $response["error"] = true;
            $response["message"] = 'No records found.';
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            foreach ($data as $key => $value) {
                $response_array["id"]                        = $value->id;
                $response_array["crowdfund_flag"]            = $value->crowdfund_flag;
                $response_array["invitation_flag"]           = $value->invitation_flag;
                $response_array["reference_number"]          = $value->reference_number;
                $response_array["title"]                     = stripslashes($value->title);
                $response_array["description"]               = stripslashes($value->description);
                $response_array["image"]                     = ADMIN_URL.'uploads/crowdfund/'.$value->image;
                $response_array["created_by"]                = ucfirst($value->firstName).' '.ucfirst($value->lastName);
                $response["active_crowdfund_list_all"][$key] = $response_array;
            } // End - Foreach
            $response["message"] = "List all active Crowdfund.";
            $this->EchoResponse(200, $response);
        } // else
    } // getCrowdfund


    public function getMyCrowdfund() {

        $data = $this->crowdfund_model->getMyCrowdfund($this->token_user_id);
        if ($data == NULL) {
            $response["error"] = true;
            $response["message"] = 'No record found.';
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            foreach ($data as $key => $value) {
                $response_array["id"]                    = $value->id;
                $response_array["reference_number"]      = $value->reference_number;
                $response_array["title"]                 = stripslashes($value->title);
                $response_array["description"]           = stripslashes($value->description);
                $response_array["image"]                 = ADMIN_URL.'uploads/crowdfund/'.$value->image;
                $response_array["status"]                = $value->status;
                $response_array["created_by"]            = ucfirst($value->firstName).' '.ucfirst($value->lastName);
                $response["my_crowdfund_list_all"][$key] = $response_array;
            } // End - Foreach
            $response["message"] = "My Crowdfund list all.";
            $this->EchoResponse(200, $response);
        } // else
    } // getMyCrowdfund


	public function pendingInvitations() 
	{
        $invitations = $this->crowdfund_model->getPendingInvitations($this->token_user_id);
		$data = array();
        if ($invitations == NULL) {
            $response["error"] = true;
			$response["invitations"] = $data;
            $response["message"] = "No record found.";
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;			
			foreach($invitations as $invitation) :
				$data[] = array('firstName'        => stripslashes($invitation->firstName),
				                'lastName'         => stripslashes($invitation->lastName),
								'invitation_id'    => $invitation->invitation_id,
								'crowdfund_id'     => $invitation->crowdfund_id,
								'title'			   => $invitation->title,
								'description'      => stripslashes($invitation->description),
								'image'            => ADMIN_URL.'uploads/crowdfund/'.$invitation->image,
								'reference_number' => $invitation->reference_number
							   );
			endforeach;
            $response["invitations"] = $data;
            $this->EchoResponse(200, $response);
        }
    } // pendingInvitations

	
	public function removeCrowdfund() 
	{
        $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {			
            $status = $this->crowdfund_model->removeCrowdfund($this->token_user_id, $this->input->post('reference_number'));
            if ($status == FALSE) {
                $response["error"] = true;
                $response["message"] = 'No record found.';
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = false;
                $response["message"] = "Crowdfund was removed successfully.";
                $this->EchoResponse(200, $response);
            } // else
        }
    } // removeCrowdfund
	

	public function myDonations() 
	{
        $data = $this->crowdfund_model->myDonations($this->token_user_id);
        if ($data == FALSE) {
            $response["error"] = true;
            $response["message"] = 'No record found.';
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            foreach($data as $key => $value) {
                $response_array["donated_amount"]    = $value->amount;
                $response_array["reference_number"]  = $value->reference_number;
                $response_array["id"]                = $value->id;
                $response_array["title"]             = stripslashes($value->title);
                $response_array["description"]       = stripslashes($value->description);
                $response["my_donations_list"][$key] = $response_array;
            }
            $response["message"] = "List all my Crowdfund donations.";
            $this->EchoResponse(200, $response);
        } // else
    } // myDonations


    public function contributeCrowdfund() {
        $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $reference_number = $this->input->post('reference_number');
            $amount = $this->input->post('amount');
            $userCr = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance
            if ($userCr['credit'] < $this->input->post('amount')) 
			{ 
				// Verify user available balance
                $response["error"] = true;
                $response["message"] = 'Insufficient balance. Your balance ' . $userCr['credit'];
                $this->EchoResponse(200, $response);
            } else {
                $CrowdfundId = $this->crowdfund_model->getCrowdfundId($this->input->post('reference_number'));
                $transaction_code = random_string('alnum', 12);
                $debit = array('userId'           => $this->token_user_id,
							   'refId'            => $CrowdfundId,
							   'dr'               => $this->input->post('amount'),
							   'gps'              => $this->input->post('gps'),
							   'devId'            => $this->input->post('dev_id'),
							   'type'             => 'TR',
							   'mode'             => 'CF',
							   'transaction_code' => $transaction_code,
							   'description'      => $this->input->post('description')
							   );
                $credit = array('userId'           => $this->token_user_id,
								'refId'            => $this->token_user_id,
								'cr'               => $this->input->post('amount'),
								'gps'              => $this->input->post('gps'),
								'devId'            => $this->input->post('dev_id'),
								'type'             => 'TR',
								'mode'             => 'CF',
								'transaction_code' => $transaction_code,
								'description'      => $this->input->post('description')
							);
                $transaction_data = $this->crowdfund_model->crowdfundTransaction($this->token_user_id, $debit, $credit, $reference_number);
                if ($transaction_data) {
                    $user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance
                    $response["error"] = false;
                    $response["transaction_id"] = $transaction_data->transId;
                    $response["user_id"]        = $transaction_data->userId;
                    $response["credit"]         = strval($user['credit']);
                    $response["debit"]          = $transaction_data->dr;
                    $response["gps"]            = $transaction_data->gps;
                    $response["dev_id"]         = $transaction_data->devId;
                    $response["created"]        = $transaction_data->created;
                    $response["message"]        = "You have successfully donated ".$amount;
                    $this->EchoResponse(200, $response);
                }
            }
        }
    } // contributeCrowdfund
	

	public function crowdfund_detail() 
	{
        $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $data = $this->crowdfund_model->crowdfund_detail($this->input->post('reference_number'));
            $total_crowdfund = $this->crowdfund_model->get_total_crowdfund_amount($this->input->post('reference_number'));

            if ($data == NULL) {
                $response["error"] = true;
                $response["message"] = 'No record found.';
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = false;
                $response["crowdfund_data"] = $data;
                $response["total_crowdfund_amount"] = $total_crowdfund;
                $response["message"] = 'Specific Crowdfund details.';
                $this->EchoResponse(200, $response);
            }
        }
    } // crowdfund_detail
	

	public function inviteToCrowdfund() 
	{
		$this->form_validation->set_rules('id', 'Crowdfund ID', 'required');
		$this->form_validation->set_rules('user_list', 'Users List Object', 'required');
		if ($this->form_validation->run() == FALSE) {
			$response["error"] = true;
			$response["message"] = validation_errors();
			$this->EchoResponse(200, $response);
		} else {
			$this->load->helper('notification');
			$crowdfund_id = $this->input->post('id');
			$crowdfund = $this->crowdfund_model->getMyCrowdfundDetail($crowdfund_id);
			// [{"id":"125"},{"id": "145"},{"id":"134"}]
            $users = json_decode($this->input->post('user_list'));
            if(!empty($users)) {
                foreach ($users as $user) {
                    $this->crowdfund_model->inviteToCrowdfund($this->token_user_id, $user->id, $crowdfund->id, $crowdfund->reference_number);
                    $msg = array('notification_type' => "crowdfund_request",
                                 'reference_number'  => $crowdfund->reference_number,
                                 'title'             => $crowdfund->title,
                                 'sender_name'       => $crowdfund->firstName,
                                 'message'           => $crowdfund->firstName.' Has invited you to join '.$crowdfund->title
                                 );
                    $notification_data = array('message' => json_encode($msg));
                    sent_notify($notification_data, $user->id);
                }   
            } // if			
			$response["error"] = false;
			$response["message"] = 'Invitation sent successfully.';
			$this->EchoResponse(200, $response);
		}
	} // inviteToCrowdfund


	public function responseToInvitation() 
	{
        $this->form_validation->set_rules('id', 'Invitation ID', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $status = $this->input->post('status');
            $invitation_id = $this->input->post('id');

            $crowdfund_id = $this->crowdfund_model->responseToInvitation($invitation_id,$status);

            $crowdfund = $this->crowdfund_model->getMyCrowdfundDetail($crowdfund_id);
            $senderId = $this->crowdfund_model->getSenderId($this->input->post('id'));
            if ($status) 
			{
                $this->load->helper('notification');
                $msg = '';
                if ($this->input->post('status') == 1) 
				{
                    $msg = array('notification_type' => "action_crowdfund_request",
								 'action_request'    => 'Pending',
								 'reference_number'  => $crowdfund->reference_number,
								 'title'             => $crowdfund->title,
								 'sender_name'       => $crowdfund->firstName,
								 'message'           => $crowdfund->firstName.' Pending '.$crowdfund->title
							     );
                    $response["message"] = 'Pending';
                } elseif ($this->input->post('status') == 2) {
                    $msg = array('notification_type' => "action_crowdfund_request",
								 'action_request'    => 'accepted',
								 'reference_number'  => $crowdfund->reference_number,
								 'title'             => $crowdfund->title,
								 'sender_name'       => $crowdfund->firstName,
								 'message'           => $crowdfund->firstName.' Has accepted request '.$crowdfund->title
							     );
                    $response["message"] = 'Accepted.';
                } else {
                    $msg = array('notification_type' => "action_crowdfund_request",
								 'action_request'    => 'rejected',
								 'reference_number'  => $crowdfund->reference_number,
								 'title'             => $crowdfund->title,
								 'sender_name'       => $crowdfund->firstName,
								 'message'           => $crowdfund->firstName.' Has rejected request '.$crowdfund->title
							     );
                    $response["message"] = 'Rejected';
                }
                $notification_data = array('message' => json_encode($msg));
                sent_notify($notification_data, $senderId);
                $response["error"] = false;
                $response["reference_number"] = $crowdfund->reference_number;
                $this->EchoResponse(200, $response);
            } else { 
				// Un-Subscribed
                $response["error"] = true;
                $response["message"] = 'Doesnt proceeed contact to admin.';
                $this->EchoResponse(200, $response);
            }
        }
    } // responseToInvitation

	
	
	public function friendlist()
	{
		$this->form_validation->set_rules('crowdfund_id', 'Ipool ID', 'trim|required');
		if ($this->form_validation->run() === FALSE) {		
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$users = $this->crowdfund_model->get_friendlist($this->token_user_id);
			if($users == NULL)
			{
				$response["error"]   = true;	
				$response["message"] = "No Record Found.";
				$response["users"] = array();
			} else {
				$response["error"] = false;
				$crowdfund_id = $this->input->post('crowdfund_id');
				$data = array();	
				foreach($users as $user) :
					$status = $this->crowdfund_model->isCrowdfundAcceptedMem($crowdfund_id,$user->userId);
					if($status==3 || $status==4) :
						$data[] = array("id"                => $user->userId,
										"firstName"         => stripslashes($user->firstName),
										"lastName"          => stripslashes($user->lastName),
										"userPIN"           => $user->userPIN,
										"invitation_status" => $status 
									   );		
					endif;			
				endforeach;
				$response["users"] = $data;
			}
			$this->EchoResponse(200, $response);
		} // else
	} // friendlist
	
	


} // Crowdfund (CI_Controller)