<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Promotions extends My_Controller {
	function __construct() 
	{
    	parent::__construct();
  		$this->load->library('form_validation');
		$this->load->model('promotions_model');
	}
	public function index(){
		echo "index";
	}
	// Functions
	public function getPromotions(){
		$promotions = $this->promotions_model->getPromotions();
		if($promotions == NULL){
			$response["error"] = true;
			$response["message"] = 'No promotions founded.';
		} else {
			$response["error"] = false;
			$response["promotions"] = $promotions;
			$response["message"] = "Promotions list all.";
		}
		$this->EchoResponse(200, $response);
	} // End - Get Promotions
	
	public function getPromotionCopouns(){
		$this->form_validation->set_rules('promotion_id', 'Promotion ID', 'trim|required');
		if ($this->form_validation->run() == FALSE) {    
            $response["error"] = true;
            $response["message"] = validation_errors();
    	} else {
			$promotion_id = $this->input->post('promotion_id');
			$coupons = $this->promotions_model->getPromotionCopouns($promotion_id);
			if($coupons == NULL){
				$response["error"] = true;
				$response["message"] = 'No coupons founded for this promotion.';
			} else {
				$response["error"] = false;
				$response["coupons"] = $coupons;
				$response["message"] = "Promotion coupons list all available.";
			}
		}
		$this->EchoResponse(200, $response);
	} // End - Get Promotion Coupons
	
	public function claimPromotionCopoun(){
		$this->form_validation->set_rules('promotion_id', 'Promotion ID', 'trim|required');
		$this->form_validation->set_rules('coupon_code', 'Promotion ID', 'trim|required');
		$this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
		$this->form_validation->set_rules('gps', 'GPS', 'trim');		
		$this->form_validation->set_rules('description', 'Description', 'trim');
		if ($this->form_validation->run() == FALSE) {    
            $response["error"] = true;
            $response["message"] = validation_errors();
    	} else {
			
			$promotion_id = $this->input->post('promotion_id');
			$coupon_code = $this->input->post('coupon_code');
			
			$status = $this->promotions_model->claimPromotionCopoun($promotion_id, $coupon_code);
			if($status == NULL){
				$response["error"] = false;
				$response["message"] = 'Sorry try again with another coupon.';
			} else {				
				$prize_amount = $status->couponPrize;
				$promotions_account_id = $this->promotions_model->getPromotionsAccountId();
				if($promotions_account_id == NULL){
					$promotions_account_id = 1;
				} else {
					$promotionsAccountId = $promotions_account_id->PromotionsAccountId;
				}
				$data = array(
								'userId' => $this->token_user_id, 
								'cr'     => $prize_amount,
								'gps'    => $this->input->post('gps'),
								'devId'  => $this->input->post('dev_id'),
								'refId'  => $promotionsAccountId,
								'type'   => 'AC',
								'mode'   => 'PRO',
								'description' => $this->input->post('description')
						      ); 
	  			$transaction = $this->promotions_model->promotionCouponTransaction($data);
      			if($transaction) 
				{
					$this->load->model('transactions_model');
					$user = $this->transactions_model->user_balance($this->token_user_id); 
					$response["error"]   = false;
					$response["transaction_id"] = $transaction->transId;	
					$response["user_id"]  = $transaction->userId;
					$response["credit"]  = strval($user['credit']);	
					$response["created"] = $transaction->created;
                	$response["message"] = '601';
					$this->EchoResponse(200, $response);  
					$response["message"] = "Congratulations! You have won prize amount of $prize_amount $.";
       			} else {
	  				$response["error"]   = true;
                	$response["message"] = '602';
					$this->EchoResponse(200, $response);                 	 
      			}
			}
		}
		$this->EchoResponse(200, $response);
	} // End - Get Clain Promotion Coupons
	
} // Contributions (CI_Controller)
