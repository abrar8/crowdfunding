<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CronJob extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cron_model');
    }
	
	public function closing_ipool_for_max()
	{
		$ipools = $this->cron_model->closing_ipool_formax();
		$ipools->result();
		foreach($ipools->result() as $ipool){
			$res = $this->cron_model->getCountInviteds($ipool->id);
			if($ipool->max_people ==$res){
				$this->cron_model->update_ipoool($ipool->id);
			}
		}				
	} // ipool corn job for case 1
	
	
		
	public function ipool_process()
	{
		$closed_ipools = $this->cron_model->get_closed_ipools();
		//standards for timings
		//weeek is of 7 days
		//bimonthly is of 15 days
		//mondthly is of 30 days
		
		foreach($closed_ipools->result() as $ipool){
			
			$ipool_current_age = $this->calculate_age_ipool($ipool->start_date);
			
			$weekly = 7;
			$bimonthly = 15;
			$monthly = 30;
			
			$members = $ipool->max_people;
			
			if($ipool->frequency == 1){
				$ipool_expected_age = $members * $weekly;
				if($ipool_current_age<$ipool_expected_age){
					$res = $this->getThis($ipool_current_age,7);
					if($res==7){
						//process for ipool
						$this->cron_model->get_random_user($ipool->id);
						
						
												
					}				
				}	
			}elseif($ipool->frequency == 2){
				$ipool_expected_age = $members * $bimonthly;
				if($ipool_current_age<$ipool_expected_age){
					$res = $this->getThis($ipool_current_age,15);
					if($res==15){
						//process for ipool
						$this->cron_model->get_random_user($ipool->id);						
					}				
				}								
			}elseif($ipool->frequency == 3){
				$ipool_expected_age = $members * $monthly;			
				if($ipool_current_age<$ipool_expected_age){
					$res = $this->getThis($ipool_current_age,30);
					if($res==30){
						//process for ipool					
						$selected_user_reciver = $this->cron_model->get_random_user($ipool->id);
						$payee_user = $this->cron_model->get_payee_users($ipool->id,$selected_user_reciver->uid);
						foreach($payee_user as $sender){
							$this->cron_model->credit_transaction($sender,$selected_user_reciver);
						}
						
					}				
				}				
			}
		}
		
	} // ipool_process
	
	public function calculate_age_ipool($start_date){
		
	 $datetime1 = new DateTime(date("Y-m-d"));
$datetime2 = new DateTime($start_date);
$interval = $datetime1->diff($datetime2);
return $interval->format('%a');
	 	
		}
		
		public function getThis($ipool_current_age,$days){
			if($ipool_current_age<=$days){
				return $ipool_current_age;
			}elseif($ipool_current_age>$days){
				$ipool_current_age = $ipool_current_age-$days;
				$this->getThisMonth($ipool_current_age);
			}
			}
	
	
	
	
	
}

// CronJob