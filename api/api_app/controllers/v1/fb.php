<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Fb extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('fb_model');
        $this->load->helper('home');
        $response = array();
    }


	public function friendlist() 
	{
		$this->form_validation->set_rules('access_token', 'Access Token', 'trim|required');
		if ($this->form_validation->run() == FALSE) 
		{
			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$access_token = $this->input->post('access_token');									
			///////////////////////////// Facebook API START ///////////////////////////////
			require_once(APPPATH.'libraries/facebook_sdk5/autoload.php');
			$fb = new Facebook\Facebook([  
				'app_id'                => FB_APP_ID,  
				'app_secret'            => FB_APP_SECRET,  
				'default_graph_version' => 'v2.4',  
			]);
				///////////////////////////////// User Friendlist START /////////////////////////////////
				try {
					$fbResponse = $fb->get('/me/friends', $access_token);
				} catch(Facebook\Exceptions\FacebookResponseException $e) {
					$response["error"] = true;
					$response["message"] = 'Graph returned an error: '.$e->getMessage();
					$this->EchoResponse(200, $response);
					exit();
				} catch(Facebook\Exceptions\FacebookSDKException $e) {
					$response["error"] = true;
					$response["message"] = 'Facebook SDK returned an error: '.$e->getMessage();
					$this->EchoResponse(200, $response);
					exit();
				}
				
				$friendlist = $fbResponse->getGraphEdge()->asArray();
/*				echo "<pre>";
				print_r($friendlist);*/

				$response["error"] = false;
				$data = array();
				if(count($friendlist) > 0)
				{
					/*foreach($friendlist as $friend) :
						$data[] = array('fb_id' => $friend['id'],
						                'name'  => $friend['name'] 
										);
					endforeach;*/
					foreach($friendlist as $friend) :
						$userInfo = get_fb_userInfo($friend["id"]);
						
						//////////// Check already friend START /////////
						$friendship = $this->fb_model->friendship($this->token_user_id, $userInfo["userId"]); // Login User ID, Friend ID
						//////////// Check already friend END /////////
						
						if($friendship)
						{
							$data[] = array('fb_id'     => $userInfo['fb_id'], 
											'firstName' => $userInfo["firstName"],
											'lastName'  => $userInfo["lastName"],
											'status'    => 0,
											'uid'       => $userInfo["userId"],
											'user_pin'  => $userInfo["userPin"]
											);
						}						
					endforeach;
					$response["friendlist"] = $data;
				} else {
					$response["message"]  = 'You don\'t have any friend yet.';
					$response["friendlist"] = $data;
				} // else
				////////////////////////////////// User Friendlist END /////////////////////////////////////				
			//////////////////////////////// Facebook API END /////////////////////////////////////
				$this->EchoResponse(200, $response);
		} // else		
    } // friendlist
	
	
	/*
	    public function login() {
        $this->form_validation->set_rules('access_token', 'Access Token', 'trim|required');
        $this->form_validation->set_rules('device_id', 'Device ID', 'trim|required');
        $this->form_validation->set_rules('device_type', 'Device Type', 'trim|required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'trim');
        $this->form_validation->set_rules('latitude', 'Latitude', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $access_token = $this->input->post('access_token');
            ///////////////////////////// Facebook API START ///////////////////////////////
            require_once(APPPATH . 'libraries/facebook_sdk5/autoload.php');
            $fb = new Facebook\Facebook([
                'app_id' => FB_APP_ID,
                'app_secret' => FB_APP_SECRET,
                'default_graph_version' => 'v2.4',
            ]);

            ////////////////////// User Profile Information START ///////////////////////
            try {
                $fbResponse = $fb->get('/me?fields=first_name,last_name,name,id,email,gender', $access_token);
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                $response["error"] = true;
                $response["message"] = 'Graph returned an error: ' . $e->getMessage();
                $this->EchoResponse(200, $response);
                exit();
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                $response["error"] = true;
                $response["message"] = 'Facebook SDK returned an error: ' . $e->getMessage();
                $this->EchoResponse(200, $response);
                exit();
            }
            $user = $fbResponse->getGraphUser()->asArray();
            ////////////////////// User Profile Information END //////////////////////////				
            //////////////////////////////// Facebook API END /////////////////////////////////////

            if (isset($user['email'])) {
                $email = $user['email'];
            } else {
                $email = '';
            }

            $data = array('fb_id' => $user['id'],
                'access_token' => $access_token,
                'first_name' => addslashes($user['first_name']),
                'last_name' => addslashes($user['last_name']),
                'name' => addslashes($user['name']),
                'email' => $email,
                'gender' => $user['gender'],
                'device_id' => $this->input->post('device_id'),
                'device_type' => $this->input->post('device_type'),
                'availability' => 1,
                //'age'        => 21,
                'longitude' => round($this->input->post('longitude'), 6),
                'latitude' => round($this->input->post('latitude'), 6),
                'registered' => date('Y-m-d H:i:s')
            );

            $status = $this->fb_model->add_fb_user($data);
            $uid = $this->fb_model->get_fbUser_id($user['id']);

            if ($status == 1) {
                $this->lastActivity($uid);
                $response["error"] = false;
                $response["id"] = $uid;
                $response["first_name"] = $user['first_name'];
                $response["last_name"] = $user['last_name'];
                $response["name"] = $user['name'];
                $response["age"] = "";
                $response["gender"] = $user['gender'];
                $response["image"] = 'https://graph.facebook.com/' . $user['id'] . '/picture?type=large';
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = 'Database Error';
                $this->EchoResponse(200, $response);
            }
        } // else		
    }
	*/



    public function availability() {
        $this->lastActivity($this->input->post('uid'));
        $this->form_validation->set_rules('availability', 'Availability', 'trim|required');
        $this->form_validation->set_rules('uid', 'User ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $availability = $this->input->post('availability');
            $uid = $this->input->post('uid');
            $this->fb_model->update_availability($uid, $availability);
            $response["error"] = false;
            if ($availability == 1) {
                $response["message"] = 'online';
            } else {
                $response["message"] = 'offline';
            }
            $this->EchoResponse(200, $response);
        } // else		
    }

// availability

    public function check_availability() {
        $this->lastActivity($this->input->post('uid'));
        $this->form_validation->set_rules('uid', 'User ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $availability = $this->fb_model->check_availability($this->input->post('uid'));
            $response["error"] = false;
            if ($availability == 1) {
                $response["availability"] = 'online';
            } else {
                $response["availability"] = 'offline';
            }
            $this->EchoResponse(200, $response);
        } // else		
    }

// check_availability

    public function users() {
        $this->lastActivity($this->input->post('uid'));
        $this->form_validation->set_rules('uid', 'User ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $uid = $this->input->post('uid');
            $users = $this->fb_model->get_users($uid);

            $response["error"] = false;
            if ($users == NULL) {
                $response["users"] = 'No Record Found';
            } else {
                foreach ($users as $user) :
                    $profile_pic = get_user_photo($user->id);

                    if ($user->availability == 1) {
                        $availability = 'Online';
                    } else {
                        $availability = 'Offline';
                    }

                    $data[] = array('id' => $user->id,
                        'first_name' => $user->first_name,
                        'last_name' => $user->last_name,
                        'name' => $user->name,
                        'age' => "", // strval(ageCalculator($user->dob)),
                        'gender' => $user->gender,
                        'profile_pic' => $profile_pic,
                        'availability' => $availability,
                        'education' => stripslashes($user->education),
                        'job' => stripslashes($user->job),
                        'employer' => stripslashes($user->employer),
                        'school' => stripslashes($user->school),
                        'about_me' => stripslashes($user->about_me),
                        'description' => stripslashes($user->description),
                        'lastlogin' => time_ago($user->lastActive),
                        'distance' => get_distance($user->latitude, $user->longitude, get_current_location($uid)),
                        'photos' => strval(total_user_photos($user->id)),
                        'is_favourite' => strval(is_favourite($uid, $user->id)), // Sender ID, Reciever ID
                        'favourites' => strval(total_favourites($user->id)), // How many users sent him favourite requests
                        'is_chat_enabled' => strval(is_chat($uid, $user->id)) // both sender and receiver favourite each other 
                    );
                endforeach;
            }
            $response["users"] = $data;
            $this->EchoResponse(200, $response);
        } // else		
    }

// users

    public function logout() {
        $this->lastActivity($this->input->post('uid'));
        $this->form_validation->set_rules('uid', 'User ID', 'trim|required');
        $this->form_validation->set_rules('availability', 'Availability', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $uid = $this->input->post('uid');
            $availability = $this->input->post('availability');
            $status = $this->fb_model->update_availability($uid, $availability);
            if ($status == FALSE) {
                $response["error"] = true;
                $response["message"] = 'No Record Found.';
            } else {
                $response["error"] = false;
                $response["message"] = 'Logout Successfully.';
            }
            $this->EchoResponse(200, $response);
        } // else		
    }

// logout
}

// Fb