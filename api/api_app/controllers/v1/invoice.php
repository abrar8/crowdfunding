<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Invoice extends MY_Controller {

    function __construct() {
        parent::__construct();

//        $this->load->helper('notification_helper');
        $this->load->helper('notification');
        $this->load->library('form_validation');
        $this->load->model('invoice_model');
    }

    // Invoice Details By ID
    public function invoice_detail() {
        $this->form_validation->set_rules('invoice_id', 'Invoice ID', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $invoice_id = $this->input->post('invoice_id');
            $invoice_data = $this->invoice_model->get_invoice_details($invoice_id);
            if ($invoice_data == NULL) {
                $response["error"] = true;
                $response["message"] = "No record found.";
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = false;
                $response["data"] = $invoice_data;
                $response["message"] = "Invoice details.";
                $this->EchoResponse(200, $response);
            }
        }
    }

    protected function EchoResponse($status_code, $response) {
        $this->output->set_status_header($status_code);
        $this->output->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

}

// Common (CI_Controller)
