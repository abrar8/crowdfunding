<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Friends extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('notification');
        $this->load->library('form_validation');
        $this->load->model('friends_model');
        $this->load->helper('home');
        $response = array();
    }

    public function add_friend() {
        $this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $friend = $this->friends_model->userInfo_by_pin($this->input->post("user_pin"));
            if ($friend == NULL) {
                $response["error"] = true;
                $response["message"] = 'Invalid User Pin. Please try again.';
                $this->EchoResponse(200, $response);
            } else {
                if ($this->token_user_id == $friend->userId) {
                    $response["error"] = true;
                    $response["message"] = "You cannot send request to your own account.";
                    $this->EchoResponse(200, $response);
                } else {
                    $data = array('uid' => $this->token_user_id,
                        'friend_id' => $friend->userId,
                        'added' => date('Y-m-d')
                    );
                    $status = $this->friends_model->add_friend($data);
                    if ($status == NULL) {
                        $response["error"] = false;
                        $response["message"] = "Friend request sent.";
                        $sender = $this->friends_model->getUserName($this->token_user_id);
                        $msg = array('notification_type' => "friend_request",
                            'sender_name' => $sender->firstName . " " . $sender->lastName,
                            'message' => $sender->firstName . " send you friend request."
                        );
                        $notification_data = array('message' => json_encode($msg));
                        sent_notify($notification_data, $friend->userId);
                        $this->EchoResponse(200, $response);
                    } else {
                        if ($status == 1) {
                            $response["error"] = true;
                            $response["message"] = "Friend request already sent.";
                            $this->EchoResponse(200, $response);
                        } elseif ($status == 2) {
                            $response["error"] = true;
                            $response["message"] = $friend->firstName . " is already in your friends list.";
                            $this->EchoResponse(200, $response);
                        } elseif ($status == 3) {
                            $response["error"] = true;
                            $response["message"] = $friend->firstName . " has already rejected your friend request.";
                            $this->EchoResponse(200, $response);
                        }
                    } // else
                }
            } // else
        } // else 
    } // add_friend
	
	
	public function add_fb_friends() 
	{
        $this->form_validation->set_rules('users', 'List of User PIN', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $users = json_decode($this->input->post("users"));
			foreach ($users as $user) {
				$friend = $this->friends_model->userInfo_by_pin($user->user_pin);
				 $data = array('uid'       => $this->token_user_id,
							   'friend_id' => $friend->userId,
							   'added'     => date('Y-m-d')
							   );
				$status = $this->friends_model->add_friend($data);
				if ($status == NULL) {
                        $response["error"] = false;
                        $response["message"] = "Friend request sent.";
                        $sender = $this->friends_model->getUserName($this->token_user_id);
                        $msg = array('notification_type' => "friend_request",
									 'sender_name'       => $sender->firstName." ".$sender->lastName,
									 'message'           => $sender->firstName." send you friend request."
								    );
                        $notification_data = array('message' => json_encode($msg));
                        sent_notify($notification_data, $friend->userId);
                    } else {
                        if ($status == 1) {
                            $response["error"] = true;
                            $response["message"] = "Friend request already sent.";
                            $this->EchoResponse(200, $response);
                        } elseif ($status == 2) {
                            $response["error"] = true;
                            $response["message"] = $friend->firstName . " is already in your friends list.";
                            $this->EchoResponse(200, $response);
                        } elseif ($status == 3) {
                            $response["error"] = true;
                            $response["message"] = $friend->firstName . " has already rejected your friend request.";
                            $this->EchoResponse(200, $response);
                        }
                    } // else
			} // foreach
            $this->EchoResponse(200, $response);
        } // else 
    } // add_fb_friends
	

	/*
    public function get_friendlist_bk() {
        $friends = $this->friends_model->get_friendlist_upd($this->token_user_id);
        if ($friends == NULL) {
            $response["error"] = false;
            $response["friends"] = array();
            $response["message"] = "You don't have any friend.";
            $this->EchoResponse(200, $response);
        } else {
            $response['error'] = false;
            foreach ($friends as $key => $value) {
                if (!empty($value[0])) {
                    $response_array["status_id"] = $value[0]->status_id;
                    $response_array["request_sent"] = $value[0]->request_sent;
                    $response_array["user_id"] = $value[0]->userId;
                    $response_array["first_name"] = $value[0]->firstName;
                    $response_array["last_name"] = $value[0]->lastName;
                    $response_array["phone_number"] = $value[0]->phoneNumber;
                    $response_array["address"] = $value[0]->address;
                    $response_array["city"] = $value[0]->city;
                    $response_array["country"] = $value[0]->country;
                    $response_array["user_pin"] = $value[0]->userPIN;
                    $response_array["email"] = $value[0]->email;
                    $response_array["currency"] = $value[0]->currency;
                    $response_array["status"] = $value[0]->status;
                    $response['friends'][] = $response_array;
                } else {
                    $response["friends"] = array();
                    $response["message"] = "You don't have any friend.";
                }
            }
            $this->EchoResponse(200, $response);
        }
    } // get_friendlist_bk
	*/
	
	public function get_friendlist() {
		$friends = $this->friends_model->get_friendlist_upd($this->token_user_id);
        if ($friends == NULL) {
            $response["error"] = false;
            $response["friends"] = array();
            $response["message"] = "You don't have any friend.";
            $this->EchoResponse(200, $response);
        } else {
            $response['error'] = false;
/*			echo "<pre>";
			print_r($friends);
			exit();*/
            foreach($friends as $key => $friend) 
			{
            	$data[] = array("status_id"    => $friend->status_id,
				                "request_sent" => $friend->request_sent,
							    "user_id"      => $friend->userId,
							    "first_name"   => $friend->firstName,
							    "last_name"    => $friend->lastName,
							    "phone_number" => $friend->phoneNumber,
							    "address"      => $friend->address,
							    "city"         => $friend->city,
							    "country"      => $friend->country,
							    "user_pin"     => $friend->userPIN,
							    "email"        => $friend->email,
							    "currency"     => $friend->currency,
							    "status"       => $friend->status						
							   );				         
            }
			$response['friends'] = $data; 
            $this->EchoResponse(200, $response);
        }
    } // get_friendlist
	
	

    public function requests_sent() {
        $requests = $this->friends_model->requests_sent($this->token_user_id);
        if ($requests == NULL) {
            $response["error"] = true;
            $response["message"] = "You don't have any friend request.";
            $this->EchoResponse(200, $response);
        } else {
            $response['error'] = false;
            //$response['requests'] = $requests;
            foreach ($requests as $key => $value) {
                $response_array["status_id"] = $value->status_id;
                $response_array["request_sent"] = $value->request_sent;
                $response_array["user_id"] = $value->userId;
                $response_array["first_name"] = $value->firstName;
                $response_array["last_name"] = $value->lastName;
                $response_array["phone_number"] = $value->phoneNumber;
                $response_array["address"] = $value->address;
                $response_array["city"] = $value->city;
                $response_array["country"] = $value->country;
                $response_array["user_pin"] = $value->userPIN;
                $response_array["email"] = $value->email;
                $response_array["currency"] = $value->currency;
                $response_array["status"] = $value->status;
                $response["requests"][$key] = $response_array;
            }
            $this->EchoResponse(200, $response);
        }
    }

// requests_sent

    public function pending_requests() {
        $requests = $this->friends_model->pending_requests($this->token_user_id);
        
        if ($requests == NULL) {
            $response["error"] = true;
            $response["message"] = "You don't have any friend requests.";

            /* $notification_message = "You don't have any friend requests.";
              $notification_array = array('message' => $notification_message, 'type' => "Pending friend request");
              sent_notify($notification_array,$this->token_user_id); */

            $this->EchoResponse(200, $response);
        } else {
            $response['error'] = false;
            //$response['requests'] = $requests;
            foreach ($requests as $key => $value) {
                $response_array["status_id"] = $value->status_id;
                $response_array["request_sent"] = $value->request_sent;
                $response_array["user_id"] = $value->userId;
                $response_array["first_name"] = $value->firstName;
                $response_array["last_name"] = $value->lastName;
                $response_array["phone_number"] = $value->phoneNumber;
                $response_array["address"] = $value->address;
                $response_array["city"] = $value->city;
                $response_array["country"] = $value->country;
                $response_array["user_pin"] = $value->userPIN;
                $response_array["email"] = $value->email;
                $response_array["currency"] = $value->currency;
                $response_array["status"] = $value->status;

                $response["requests"][$key] = $response_array;
            }
            $this->EchoResponse(200, $response);
        }
    }

// pending_requests

    public function requestResponse() {
        $this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required');
        $this->form_validation->set_rules('status_id', 'Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $status = $this->input->post("status_id");
            $user_pin = $this->input->post("user_pin");
            $friend = $this->friends_model->userInfo_by_pin($user_pin);
            $user = get_userInfo($this->token_user_id);
            if ($friend == NULL) {
                $response["error"] = true;
                $response["message"] = 'Invalid User Pin. Please try again.';
                $this->EchoResponse(200, $response);
            } else {
                $data = array('uid' => $friend->userId,
                    'friend_id' => $this->token_user_id,
                    'status' => $status
                );
                $this->friends_model->update_friend($data);
                if ($status == 2) {
                    $response["error"] = false;
                    $response["message"] = "Request accepted";
                    $msg = array('notification_type' => "action_friend_request",
                        'sender_name' => $user->firstName,
                        'action_request' => "accepted",
                        'message' => $user->firstName . " approved your friend request."
                    );
                    $notification_data = array('message' => json_encode($msg));
                    sent_notify($notification_data, $friend->userId);
                    $this->EchoResponse(200, $response);
                } elseif ($status == 3) {
                    $response["error"] = false;
                    //$response["message"] = "Request rejected";
                    $response["message"] = "You have declined friend request";
                    $msg = array('notification_type' => "action_friend_request",
                        'sender_name' => $user->firstName,
                        'action_request' => "rejected",
                        'message' => $user->firstName . " rejected your friend request."
                    );
                    $notification_data = array('message' => json_encode($msg));
                    sent_notify($notification_data, $friend->userId);
                    $this->EchoResponse(200, $response);
                }
            } // else
        } // else
    }

// update_friend

    public function unfriend() {
        $this->form_validation->set_rules('user_pin', 'User Pin', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $userPin = $this->input->post("user_pin");
            $friend = $this->friends_model->userInfo_by_pin($userPin);
            $status = $this->friends_model->unfriend($this->token_user_id, $friend->userId);
            if ($status) {
                $response["error"] = false;
                $response["message"] = "$friend->firstName $friend->lastName removed from your friends list.";
                /* $notification_message = "$friend->firstName approved your friend request.";
                  $notification_array = array('message' => $notification_message, 'type' => "Friend request approved.");
                  sent_notify($notification_array,$this->token_user_id); */
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = 'Invalid Pin Number. Please try again.';
                $this->EchoResponse(200, $response);
            }
        }
    }

// unfriend	
}

// Friends (CI_Controller)