<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Market extends My_Controller {
	function __construct() 
	{
    	parent::__construct();
  		$this->load->library('form_validation');
		$this->load->model('market_model');
	}
	public function index(){
		echo "index";
	}
	// Add new Market Item (Product)
	public function add(){
		
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		$this->form_validation->set_rules('item_name', 'Item Name', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim|required');		
		$this->form_validation->set_rules('item_image', 'Item Image', 'trim|required');
		$this->form_validation->set_rules('gallery_images', 'Gallery Images', 'trim');
		$this->form_validation->set_rules('price', 'Price', 'trim|required');
		$this->form_validation->set_rules('contact_direct', 'Contact Direct', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
   			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			
			// Add Process Here
			$userId = $this->token_user_id;
			if($this->input->post('item_image')){
				$image = $this->input->post('item_image');
				// Upload Image from binary to server
				$image_name = $this->market_model->get_unique_product_image_name();
				file_put_contents('./assets/market_images/'.$image_name, base64_decode($image));
			} else {
				$image_name = 'item_avataar.png';
			}
			$data = array('userId' => $userId, 'category' => $this->input->post('category'), 'item_name' => $this->input->post('item_name'), 'description' => $this->input->post('description'), 'item_image' => $image_name, 'gallery_images' => $this->input->post('gallery_images'), 'price' => $this->input->post('price'), 'contact_direct' => $this->input->post('contact_direct'), 'available' => 1, 'deleted' => NULL);
			$status = $this->market_model->add_item($data);
			if($status == FALSE){
				$response["error"] = true;
				$response["message"] = "Oops! Something went wrong.";
				$this->EchoResponse(200, $response); 
			} else {
				$response["error"] = false;
				$response["message"] = "New market item was added successfully.";
				$this->EchoResponse(200, $response); 
			}
		}
	}
	
	// Get all Market Items (Products)
	public function get_all_items(){
		
		$all_items = $this->market_model->get_all_items();
		if($all_items == NULL){
			$response["error"] = true;
			$response["message"] = "No records founded.";
			$this->EchoResponse(200, $response); 
		} else {
			$response["error"] = false;
			$response["all_market_items"] = $all_items;
			$response["message"] = "Market items list all.";
			$this->EchoResponse(200, $response); 
		}
	}
	
	// Get all My Added Market Items (Products)
	public function get_my_all_items(){
		$all_items = $this->market_model->get_all_my_items($this->token_user_id);
		if($all_items == NULL){
			$response["error"] = true;
			$response["message"] = "No records founded.";
			$this->EchoResponse(200, $response); 
		} else {
			$response["error"] = false;
			$response["my_all_market_items"] = $all_items;
			$response["message"] = "My market items list all.";
			$this->EchoResponse(200, $response); 
		}
	}
	// Get Single Item (Product) By Id
	public function get_item(){
		
		$this->form_validation->set_rules('item_id', 'Deleted', 'trim');
		if($this->form_validation->run() == FALSE) {
   			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$item_id = $this->input->post('item_id');
			$data = $this->market_model->get_item($item_id);
			
			if($data == NULL){
				$response["error"] = true;
				$response["message"] = "Item not found.";
				$this->EchoResponse(200, $response); 
			} else {
				$response["error"] = false;
				$response["item_data"] = $data['item'][0];
				if(!empty($data['contact_details'][0])){
					$response["contact_details"] = $data['contact_details'][0];
				} else {
					$response["contact_details"] = $data['contact_details'];
				}
				$response["message"] = "Item details.";
				$this->EchoResponse(200, $response); 
			}
		}
	}
	// Remove Market Item (Product) By Id
	public function remove_item(){
		$this->form_validation->set_rules('item_id', 'Deleted', 'trim');
		if($this->form_validation->run() == FALSE) {
   			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$item_id = $this->input->post('item_id');
			$status = $this->market_model->remove_item($item_id);
			if($status == FALSE){
				$response["error"] = true;
				$response["message"] = "Oops! Something went wrong.";
				$this->EchoResponse(200, $response); 
			} else if($status == TRUE){
				$response["error"] = false;
				$response["message"] = "Item was successfully removed.";
				$this->EchoResponse(200, $response); 
			}
		}
	}
	// search Market Items (Products) Dynamically
	public function search_items(){
		$this->form_validation->set_rules('price_from', 'Price From', 'trim');
		$this->form_validation->set_rules('price_to', 'Price To', 'trim');
		$this->form_validation->set_rules('keywords', 'Keywords', 'trim');
		$this->form_validation->set_rules('category', 'Category', 'trim');
		
		if($this->form_validation->run() == FALSE) {
   			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$price_from = $this->input->post('price_from');
			$price_to = $this->input->post('price_to');
			$keywords = $this->input->post('keywords');
			$category = $this->input->post('category');
			
			$posted_data = array('price_from' => $price_from, 'price_to' => $price_to, 'keywords' => $keywords, 'category' => $category);
			$result = $this->market_model->get_search_items_filtered_records($posted_data);
			if(!empty($result)){
				$response["error"] = false;
				$response["items"] = $result;
				$response["message"] = "Search results market items.";
				$this->EchoResponse(200, $response); 
			} else {
				$response["error"] = true;
				$response["message"] = "No records founded.";
				$this->EchoResponse(200, $response); 
			}
		}
	}
	// Buy Item
	public function buy_item(){
		$this->form_validation->set_rules('item_id', 'Reference Number', 'trim|required');
		$this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
		$this->form_validation->set_rules('gps', 'GPS', 'trim');		
		$this->form_validation->set_rules('description', 'Description', 'trim');
		
		if ($this->form_validation->run() == FALSE)
		{    
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 

    	} else {
			$item_id = $this->input->post('item_id');
			$item = $this->market_model->get_item_price($item_id);
			
			if($item == NULL){
				$response["error"] = true;
				$response["message"] = "Item not founded.";
				$this->EchoResponse(200, $response);
			} else {
				$this->load->model('transactions_model');
				$userCr = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance
				if ($userCr['credit'] < $item->price) { // Verify user available balance
					$response["error"]   = true;
					$response["message"] = 'Insufficient balance. Your balance '.$userCr['credit'];
					$this->EchoResponse(200, $response);
				} else {
					
					$guid = rand(1, 99999);
					$debit = array('userId'       => $this->token_user_id,
								   'refId'        => $item_id, 
								   'dr'           => $item->price,
								   'gps'          => $this->input->post('gps'),
								   'devId'        => $this->input->post('dev_id'),
								   'type'         => 'TR',
								   'mode'		  => 'MI', // mode = MI for Market item transaction (Purchase)
								   'transaction_code' => $guid,
								   'description'  => $this->input->post('description')	 
								  ); 
					$credit = array('userId'      => $this->token_user_id,
									'refId'       => $this->token_user_id, 
									'cr'          => $item->price,
									'gps'         => $this->input->post('gps'),
									'devId'       => $this->input->post('dev_id'),
									'type'        => 'TR',
									'mode'		  => 'MI', // mode = MI for Market item transaction (Purchase)
									'transaction_code' => $guid,
									'description' => $this->input->post('description')
									);
					$transaction_data = $this->market_model->itemBuyTransaction($this->token_user_id, $debit, $credit);
					if($transaction_data) 
					{
						$user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance
							
						$response["error"] = false;
						$response["transaction_id"] = $transaction_data->transId;	
						$response["user_id"] = $transaction_data->userId;
						$response["credit"] = strval($user['credit']);
						$response["debit"] = $transaction_data->dr;			
						$response["gps"] = $transaction_data->gps;
						$response["dev_id"] = $transaction_data->devId;
						$response["created"] = $transaction_data->created;
						$response["message"] = "Your request for buy item is received.";
						$this->EchoResponse(200, $response);
					}
				}
			}
		}
	}
} // Contributions (CI_Controller)
