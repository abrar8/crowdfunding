<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Currency extends MY_Controller {
	function __construct() 
	{
    	parent::__construct();
		$this->load->model('currency_model');
		$this->load->helper('home');
		$response = array();
	}


	public function exchange_rates()
	{	
		$exchange_rates = $this->currency_model->get_exchange_rates();
		if($exchange_rates == NULL){
			$response["error"]   = true;
			$response["message"] = 'No Currency Record Found.';
			$this->EchoResponse(200, $response); 
		} else {
			$response["error"]   = false;
			foreach($exchange_rates as $exchange_rate) :
				$data[] = array("id"            => $exchange_rate->id,
				                "currency_name" => $exchange_rate->currency_name,
							    "currency_type" => $exchange_rate->currency_type,
							    "rate"          => $exchange_rate->rate
							   );
			endforeach;
			$response["exchange_rates"] = $data;
			$this->EchoResponse(200, $response); 
		}
	} // exchange_rates
	
	
	public function update_currency()
	{
		$this->form_validation->set_rules('currency', 'Currency', 'trim|required');
		$this->form_validation->set_rules('is_merchant', 'IS Merchant', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$currency = $this->input->post('currency');
			$is_merchant = $this->input->post('is_merchant');
			$data = array("currency" => $currency);
			if($is_merchant == 1)
			{
				$uid = $this->token_merchant_id;
			} else {
				$uid = $this->token_user_id;
			}
			$status = $this->currency_model->update_currency($data, $uid);
			$response["error"] = false;
			$response["currency"] = $currency;
            $response["message"] = "Currency updated successfully.";
		}
		$this->EchoResponse(200, $response); 
	} // update_currency
	
	
	
	

	
} // Currency (CI_Controller)
