<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->helper('notification');
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index() {
        $msg = array(
            "type"=>"creditAdded",
            "amount"=>"400",
            "user_pin"=>"1234"
            );
        $msg = json_encode($msg);
        $msg_notify = array(
                    'message' => $msg
                );
        $device_id="APA91bHL1P46_zzZSvWc3zFsKtqME-DFpaKWMEBfEV-5UHG9P6hbiyPUf-YnxPSpxmMoaz_cJj6F9aTCcddDvci2K92P_RZSKTnNRxS3es9uDTn8nDH4NuYSnNsXP-lsP8awvN4jQUfa";
//        $device_id="APA91bHjC61Y-TIE5zsM1J_tj-mI_tmN8icWpnOgafLrFg9L86ThmGqhOqknTJ1d7IOA9FYFH5Zp4Hl4L2J0ihXaDxQQ_kqVPqzbR6dCdQpC3dJFW7_mdiQgxQbr1BwgNIFhKtOKrtCE";
//        $device_id="APA91bFsplc5lnq1Kr6T5Jh5iMUUjokKo8CoZYTuoGToC706-Z5F3r6RdcSYKXeaLSNLtKFzyrsyXgpzJAYafXuiGCQUpdE61blgiW-PjsoE7zLKk5iYa7V-HuRA3RYyKcabYccEtbjq";
        push_andriod($device_id, $msg_notify);
        echo "done";
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */