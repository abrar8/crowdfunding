<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class banks extends MY_Controller {
    function __construct() {
		parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->model('banks_model');
        $this->load->library('encrypt');
        $this->lang->load('en_admin', 'english');
        $this->load->library('form_validation');
        $response = array();
    }
	

    public function available() {
        $banks = $this->banks_model->get_banks($this->token_user_id);
		$data = array();
        if ($banks) {
            $response["error"] = false;
            foreach ($banks as $bank) {
                $data[] = array("bank_id"   => $bank->id,
				                "bank_name" => stripslashes($bank->bank_name) 
								);
            }
			$response["available_banks"] = $data;
        } else {
            $response["error"] = true;
            $response["message"] = 'No Record Found';
			$response["available_banks"] = $data;
        }
        $this->EchoResponse(200, $response);
    } // available


    public function save() {
        //$this->form_validation->set_rules('user_id', 'User ID', 'trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('bank_id', 'Bank ID', 'trim|required|min_length[1]|max_length[50]');
        $this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required|min_length[1]|max_length[255]');
        $this->form_validation->set_rules('account_title', 'Account Title', 'trim|required|min_length[1]|max_length[255]');
        $this->form_validation->set_rules('account_number', 'Account nnumber', 'trim|required|min_length[1]|max_length[255]');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $userId = ($this->token_user_id == "") ? $this->token_merchant_id : $this->token_user_id;

            $data = array('userId' => $userId, //$this->input->post('user_id'), 
                'bankId' => (int) $this->input->post('bank_id'),
                'branchName' => $this->input->post('branch_name'),
                'accountTitle' => $this->input->post('account_title'),
                'accountNumber' => $this->input->post('account_number'),
                'available' => 1
            );
            if ($this->banks_model->save_bank($data)) {
                $response["error"] = false;
                $response["message"] = 'Bank Detail Added successfully';
            } else {
                $response["error"] = true;
                $response["message"] = 'An error occurred. Please try again';
            } // else
            $this->EchoResponse(200, $response);
        } // else
    }

// save

    public function my_bank_accounts() {
        $auth_key = $this->input->get_request_header('Authorization', TRUE);
        if ($auth_key) {
            $this->load->model('authenticate_model');
            $valid_response = $this->authenticate_model->is_valid_api_key($auth_key);
            if(!$valid_response){
                $valid_response = $this->authenticate_model->is_valid_merchant_api_key($auth_key);
            }
            if (!$valid_response) {
                $response["error"] = true;
                $response["message"] = "Invalid Api Key";
                header('Content-type: application/json');
                echo json_encode($response);
                exit();
            }
        } else {
            $response["error"] = true;
            $response["message"] = "Api key is misssing";
            header('Content-type: application/json');
            echo json_encode($response);
            exit;
        }
        $userId = ($this->token_user_id == "") ? $this->token_merchant_id : $this->token_user_id;
        $bank_data = $this->banks_model->banks_by_user_id($userId);
        if ($bank_data) {
            $response["error"] = false;
            $response["user_id"] = $userId;
            //$response["accounts"] = $bank_data;
            foreach ($bank_data as $key => $value) {
                $response_array["bank_account_id"] = $value->bankAccountId;
                $response_array["user_id"] = $value->userId;
                $response_array["bank_id"] = $value->bankId;
                $response_array["branch_name"] = $value->branchName;
                $response_array["account_title"] = $value->accountTitle;
                $response_array["account_number"] = $value->accountNumber;
                $response_array["available"] = $value->available;
                $response_array["bank_name"] = $value->bank_name;
                $response["accounts"][$key] = $response_array;
            }
        } else {
            $response["error"] = true;
            $response["message"] = 'No Record Found';
        }
        $this->EchoResponse(200, $response);
    }

    public function by_bankId($userId, $bankId) {
        $auth_key = $this->input->get_request_header('Authorization', TRUE);
        if ($auth_key) {
            $this->load->model('authenticate_model');
            $valid_response = $this->authenticate_model->is_valid_api_key($auth_key);
            if (!$valid_response) {
                $response["error"] = true;
                $response["message"] = "Invalid Api Key";
                header('Content-type: application/json');
                echo json_encode($response);
                exit();
            }
        } else {
            $response["error"] = true;
            $response["message"] = "Api key is misssing";
            header('Content-type: application/json');
            echo json_encode($response);
            exit();
        }
        $bank_data = $this->banks_model->banks_by_bank_id($userId, $bankId);
        if ($bank_data) {
            $response["error"] = false;
            $response["user_id"] = $userId;
            //$response["accounts"] = $bank_data;			
            foreach ($bank_data as $key => $value) {
                $response_array["bank_account_id"] = $value->bankAccountId;
                $response_array["user_id"] = $value->userId;
                $response_array["bank_id"] = $value->bankId;
                $response_array["branch_name"] = $value->branchName;
                $response_array["account_title"] = $value->accountTitle;
                $response_array["account_number"] = $value->accountNumber;
                $response_array["available"] = $value->available;
                $response_array["bank_name"] = $value->bankName;
                $response_array["display_name"] = $value->displayName;

                $response["accounts"][$key] = $response_array;
            }
        } else {
            $response["error"] = true;
            $response["message"] = 'No Record Found';
        }
        $this->EchoResponse(200, $response);
    }

    /* private function EchoResponse($status_code, $response)
      {
      $this->output->set_status_header($status_code);
      $this->output->set_content_type('application/json')
      ->set_output(json_encode($response));

      } */
}
