<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Register_model');
		$this->load->helper('home');
		$response = array();
    }

	
    public function index() {
        $response = array();
        $this->form_validation->set_rules('address', 'Address', 'trim|required');
        $this->form_validation->set_rules('country', 'Country', 'trim|required');
        $this->form_validation->set_rules('city', 'City', 'trim|required');
        $this->form_validation->set_rules('currency', 'Currency', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			if($this->input->post('currency') == "")
			{
				$currency = "USD";
			} else {
				$currency = $this->input->post('currency');
			}
            $data = array('address'  => addslashes($this->input->post('address')),
						  'country'  => addslashes($this->input->post('country')),
						  'city'     => addslashes($this->input->post('city')),
						  'currency' => $currency
					     );
            //$this->EchoResponse(200, $response); 
            if ($this->token_user_id != "") 
			{
                $row = $this->Register_model->process_update_user($data, $this->token_user_id);
                if ($row) {
                    $response["error"] = false;
                    $response["user_id"] = $row->userId;
                    $response["first_name"] = stripslashes($row->firstName);
                    $response["last_name"] = stripslashes($row->lastName);
                    $response["email"] = $row->email;
                    $response["phone_number"] = $row->phoneNumber;
                    $response["address"] = stripslashes($row->address);
                    $response["country"] = stripslashes($row->country);
                    $response["city"] = stripslashes($row->city);
                    $response["currency"] = $row->currency;
                    $response["pin_code"] = $row->userPIN;
                    $response["message"] = 'Profile updated successfully'; 
                    /* $notification_message = "Profile updated successfully.";
                      $notification_array = array('message' => $notification_message, 'type' => "Profile updated.");
                      sent_notify($notification_array,$this->token_user_id); */
                } else {
                    $response["error"] = true;
                    $response["message"] = 'An error occurred. Please try again';
                } // else
            } elseif ($this->token_merchant_id != "") {
                $this->form_validation->set_rules('merchant_title', 'Merchant Title', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $response["error"] = true;
                    $response["message"] = validation_errors();
                    $this->EchoResponse(200, $response);
                } else {
                    $data["merchant_title"]=$this->input->post('merchant_title');
                    $row = $this->Register_model->process_update_merchant($data, $this->token_merchant_id);
                    if ($row) {
                        $response["error"] = false;
                        $response["user_id"] = $row->userId;
                        $response["merchant_title"] = $row->merchant_title;
                        $response["first_name"] = stripslashes($row->firstName);
                        $response["last_name"] = stripslashes($row->lastName);
                        $response["email"] = $row->email;
                        $response["phone_number"] = $row->phoneNumber;
                        $response["address"] = stripslashes($row->address);
                        $response["country"] = stripslashes($row->country);
                        $response["city"] = stripslashes($row->city);
                        $response["currency"] = $row->currency;
                        $response["pin_code"] = $row->userPIN;
                        $response["message"] = 'Profile updated successfully'; 
                        /* $notification_message = "Profile updated successfully.";
                          $notification_array = array('message' => $notification_message, 'type' => "Profile updated.");
                          sent_notify($notification_array,$this->token_user_id); */
                    } else {
                        $response["error"] = true;
                        $response["message"] = 'An error occurred. Please try again';
                    } // else
                }
            }
            $this->EchoResponse(200, $response);
        } // else 
    } // index
	
	
	public function get_profile()
	{
		$response = false;
		if($this->token_user_id != "")
		{	
			$user = get_userInfo($this->token_user_id);
			$response["error"]       = false;	
			$response["userId"]      = $user->userId;
			$response["first_name"]  = stripslashes($user->firstName);
			$response["last_name"]   = stripslashes($user->lastName);
			$response["address"]     = stripslashes($user->address);
			$response["city"]        = stripslashes($user->city);
			$response["country_id"]  = $user->country_id;
			$response["currency_id"] = $user->currency_id;	
			$response["country"]     = stripslashes($user->country);	
			$response["currency"]    = $user->currency;
		} elseif($this->token_merchant_id != "") {
			$user = get_merchantInfo($this->token_merchant_id);
			$response["error"]          = false;	
			$response["userId"]         = $user->userId;
			$response["merchant_title"] = stripslashes($user->merchant_title);
			$response["first_name"]     = stripslashes($user->firstName);
			$response["last_name"]      = stripslashes($user->lastName);
			$response["address"]        = stripslashes($user->address);
			$response["city"]           = stripslashes($user->city);
			$response["country_id"]     = $user->country_id;
			$response["currency_id"]    = $user->currency_id;	
			$response["country"]        = stripslashes($user->country);
			$response["currency"]       = $user->currency; 
		}
		$this->EchoResponse(200, $response);		
	} // get_profile
	
	
	public function get_countries()
	{
		$countries = $this->Register_model->get_countries();
		$data = array();
		if($countries == NULL)
		{
			$response["error"]     = true;
			$response["countries"] = $data;
			$response["message"]   = "No Record Found.";
		} else {
			$response["error"] = false;
			foreach($countries as $country) :
				$data[] = array("id"   => $country->id,
				                "name" => $country->printable_name,
								"code" => $country->iso3
				                );
			endforeach;
			$response["countries"] = $data;
		}
		$this->EchoResponse(200, $response);
	} // get_countries
	
	
	public function get_currency()
	{
		$this->load->model('currency_model');
		$currencys = $this->currency_model->get_exchange_rates();
		$data = array();
		if($currencys == NULL)
		{
			$response["error"]    = true;
			$response["currency"] = $data;
			$response["message"]  = "No Record Found.";
		} else {
			$response["error"] = false;
			foreach($currencys as $currency) :
				$data[] = array("id"     => $currency->id,
				                "name"   => $currency->currency_name,
								"type"   => $currency->currency_type,
								"symbol" => $currency->symbol,
								);
			endforeach;
			$response["currency"] = $data;
		}
		$this->EchoResponse(200, $response);
	} // get_currency
		
	
	public function update_userpin()
	{
		$response = array();
        $this->form_validation->set_rules('userPIN', 'UserPIN', 'trim|required|min_length[10]');
		$this->form_validation->set_rules('usertype', 'User Type', 'trim|required|min_length[1]');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$usertype = $this->input->post('usertype');
			$userPIN = $this->input->post('userPIN');
			if($usertype == 2)
			{
				$table = 'tblUsers';
				$uid = $this->token_user_id;
			} elseif($usertype == 3) {
				$table = 'tblMerchantUsers';
				$uid = $this->token_merchant_id;
			} elseif($usertype == 4) {
				$table = 'tblAgents';
				$uid = $this->token_agent_id;	
			}				
			$status = $this->Register_model->update_user_pin($uid,$userPIN,$table);
			$response["error"] = false;      
			$response["message"] = "UserPIN updated successfully.";      	
			$this->EchoResponse(200, $response);
		}		
	} // update_userpin
	
	
	public function auth_password()
	{
		$response = array();
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('usertype', 'User Type', 'trim|required|min_length[1]');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$usertype = $this->input->post('usertype');
			$password = $this->input->post('password');
			if($usertype == 2)
			{
				$table = 'tblUsers';
			} elseif($usertype == 3) {
				$table = 'tblMerchantUsers';
			} elseif($usertype == 4) {
				$table = 'tblAgents';	
			}
			$status = $this->Register_model->validate_password($table,$password);
			if($status)
			{
				$response["error"] = false;      
				$response["message"] = "Password Match.";      	
            	$this->EchoResponse(200, $response);	
			} else {				
				$response["error"] = true;
				$response["message"] = "Invalid Password.";
            	$this->EchoResponse(200, $response);
			}
		}		
	} // auth_password
	
	
	
	
} // Profile