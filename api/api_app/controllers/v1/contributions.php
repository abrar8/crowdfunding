<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Contributions extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('contributions_model');
        $this->load->model('transactions_model');
        $this->load->helper('string');
    }


    public function subscribeMe() 
	{
    	$status = $this->contributions_model->subscribeMe($this->token_user_id);
        if($status == 0) 
		{
            $response["error"] = true;
            $response["message"] = "You are already subscribed to Daily Contributions.";
        } else {
			$response["error"] = false;
            $response["message"] = "You are now subscribed to Daily Contributions.";
		}
		$this->EchoResponse(200, $response);
    } // subscribeMe
	

    public function mySubscriptionStatus() {
        $status = $this->contributions_model->mySubscriptionStatus($this->token_user_id);
        $response["error"] = false;
        $response["subscription_status"] = $status;
        $this->EchoResponse(200, $response);
    }
	

    public function addBalance() {
        $this->form_validation->set_rules('dc_amount', 'Amount', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim|required');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            // Verify Subscription
            $status = $this->contributions_model->mySubscriptionStatus($this->token_user_id);
            if ($status == 1) { // Subscribed
                $userCr = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance
                if ($userCr['credit'] < $this->input->post('dc_amount')) { // Verify user available balance
                    $response["error"] = true;
                    $response["message"] = 'Insufficient balance. Your balance ' . $userCr['credit'];
                    $this->EchoResponse(200, $response);
                } else {
                    $guid = $this->contributions_model->get_unique_guid();
                    $dcAccountId = $this->contributions_model->getDcAccountId();
                    $debit = array('userId' => $this->token_user_id,
                        'refId' => $dcAccountId,
                        'dr' => $this->input->post('dc_amount'),
                        'gps' => $this->input->post('gps'),
                        'devId' => $this->input->post('dev_id'),
                        'type' => 'TR',
                        'mode' => 'DC',
                        'transaction_code' => $guid,
                        'description' => $this->input->post('description')
                    );
                    $credit = array('userId' => $this->token_user_id,
                        'refId' => $this->token_user_id,
                        'cr' => $this->input->post('dc_amount'),
                        'gps' => $this->input->post('gps'),
                        'devId' => $this->input->post('dev_id'),
                        'type' => 'TR',
                        'mode' => 'DC',
                        'transaction_code' => $guid,
                        'description' => $this->input->post('description')
                    );
                    $transaction_data = $this->contributions_model->transfer_credit_to_daily_contributions($this->token_user_id, $debit, $credit);
                    if ($transaction_data) {
                        $user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance
                        $response["error"] = false;
                        $response["transaction_id"] = $transaction_data->transId;
                        $response["user_id"] = $transaction_data->userId;
                        $response["credit"] = strval($user['credit']) . '.00';
                        $response["debit"] = $transaction_data->dr;
                        $response["gps"] = $transaction_data->gps;
                        $response["dev_id"] = $transaction_data->devId;
                        $response["created"] = $transaction_data->created;
                        $response["message"] = 'Balance added to your Daily Contribution Account.';
                        $this->EchoResponse(200, $response);
                    }
                }
            } elseif ($status == 0) { // Un-Subscribed
                $response["error"] = true;
                $response["message"] = 'You are not subscribed to this service.';
                $this->EchoResponse(200, $response);
            }
        }
    }

    public function viewBalance() {
        $dc_balance = $this->contributions_model->viewDcBalance($this->token_user_id);
        if ($dc_balance == NULL) {
            $response["error"] = true;
            $response["message"] = "You have no balance in your Daily Contributions Account.";
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            $response["credit"] = strval($dc_balance['credit']);
            $this->EchoResponse(200, $response);
        }
    }

    public function viewAll() {
        /*
          $this->form_validation->set_rules('from_date', 'From Date', 'trim|required');
          $this->form_validation->set_rules('to_date', 'To Date', 'trim|required');
          $this->form_validation->set_rules('limit', 'Limit', 'trim|required');
          $this->form_validation->set_rules('offset', 'Offset', 'trim|required');
         */

        /*
          if ($this->form_validation->run() == FALSE)
          {
          $response["error"] = true;
          $response["message"] = validation_errors();
          $this->EchoResponse(200, $response);
          } else {
         */

        /*
          $fromDate = $this->input->post('from_date');
          $toDate = $this->input->post('to_date');
          $limit = $this->input->post('limit');
          $offset = $this->input->post('offset');
         */

        //$user_data = $this->contributions_model->myTransactionsDc($fromDate, $toDate, $this->token_user_id, $limit, $offset); // journal entries
        $user_data = $this->contributions_model->myTransactionsDc($this->token_user_id); // journal entries
        if ($user_data) {
            $response["error"] = false;
            //////////////////////////////////////////////////////
            //$response["myTransactions"]  = $user_data;
            foreach ($user_data as $key => $value) {

                $response_array["transaction_id"] = $value->transId;
                $response_array["description"] = $value->description;
                $response_array["debit"] = $value->debit;
                $response_array["credit"] = $value->credit;
                $response_array["transaction_date"] = $value->transaction_date;
                $response_array["transaction_code"] = $value->transaction_code;
                $response["my_dc_transactions"][$key] = $response_array;
            }
            ///////////////////////////////////////////////////////
            $response["message"] = 'My Daily Contribution Transactions.';
        } else {
            $response["error"] = true;
            $response["message"] = 'No records founded.';
        } // else
        $this->EchoResponse(200, $response);
        //} // else 
    }

// View (User Transactions (Daily Contributions))

    public function createGroupContribution() {
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        $this->form_validation->set_rules('suggested_amount', 'Suggested Amount', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $suggested_amount = $this->input->post('suggested_amount');
            $data = $this->contributions_model->createGroupContribution($this->token_user_id, $title, $description, $suggested_amount);
            if ($data == FALSE) {
                $response["error"] = true;
                $response["message"] = 'Something went wrong. Try again later.';
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = false;
                $response["title"] = $data->title;
                $response["description"] = $data->description;
                $response["suggested_amount"] = $data->suggested_amount;
                $response["message"] = "Group-Contribution created successfully.";
                $this->EchoResponse(200, $response);
            } // else
        } //else
    }

// End - createGroupContribution
    // Start-Charities

    public function createCharity() {        
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            ///////////////////////// Image Upload START //////////////////////
            $config['upload_path'] = UPLOADS_DIR.'charity/';
            $config['allowed_types'] = 'jpg|png|jpeg|gif|bmp';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            $config['overwrite'] = FALSE;
            $config['file_name'] = mt_rand().'.png';

            $this->load->library('upload', $config);			
            if (!$this->upload->do_upload('file_name')) {
                $response["error"] = true;
                $response["message"] = $this->upload->display_errors();
                $this->EchoResponse(200, $response);
            } else {
                $file = $this->upload->data();
                $image_name = $file['file_name']; // Original Image
				$reference_number = random_string('alnum', 12); 
				$data = array('reference_number' => $reference_number,
				              'title'            => addslashes($this->input->post('title')),
							  'description'      => addslashes($this->input->post('description')),
							  'userId'           => $this->token_user_id,
							  'image'            => $image_name,
							  'status'           => 1
							  );
                $status = $this->contributions_model->createCharity($data);
                if ($status) {
					$response["error"] = false;
                    $response["message"] = "Charity created successfully.";
                    $this->EchoResponse(200, $response);
                } else {
					$response["error"] = true;
                    $response["message"] = 'Unable to create Charity.';
					$this->EchoResponse(200, $response);
                }
            }
        } //else
    }

// End - createCharity
    // Get Charities (List All Active - Status = 1)

    public function getCharities() {
        $data = $this->contributions_model->getCharities($this->token_user_id);
        if ($data == NULL) {
            $response["error"] = true;
            $response["message"] = 'No record found.';
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            foreach ($data as $key => $value) {
				$response_array["charity_id"] = $value->charity_id;
                $response_array["charity_flag"] = $value->charity_flag;
                $response_array["invitation_flag"] = $value->invitation_flag;
                $response_array["reference_number"] = $value->reference_number;
                $response_array["title"] = $value->title;
                $response_array["description"] = $value->description;
                $response_array["image"] = $value->image;
                //$response_array["status"] = $value->status;
                $response_array["created_by"] = ucfirst($value->firstName) . ' ' . ucfirst($value->lastName);
                $response["active_charities_list_all"][$key] = $response_array;
            } // End - Foreach
            $this->EchoResponse(200, $response);
        } // else
    }

//End - getCharities

    public function getMyCharities() {
        $data = $this->contributions_model->getMyCharities($this->token_user_id);
        if ($data == NULL) {
            $response["error"] = true;
            $response["message"] = 'No record found.';
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            foreach ($data as $key => $value) {
                $response_array["reference_number"] = $value->reference_number;
                $response_array["title"] = $value->title;
                $response_array["description"] = $value->description;
                $response_array["image"] = ADMIN_URL.'uploads/charity/'.$value->image;
                $response_array["status"] = $value->status;
                $response_array["created_by"] = ucfirst($value->firstName) . ' ' . ucfirst($value->lastName);
                $response["my_charities_list_all"][$key] = $response_array;
            } // End - Foreach
            $this->EchoResponse(200, $response);
        } // else
    }

//End - getMyCharities

    public function get_charity() {
        $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $data = $this->contributions_model->get_charity($this->input->post('reference_number'));
            $total_charity = $this->contributions_model->get_total_charity_amount($this->input->post('reference_number'));

            if ($data == NULL) {
                $response["error"] = true;
                $response["message"] = 'No records founded.';
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = false;
                $response["charity_data"] = $data;
                $response["total_charity_amount"] = $total_charity;
                $response["message"] = 'Specific charity details.';
                $this->EchoResponse(200, $response);
            }
        }
    }

    public function removeCharity() {
        $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $status = $this->contributions_model->removeCharity($this->token_user_id, $this->input->post('reference_number'));
            if ($status == FALSE) {
                $response["error"] = true;
                $response["message"] = 'No records founded.';
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = false;
                $response["message"] = "Charity was removed successfully.";
                $this->EchoResponse(200, $response);
            } // else
        }
    }

//End - removeCharity

    public function contributeCharity() {
        $this->form_validation->set_rules('reference_number', 'Reference Number', 'trim|required');
        $this->form_validation->set_rules('amount', 'Charity Amount', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {

            $reference_number = $this->input->post('reference_number');
            $amount = $this->input->post('amount');
            $userCr = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance
            if ($userCr['credit'] < $this->input->post('amount')) { // Verify user available balance
                $response["error"] = true;
                $response["message"] = 'Insufficient balance. Your balance ' . $userCr['credit'];
                $this->EchoResponse(200, $response);
            } else {
                $CharityId = $this->contributions_model->getCharityId($this->input->post('reference_number'));
				$transaction_code = random_string('alnum', 12);
                $debit = array('userId'           => $this->token_user_id,
							   'refId'            => $CharityId,
							   'dr'               => $this->input->post('amount'),
							   'gps'              => $this->input->post('gps'),
							   'devId'            => $this->input->post('dev_id'),
							   'type'             => 'TR',
							   'mode'             => 'CHC',
							   'transaction_code' => $transaction_code,
							   'description'      => $this->input->post('description')
							   );
                $credit = array('userId'           => $this->token_user_id,
								'refId'            => $this->token_user_id,
								'cr'               => $this->input->post('amount'),
								'gps'              => $this->input->post('gps'),
								'devId'            => $this->input->post('dev_id'),
								'type'             => 'TR',
								'mode'             => 'CHC',
								'transaction_code' => $transaction_code,
								'description'      => $this->input->post('description')
							);
                $transaction_data = $this->contributions_model->charityTransaction($this->token_user_id, $debit, $credit, $reference_number);
                if ($transaction_data) {
                    $user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance
                    $response["error"]          = false;
                    $response["transaction_id"] = $transaction_data->transId;
                    $response["user_id"]        = $transaction_data->userId;
                    $response["credit"]         = strval($user['credit']);
                    $response["debit"]          = $transaction_data->dr;
                    $response["gps"]            = $transaction_data->gps;
                    $response["dev_id"]         = $transaction_data->devId;
                    $response["created"]        = $transaction_data->created;
                    $response["message"]        = "You have successfully donated ".$amount;
                    $this->EchoResponse(200, $response);
                }
            }
        }
    }

// End contributeCharity

    public function myDonations() {

        $data = $this->contributions_model->myDonations($this->token_user_id);

        if ($data == FALSE) {
            $response["error"] = true;
            $response["message"] = 'No records founded.';
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            foreach ($data as $key => $value) {
                $response_array["donated_amount"] = $value->amount;
                $response_array["reference_number"] = $value->reference_number;
                $response_array["title"] = $value->title;
                $response_array["description"] = $value->description;
                $response["my_donations_list"][$key] = $response_array;
            } // End - Foreach
            $response["message"] = "List all my charity donations.";
            $this->EchoResponse(200, $response);
        } // else
    }

//End - myDonations
    // End-Charities

    public function myGroupContributions() {

        $data = $this->contributions_model->myGroupContributions($this->token_user_id);
        if ($data == FALSE) {
            $response["error"] = true;
            $response["message"] = 'No records founded.';
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            foreach ($data as $key => $value) {
                $response_array["reference_number"] = $value->reference_number;
                $response_array["title"] = $value->title;
                $response_array["description"] = $value->description;
                $response_array["suggested_amount"] = $value->suggested_amount;
                $response_array["status"] = $value->status;
                $response["my_gc_list"][$key] = $response_array;
            } // End - Foreach
            $response["message"] = "List my Group-Contributions.";
            $this->EchoResponse(200, $response);
        } // else
    }

//End - myGroupContributions

    public function friendListByGc() {
        $gc_reference_number = $this->input->post('gc_reference_number');
        $friends = $this->contributions_model->get_friendlist_upd($this->token_user_id, $gc_reference_number);

        echo '<pre>';
        print_r($friends);
        exit;

        if ($friends == NULL) {
            $response["error"] = true;
            $response["message"] = "You don't have any friend.";
            $this->EchoResponse(200, $response);
        } else {
            $response['error'] = false;
            //$response['friends'] = $friends;
            foreach ($friends as $key => $value) {
                if (!empty($value[0])) {
                    $response_array["status_id"] = $value[0]->status_id;
                    $response_array["request_sent"] = $value[0]->request_sent;
                    $response_array["user_id"] = $value[0]->userId;
                    $response_array["first_name"] = $value[0]->firstName;
                    $response_array["last_name"] = $value[0]->lastName;
                    $response_array["phone_number"] = $value[0]->phoneNumber;
                    $response_array["address"] = $value[0]->address;
                    $response_array["city"] = $value[0]->city;
                    $response_array["country"] = $value[0]->country;
                    $response_array["user_pin"] = $value[0]->userPIN;
                    $response_array["email"] = $value[0]->email;
                    $response_array["currency"] = $value[0]->currency;
                    $response_array["status"] = $value[0]->status;

                    $response['friends'][] = $response_array;
                }
            }
            $this->EchoResponse(200, $response);
        }
    }

// End - friendListByGc

    public function invitePeopleGc() {
        $this->form_validation->set_rules('user_pin', 'User Pin', 'trim|required');
        $this->form_validation->set_rules('gc_reference_number', 'Reference number', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $friend = $this->contributions_model->user_profile_by_pin($this->input->post("user_pin"));

            if ($friend == NULL) {
                $response["error"] = true;
                $response["message"] = 'Invalid User Pin. Please try again.';
                $this->EchoResponse(200, $response);
            } else {
                $data = array('userId' => $this->token_user_id,
                    'friend_id' => $friend->userId,
                    'gc_reference_number' => $this->input->post('gc_reference_number')
                );
                $status = $this->contributions_model->sendGcRequest($data);
                if ($status == NULL) {
                    $response["error"] = false;
                    $response["message"] = "Group-Contribution request successfully sent.";
                    $this->EchoResponse(200, $response);
                } else {
                    if ($status == 1) { // Pending for Response
                        $response["error"] = true;
                        $response["message"] = "Request already sent.";
                        $this->EchoResponse(200, $response);
                    } elseif ($status == 4) { // Transaction done
                        $response["error"] = true;
                        $response["message"] = "Group-Contribution Transaction done.";
                        $this->EchoResponse(200, $response);
                    } elseif ($status == 3) { // GC Request Rejected
                        $response["error"] = true;
                        $response["message"] = "Group-Contribution request rejected.";
                        $this->EchoResponse(200, $response);
                    } elseif ($status == 2) { // GC Request Approved
                        $response["error"] = true;
                        $response["message"] = "Group-Contribution request approved.";
                        $this->EchoResponse(200, $response);
                    }
                } // else
            } // else
        } // else
    }

    public function requestResponseGc() {
        $this->form_validation->set_rules('gc_reference_number', 'Reference Number', 'trim|required');
        $this->form_validation->set_rules('status', 'Status', 'trim|required');

        $this->form_validation->set_rules('gc_amount', 'GC-Amount', 'trim');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $reference_number = $this->input->post('gc_reference_number');
            $status = $this->input->post('status');
            if ($status == 4) { // Make GC Transaction - Contribute to Group-Contribution
                if ($this->input->post('gc_amount')) {

                    $amount = $this->input->post('gc_amount');
                    $userCr = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance
                    if ($userCr['credit'] < $this->input->post('gc_amount')) { // Verify user available balance
                        $response["error"] = true;
                        $response["message"] = 'Insufficient balance. Your balance ' . $userCr['credit'];
                        $this->EchoResponse(200, $response);
                    } else {

                        $guid = $this->contributions_model->get_unique_guid_gc();
                        $gcId = $this->contributions_model->getGcId($this->input->post('gc_reference_number'));

                        $debit = array('userId' => $this->token_user_id,
                            'refId' => $gcId,
                            'dr' => $this->input->post('gc_amount'),
                            'gps' => $this->input->post('gps'),
                            'devId' => $this->input->post('dev_id'),
                            'type' => 'TR',
                            'mode' => 'GC',
                            'transaction_code' => $guid,
                            'description' => $this->input->post('description')
                        );
                        $credit = array('userId' => $this->token_user_id,
                            'refId' => $this->token_user_id,
                            'cr' => $this->input->post('gc_amount'),
                            'gps' => $this->input->post('gps'),
                            'devId' => $this->input->post('dev_id'),
                            'type' => 'TR',
                            'mode' => 'GC',
                            'transaction_code' => $guid,
                            'description' => $this->input->post('description')
                        );
                        $transaction_data = $this->contributions_model->gcTransaction($this->token_user_id, $debit, $credit, $reference_number, $this->input->post('gc_amount'));
                        if ($transaction_data) {
                            $user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance

                            $response["error"] = false;
                            $response["transaction_id"] = $transaction_data->transId;
                            $response["user_id"] = $transaction_data->userId;
                            $response["credit"] = strval($user['credit']);
                            $response["debit"] = $transaction_data->dr;
                            $response["gps"] = $transaction_data->gps;
                            $response["dev_id"] = $transaction_data->devId;
                            $response["created"] = $transaction_data->created;
                            $response["message"] = "You have credited $amount to Group-Contributions Account.";
                            $this->EchoResponse(200, $response);
                        }
                    }
                } else {
                    $response["error"] = true;
                    $response["message"] = 'Amount required';
                    $this->EchoResponse(200, $response);
                }
            } else {

                $result = $this->contributions_model->requestResponseGc($this->token_user_id, $reference_number, $status);
                if ($result == TRUE) {
                    $response["error"] = false;
                    $response["message"] = 'Success';
                    $this->EchoResponse(200, $response);
                } else {
                    $response["error"] = true;
                    $response["message"] = 'Oops! Something went wrong.';
                    $this->EchoResponse(200, $response);
                }
            }
        }
    }

    public function myGcRequests() {

        $history = $this->contributions_model->myGcRequests($this->token_user_id);

        if ($history == NULL) {
            $response["error"] = true;
            $response["message"] = "No records founded.";
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            $response["my_gc_requests"] = $history;
            $response["message"] = "My Group-Contribution Requests.";
            $this->EchoResponse(200, $response);
        }
    }

    public function gcHistory() {

        $this->form_validation->set_rules('gc_reference_number', 'GC Reference-Number', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $reference_number = $this->input->post('gc_reference_number');
            $gc_requests = $this->contributions_model->getMyGcDetails($reference_number);
            if ($gc_requests == NULL) {
                $response["error"] = true;
                $response["message"] = "No records founded.";
                $this->EchoResponse(200, $response);
            } else {

                $response["error"] = false;
                //////////////////////////////////////////////////////
                //$response["myTransactions"]  = $user_data;
                foreach ($gc_requests as $key => $value) {

                    $response_array["first_name"] = $value->firstName;
                    $response_array["last_name"] = $value->lastName;
                    $response_array["pin_number"] = $value->userPin;
                    $response_array["suggested_amount"] = $value->suggested_amount;
                    if ($value->status == 4) {
                        $response_array["paid_amount"] = $value->amount;
                    }
                    $response_array["status"] = $value->status;
                    $response_array["gc_reference_number"] = $value->gc_reference_number;
                    $response["gc_all_requests"][$key] = $response_array;
                }
                ///////////////////////////////////////////////////////
                $response["message"] = 'Group-Contribution all requests.';

                $this->EchoResponse(200, $response);
            }
        }
    }

    public function test() {

        // Push notification to android		
        $deviceToken = 'dbf4a5b95dab7b48800255b091e407d7204d222ba2b509468946bf22aab3691e';
        $passphrase = 'devdesks321';  // Put your private key's passphrase here
        $message = 'Ustaad g message agaya ne ?'; // // Put your alert message here

        $pem_file = APPPATH . '/libraries/ck.pem';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem_file);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx
        );

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

        // Create the payload body
        $body['aps'] = array('alert' => $message,
            'badge' => '5',
            'sound' => 'default'
        );

        // Encode the payload as JSON		
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        /* if (!$result)
          echo 'Message not delivered' . PHP_EOL;
          else
          echo 'Message successfully delivered' . PHP_EOL;
         */

        // Close the connection to the server
        fclose($fp);
    }

//End - getMyCharities


    /*
     * Edited By ::: Adnan Haider
     */
    public function inviteToCharity() {
        $this->form_validation->set_rules('id', 'Charity ID', 'required');
        $this->form_validation->set_rules('user_list', 'Users list Object', 'required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $this->load->helper('notification');
            $charity_id = $this->input->post('id');
            $charity = $this->contributions_model->getMyCharityDetail($charity_id);
            $users = $this->input->post('user_list');
            // [{"id":"125"},{"id": "145"},{"id":"134"}]
            $users = json_decode($users);

            foreach ($users as $user) {
                $this->contributions_model->inviteToCharity($this->token_user_id, $user->id, $charity->id, $charity->reference_number);
                $msg = array('notification_type' => "charity_request",
                             'reference_number'  => $charity->reference_number,
                             'title'             => $charity->title,
                             'sender_name'       => $charity->firstName,
                             'message'           => $charity->firstName.' Has invited you to join '.$charity->title
                            );
                $notification_data = array('message' => json_encode($msg));
                sent_notify($notification_data, $user->id);
            }
            $response["error"] = false;
            $response["message"] = 'Invitation sent successfully.';
            $this->EchoResponse(200, $response);
        }
    }

    public function pendingInvitations() {
        $invitations = $this->contributions_model->getPendingInvitations($this->token_user_id);
		$data = array();
        if ($invitations == NULL) {
            $response["error"] = true;
			$response["invitations"] = $data;
            $response["message"] = "No record found.";
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
			foreach($invitations as $invitation) :
				$data[] = array('invitation_id'    => $invitation->invitation_id,
				                'title'            => stripslashes($invitation->title),
							    'description'      => stripslashes($invitation->description),
								'image'            => ADMIN_URL.'uploads/charity/'.$invitation->image,
								'reference_number' => $invitation->reference_number,
								'sender_id'        => $invitation->sender_id,
								'firstName'        => stripslashes($invitation->firstName),
								'lastName'         => stripslashes($invitation->lastName)
							   );
			endforeach;			
            $response["invitations"] = $data;
            $this->EchoResponse(200, $response);
        }
    } // pendingInvitations
	

    public function responseToInvitation() {
        $this->form_validation->set_rules('id', 'Invitation Id', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $status = $this->input->post('status');
            $invitation_id = $this->input->post('id');

            $charity_id = $this->contributions_model->responseToInvitation($invitation_id,$status);

            $charity = $this->contributions_model->getMyCharityDetail($charity_id);
            $senderId = $this->contributions_model->getSenderId($this->input->post('id'));
            if ($status) {
                $this->load->helper('notification');
                $msg = '';
                if ($this->input->post('status') == 1) {
                    $msg = array('notification_type' => "action_charity_request",
                        'action_request' => 'Pending',
                        'reference_number' => $charityDetial->reference_number,
                        'title' => $charityDetial->title,
                        'sender_name' => $charityDetial->firstName,
                        'message' => $charityDetial->firstName . ' Pending ' . $charityDetial->title
                    );
                    $response["message"] = 'Pending.';
                } elseif ($this->input->post('status') == 2) {
                    $msg = array('notification_type' => "action_charity_request",
                        'action_request' => 'accepted',
                        'reference_number' => $charityDetial->reference_number,
                        'title' => $charityDetial->title,
                        'sender_name' => $charityDetial->firstName,
                        'message' => $charityDetial->firstName . ' Has accepted request ' . $charityDetial->title
                    );
                    $response["message"] = 'Accepted.';
                } else {
                    $msg = array('notification_type' => "action_charity_request",
                        'action_request' => 'rejected',
                        'reference_number' => $charityDetial->reference_number,
                        'title' => $charityDetial->title,
                        'sender_name' => $charityDetial->firstName,
                        'message' => $charityDetial->firstName . ' Has rejected request ' . $charityDetial->title
                    );
                    $response["message"] = 'Rejected.';
                }

                $notification_data = array('message' => json_encode($msg));
                sent_notify($notification_data, $senderId);
                $response["error"] = false;
                $response["reference_number"] = $status;
                $this->EchoResponse(200, $response);
            } else { // Un-Subscribed
                $response["error"] = true;
                $response["message"] = 'Doesnt proceeed contact to admin.';
                $this->EchoResponse(200, $response);
            }
        }
    }

}

// Contributions (CI_Controller)