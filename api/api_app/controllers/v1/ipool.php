<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ipool extends MY_Controller {
    function __construct() {
        parent::__construct();		
        $this->load->model('ipool_model');
		$this->load->helper('home');
		$this->load->helper('notification');
		$response = array();
    }
	
	
    public function create() 
	{		
        $this->form_validation->set_rules('title', 'Title', 'trim|required');		
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
        $this->form_validation->set_rules('frequency', 'Frequency', 'trim|required');
        $this->form_validation->set_rules('max_people', 'Max People', 'trim|required');
		$this->form_validation->set_rules('start_date', 'Start Date', 'trim|required');
        if ($this->form_validation->run() === FALSE) {		
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $data = array('title'      => addslashes($this->input->post('title')),
						  'amount'     => addslashes($this->input->post('amount')),
						  'uid'        => $this->token_user_id,
						  'frequency'  => $this->input->post('frequency'),
						  'max_people' => $this->input->post('max_people'),
						  'start_date' => date("Y-m-d",strtotime($this->input->post('start_date'))),
						  'created'    => date("Y-m-d H:i:s")
						 );	 	 
			$status = $this->ipool_model->create_ipool($data);			  
            if($status) 
			{
                $response["error"]      = false;
                $response["message"]    = 'E-susu Created Successfully.';
				$response["amount"]     = $this->input->post('amount');
				$response["uid"]        = $this->token_user_id;
				$response["frequency"]  = $this->input->post('frequency');
				$response["max_people"] = $this->input->post('max_people');
				$response["start_date"] = date("Y-m-d",strtotime($this->input->post('start_date')));
				$response["created"]    = date("Y-m-d g:i A",strtotime(date("Y-m-d H:i:s")));				
			} else {
				$response["error"]   = true;
				$response["message"] = 'Unable to create Ipool.';
			}
            $this->EchoResponse(200, $response);
        } // else
    } // create
	
	
	public function ipool_list()
	{			
		$ipool = $this->ipool_model->get_ipool_list($this->token_user_id);
		if($ipool == NULL)
		{
			$response["error"]   = true;	
			$response["message"] = "No Record Found.";
			$response["ipools"] = array();
		} else {
			$response["error"]   = false;
			$data = array();	
			foreach($ipool as $row) :
				$data[] = array("id"         => $row->id,
				                "userId"     => $this->token_user_id,
				                "title"      => stripslashes($row->title),
								"amount"     => $row->amount,
								"frequency"  => $row->frequency,
								"max_people" => $row->max_people,
								"start_date" => $row->start_date,
								"created"    => date("Y-m-d g:i A",strtotime($row->created))								
							   );				
			endforeach;	
			$response["ipools"] = $data;
		}
		$this->EchoResponse(200, $response);
	} // ipool_list
	
	
	public function send_invitation() 
	{		
        $this->form_validation->set_rules('ipool_id', 'E-susu ID', 'trim|required');
        if ($this->form_validation->run() === FALSE) {		
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$ipool_id = $this->input->post('ipool_id');
            $users = json_decode($this->input->post('users')); 
			// [{"id":"134"},{"id":"135"}]
/*			if(is_array($users))
			{
				echo "array";	
			} else {
				echo "object";
			}
			exit();*/
			foreach($users as $user) :
				$data = array('ipool_id'   => $ipool_id,
							  'uid'        => $user->id,
							  'status'     => 1, // 1 for pending, 2 for accepted, 3 for rejected
							  'datetime'   => date("Y-m-d H:i:s")
							 );	 	 
				$this->ipool_model->send_invitation($data);	
				
				$userInfo = get_userInfo($this->token_user_id);
				$ipool = $this->ipool_model->get_ipool_info($ipool_id);
				
				$msg = array('notification_type' => "ipool_request",
                             'ipool_id'          => $ipool->id,
                             'title'             => stripslashes($ipool->title),
                             'sender_name'       => $userInfo->firstName,
                             'message'           => $userInfo->firstName." Has invited you to join ".$ipool->title
                            );
				$notification_data = array('message' => json_encode($msg));
				sent_notify($notification_data, $user->id);
			endforeach;    
			$response["error"]   = false;
            $response["message"] = 'Invitation send to selected users.';
            $this->EchoResponse(200, $response);			
        } // else
    } // send_invitation
	
	
	public function pending_invitation()
	{			
		$ipool = $this->ipool_model->pending_invitation($this->token_user_id);
		if($ipool == NULL)
		{
			$response["error"]   = true;	
			$response["message"] = "No Record Found.";
			$response["ipools"] = array();
		} else {
			$response["error"]   = false;
			$data = array();	
			foreach($ipool as $row) :
				$data[] = array("id"           => $row->id,
				                "userId"       => $this->token_user_id,
				                "title"        => stripslashes($row->title),
								"amount"       => $row->amount,
								"frequency"    => $row->frequency,
								"max_people"   => $row->max_people,
								"start_date"   => $row->start_date,
								"invited_date" => date("Y-m-d g:i A",strtotime($row->invited_date))								
							   );				
			endforeach;	
			$response["ipools"] = $data;
		}
		$this->EchoResponse(200, $response);
	} // pending_invitation
	
	
	public function requestResponse() 
	{		
        $this->form_validation->set_rules('invitation_id', 'Invitation ID', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
        if ($this->form_validation->run() === FALSE) {		
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$status = $this->input->post('status');
			$data = array('status' => $status); // 1 for pending, 2 for accepted, 3 for rejected
			$invitation_id = $this->input->post('invitation_id');
			$resStatus = $this->ipool_model->requestResponse($data,$invitation_id);	
			$response["error"] = false;
			
			$ipool_id = $this->ipool_model->get_ipool_id($invitation_id);
			$ipool = $this->ipool_model->get_ipool_info($ipool_id);
			$userInfo = get_userInfo($this->token_user_id);
							
			if($status == 1) {
				$response["message"] = 'Request is Pending.';
			} elseif($status == 2) {
				$msg = array('notification_type' => "action_ipool_request",
                             'ipool_id'          => $ipool->id,
                             'title'             => stripslashes($ipool->title),
                             'sender_name'       => $userInfo->firstName,
							 'action'            => 'accepted',
                             'message'           => $userInfo->firstName." accepted your E-susu request"
                            );			
				$notification_data = array('message' => json_encode($msg));
				sent_notify($notification_data, $user->id);
				$response["message"] = 'Request is Accepted.';
			} else {
				$msg = array('notification_type' => "action_ipool_request",
                             'ipool_id'          => $ipool->id,
                             'title'             => stripslashes($ipool->title),
                             'sender_name'       => $userInfo->firstName,
							 'action'            => 'rejected',
                             'message'           => $userInfo->firstName." rejected your E-susu request"
                            );			
				$notification_data = array('message' => json_encode($msg));
				sent_notify($notification_data, $user->id);				
				$response["message"] = 'Request is Rejected.';
			}
            $this->EchoResponse(200, $response);			
        } // else
    } // requestResponse
	
	
	public function participation()
	{			
		$ipool = $this->ipool_model->participation($this->token_user_id);
		if($ipool == NULL)
		{
			$response["error"]   = true;	
			$response["message"] = "No Record Found.";
			$response["participation"] = array();
		} else {
			$response["error"]   = false;
			$data = array();	
			foreach($ipool as $row) :
				$data[] = array("id"           => $row->id,
				                "userId"       => $this->token_user_id,
				                "title"        => stripslashes($row->title),
								"amount"       => $row->amount,
								"frequency"    => $row->frequency,
								"max_people"   => $row->max_people,
								"start_date"   => $row->start_date,
								"invited_date" => date("Y-m-d g:i A",strtotime($row->invited_date))								
							   );				
			endforeach;	
			$response["participation"] = $data;
		}
		$this->EchoResponse(200, $response);
	} // participation
	
		
	public function my_ipool_participants()
	{			
		$this->form_validation->set_rules('ipool_id', 'E-susu ID', 'trim|required');
		if ($this->form_validation->run() === FALSE) {		
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$ipool_id = $this->input->post('ipool_id');
			$users = $this->ipool_model->my_ipool_participants($ipool_id);
			if($users == NULL)
			{
				$response["error"]   = true;	
				$response["message"] = "No Record Found.";
				$response["users"] = array();
			} else {
				$response["error"]   = false;
				$data = array();	
				foreach($users as $user) :
					$data[] = array("uid"       => $user->uid,
									"firstName" => stripslashes($user->firstName),
									"lastName"  => stripslashes($user->lastName),
									"userPIN"   => $user->userPIN
								   );				
				endforeach;
				$response["users"] = $data;
			}
			$this->EchoResponse(200, $response);
		}
	} // my_ipool_participants
	
	
	public function friendlist()
	{
		$this->form_validation->set_rules('ipool_id', 'E-susu ID', 'trim|required');
		if ($this->form_validation->run() === FALSE) {		
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$users = $this->ipool_model->get_friendlist($this->token_user_id);
			if($users == NULL)
			{
				$response["error"]   = false;	
				$response["message"] = "Please add friends.";
				$response["users"] = array();
			} else {
				$response["error"] = false;
				$ipool = $this->input->post('ipool_id');
				$data = array();	
				foreach($users as $user) :
					$status = $this->ipool_model->isIpoolAcceptedMem($ipool,$user->userId);
					if($status==3 || $status==4) :
						$data[] = array("id"                => $user->userId,
										"firstName"         => stripslashes($user->firstName),
										"lastName"          => stripslashes($user->lastName),
										"userPIN"           => $user->userPIN,
										"invitation_status" => $status 
									   );		
					endif;			
				endforeach;
				$response["users"] = $data;
			}
			$this->EchoResponse(200, $response);
		} // else
	} // friendlist
	
	
	
	
	
} // Ipool