<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class transactions extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('notification');
        $this->load->library('form_validation');
        $this->load->model('transactions_model');
        $this->load->model('contributions_model');
        $response = array();
    }

    public function AddCreditCard() {
        $this->form_validation->set_rules('credit', 'Credit', 'trim|required');
        $this->form_validation->set_rules('card_number', 'Card Number', 'trim|required');
        $this->form_validation->set_rules('exp_date', 'Expiry Date', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim|required');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $amount = number_format($this->input->post('credit'), 2);  // 6.00
            $card_num = $this->input->post('card_number'); // 4007000000027
            $exp_date = $this->input->post('exp_date'); // 08/20
            $gps = $this->input->post('gps');
            $devId = $this->input->post('dev_id');

            $user = $this->transactions_model->user_profile_by_id($this->token_user_id); // Get User profile by User ID

            $data = array('card_num' => $card_num,
                'exp_date' => $exp_date,
                'amount' => $amount,
                'first_name' => $user->firstName,
                'last_name' => $user->lastName,
                'phone' => $user->phoneNumber,
                'address' => $user->address,
                'city' => $user->city,
                'country' => $user->country
            );
            $transaction = $this->authorized_dot_net($data);
            if ($transaction["error"] == 1) {
                $response["error"] = true;
                $response["message"] = $transaction["error_message"];
                $this->EchoResponse(200, $response);
            } else {
                $data = array('userId' => $this->token_user_id,
                    'transaction_id' => $transaction["transaction_id"],
                    'cr' => $amount,
                    'gps' => $gps,
                    'devId' => $devId,
                    'type' => 'AC',
                    'mode' => 'CC',
                    'auth_code' => $transaction["auth_code"],
                    'description' => $this->input->post('description')
                );
                $transaction = $this->transactions_model->add_credit_card($data);
                if ($transaction) {
                    $user = $this->transactions_model->user_balance($this->token_user_id);

                    $response["error"] = false;
                    $response["transaction_id"] = $transaction->transId;
                    $response["user_id"] = $transaction->userId;
                    $response["credit"] = strval($user['credit']);
                    $response["created"] = $transaction->created;
                    $response["message"] = '601';
                    $this->EchoResponse(200, $response);
                } else {
                    $response["error"] = true;
                    $response["message"] = '602';
                    $this->EchoResponse(200, $response);
                }
            } // else
        } // else
    }

// AddCreditCard

    /*
      public function AddScratchCard() {
      echo "i m here"; exit();
      $this->form_validation->set_rules('card_number', 'Scrath card no', 'trim|required');
      $this->form_validation->set_rules('dev_id', 'Device ID', 'trim|required');
      $this->form_validation->set_rules('gps', 'GPS', 'trim');
      $this->form_validation->set_rules('description', 'Description', 'trim');

      if ($this->form_validation->run() == FALSE) {
      echo "here"; exit();
      $response["error"] = true;
      $response["message"] = validation_errors();
      $this->EchoResponse(200, $response);
      } else {
      echo "here"; exit();
      /*    $scratch_card = $this->transactions_model->scratch_card_by_number($this->input->post('card_number'));
      if($scratch_card)
      {
      $data = array('userId'      => $this->token_user_id,
      'cr'          => $scratch_card->cardAmount,
      'gps'         => $this->input->post('gps'),
      'devId'       => $this->input->post('dev_id'),
      'cardId'      => $scratch_card->cardId,
      'type'        => 'AC',
      'mode'        => 'SC',
      'description' => $this->input->post('description')
      );
      $transaction = $this->transactions_model->add_scratch_card($data);
      if ($transaction) {
      $user = $this->transactions_model->user_balance($this->token_user_id);
      $response["error"]          = false;
      $response["user_id"]        = $transaction->userId;
      $response["credit"]         = strval($user['credit']);
      $response["created"]        = $transaction->created;
      $response["message"]        = '601';
      $response["transaction_id"] = $transaction->transId;
      $this->EchoResponse(200, $response);
      } else {
      $response["error"]   = true;
      $response["message"] = '602';
      $this->EchoResponse(200, $response);
      }
      } else {
      $response["error"]   = true;
      $response["message"] = 'Invalid Card Number.';
      $this->EchoResponse(200, $response);
      } // else
     */

    // } // else 
    //  } // AddScratchCard


    public function AddScratchCard() {
        $this->form_validation->set_rules('card_number', 'Scrath card no', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim|required');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $scratch_card = $this->transactions_model->scratch_card_by_number($this->input->post('card_number'));

            if ($scratch_card) {
                $data = array('userId' => $this->token_user_id,
                    'cr' => $scratch_card->cardAmount,
                    'gps' => $this->input->post('gps'),
                    'devId' => $this->input->post('dev_id'),
                    'cardId' => $scratch_card->cardId,
                    'type' => 'AC',
                    'mode' => 'SC',
                    'description' => $this->input->post('description')
                );
                $transaction = $this->transactions_model->add_scratch_card($data);
                if ($transaction) {
                    $user = $this->transactions_model->user_balance($this->token_user_id);
                    $response["error"] = false;
                    $response["transaction_id"] = $transaction->transId;
                    $response["user_id"] = $transaction->userId;
                    $response["credit"] = strval($user['credit']);
                    $response["created"] = $transaction->created;
                    $response["message"] = '601';
                    $this->EchoResponse(200, $response);
                } else {
                    $response["error"] = true;
                    $response["message"] = '602';
                    $this->EchoResponse(200, $response);
                }
            } else {
                $response["error"] = true;
                $response["message"] = 'Invalid Card Number.';
                $this->EchoResponse(200, $response);
            } // else
        } // else 
    }

// AddScratchCard

    public function TransferCredit() {
        $this->form_validation->set_rules('credit', 'Credit', 'trim|required');
        $this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim|required');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $userPIN = $this->input->post('user_pin');
            $user = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance
            $userPinStatus = $this->transactions_model->check_userPin($userPIN, $this->token_user_id);

            if ($userPinStatus == 1) {
                $response["error"] = true;
                $response["message"] = 'You cannot transfer credit to your own account.';
                $this->EchoResponse(200, $response);
            } else if ($user['credit'] < $this->input->post('credit')) {
                $response["error"] = true;
                $response["message"] = 'Insufficient balance. Your balance ' . $user['credit'];
                $this->EchoResponse(200, $response);
            } else {
                $user_profile = $this->transactions_model->user_profile_by_pin($userPIN);

                if ($user_profile) {
                    $debit = array(
                        'userId' => $this->token_user_id,
                        'refId' => $user_profile->userId,
                        'dr' => $this->input->post('credit'),
                        'gps' => $this->input->post('gps'),
                        'devId' => $this->input->post('dev_id'),
                        'type' => 'TR',
                        'mode' => 'TCP',
                        'description' => $this->input->post('description')
                    );
                    $credit = array('userId' => $user_profile->userId,
                        'refId' => $this->token_user_id,
                        'cr' => $this->input->post('credit'),
                        'gps' => $this->input->post('gps'),
                        'devId' => $this->input->post('dev_id'),
                        'type' => 'TR',
                        'mode' => 'TCP',
                        'description' => $this->input->post('description')
                    );
                    $transaction_data = $this->transactions_model->transfer_credit($this->token_user_id, $debit, $credit);
                    if ($transaction_data) {
                        $transfer_credit_amount = $this->input->post('credit');
                        $newCredit = $this->transactions_model->user_balance($user_profile->userId); // Get Updated Balance
                        if (!empty($newCredit)) {
                            $totalBalance = $newCredit['credit'];
                        } else {
                            $totalBalance = 0;
                        }
                        $senderPin = $this->transactions_model->getUserPin($this->token_user_id); // Get sender's pin
//                        $notification_data = array(
//                            'message' => array('notification_type' => "creditTransfer",
//                                'amount' => $transfer_credit_amount,
//                                'newCredit' => $totalBalance),
//                            'vibrate' => 1,
//                            'sound' => "default"
//                        );
                        //sent_notify($notification_data,$user_profile->userId);

                        $senderProfile = $this->transactions_model->user_profile_by_id($this->token_user_id);


                        $msg = array(
                            'notification_type' => "credit_transfer",
                            'amount' => $transfer_credit_amount,
                            'new_credit' => $totalBalance,
                            'sender_name' => $senderProfile->firstName,
                            'message' => "Credit transfered successfully"
                        );

                        $msg = json_encode($msg);
                        $notification_data = array(
                            'message' => $msg
                        );

//                        $notification_data = json_encode($notification_data);
//                        $this->load->model('transactions_model');
//                        $user_profile = $this->transactions_model->user_profile_by_pin($this->input->post('imali_account_user_pin'));

                        sent_notify($notification_data, $user_profile->userId);

                        $user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance

                        $response["error"] = false;
                        $response["transaction_id"] = $transaction_data->transId;
                        $response["user_id"] = $transaction_data->userId;
                        $response["credit"] = strval($user['credit']);
                        $response["debit"] = $transaction_data->dr;
                        $response["gps"] = $transaction_data->gps;
                        $response["dev_id"] = $transaction_data->devId;
                        $response["created"] = $transaction_data->created;
                        $response["message"] = '601';
                        $this->EchoResponse(200, $response);
                    } // if
                } else {
                    $response["error"] = true;
                    $response["message"] = 'Invalid User Pin. Please try again.';
                    $this->EchoResponse(200, $response);
                }
            } // else
        } // else
    }

// TransferCredit

    public function AvailableBalance() {
        $user = $this->transactions_model->user_balance($this->token_user_id);
//        $symbol = $this->transactions_model->my_currency_symbol($this->token_user_id);
        $currency_code = $this->transactions_model->my_currency_code($this->token_user_id);
        if ($user == NULL) {
            $response["error"] = true;
            $response["message"] = "No balance.";
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            //$response["user_id"] = $user['userId'];
            $response["credit"] = number_format(strval($user['credit']), 2); //strval($user['credit']).'.00';
            $response["debit"] = number_format(strval($user['debit']), 2); //$user['debit'].'.00';
            $response["daily_contributions_credit"] = number_format(strval($user['total_daily_contributions_credit']), 2); //$user['total_daily_contributions_credit'];
            $response["credit_sum"] = number_format($user['total_daily_contributions_credit'] + $user['credit'], 2); //$user['debit'].'.00';
            $response["currency_code"] = $currency_code; //$user['debit'].'.00';
            //$response["group_contributions_credit"] = "$symbol ".number_format(strval($user['total_group_contributions_credit']),2); //$user['total_group_contributions_credit'];
            //$response["charities_credit"] = "$symbol ".number_format(strval($user['total_my_created_charities_credit']),2); //$user['total_my_created_charities_credit'];
            //$response["charities_debit"] = "$symbol ".number_format(strval($user['total_charities_debit']),2); //$user['total_charities_debit'];
            $this->EchoResponse(200, $response);
        }
    }

// AvailableBalance

    public function AvailableBalanceAll() {
        $user = $this->transactions_model->user_balance($this->token_user_id);
        if ($user == NULL) {
            $response["error"] = true;
            $response["message"] = "No balance.";
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            //$response["user_id"] = $user['userId'];
            $response["credit"] = strval($user['credit']) . '.00';
            $response["debit"] = $user['debit'] . '.00';
            $this->EchoResponse(200, $response);
        }
    }

// AvailableBalance All

    public function bankTransfer() {
        $this->form_validation->set_rules('bank', 'Bank', 'trim|required');
        $this->form_validation->set_rules('branch', 'Branch', 'trim|required');
        $this->form_validation->set_rules('account_number', 'Account Number', 'trim|required');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim|required');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim');
        $this->form_validation->set_rules('email', 'Email', 'trim');
        $this->form_validation->set_rules('address', 'Address', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {

            $data = array('bank' => $this->input->post('bank'),
                'branch' => $this->input->post('branch'),
                'account_no' => $this->input->post('account_number'),
                'amount' => $this->input->post('amount'),
                'firstname' => $this->input->post('first_name'),
                'lastname' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'userId' => $this->token_user_id,
                'address' => $this->input->post('address'),
                'gps' => $this->input->post('gps'),
                'devId' => $this->input->post('dev_id'),
                'description' => $this->input->post('description')
            );
            $user_data = $this->transactions_model->bankTransfer($data);
            if ($user_data) {
                $user = $this->transactions_model->user_balance($this->token_user_id);
                $response["error"] = false;
                $response["transaction_Id"] = $user_data->transId;
                $response["user_id"] = $user_data->userId;
                $response["dev_id"] = $user_data->devId;
                $response["credit"] = strval($user['credit']);
                $response["debit"] = $user_data->dr;
                $response["gps"] = $user_data->gps;
                $response["message"] = 'Transfer successful';
            } else {
                $response["error"] = true;
                $response["message"] = '602';
            } // else
            $this->EchoResponse(200, $response);
        } // else 
    }

// bankTransfer

    public function cashTransfer() {
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required');
        $this->form_validation->set_rules('phone_number', 'Phone', 'trim|required');
        $this->form_validation->set_rules('secret_code', 'Secret Code', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim|required');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('first_name', 'First Name', 'trim');
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim');
        $this->form_validation->set_rules('email', 'Email', 'trim');
        $this->form_validation->set_rules('address', 'Address', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;

            $error_response = str_replace("<p>", "", validation_errors());
            $error_response_1 = str_replace("</p>", "\n", $error_response);
            $response["message"] = $error_response_1;
            $this->EchoResponse(200, $response);
        } else {

            $user = $this->transactions_model->user_balance($this->token_user_id);
            if ($user['credit'] < $this->input->post('amount')) {
                $response["error"] = true;
                $response["message"] = 'Insufficient balance. Your balance ' . $user['credit'];
                $this->EchoResponse(200, $response);
            } else {
                $data = array('amount' => $this->input->post('amount'),
                    'phone' => $this->input->post('phone_number'),
                    'secret_code' => $this->input->post('secret_code'),
                    'userId' => $this->token_user_id,
                    'firstname' => $this->input->post('first_name'),
                    'lastname' => $this->input->post('last_name'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'gps' => $this->input->post('gps'),
                    'devId' => $this->input->post('dev_id'),
                    'description' => $this->input->post('description')
                );
                $user_data = $this->transactions_model->cashTransfer($data);
                if ($user_data) {
                    $response["error"] = false;
                    $response["transaction_Id"] = $user_data->transId;
                    $response["user_id"] = $user_data->userId;
                    $response["dev_id"] = $user_data->devId;
                    $response["credit"] = strval($user['credit']);
                    $response["debit"] = $user_data->dr;
                    $response["gps"] = $user_data->gps;
                    $response["type"] = $user_data->type;
                    $response["mode"] = $user_data->mode;
                    $response["description"] = $user_data->description;
                    $response["message"] = 'Cash Transfer successfully';
                } else {
                    $response["error"] = true;
                    $response["message"] = '602';
                } // else
                $this->EchoResponse(200, $response);
            }
        } // else 
    }

// cashTransfer

    public function myRecentTransactions() {
        $this->form_validation->set_rules('limit', 'Limit', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {

            $limit = $this->input->post('limit');
            $user_data = $this->transactions_model->myRecentTransactions($limit, $this->token_user_id); // journal entries
            if ($user_data) {
                $response["error"] = false;
                //$response["myTransactions"]  = $user_data;
                foreach ($user_data as $key => $value) {

                    $response_array["transaction_id"] = $value->transId;
                    $response_array["description"] = $value->description;
                    $response_array["debit"] = $value->debit;
                    $response_array["credit"] = $value->credit;
                    $response_array["transaction_date"] = $value->transaction_date;
                    $response_array["mode"] = $value->mode;

                    $response["my_transactions"][$key] = $response_array;
                }
                $response["message"] = 'My Recent Transactions.';
            } else {
                $response["error"] = true;
                $response["message"] = 'No records founded.';
            } // else
            $this->EchoResponse(200, $response);
        } // else 
    }

// myTransactions (User Transactions)

    public function myTransactions() {
        $this->form_validation->set_rules('from_date', 'From Date', 'trim|required');
        $this->form_validation->set_rules('to_date', 'To Date', 'trim|required');
        $this->form_validation->set_rules('limit', 'Limit', 'trim|required');
        $this->form_validation->set_rules('offset', 'Offset', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $fromDate = $this->input->post('from_date');
            $toDate = $this->input->post('to_date');
            $limit = $this->input->post('limit');
            $offset = $this->input->post('offset');

            $user_data = $this->transactions_model->new_myTransactions($fromDate, $toDate, $this->token_user_id, $limit, $offset); // journal entries
            if ($user_data) {
                $response["error"] = false;
                //////////////////////////////////////////////////////
                //$response["myTransactions"]  = $user_data;
                foreach ($user_data as $key => $value) {

                    $response_array["transaction_id"] = $value->transId;
                    $response_array["description"] = $value->description;
                    $response_array["debit"] = $value->debit;
                    $response_array["credit"] = $value->credit;
                    $response_array["transaction_date"] = $value->transaction_date;
                    $response_array["mode"] = $value->mode;

                    $response["my_transactions"][$key] = $response_array;
                }
                ///////////////////////////////////////////////////////
                $response["message"] = 'My Transactions.';
            } else {
                $response["error"] = true;
                $response["message"] = 'No records founded.';
            } // else
            $this->EchoResponse(200, $response);
        } // else 
    }

// myTransactions (User Transactions)
    // Start Merchant
    // Get My Merchant Invoices
    public function get_my_invoices() {

        $this->form_validation->set_rules('status', "Status", "trim|required");
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $status = $this->input->post('status');
            $data = $this->transactions_model->get_my_invoices($this->token_user_id, $status); // Get Imali User Invoices By Status
            if (!empty($data)) {
                $response["error"] = false;
                $response["data"] = $data;
                $response["message"] = "List invoices.";
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = true;
                $response["message"] = "No records founded.";
                $this->EchoResponse(200, $response);
            }
        }
    }

//	public function invoice_details(){
//		$this->form_validation->set_rules('invoice_id', 'Invoice ID', 'trim|required');
//		if($this->form_validation->run() == FALSE){
//			$response["error"] = true;
//          	$response["message"] = validation_errors();
//            $this->EchoResponse(200, $response); 
//		} else {
//			$invoice_id = $this->input->post('invoice_id');
//			$invoice_data = $this->transactions_model->get_invoice_details($invoice_id, $this->token_user_id);
//			if($invoice_data == NULL){
//				$response["error"] = true;
//				$response["message"] = "No record founded.";
//				$this->EchoResponse(200, $response); 
//			} else {
//				$response["error"] = false;
//				$response["data"] = $invoice_data;
//				$response["message"] = "Invoice details.";
//				$this->EchoResponse(200, $response); 
//			}
//		}
//	}
    public function invoice_response($invoice_status, $invoice_id, $merchant_id) {
//        $this->form_validation->set_rules('invoice_id', 'Invoice ID', 'trim|required');
//        $this->form_validation->set_rules('status', 'Status', 'trim|required');
//        if ($this->form_validation->run() == FALSE) {
//            $response["error"] = true;
//            $response["message"] = validation_errors();
//            $this->EchoResponse(200, $response);
//        } else {
//            $invoice_id = $this->input->post('invoice_id');
//            $status = $this->input->post('status');
        $invoice_id = $invoice_id;
        $status = $invoice_status;
        $response_status = $this->transactions_model->set_invoice_status($invoice_id, $status);
        if ($response_status == FALSE) {
            $response["error"] = true;
            $response["message"] = "Oops! Something went wrong.";
            $this->EchoResponse(200, $response);
        } else {
            $response["error"] = false;
            $response["message"] = "Invoice status is updated.";
            $user = $this->transactions_model->user_profile_by_id($this->token_user_id); // Get User profile by User ID
            $msg = array(
                'notification_type' => "action_invoice",
                'invoice_id' => $invoice_id,
                'sender_name' => $user->firstName . " " . $user->lastName,
                'action_invoice' => "rejected",
                'message' => "Invoice rejected"
            );
            $msg = json_encode($msg);
            $notification_data = array(
                'message' => $msg
            );
            sent_notify($notification_data, $merchant_id, "merchant");
            $this->EchoResponse(200, $response);
        }
//        }
    }

    // Make Merchant Invoice Transaction
    public function pay_invoice() {
        $this->form_validation->set_rules('merchant_id', 'Merchant ID', 'trim|required');
        $this->form_validation->set_rules('invoice_id', 'Invoice ID', 'trim|required');
        $this->form_validation->set_rules('invoice_status', 'Invoice status', 'trim|required');
        $this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            if ($this->input->post('invoice_status') == '3') {
                $this->invoice_response($this->input->post('invoice_status'), $this->input->post('invoice_id'), $this->input->post('merchant_id'));
                return;
            }
            $merchant_id = $this->input->post('merchant_id');
            $invoice_id = $this->input->post('invoice_id');
            $invoice = $this->transactions_model->get_invoice_price($merchant_id, $invoice_id);

            if ($invoice == NULL) {
                $response["error"] = true;
                $response["message"] = $invoice_id . " " . $merchant_id . " Invoice not founded.";
                $this->EchoResponse(200, $response);
            } else {

                $userCr = $this->transactions_model->user_balance($this->token_user_id); // Get User Balance
                if ($userCr['credit'] < $invoice->net_total) { // Verify user available balance
                    $response["error"] = true;
                    $response["message"] = 'Insufficient balance. Your balance ' . $userCr['credit'];
                    $this->EchoResponse(200, $response);
                } else {

                    $trx_code = rand(1, 99999);
                    $debit = array('userId' => $this->token_user_id,
                        'refId' => $merchant_id,
                        'dr' => $invoice->net_total,
                        'gps' => $this->input->post('gps'),
                        'devId' => $this->input->post('dev_id'),
                        'type' => 'TR',
                        'mode' => 'MIB', // mode = MIB for Merchant Invoice Bill transaction (Pay Invoice Bill)
                        'transaction_code' => $trx_code,
                        'description' => $this->input->post('description')
                    );
                    $credit = array('userId' => $this->token_user_id,
                        'refId' => $this->token_user_id,
                        'cr' => $invoice->net_total,
                        'gps' => $this->input->post('gps'),
                        'devId' => $this->input->post('dev_id'),
                        'type' => 'TR',
                        'mode' => 'MIB', // mode = MIB for Merchant Invoice Bill transaction (Pay Invoice Bill)
                        'transaction_code' => $trx_code,
                        'description' => $this->input->post('description')
                    );
                    $transaction_data = $this->transactions_model->payInvoiceBillTransaction($this->token_user_id, $debit, $credit, $invoice_id);
                    if ($transaction_data) {
                        $user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance of User

                        $response["error"] = false;
                        $response["transaction_id"] = $transaction_data->transId;
                        $response["user_id"] = $transaction_data->userId;
                        $response["credit"] = strval($user['credit']);
                        $response["debit"] = $transaction_data->dr;
                        $response["gps"] = $transaction_data->gps;
                        $response["dev_id"] = $transaction_data->devId;
                        $response["created"] = $transaction_data->created;
                        $response["message"] = "You have successfully paid the invoice bill.";
                        $user = $this->transactions_model->user_profile_by_id($this->token_user_id); // Get User profile by User ID
                        
						$merchant = $this->transactions_model->user_balance($merchant_id); // Get Updated Balance of Merchant
						$msg = array('notification_type' => "action_invoice",
									 'invoice_id'        => $invoice_id,
									 'sender_name'       => $user->firstName . ' ' . $user->lastName,
									 'action_invoice'    => "accepted",
									 'total_balance'     => $merchant['credit'],
									 'message'           => "Invoice accepted"
								     );

                        $msg = json_encode($msg);
                        $notification_data = array('message' => $msg);
                        sent_notify($notification_data, $merchant_id, "merchant");
                        $this->EchoResponse(200, $response);
                    }
                }
            }
        }
    }

    /*
      // Pending
      public function myTransactionsJournalEntries() // journal entries
      {
      $this->form_validation->set_rules('from_date', 'From Date', 'trim|required');
      $this->form_validation->set_rules('to_date', 'To Date', 'trim|required');

      if ($this->form_validation->run() == FALSE)
      {
      $response["error"] = true;
      $response["message"] = validation_errors();
      $this->EchoResponse(200, $response);
      } else {

      $fromDate = $this->input->post('from_date');
      $toDate = $this->input->post('to_date');
      $user_data = $this->transactions_model->myTransactions($fromDate, $toDate, $this->token_user_id); // journal entries
      if($user_data)
      {
      $user = $this->transactions_model->user_debit_credit_entries($fromDate, $toDate, $this->token_user_id);
      $response["error"]   = false;

      //////////////////////////////////////////////////////
      $response["myTransactions"]  = $user_data;
      ///////////////////////////////////////////////////////
      $response["total_debit"]  = $user['debit'];
      $response["total_credit"]  = $user['credit'];
      $response["message"] = 'My Transactions (Journal entries)';
      } else {
      $response["error"] = true;
      $response["message"] = '602';
      } // else
      $this->EchoResponse(200, $response);
      } // else
      } // myTransactions (User Transactions)
     */

    public function authorized_dot_net($userdata) {
        require_once(APPPATH . 'libraries/authorized_sdk/autoload.php');
        define("AUTHORIZENET_API_LOGIN_ID", "9QrS4zkt5qS");
        define("AUTHORIZENET_TRANSACTION_KEY", "7uD3R65Tcb7C52x6");
        define("AUTHORIZENET_SANDBOX", true);
        $sale = new AuthorizeNetAIM;
        $sale->amount = $userdata['amount'];
        $sale->card_num = $userdata['card_num'];
        $sale->exp_date = $userdata['exp_date'];
        $sale->first_name = $userdata['first_name'];
        $sale->last_name = $userdata['last_name'];
        $sale->phone = $userdata['phone'];
        $sale->address = $userdata['address'];
        $sale->city = $userdata['city'];
        $sale->country = $userdata['country'];
        $response = $sale->authorizeAndCapture();
        if ($response->approved) {
            $data = array('transaction_id' => $response->transaction_id,
                'auth_code' => $response->authorization_code,
                'error' => $response->error
            );
            return $data;
        } else {
            $data = array('error' => $response->error,
                'error_message' => $response->response_reason_text
            );
            return $data;
        }
    }

// authorized_dot_net
    //Test Function
    public function test_balance() {
        $user = $this->transactions_model->user_balance($this->token_user_id);
        print_r($user);
    }

    public function checkSlip() {
        $this->form_validation->set_rules('user_pin', 'User Pin', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            ///////////////////////// Image Upload START //////////////////////
            $config['upload_path'] = UPLOADS_DIR . 'check_deposit/' . $this->token_user_id;
            $config['allowed_types'] = 'jpg|png|jpeg|gif|bmp';
            $config['max_size'] = 0;
            $config['max_width'] = 0;
            $config['max_height'] = 0;
            $config['overwrite'] = FALSE;
            $config['file_name'] = uniqid(mt_rand());
                      
            if (!is_dir($config['upload_path'])) {
                mkdir($config['upload_path'], 0777);
                mkdir($config['upload_path'].'/thumbnails/', 0777);                
            }
            $this->load->library('upload', $config);
            
            if (!$this->upload->do_upload('file_name')) {
                $response["error"] = true;
                $response["message"] = $this->upload->display_errors();
                $this->EchoResponse(200, $response);
            } else {
                $file = $this->upload->data();
                $imageFile = $file['file_name']; // Original Image
                ///////////////////////// Image Thumbnail Start ///////////////////////				
                $image_sizes = array('tmb1_' => array(800, 800)
                );
                $this->load->library('image_lib');
                foreach ($image_sizes as $keys => $resize) :
                    $config = array('image_library' => 'gd2',
                        'source_image' => UPLOADS_DIR . 'check_deposit/' . $this->token_user_id . '/' . $imageFile,
                        'quality' => 100,
                        'thumb_marker' => '',
                        'create_thumb' => TRUE,
                        'maintain_ratio' => TRUE,
                        'rotation_angle' => 'None',
                        'new_image' => UPLOADS_DIR . 'check_deposit/' . $this->token_user_id . '/thumbnails/' . $keys . $file['raw_name'] . $file['file_ext'],
                        'width' => $resize[0],
                        'height' => $resize[1]
                    );
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                endforeach;
                ///////////////////////// Image Thumbnail END ///////////////////////

                $data = array('uid' => $this->token_user_id,
                    'user_pin' => $this->input->post('user_pin'),
                    'image' => $imageFile,
                    'created' => date("Y-m-d H:i:s")
                );
                $status = $this->transactions_model->save_deposit_slip($data);
                if ($status) {
                    $response["error"] = false;
                    $response["message"] = "Deposit Slip uploaded successfully.";
                    $this->EchoResponse(200, $response);
                }
            } // else
            ///////////////////////////// Image Upload END /////////////////////////////
        } // else
    } //checkSlip

	
	public function transactions_out() 
	{
        $this->form_validation->set_rules('amount', "Amount", "trim|required");
		$this->form_validation->set_rules('secret_code', "Secret Code", "trim|required");
		$this->form_validation->set_rules('dev_id', "Device ID", "trim|required");
		$this->form_validation->set_rules('gps', "GPS", "trim");
		$this->form_validation->set_rules('receiver_name', "Receiver Name", "trim|required");
		$this->form_validation->set_rules('receiver_phone', "Receiver Phone", "trim");
		$this->form_validation->set_rules('receiver_email', "Receiver Email", "trim|required");
		$this->form_validation->set_rules('receiver_address', "Receiver Address", "trim");
		$this->form_validation->set_rules('description', "Description", "trim");
        if ($this->form_validation->run() == FALSE) {
            $response["error"]   = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
			$secret_code = $this->input->post('secret_code');
			$debit = array('userId'      => $this->token_user_id,
                           'refId'       => 142,
                           'dr'          => $this->input->post('amount'),
                           'gps'         => $this->input->post('gps'),
                           'devId'       => $this->input->post('dev_id'),
                           'type'        => 'TR',
                           'mode'        => 'TCP',
                           'description' => $this->input->post('description')
                           );
			$credit = array('userId'      => 142,
							'refId'       => $this->token_user_id,
							'cr'          => $this->input->post('amount'),
							'gps'         => $this->input->post('gps'),
							'devId'       => $this->input->post('dev_id'),
							'type'        => 'TR',
							'mode'        => 'TCP',
							'description' => $this->input->post('description')
							);			   
			$transaction_data = $this->transactions_model->transfer_credit($this->token_user_id, $debit, $credit);
			$data = array("fullname"    => $this->input->post('receiver_name'),
			              "email"       => $this->input->post('receiver_email'),
						  "phone"       => $this->input->post('receiver_phone'),
						  "address"     => $this->input->post('receiver_address'),
						  "secret_code" => $this->input->post('secret_code'),
						  "uid"         => $this->token_user_id,
						  "type"        => 2, // 1 for IN and 2 for OUT
						  "amount"      => $this->input->post('amount'),
						  "datetime"    => date('Y-m-d H:i:s')
						 );
			$this->transactions_model->manual_transactions($data);
			$user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance			
			$response["error"]          = false;
			$response["transaction_id"] = $transaction_data->transId;
			$response["user_id"]        = $transaction_data->userId;
			$response["credit"]         = strval($user['credit']);
			$response["debit"]          = $transaction_data->dr;
			$response["gps"]            = $transaction_data->gps;
			$response["dev_id"]         = $transaction_data->devId;
			$response["created"]        = $transaction_data->created;
			$response["message"]        = "You have successfully sent the money.";	
			$this->EchoResponse(200, $response);		
        }
    } // transactions_out
	

    /*
      public function checkSlip()
      {
      if(!empty($_FILES)){
      $image = $_FILES['image']['name'];
      $image_name = $this->transactions_model->get_unique_charity_image_name();
      file_put_contents('./assets/checkslip/'.$image_name, base64_decode($image));
      } else {
      $image_name = 'charity_avatar.png';
      }

      $data = array('userId'       => $this->token_user_id,
      'image'        => $image_name,
      'created_date' => date("Y-m-d H:i:s")
      );
      $query = $this->db->insert('tblcheckslip', $data);
      if($query)
      {
      $response["error"] = false;
      $response["message"] = "Image Upload Successfully";
      $this->EchoResponse(200, $response);
      } else {
      $response["error"] = true;
      $response["message"] = "Some thing went Wrong";
      $this->EchoResponse(200, $response);
      }
      } // checkSlip
     */
}

// transactions
