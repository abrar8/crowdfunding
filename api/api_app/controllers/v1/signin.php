<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Signin extends CI_Controller {
	function __construct() 
	{
    	parent::__construct();
  		$this->load->helper('security');
  		$this->load->library('encrypt');
  		$this->lang->load('en_admin', 'english');   
  		$this->load->library('form_validation');
		$this->load->model('Signin_model');
	}
	

	public function index() 
	{
		$response = array();
    	$this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required|min_length[1]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('device_id', 'Device ID', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('device_type', 'Device Type', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('fb_id', 'Facebook ID', 'trim');
		if ($this->form_validation->run() == FALSE) 
		{ 
			$response["error"] = true;
          	$response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$data = array('userPIN'      => $this->input->post('user_pin'),
			              'userPassword' => $this->input->post('password'),
						  'device_id'    => $this->input->post('device_id'),  // 5654bfe5bcc825fb5eb487d2d56ff02f72503b9d477ffb5755b6fdbbe542a313
						  'device_type'  => $this->input->post('device_type'), // andriod | ios
						  'fb_id'        => $this->input->post('fb_id')
			             );
			$user = $this->Signin_model->does_user_exist($data);
			
	  		if ($user) 
	  		{
				if($user->currency == "")
				{
					$currency = "USD";	
				} else {
					$currency = $user->currency;
				}
				$response["error"]     	  = false;
				$response["user_id"]      = $user->userId;
				$response["first_name"]   = ucfirst(stripslashes($user->firstName));
				$response["last_name"]    = ucfirst(stripslashes($user->lastName));
				$response["email"]    	  = $user->email;
				$response["phone_number"] = $user->phoneNumber;
				$response["address"]      = stripslashes($user->address);
				$response["country"]      = stripslashes($user->country);
				$response["city"]         = stripslashes($user->city);
				$response["currency"]     = $currency;
				$response["pin_code"]     = $user->userPIN;
				$response["api_key"]      = $user->api_key;
				$response["fb_id"]        = $user->fb_id;
				
				// Profile status
				$profile_status = $this->Signin_model->get_profile_status($user->userId);
				$response["profile_status"] = $profile_status;				
			} else {
	  			$response["error"]   = true;
                $response["message"] = 'An error occurred. Invalid Credentials';
      		} // else
			$this->EchoResponse(200, $response);
		} // else 
	} // index
			

	// Start Signin Merchant
	public function merchant() 
	{
		$response = array();
    	$this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required|min_length[1]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		//$this->form_validation->set_rules('device_id', 'Device ID', 'trim|required|min_length[3]');
		//$this->form_validation->set_rules('device_type', 'Device Type', 'trim|required|min_length[3]');
		if ($this->form_validation->run() == FALSE) 
		{ 
			$response["error"] = true;
          	$response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$data = array('userPIN'      => $this->input->post('user_pin'),
			              'userPassword' => $this->input->post('password'),
						  'device_id'    => $this->input->post('device_id'),  // 5654bfe5bcc825fb5eb487d2d56ff02f72503b9d477ffb5755b6fdbbe542a313
						  'device_type'  => $this->input->post('device_type') // andriod | ios
			              );
			$user = $this->Signin_model->does_merchant_exist($data);
			
	  		if ($user) 
	  		{
				if($user->currency == "")
				{
					$currency = "USD";	
				} else {
					$currency = $user->currency;
				}
				$response["error"]     	= false;
				$response["user_id"]    = $user->userId;
				$response["first_name"] = ucfirst(stripslashes($user->firstName));
				$response["last_name"]  = ucfirst(stripslashes($user->lastName));
				$response["email"]    	= $user->email;
				$response["phone_number"]  = $user->phoneNumber;
				$response["address"]   = stripslashes($user->address);
				$response["country"]   = stripslashes($user->country);
				$response["city"]      = stripslashes($user->city);
				$response["currency"]  = $currency;
				$response["pin_code"]  = $user->userPIN;
				$response["api_key"]    = $user->api_key;
				
				// Profile status
				$profile_status = $this->Signin_model->merchant_profile_status($user->userId);
				$response["merchant_profile_status"] = $profile_status;
				
			} else {
	  			$response["error"]   = true;
                $response["message"] = 'An error occurred. Invalid Credentials';
      		} // else
			$this->EchoResponse(200, $response);
		} // else 
	} // merchant
	
	
	public function agent() 
	{
		$response = array();
    	$this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required|min_length[1]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		/*$this->form_validation->set_rules('device_id', 'Device ID', 'trim');
		$this->form_validation->set_rules('device_type', 'Device Type', 'trim');*/
		if ($this->form_validation->run() == FALSE) 
		{ 
			$response["error"] = true;
          	$response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$data = array('userPIN'      => $this->input->post('user_pin'),
			              'userPassword' => $this->input->post('password'),
						  'device_id'    => $this->input->post('device_id'),  // 5654bfe5bcc825fb5eb487d2d56ff02f72503b9d477ffb5755b6fdbbe542a313
						  'device_type'  => $this->input->post('device_type') // andriod | ios
			              );
			$user = $this->Signin_model->does_agent_exist($data);			
	  		if ($user) 
	  		{
				if($user->currency == "")
				{
					$currency = "USD";	
				} else {
					$currency = $user->currency;
				}
				$response["error"]     	  = false;
				$response["user_id"]      = $user->userId;
				$response["first_name"]   = ucfirst(stripslashes($user->firstName));
				$response["last_name"]    = ucfirst(stripslashes($user->lastName));
				$response["email"]    	  = $user->email;
				$response["phone_number"] = $user->phoneNumber;
				$response["address"]      = stripslashes($user->address);
				$response["country"]      = stripslashes($user->country);
				$response["city"]         = stripslashes($user->city);
				$response["currency"]     = $currency;
				$response["pin_code"]     = $user->userPIN;
				$response["api_key"]      = $user->api_key;
				$profile_status = $this->Signin_model->agent_profile_status($user->userId);
				$response["merchant_profile_status"] = $profile_status;
			} else {
	  			$response["error"]   = true;
                $response["message"] = 'An error occurred. Invalid Credentials';
      		} // else
			$this->EchoResponse(200, $response);
		} // else 
	} // agent
	
	

	public function signout() 
	{
    	$this->session->sess_destroy();
    	redirect('signin');
  	} // signout
	
	 
   	private function EchoResponse($status_code, $response)
  	{
		$this->output->set_status_header($status_code);
		$this->output->set_content_type('application/json')
    				 ->set_output(json_encode($response));  
	} // EchoResponse
	
	
	public function account_forgot_password()
	{	
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		if($this->form_validation->run() == FALSE)
		{ 
			$response["error"] = true;
          	$response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
		} else {
			$email = $this->input->post('email');
			$this->load->model('Register_model');
			$email_status = $this->Register_model->email_already_exists($email);
			if($email_status == FALSE)
			{
				$response["error"] = false;
          		$response["message"] = 'Email Address not matched.';
           	 	$this->EchoResponse(200, $response);
			} else {
				//$this->load->helper('string');								
				//$password = random_string('numeric', 4);; // Generate New Password
				$activation = mt_rand(); // Activation Code		
				$user = $this->Signin_model->get_merchant_info($email);				
				$this->Signin_model->add_activation_code($user->userId,$activation);	
				
				//$user = $this->Signin_model->update_user_password($email, $password);
				$this->load->library('email');			
				$this->email->from(SITE_EMAIL); 
				$this->email->to($email); 
				$this->email->subject("Forgot Password | ".SITE_NAME);
				$message = "<p>Please click the link below to change your password</p>
				            <p><a href=".ADMIN_URL."signin/merchant_forgot_pass_activation/".$user->userId."/".$activation." target=_blank>".ADMIN_URL."signin/merchant_forgot_pass_activation/".$user->userId."/".$activation."</a></p>"; 				
				/*$message = '<p>Here is your account credentials given below,</p>
				            <p>UserPin: '.$user->userPIN.'</p>
							<p>Password: '.$password.'</p>';*/ 
				$this->email->message($message);
				if($this->email->send())
				{ 
					$response["error"] = false;
          			$response["message"] = 'Password reset activation link send to your email address.';
					$this->EchoResponse(200, $response);
				} else {
					$response["error"] = true;
          			$response["message"] = 'Sorry Unable to send you email.';
					$this->EchoResponse(200, $response);
				}
			} // END ELSE
		}
	} // account_forgot_password
	
	

	public function merchant_account_forgot_password()
	{	
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
		if($this->form_validation->run() == FALSE)
		{ 
			$response["error"] = true;
          	$response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
		} else {
			$email = $this->input->post('email');
			$this->load->model('Register_model');
			$email_status = $this->Register_model->merchant_email_already_exists($email);
			if($email_status == FALSE)
			{
				$response["error"] = false;
          		$response["message"] = 'Email Address not matched.';
           	 	$this->EchoResponse(200, $response);
			} else {
				//$this->load->helper('string');								
				//$password = random_string('numeric', 4);; // Generate New Password
				$activation = mt_rand(); // Activation Code		
				$user = $this->Signin_model->get_merchant_info($email);		
				$this->Signin_model->add_merchant_activation_code($user->userId,$activation);	
				
				//$user = $this->Signin_model->update_user_password($email, $password);
				$this->load->library('email');			
				$this->email->from(SITE_EMAIL); 
				$this->email->to($email); 
				$this->email->subject("Forgot Password | ".SITE_NAME);
				$message = "<p>Please click the link below to change your password</p>
				            <p><a href=".ADMIN_URL."signin/merchant_forgot_pass_activation/".$user->userId."/".$activation." target=_blank>".ADMIN_URL."signin/merchant_forgot_pass_activation/".$user->userId."/".$activation."</a></p>"; 				
				/*$message = '<p>Here is your account credentials given below,</p>
				            <p>UserPin: '.$user->userPIN.'</p>
							<p>Password: '.$password.'</p>';*/ 
				$this->email->message($message);
				if($this->email->send())
				{ 
					$response["error"] = false;
          			$response["message"] = 'Password reset activation link send to your email address.';
					$this->EchoResponse(200, $response);
				} else {
					$response["error"] = true;
          			$response["message"] = 'Sorry Unable to send you email.';
					$this->EchoResponse(200, $response);
				}
			} // END ELSE
		}
	} // merchant_account_forgot_password


	 
}
