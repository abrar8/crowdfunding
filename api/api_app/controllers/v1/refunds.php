<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refunds extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('refunds_model');
        $this->load->helper('home');
    }

    public function user_refunds() {
        $refunds = $this->refunds_model->get_user_refunds($this->token_user_id);
        $response['error'] = false;
        if ($refunds == NULL) {
            $response["refunds"] = array();
        } else {
            $data = array();
            foreach ($refunds as $refund) :
                $data[] = array("refund_id" => $refund->id,
                    "invoice_no" => $refund->invoice_no,
                    "message" => stripslashes($refund->message),
                    "datetime" => $refund->datetime
                );
            endforeach;
            $response["refunds"] = $data;
        }
        $this->EchoResponse(200, $response);
    }

// user_refunds

    public function merchant_refunds() {
        $refunds = $this->refunds_model->get_merchant_refunds($this->token_merchant_id);
        $response['error'] = false;
        if ($refunds == NULL) {
            $response["refunds"] = array();
        } else {
            $data = array();
            foreach ($refunds as $refund) :
                $data[] = array("refund_id" => $refund->id,
                    "invoice_no" => $refund->invoice_no,
                    "message" => stripslashes($refund->message),
                    "datetime" => $refund->datetime
                );
            endforeach;
            $response["refunds"] = $data;
        }
        $this->EchoResponse(200, $response);
    }

    public function merchant_refunds_list() {

        $this->form_validation->set_rules('refund_status', 'Status', 'required|numeric');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {

            $refunds = $this->refunds_model->get_merchant_refunds_list($this->token_merchant_id, $this->input->post('refund_status'));
            $response['error'] = false;
            if ($refunds == NULL) {
                $response["refunds"] = array();
            } else {
                $data = array();
                foreach ($refunds as $refund) :
                    $data[] = array("refund_id" => $refund->id,
                        "invoice_no" => $refund->invoice_no,
                        "message" => stripslashes($refund->message),
                        "datetime" => $refund->datetime
                    );
                endforeach;
                $response["refunds"] = $data;
            }
            $this->EchoResponse(200, $response);
        }
    }

// merchant_refunds

    public function create_refund() {
        $this->form_validation->set_rules('user_text', 'User Text', 'required');
        $this->form_validation->set_rules('user_invoice', 'User Invoice', 'required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $invoice_no = $this->input->post("user_invoice");
            $message = $this->input->post("user_text");

            $this->load->model('refunds_model');
            $data = array('invoice_no' => $invoice_no,
                'uid' => $this->token_user_id,
                'merchant_id' => get_Merchant_id($this->input->post("user_invoice")),
                'message' => addslashes($message),
                'datetime' => date("Y-m-d H:i:s")
            );
            $refund_id = $this->refunds_model->create_refund($data);

            $this->db->select('*');
            $this->db->where('admin_id', 1);
            $record = $this->db->get('tblAdmins')->result_array();
            $to = $record[0]['admin_email'];

            $auth_key = $this->input->get_request_header('Authorization', TRUE);

            $this->db->select('*');
            $this->db->where('api_key', $auth_key);
            $user_record = $this->db->get('tblUsers')->result_array();
            $from = $user_record[0]['email'];

            $this->db->select('*');
            $this->db->from('tblMerchantInvoices');
            $this->db->join('tblMerchantUsers', 'tblMerchantUsers.userId= tblMerchantInvoices.merchantId');
            $this->db->where('tblMerchantInvoices.id', $invoice_no);
            $merchant = $this->db->get()->result_array();

            if ($to != "" AND $from != "" AND $merchant != "") {
                $variable = '<div style="margin:auto;">
							 <table border="0" width="980" cellspacing="0" cellpadding="0"> 
							 <tr> 
							 	<td width="50%"><b>Invoice Number: </b>' . $invoice_no . '</td>
							 	<td width="50%"></td>
							 </tr>
							 <tr>
							 	<td width="50%"><b>Message: </b></td>
							 	<td width="50%"></td>
							 </tr>
							 <tr>
							 	<td width="100%">' . $message . '</td>
							 </tr>					
						     </table>
						     </div>';
                $this->load->library('email');
                $this->email->from($from, 'Complain To Admin');
                $this->email->to($to);
                $this->email->bcc($merchant[0]['email']);
                $this->email->subject('Complain To Admin');
                $this->email->message($variable);
                if ($this->email->send()) {
                    $response["error"] = false;
                    $response["refund_id"] = $refund_id;
                    $response["invoice_no"] = $invoice_no;
                    $response["message"] = 'Email sent';
                    $this->EchoResponse(200, $response);
                } else {
                    $response["error"] = true;
                    $response["refund_id"] = $refund_id;
                    $response["invoice_no"] = $invoice_no;
                    $response["message"] = 'Not sent.';
                    $this->EchoResponse(200, $response);
                }
            } else {
                $response["error"] = true;
                $response["message"] = 'Please Provide Correct Info';
                $this->EchoResponse(200, $response);
            }
        } // else
    }

// create_refund

    public function refund_detail() {
        $this->form_validation->set_rules('refund_id', 'Refund ID', 'required');
        $this->form_validation->set_rules('is_merchant', 'IS Merchant', 'required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $invoice_no = $this->input->post("invoice_no");
            $refund_id = $this->input->post("refund_id");
            $is_merchant = $this->input->post("is_merchant");
            $refund = $this->refunds_model->get_refund($refund_id);
            $invoice = $this->refunds_model->get_invoice_detail($refund->invoice_no);
            if ($invoice == NULL) {
                $response['error'] = true;
                $response["message"] = "Invalid Invoice ID";
            } else {
                $response['error'] = false;
                $response["refund"] = array("refund_id" => $refund->id,
                    "message" => stripslashes($refund->message),
                    "datetime" => date("Y-m-d g:i A", strtotime($refund->datetime))
                );
                $response["invoice"] = array("invoice_no" => $invoice->id,
                    "details" => $invoice->details,
                    "datetime" => date("Y-m-d g:i A", strtotime($invoice->dated)),
                    "net_total" => $invoice->net_total,
                    "status" => $invoice->status
                );
                if ($is_merchant == 1) {
                    $user = get_merchantInfo($this->token_merchant_id);
                    $data = array("id" => $user->userId,
                        "first_name" => stripslashes($user->firstName),
                        "last_name" => stripslashes($user->lastName),
                        "merchant_title" => stripslashes($user->merchant_title)
                    );
                    $response["user"] = $data;
                } else {
                    $user = get_userInfo($this->token_user_id);
                    $data = array("id" => $user->id,
                        "first_name" => $user->firstname,
                        "last_name" => $user->lastname
                    );
                    $response["user"] = $data;
                }
            }
            $this->EchoResponse(200, $response);
        } // else
    }

// refund_detail
// Response to Refund    
    public function responseToRefund() {
        $this->form_validation->set_rules('id', 'Refund Request Id', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            

            $refundRequestDetail = $this->refunds_model->responseToRefund($this->token_merchant_id);

            /*
             * <pre>stdClass Object
              (
              [id] => 16
              [invoice_no] => 311
              [uid] => 141
              [merchant_id] => 16
              [message] => kj
              [datetime] => 2016-05-10 12:44:20
              [status] => 2
              [merchant_title] => Shopping Mall
              )
             */
            $data = array(
                'merchant_id'=>$refundRequestDetail->merchant_id,
                'uid'=>$refundRequestDetail->uid,
                'invoice_no'=>$refundRequestDetail->invoice_no,
                'amount'=>$this->refunds_model->getInvoiceAmount($refundRequestDetail->invoice_no,$this->token_merchant_id)
            );
            $availableBalance = $this->getAvailableBalance();
            
            $this->refundProcess($data);
            $receiverId = $refundRequestDetail->uid;
            $status = $refundRequestDetail->status;

            if ($status) {
                $this->load->helper('notification');
                $msg = '';
                $response["error"] = false;
                $response["total"] = $availableBalance['total'];
                $response["credit_sum"] = $availableBalance['credit_sum'];
                $response["currency"] = $availableBalance['currency'];
                $response["action_status"] = $status;
                if ($this->input->post('status') == 1) {
                    $msg = array('notification_type' => "action_refund_request",
                        'action_request' => 'Pending',
                        'invoice_no' => $refundRequestDetail->invoice_no,
                        'sender_name' => $refundRequestDetail->merchant_title,
                        'message' => $refundRequestDetail->merchant_title . ' Refund Request is Pending ' . $refundRequestDetail->invoice_no
                    );
                    $response["message"] = 'Refund Request is Pending.';
                } elseif ($this->input->post('status') == 2) {
                    $msg = array('notification_type' => "action_refund_request",
								 'action_request'    => 'accepted',
								 'invoice_no'        => $refundRequestDetail->invoice_no,
								 'sender_name'       => $refundRequestDetail->merchant_title,
								 'total_balance'     => $user['credit'],
								 'message'           => $refundRequestDetail->merchant_title. ' Refund Request is Accepted '.$refundRequestDetail->invoice_no
							     );
                    $response["message"] = 'Refund Request is Accepted.';
                } else {
					$user = $this->transactions_model->user_balance($this->token_user_id); // Get Updated Balance of User
                    $msg = array('notification_type' => "action_refund_request",
								'action_request'     => 'rejected',
								'invoice_no'         => $refundRequestDetail->invoice_no,
								'sender_name'        => $refundRequestDetail->merchant_title,
								'total_balance'      => $user['credit'],
								'message'            => $refundRequestDetail->merchant_title.' Refund Request is Rejected '.$refundRequestDetail->invoice_no
							    );
                    $response["message"] = 'Refund Request is Rejected.';
                }
                $notification_data = array('message' => json_encode($msg));
                sent_notify($notification_data, $receiverId);
                $this->EchoResponse(200, $response);
            } else { // Un-Subscribed
                $response["error"] = true;
                $response["message"] = 'Doesnt proceeed contact to admin.';
                $this->EchoResponse(200, $response);
            }
        }
    }


    public function refundProcess($data) 
	{    
        $invoice_id = $data['invoice_no'];
        $trx_code = rand(1, 99999);
        $debit = array('userId'            => $data['uid'],
						'refId'            => $data['uid'],
						'dr'               => $data['amount'],
						'gps'              => 'gps',
						'devId'            => 'dev_id',
						'type'             => 'TR',
						'mode'             => 'MIB', // mode = MIB for Merchant Invoice Bill transaction (Pay Invoice Bill)
						'transaction_code' => $trx_code,
						'description'      => 'Merchant Refunding Invoice paid credit'
					);
        $credit = array('userId'           => $data['uid'],
						'refId'            => $data['merchant_id'],
						'cr'               => $data['amount'],
						'gps'              => 'gps',
						'devId'            => 'dev_id',
						'type'             => 'TR',
						'mode'             => 'MIB', // mode = MIB for Merchant Invoice Bill transaction (Pay Invoice Bill)
						'transaction_code' => $trx_code,
						'description'      => 'Merchant Refunding Invoice paid credit'
					);
        $transaction_data = $this->refunds_model->refundInvoiceBillTransaction($data['uid'], $debit, $credit, $invoice_id);
    }
    
    
	public function getAvailableBalance()
	{
        $this->load->model('merchant_model');
        $merchant_id = $this->token_merchant_id;
        
        $data = $this->merchant_model->get_account_balance($merchant_id);
        $currency_code = $this->merchant_model->my_currency_code($merchant_id);
   
        $result = array('total'      => $data->total,
                        'credit_sum' => $data->credit_sum,
                        'currency'   => $currency_code
                        );
        return $result;
    }

}

// Refunds
