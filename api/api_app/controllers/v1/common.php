<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MY_Controller {
	function __construct() 
	{
    	parent::__construct();		
  		$this->load->library('form_validation');
		$this->load->model('common_model');
		$this->load->helper('home');
	}
	
	public function getNameByPin() 
	{
		$this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required');
		if ($this->form_validation->run() == FALSE) 
		{
			$response["error"]   = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else { 
			$userPin = $this->input->post('user_pin');
			$userName = $this->common_model->getNameByPin($userPin);
			if($userName == FALSE){
				$response["error"]   = true;
            	$response["message"] = 'Invalid Pin Number.';
            	$this->EchoResponse(200, $response); 
			} else {
				$fullName = $userName->firstName.' '.$userName->lastName;
				$response["error"]   = false;
            	$response["full_name"] = $fullName;
            	$this->EchoResponse(200, $response); 
			} // else
		} // else
	}
	
	public function basicInfo(){
		$basicInfo = $this->common_model->getBasicInfo($this->token_user_id);
		if($basicInfo == FALSE){
			$response["error"]   = true;
			$response["message"] = 'Something went wrong.';
			$this->EchoResponse(200, $response); 
		} else {
			
			$user = $this->common_model->user_balance($this->token_user_id);
			$symbol = $this->common_model->my_currency_symbol($this->token_user_id);  
			$response["error"]   = false;
			$response["first_name"] = $basicInfo->firstName;
			$response["last_name"] = $basicInfo->lastName;
			$response["currency"] = $basicInfo->currency;
			$response["credit"] = $symbol.strval($user['credit']);
			$this->EchoResponse(200, $response); 
		} // else
	} 
	public function available_currencies(){
		
		$data = $this->common_model->get_all_available_currencies();
		if($data == NULL){
			$response["error"]   = true;
			$response["message"] = 'No records founded.';
			$this->EchoResponse(200, $response); 
		} else {
			$response["error"]   = false;
			$response["data"]    = $data;
			$response["message"] = 'All available currency types.';
			$this->EchoResponse(200, $response); 
		}
	}

	
} // Common (CI_Controller)
