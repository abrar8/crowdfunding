<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Merchant extends MY_Controller {
    function __construct() {
        parent::__construct();

//        $this->load->helper('notification_helper');
        $this->load->helper('notification');
        $this->load->library('form_validation');
        $this->load->model('merchant_model');
    }

    // Save Invoice
    public function save_invoice() {
        $this->form_validation->set_rules('merchant_id', 'Merchant ID', 'trim|required');
        $this->form_validation->set_rules('details', 'Details', 'trim|required');
        $this->form_validation->set_rules('net_total', 'Net Total', 'trim|required');
        $this->form_validation->set_rules('imali_account_user_pin', 'Net Total', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {

            // Implode $this->input->post('details') and save in variable in this form...
            // $details = [=,=,=,=[=,=,=,=[=,=,=,=[=,=,=,=
            $merchant_id = $this->input->post('merchant_id');
            $details = $this->input->post('details');

            $data = array(
                'merchantId' => $this->input->post('merchant_id'),
                'details' => $details,
                'net_total' => $this->input->post('net_total'),
                'user_pin' => $this->input->post('imali_account_user_pin'),
                'status' => 1 // (1) = 'Pending' (2) = Approve or Accept, (3) = Reject
            );
//          $status = true;
            $invId = $this->merchant_model->save_invoice($data);

            if ($invId == FALSE) {
                $response["error"] = true;
                $response["message"] = "Oops! Something went wrong.";
                $this->EchoResponse(200, $response);
            } else {

//              Send push Notification to Android and IOS devices.
                $item_detail = json_decode($details, true);

                $msg = array(
                    'notification_type' => "invoice",
                    'invoice_id' => $invId,
                    'merchant_id' => $merchant_id,
                    'detail' => $item_detail,
                    'message' => "Your invoice generated successfully"
                );

                $msg = json_encode($msg);
                $notification_data = array(
                    'message' => $msg
                );
//              $notification_data = json_encode($notification_data);

                $this->load->model('transactions_model');
                $user_profile = $this->transactions_model->user_profile_by_pin($this->input->post('imali_account_user_pin'));

                sent_notify($notification_data, $user_profile->userId);

                $response["error"] = false;
                $response["message"] = "Invoice saved successfully..";
                $this->EchoResponse(200, $response);
            }
        }
    }

    // List all invoices by Status
    public function get_invoices() {
        $this->form_validation->set_rules('status', 'Status', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {
            $merchant_id = $this->token_merchant_id;
            $status = $this->input->post('status');
            $invoices = $this->merchant_model->get_invoices($merchant_id, $status);
            if ($invoices == NULL) {
                $response["error"] = false;
                $response["data"] = array();
                $response["message"] = "No records founded.";
                $this->EchoResponse(200, $response);
            } else {
                $response["error"] = false;
                $response["data"] = $invoices;
                $response["message"] = "List Invoices.";
                $this->EchoResponse(200, $response);
            }
        }
    }

//    // Invoice Details By ID
//    public function invoice_details() {
//        $this->form_validation->set_rules('invoice_id', 'Invoice ID', 'trim|required');
//        if ($this->form_validation->run() == FALSE) {
//            $response["error"] = true;
//            $response["message"] = validation_errors();
//            $this->EchoResponse(200, $response);
//        } else {
//            $invoice_id = $this->input->post('invoice_id');
//            $invoice_data = $this->merchant_model->get_invoice_details($invoice_id);
//            if ($invoice_data == NULL) {
//                $response["error"] = true;
//                $response["message"] = "No record founded.";
//                $this->EchoResponse(200, $response);
//            } else {
//                $response["error"] = false;
//                $response["data"] = $invoice_data;
//                $response["message"] = "Invoice details.";
//                $this->EchoResponse(200, $response);
//            }
//        }
//    }

    public function account_balance() 
	{
		$data = $this->merchant_model->get_account_balance($this->token_merchant_id);
		$currency_code = $this->merchant_model->my_currency_code($this->token_merchant_id);
		if ($data == false) {
//          $symbol = $this->merchant_model->my_currency_symbol($merchant_id);
			$response['error'] = false;
			$response['total'] = '0.00';
			$response['credit_sum'] = '0.00';
			$response['currency_code'] = $currency_code;
			$response['message'] = "Your balance.";
			$this->EchoResponse(200, $response);
//          $response['error'] = false;
//          $response['message'] = "Your balance is 0.00.";
//          $this->EchoResponse(200, $response);
		} else {
//          $symbol = $this->merchant_model->my_currency_symbol($merchant_id);
			$response['error'] = false;
			$response['total'] = $data->total;
			$response['credit_sum'] = $data->credit_sum;
			$response['currency_code'] = $currency_code;
			$response['message'] = "Your balance.";
			$this->EchoResponse(200, $response);
		}
    } // account_balance


    /*
      public function request_cashout(){

      $this->form_validation->set_rules('merchant_id', 'Merchant ID', 'trim|required');
      $this->form_validation->set_rules('cashout_amount', 'Cash Out Amount', 'trim|required');
      $this->form_validation->set_rules('bank_account_id','Bank Account ID','trim|required');
      $this->form_validation->set_rules('gps', 'GPS', 'trim');
      $this->form_validation->set_rules('dev_id', 'Device ID', 'trim');
      $this->form_validation->set_rules('description', 'Description', 'trim');

      if($this->form_validation->run() == FALSE)
      {
      $response["error"] = true;
      $response["message"] = validation_errors();
      $this->EchoResponse(200, $response);
      } else {

      $cashout_amount = $this->input->post('cashout_amount');
      $bank_account_id = $this->input->post('bank_account_id');
      $userId = $this->input->post('merchant_id');

      // Entry for cash in hand in cashInHand table
      // status = 0 for pending state of request ->
      // current state cash in hand (status = 0)
      $cash_in_hand_insert_data = array('userId' => $userId, 'amount' => $cashout_amount, 'status' => 0);
      $cashout_id = $this->cashout_model->insert_cashout($cash_in_hand_insert_data);
      $accounts_payable = $this->cashout_model->get_accounts_payable_id();
      if($accounts_payable == false){
      $accounts_payable = 1;
      }
      if($cashout_id != false){
      // Make 1st Transaction for cashout
      // <Debit = UserId> & <Credit = Accounts/Payable>
      $transaction_track_code = rand(1, 99999);
      $debit = array('userId'       => $userId,
      'refId'        => $userId, // Debit user_id as ref,
      'dr'           => $cashout_amount,
      'gps'          => $this->input->post('gps'),
      'devId'        => $this->input->post('dev_id'),
      'type'         => 'TR',
      'mode'		  => 'CO', // mode = CO for Cash Out Request (Pending) transaction
      'transaction_code' => $transaction_track_code,
      'description'  => $this->input->post('description')
      );
      $credit = array('userId'      => $userId,
      'refId'       => $accounts_payable,
      'cr'          => $cashout_amount,
      'gps'         => $this->input->post('gps'),
      'devId'       => $this->input->post('dev_id'),
      'type'        => 'TR',
      'mode'		  => 'CO', // mode = CO for Cash Out Request (Pending) transaction
      'transaction_code' => $transaction_track_code,
      'description' => $this->input->post('description')
      );
      // Cash out request transaction
      $transaction = $this->cashout_model->cashout_transaction($userId, $debit, $credit);
      if($transaction != false)
      {
      $this->load->model('common_model');
      $user = $this->common_model->user_balance($merchant_id);

      $symbol = $this->transactions_model->my_currency_symbol($merchant_id);

      $response["error"]   = false;
      $response["transaction_id"] = $transaction->transId;
      $response["user_id"]  = $transaction->userId;
      $response["credit"]  = $symbol.' '.strval($user['credit']);
      $response["created"] = $transaction->created;
      $response["message"] = 'Your cash out request is received and will be processed in 3 working days.';
      $this->EchoResponse(200, $response);
      } else {
      $response["error"] = true;
      $response["message"] = '602';
      $this->EchoResponse(200, $response);
      }
      }
      }
      }
     */

    protected function EchoResponse($status_code, $response) {
        $this->output->set_status_header($status_code);
        $this->output->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

}

// Common (CI_Controller)
