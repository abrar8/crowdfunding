<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class signup extends CI_Controller {
	function __construct() 
	{
  		parent::__construct();
  		$this->load->helper('security');
  		$this->lang->load('en_admin', 'english');   
  		$this->load->library('form_validation');
  		$this->load->library('email');
		$this->load->library('encrypt');  		
		$this->load->model('Register_model');       
  	}


	public function index() 
	{
		$response = array();
	  	$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[1]|max_length[50]');
    	$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]|max_length[100]|valid_email');
		if ($this->form_validation->run() == FALSE) 
		{
			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else { 
			$email_status = $this->Register_model->email_already_exists($this->input->post('email'));
			if($email_status)
			{
				$response["error"] = true;
				$response["message"] = "An account is already associated with this email address.";
				$this->EchoResponse(200, $response);	
			} else {
				$user_pin_code = $this->gen_pin_code(); 
				$hash_user_pin = $this->encrypt->sha1($user_pin_code);
				$api_key       = $this->GenerateApiKey();
				$user_pin      = $this->gen_pin();
				$data = array('firstName'    => $this->input->post('first_name'), 
							  'lastName'     => $this->input->post('last_name'), 
							  'phoneNumber'  => $this->input->post('phone_number'), 
							  'email'        => $this->input->post('email'),
							  'api_key'      => $api_key,
							  'userPIN'      => $user_pin,
							  'available'    => 1,
							  'userPassword' => $hash_user_pin 
							 ); 
				$user = $this->Register_model->register_user($data);
				if($user)
				{
					$response["error"]  = false;
					$data['firstName']  = $user->firstName;
					$data['userPIN']    = $user->userPIN;
					$data['pin_code']   = $user_pin_code;
					/*$this->email->from(SITE_EMAIL, SITE_NAME);
					$this->email->to($user->email);
					$this->email->subject('Imali Signup');
					$this->email->message($this->load->view('email_templates/signup',$data,TRUE));	
					$this->email->send();*/					
					$response["user_id"]    = $user->userId;
					$response["first_name"] = $user->firstName;
					$response["last_name"]  = $user->lastName;
					$response["email"]     = $user->email;
					$response["phone_number"]  = $user->phoneNumber;
					$response["pin_code"]  = $user->userPIN;
					$response["api_key"]    = $user->api_key;
					$response["message"]   = 'You are successfully registered';
				} else {
					$response["error"]   = true;
					$response["message"] = 'Unable to registered. Please try again';                  
				} // else
				$this->EchoResponse(200, $response);
			} // else			
		} // else
	} // index
  
	
	// Merchant Signup Start
	public function merchant() 
	{
		$response = array();
	  	$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[1]|max_length[50]');
    	$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]|max_length[100]|valid_email');
		if ($this->form_validation->run() == FALSE) 
		{
			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else { 
    		$email_status = $this->Register_model->merchant_email_already_exists($this->input->post('email'));
			if($email_status)
			{
				$response["error"] = true;
				$response["message"] = "An account is already associated with this email address.";
				$this->EchoResponse(200, $response);	
			} else {
	      		$user_pin_code = $this->gen_pin_code_for_merchant(); 
		  		$hash_user_pin = $this->encrypt->sha1($user_pin_code);
		  		$api_key       = $this->GenerateApiKeyForMerchant();
		  		$user_pin      = $this->gen_pin_for_merchant();
	       		$data = array('firstName'    => $this->input->post('first_name'), 
							  'lastName'     => $this->input->post('last_name'), 
							  'phoneNumber'  => $this->input->post('phone_number'), 
							  'email'        => $this->input->post('email'),
							  'api_key'      => $api_key,
							  'userPIN'      => $user_pin,
							  'available'    => 1,
							  'userPassword' => $hash_user_pin 
						     ); 
				$user = $this->Register_model->register_merchant($data);
	      		if($user)
				{
					$response["error"]  = false;
					$data['firstName']  = $user->firstName;
					$data['userPIN']    = $user->userPIN;
					$data['pin_code']   = $user_pin_code;				
					
					$this->email->from(SITE_EMAIL, SITE_NAME);
					$this->email->to($user->email);
					$this->email->subject('Imali Signup (Merchant)');
					$this->email->message($this->load->view('email_templates/signup',$data,TRUE));	
					$this->email->send();
					
					$response["user_id"]    = $user->userId;
					$response["first_name"] = $user->firstName;
					$response["last_name"]  = $user->lastName;
					$response["email"]     = $user->email;
					$response["phone_number"]  = $user->phoneNumber;
					$response["pin_code"]  = $user->userPIN;
					$response["api_key"]    = $user->api_key;
					$response["message"]   = 'You are successfully registered';
				} else {
					$response["error"]   = true;
	                $response["message"] = 'Unable to registered. Please try again';                  
	      		} // else
				$this->EchoResponse(200, $response);
			} // else
		} // else
	}
	// Merchant Signup End
  
	private function EchoResponse($status_code, $response)
  	{
		$this->output->set_status_header($status_code);
		$this->output->set_content_type('application/json')
    				 ->set_output(json_encode($response));  
	} // EchoResponse
	
	private function GenerateApiKey() 
	{
		do {
      		$private_key = random_string('unique'); 
			$this->db->where('api_key', $private_key);
      		$this->db->from('tblUsers');
      		$num = $this->db->count_all_results();
    	} while ($num >= 1);
	    return $private_key;
    } // GenerateApiKey
	
	private function GenerateApiKeyForMerchant() 
	{
		do {
      		$private_key = random_string('unique'); 
			$this->db->where('api_key', $private_key);
      		$this->db->from('tblMerchantUsers');
      		$num = $this->db->count_all_results();
    	} while ($num >= 1);
	    return $private_key;
    } // GenerateApiKeyForMerchant
	
	private function gen_pin() 
	{
    	do {
      		$pin_no = random_string('nozero', 10); 
			$this->db->where('userPIN', $pin_no);
      		$this->db->from('tblUsers');
      		$num = $this->db->count_all_results();
    	} while ($num >= 1);
	    return $pin_no;
	} // gen_pin
	
	private function gen_pin_for_merchant() 
	{
    	do {
      		$pin_no = random_string('nozero', 10); 
			$this->db->where('userPIN', $pin_no);
      		$this->db->from('tblMerchantUsers');
      		$num = $this->db->count_all_results();
    	} while ($num >= 1);
	    return $pin_no;
	} // gen_pin_for_merchant
	
	private function gen_pin_code()
	{
    	do {
    		$pin_no = random_string('nozero', 4); 
	  		$pin_hash = $this->encrypt->sha1($pin_no); 
			$this->db->where('userPassword', $pin_hash);
      		$this->db->from('tblUsers');
      		$num = $this->db->count_all_results();
    	} while ($num >= 1);
		return $pin_no;
	} // gen_pin_code
	
	private function gen_pin_code_for_merchant()
	{
    	do {
    		$pin_no = random_string('nozero', 4); 
	  		$pin_hash = $this->encrypt->sha1($pin_no); 
			$this->db->where('userPassword', $pin_hash);
      		$this->db->from('tblMerchantUsers');
      		$num = $this->db->count_all_results();
    	} while ($num >= 1);
		return $pin_no;
	} // gen_pin_code
	
	
	public function validate_login() 
	{
		$response = array();
    	$this->form_validation->set_rules('user_pin', 'User PIN', 'trim|required|min_length[1]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('device_id', 'Device ID', 'trim|required|min_length[3]');
		$this->form_validation->set_rules('device_type', 'Device Type', 'trim|required|min_length[3]');
		if ($this->form_validation->run() == FALSE) 
		{ 
			$response["error"] = true;
          	$response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$data = array('userPIN'      => $this->input->post('user_pin'),
			              'userPassword' => $this->input->post('password'),
						  'device_id'    => $this->input->post('device_id'),  // 5654bfe5bcc825fb5eb487d2d56ff02f72503b9d477ffb5755b6fdbbe542a313
						  'device_type'  => $this->input->post('device_type') // andriod | ios
						 );
			$this->load->model('Signin_model'); 			 
			$user = $this->Signin_model->does_user_exist($data);
			if($user)
	  		{
				$response["error"] = false;
				$response["status"] = true;
				$response["message"] = 'Login Successfully.';
			} else {
	  			$response["error"] = true;
				$response["status"] = false;
				$response["message"] = 'Invalid UserPin or Password.';
      		} // else
			$this->EchoResponse(200, $response);
		} // else 
	} // validate_login
	
	
	// Agent Users Signup Start
	public function agent_users_signup() 
	{
		$response = array();
	  	$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[1]|max_length[50]');
    	$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|required|min_length[1]|max_length[50]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|min_length[5]|max_length[100]|valid_email|is_unique[tblUsers.email]');
		if ($this->form_validation->run() == FALSE) 
		{
			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else { 
      		$user_pin_code = $this->gen_pin_code(); 
	  		$hash_user_pin = $this->encrypt->sha1($user_pin_code);
	  		$api_key       = $this->GenerateApiKey();
	  		$user_pin      = $this->gen_pin();
       		$data = array('firstName'    => $this->input->post('first_name'), 
						  'lastName'     => $this->input->post('last_name'), 
						  'phoneNumber'  => $this->input->post('phone_number'), 
						  'email'        => $this->input->post('email'),
						  'api_key'      => $api_key,
						  'userPIN'      => $user_pin,
						  'available'    => 1,
						  'userPassword' => $hash_user_pin,
						  'agent_id'     => 0
					     ); 
			$user = $this->Register_model->register_user($data);
      		if($user)
			{
				$response["error"]  = false;
				$data['firstName']  = $user->firstName;
				$data['userPIN']    = $user->userPIN;
				$data['pin_code']   = $user_pin_code;				
				
				$this->email->from(SITE_EMAIL, SITE_NAME);
				$this->email->to($user->email);
				$this->email->subject('Imali Signup');
				$this->email->message($this->load->view('email_templates/signup',$data,TRUE));	
				$this->email->send();
				
				$response["user_id"]    = $user->userId;
				$response["first_name"] = $user->firstName;
				$response["last_name"]  = $user->lastName;
				$response["email"]     = $user->email;
				$response["phone_number"]  = $user->phoneNumber;
				$response["pin_code"]  = $user->userPIN;
				$response["api_key"]    = $user->api_key;
				$response["message"]   = 'You are successfully registered';
			} else {
				$response["error"]   = true;
                $response["message"] = 'Unable to registered. Please try again';                  
      		} // else
			$this->EchoResponse(200, $response);
		} // else
	} // Agent Users Signup
	
	
	
	
	
	
} 
