<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Market extends MY_Controller {
	function __construct() 
	{
    	parent::__construct();
  		$this->load->library('form_validation');
		$this->load->model('market_model');
	}
	public function index(){
		exit('here');
	}
	// Add new Market Item (Product)
	public function add(){
		
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		$this->form_validation->set_rules('item_name', 'Item Name', 'trim|required');
		$this->form_validation->set_rules('description', 'Description', 'trim');		
		$this->form_validation->set_rules('item_image', 'Item Image', 'trim');
		$this->form_validation->set_rules('gallery_images', 'Gallery Images', 'trim');
		$this->form_validation->set_rules('price', 'Price', 'trim');
		$this->form_validation->set_rules('contact_direct', 'Contact Direct', 'trim');
		
		if($this->form_validation->run() == FALSE)
		{
   			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			
			// Add Process Here
			$userId = $this->token_user_id;
			$data = array('userId' => $userId, 'category' => $this->input->post('category'), 'item_name' => $this->input->post('item_name'), 'description' => $this->input->post('description'), 'item_image' => $this->input->post('item_image'), 'gallery_images' => $this->input->post('gallery_images'), 'price' => $this->input->post('price'), 'contact_direct' => $this->input->post('contact_direct'), 'available' => 1, 'deleted' => NULL);
			$status = $this->market_model->add_item($data);
			if($status == FALSE){
				$response["error"] = true;
				$response["message"] = "Oops! Something went wrong.";
				$this->EchoResponse(200, $response); 
			} else {
				$response["error"] = false;
				$response["message"] = "New market item was added successfully.";
				$this->EchoResponse(200, $response); 
			}
		}
	}
	// Get all Market Items (Products)
	public function get_all(){
		$all_items = $this->market_model->get_all_items();
		if($all_items == NULL){
			$response["error"] = true;
			$response["message"] = "No records founded.";
			$this->EchoResponse(200, $response); 
		} else {
			$response["error"] = false;
			$response["all_market_items"] = $data;
			$response["message"] = "Market items list all.";
			$this->EchoResponse(200, $response); 
		}
	}
	// Get all My Added Market Items (Products)
	public function get_all(){
		$all_items = $this->market_model->get_all_my_items($this->token_user_id);
		if($all_items == NULL){
			$response["error"] = true;
			$response["message"] = "No records founded.";
			$this->EchoResponse(200, $response); 
		} else {
			$response["error"] = false;
			$response["all_market_items"] = $data;
			$response["message"] = "Market items list all.";
			$this->EchoResponse(200, $response); 
		}
	}
	// Get Single Item (Product)
	public function get_item(){
		
		$this->form_validation->set_rules('item_id', 'Deleted', 'trim');
		if($this->form_validation->run() == FALSE) {
   			$response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response); 
    	} else {
			$item_id = $this->input->post('item_id');
			$items = $this->market_model->get_item($item_id);
			if($items == NULL){
				$response["error"] = true;
				$response["message"] = "No records founded.";
				$this->EchoResponse(200, $response); 
			} else {
				$response["error"] = false;
				$response["item_data"] = $items;
				$response["message"] = "Item details.";
				$this->EchoResponse(200, $response); 
			}
		}
	}
} // Market (CI_Controller)
