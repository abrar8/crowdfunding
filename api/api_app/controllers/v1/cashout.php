<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cashout extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('cashout_model');
        $this->load->model('transactions_model');
    }

    // Function to return charges list on the basis of type 
    public function get_charges() {
        $this->form_validation->set_rules('cashout_type', 'Cash Out Type', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = "Cash out type missing";
            $this->EchoResponse(200, $response);
        } else {
            $type = $this->input->post('cashout_type');
            $charges_data = $this->cashout_model->get_charges($type);

            if ($charges_data == FALSE) {
                $response['error'] = false;
                $response['message'] = "No record found";
                $this->EchoResponse(200, $response);
            } else {

                $response["error"] = false;
                $response["charges"] = $charges_data;
                $response["message"] = 'List of charges';
                $this->EchoResponse(200, $response);
            }
        }
    }

    // Public Functions
    public function request_cashout() {

        $this->form_validation->set_rules('bank_account_id', 'Bank Account ID', 'trim|required');
        $this->form_validation->set_rules('cashout_amount', 'Cash Out Amount', 'trim|required|min_length[1]|max_length[255]');
        $this->form_validation->set_rules('cashout_charges', 'Cash Out Charges', 'trim|required|min_length[1]|max_length[255]');
        $this->form_validation->set_rules('dev_id', 'Device Id', 'trim');
        $this->form_validation->set_rules('gps', 'GPS', 'trim');
        $this->form_validation->set_rules('description', 'Description', 'trim');

        if ($this->form_validation->run() == FALSE) {
            $response["error"] = true;
            $response["message"] = validation_errors();
            $this->EchoResponse(200, $response);
        } else {

            $cashout_amount = $this->input->post('cashout_amount');
            $bank_account_id = $this->input->post('bank_account_id');
            $charges = $this->input->post('cashout_charges');
            $userId = $this->token_user_id;

            // Entry for cash out in tblDepositAccounts table
            // status = 0 for pending state of request
            // current state tblDepositAccounts (status = 0)

            $cash_out_insert_data = array(
                'userId' => $userId,
                'requestDate' => date("Y-m-d h:i:s"),
                'cashout_amount' => $cashout_amount,
                'bankAccountId' => $bank_account_id,
                'charges' => $charges,
                'status' => 0
                );
            $cashout_id = $this->cashout_model->insert_cashout($cash_out_insert_data);
            $accounts_payable = $this->cashout_model->get_accounts_payable_id();
            if ($accounts_payable == false) {
                $accounts_payable = 1;
            }
            if ($cashout_id != false) {
                // Make 1st Transaction for cashout 
                // <Debit = UserId> & <Credit = Accounts/Payable>
                $transaction_track_code = rand(1, 99999);
                $cashout_amount=$cashout_amount+$charges;
                $debit = array('userId' => $userId,
                    'refId' => $userId, // Debit user_id as ref, 
                    'dr' => $cashout_amount,
                    'gps' => $this->input->post('gps'),
                    'devId' => $this->input->post('dev_id'),
                    'type' => 'TR',
                    'mode' => 'CO', // mode = CO for Cash Out Request (Pending) transaction
                    'transaction_code' => $transaction_track_code,
                    'description' => $this->input->post('description')
                );
                $credit = array('userId' => $userId,
                    'refId' => $accounts_payable,
                    'cr' => $cashout_amount,
                    'gps' => $this->input->post('gps'),
                    'devId' => $this->input->post('dev_id'),
                    'type' => 'TR',
                    'mode' => 'CO', // mode = CO for Cash Out Request (Pending) transaction 
                    'transaction_code' => $transaction_track_code,
                    'description' => $this->input->post('description')
                );
                // Cash out request transaction
                $transaction = $this->cashout_model->cashout_transaction($userId, $debit, $credit);
                if ($transaction != false) {
                    $user = $this->transactions_model->user_balance($this->token_user_id);

//                    $symbol = $this->transactions_model->my_currency_symbol($this->token_user_id);
                    $currency_code = $this->transactions_model->my_currency_code($this->token_user_id);

                    $response["error"] = false;
                    $response["transaction_id"] = $transaction->transId;
                    $response["user_id"] = $transaction->userId;
                    $response["credit"] = strval($user['credit']) . '.00';
                    $response["currency_code"] = $currency_code;
                    $response["created"] = $transaction->created;
                    $response["message"] = 'Your cash out request is received and will be processed in 3 working days.';
                    $this->EchoResponse(200, $response);
                } else {
                    $response["error"] = true;
                    $response["message"] = '602';
                    $this->EchoResponse(200, $response);
                }
            }
        }
    }
				public function CashOutAgent() 
					{
						 $auth_key = $this->input->get_request_header('Authorization', TRUE);		   
						 $cashout_amount=$this->input->post('user_amount');
						
						
						//$this->form_validation->set_rules('user_text', 'User text', 'trim|required');
						$this->form_validation->set_rules('user_amount', 'User Amount', 'required');
						if ($this->form_validation->run() == FALSE) 
						{
							$response["error"]   = true;
							$response["message"] = validation_errors();
							$this->EchoResponse(200, $response); 
						} else { 
							
							   if($cashout_amount){
									$digits = 14;
									$transaction_track_code = rand(pow(10, $digits-1), pow(10, $digits)-1);
									$userId = $this->token_user_id;
									$charges = 0;
							
							 $cash_out_insert_data = array(
													'userId' => $userId,
													'requestDate' => date("Y-m-d h:i:s"),
													'cashout_amount' => $cashout_amount,
													'charges' => 0,
													'status' => 0
													);
            $cashout_id = $this->cashout_model->insert_cashout($cash_out_insert_data);
			$accounts_payable = $this->cashout_model->get_accounts_payable_id();
			
            if ($accounts_payable == false) {
                $accounts_payable = 1;
            }				
			if ($cashout_id != false) {
               $cashout_amount=$cashout_amount+$charges;
                $debit = array('userId' => $userId,
                    'refId' => $userId, // Debit user_id as ref, 
                    'dr' => $cashout_amount,
					'type' => 'TR',
                    'mode' => 'CO',
                    'transaction_code' => $transaction_track_code
                );
                $credit = array('userId' => $userId,
                    'refId' => $userId,
                    'cr' => $cashout_amount,
					'type' => 'TR',
                    'mode' => 'CO',
                    'transaction_code' => $transaction_track_code
                    
                );
                // Cash out request transaction
                $transaction = $this->cashout_model->cashout_transaction($userId, $debit, $credit);
                     
			   if ($transaction != false) {
                    $user = $this->transactions_model->user_balance($this->token_user_id);
                     
//                    $symbol = $this->transactions_model->my_currency_symbol($this->token_user_id);
                    $currency_code = $this->transactions_model->my_currency_code($this->token_user_id);
                    
					 $cashoutagent = array(
													'userId' => $userId,
													'requestDate' => date("Y-m-d h:i:s"),
													'cashout_amount' => $cashout_amount,
													'transaction_id' => $transaction->transId,
													'pin' => $transaction_track_code
													);
					
					$cashoutagent_transcation = $this->cashout_model->insert_cashoutagent($cashoutagent);
                    
					$response["error"] = false;
                    $response["transaction_id"] = $transaction->transId;
                    $response["user_id"] = $transaction->userId;
                    $response["credit"] = strval($user['credit']) . '.00';
                    $response["verfication_code"] = $transaction_track_code;
					$response["currency_code"] = $currency_code;
                    $response["created"] = $transaction->created;
                    $response["message"] = 'Your cash out request is received and will be processed in 3 working days.';
                    $this->EchoResponse(200, $response);
                } else {
                    $response["error"] = true;
                    $response["message"] = '602';
                    $this->EchoResponse(200, $response);
                }
         }				
						}
							else{
							$response["error"]   = true;
							$response["message"] = 'Please Enter Amount';
							$this->EchoResponse(200, $response); 	
							}
							} // else
					}
    // Get My Cash In Hand
    public function cash_in_hand() {
        $data = $this->cashout_model->get_my_cash_in_hand($this->token_user_id);
        if ($data == false) {
            $response['error'] = true;
            $response['message'] = "You have no cash in hand.";
            $this->EchoResponse(200, $response);
        } else {

            $symbol = $this->transactions_model->my_currency_symbol($this->token_user_id);
            $response['error'] = true;
            $response['total'] = $symbol . ' ' . $data->total;
            $response['message'] = "Your cash in hand.";
            $this->EchoResponse(200, $response);
        }
    }

    // Function to process the Cash Out Request - (will be used in admin for process the payment)
    public function process_cashout($ids) {
        
    }

    public function get_accounts_payable_co() {
        //$data = $this->cashout_model->get_accounts_payable_co(); 
    }

    public function test() {
        //setcookie('mycookie', 'this is a test value of my cookie.', time() + (86400 * 30), "/");
        foreach (array('one', 'two', 'three') as $key => $value) {
            setcookie('visitor[' . $key . ']', $value, time() + 3600, "/");
        }
        //if(!isset($_COOKIE)){
        //echo 'not set';
        //} else {
        //echo '<visitor> cookie is set value =';
        print_r($_COOKIE);
        //}
    }
	


}// cashout (My_Controller)