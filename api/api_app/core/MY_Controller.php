<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    protected $token_user_id;
    protected $token_merchant_id;

    function __construct() {
        parent::__construct();

        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('security');
        $this->load->library('encrypt');
        //$this->lang->load('en_admin', 'english');   
        $this->load->library('form_validation');
        $this->load->library('email');
        $response = array();
        $auth_key = $this->input->get_request_header('Authorization', TRUE);
        if ($auth_key) {
            $this->load->model('authenticate_model');
            $valid_response = $this->authenticate_model->is_valid_api_key($auth_key);
            if ($valid_response) {
                $this->token_user_id = $valid_response->userId;
            } else {
                $valid_response = $this->authenticate_model->is_valid_merchant_api_key($auth_key);
                if ($valid_response) {
                    $this->token_merchant_id = $valid_response->userId;
                } else {
					$valid_response = $this->authenticate_model->is_valid_agent_api_key($auth_key);					
					if($valid_response)
					{
						$this->token_agent_id = $valid_response->userId;
					} else {
						$response["error"] = true;
                    	$response["message"] = "Invalid Api Key";
                    	header('Content-type: application/json');
                    	echo json_encode($response);
                    	exit();
					}
                }
            }
        } else {
            $response["error"] = true;
            $response["message"] = "Api key is misssing";
            header('Content-type: application/json');
            echo json_encode($response);
            exit();
        }
    }

    protected function EchoResponse($status_code, $response) {
        $this->output->set_status_header($status_code);
        $this->output->set_content_type('application/json')
                ->set_output(json_encode($response));
    }

}
