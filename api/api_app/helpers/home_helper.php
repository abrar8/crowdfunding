<?php defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_Merchant_id')) :
function get_Merchant_id($id)
{			
	$ci =& get_instance();
	$ci->db->select('merchantId						
				   ');
	$ci->db->from('tblMerchantInvoices');
	$ci->db->where('id', $id);
	$query = $ci->db->get();
	if($query->num_rows() > 0)
	{
		$row = $query->row();
		return $row->merchantId;		
	} else {			
		return NULL;
	}
} // get_Merchant_id
endif;


if ( ! function_exists('get_merchantInfo')) :
function get_merchantInfo($id)
{			
	$ci =& get_instance();
	$ci->db->select('tblMerchantUsers.userId,
	                 tblMerchantUsers.firstName,
					 tblMerchantUsers.lastName,
					 tblMerchantUsers.merchant_title,
					 tblMerchantUsers.address,
					 tblMerchantUsers.city,
					 tblMerchantUsers.country AS country_id,
					 tblMerchantUsers.currency AS currency_id,
					 countries.printable_name AS country,
					 tblExchangeRates.currency_type AS currency
				   ');
	$ci->db->from('tblMerchantUsers');
	$ci->db->join('countries', 'countries.id=tblMerchantUsers.country', 'inner');
	$ci->db->join('tblExchangeRates', 'tblExchangeRates.id=tblMerchantUsers.currency', 'inner');
	$ci->db->where('tblMerchantUsers.userId', $id);
	$query = $ci->db->get();
	if($query->num_rows() > 0)
	{
		return $query->row();	
	} else {			
		return NULL;
	}
} // get_merchantInfo
endif;


if ( ! function_exists('get_userInfo')) :
function get_userInfo($id)
{			
	$ci =& get_instance();
	$ci->db->select('tblUsers.userId,
	                 tblUsers.firstName,
					 tblUsers.lastName,
					 tblUsers.address,
					 tblUsers.city,
					 tblUsers.country AS country_id,
					 tblUsers.currency AS currency_id,
					 countries.printable_name AS country,
					 tblExchangeRates.currency_type AS currency
				   ');
	$ci->db->from('tblUsers');
	$ci->db->join('countries', 'countries.id=tblUsers.country', 'inner');
	$ci->db->join('tblExchangeRates', 'tblExchangeRates.id=tblUsers.currency', 'inner');
	$ci->db->where('tblUsers.userId', $id);
	$query = $ci->db->get();
	if($query->num_rows() > 0)
	{
		return $query->row();	
	} else {			
		return NULL;
	}
} // get_userInfo
endif;


if ( ! function_exists('get_fb_userInfo')) :
function get_fb_userInfo($fb_id)
{			
	$ci =& get_instance();
	$ci->db->select('userId,
	                 firstName,
					 lastName,
					 fb_id,
					 userPin						
				   ');
	$ci->db->from('tblUsers');
	$ci->db->where('fb_id', $fb_id);
	$query = $ci->db->get();
	if($query->num_rows() == 1)
	{
		$row = $query->row_array();
		/*		
		echo "<pre>";
		print_r($row);
		exit();
		*/
		return $row;	
	} else {			
		return NULL;
	}
} // get_fb_userInfo
endif;
?>