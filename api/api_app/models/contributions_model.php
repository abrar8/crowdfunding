<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contributions_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    
	public function subscribeMe($uid) 
	{
        $this->db->where('userId', $uid);
        $query = $this->db->get('tblDcSubscriptions');
        if ($query->num_rows() > 0) {
            return 0;
        } else {
            $data = array('userId'    => $uid,
			              'subStatus' => 1);
            $this->db->insert('tblDcSubscriptions', $data);
            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }
    } // subscribeMe
	

    public function mySubscriptionStatus($uid) {
        $this->db->where('userId', $uid);
        $this->db->where('subStatus', 1);
        $query = $this->db->get('tblDcSubscriptions');
        if ($query->num_rows() > 0) {
            return 1;
        } else {
            return 0;
        }
    }


    /*
    public function user_balance($id) {
        $this->db->where('userId', $id);
        $this->db->where('mode !=', 'DC');
        $this->db->where('mode !=', 'GC');
        $this->db->where('mode !=', 'CHC');
        $this->db->where('mode !=', 'MI');
        $this->db->select('userId, 
		                   SUM(cr) AS credit, 
						   SUM(dr) AS debit');
        $data1 = $this->db->get('tblTransactions')->row();

        if (!empty($data1->userId)) {
            $credit = $data1->credit;
            $debit = $data1->debit;
        } else {
            $credit = '0.00';
            $debit = '0.00';
        }

        // Sum of Debit from DC (Daily Contribution)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'DC');
        $data2 = $this->db->get('tblTransactions')->row();

        if (!empty($data2->userId)) {
            $debit_dc = $data2->debit;
        } else {
            $debit_dc = '0.00';
        }

        // Sum of Debit from GC (Group Contribution)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'GC');
        $data3 = $this->db->get('tblTransactions')->row();

        if (!empty($data3->userId)) {
            $debit_gc = $data3->debit;
        } else {
            $debit_gc = '0.00';
        }

        // Sum of Debit from CHC (Charity Contribution)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'CHC');
        $data4 = $this->db->get('tblTransactions')->row();

        if (!empty($data4->userId)) {
            $debit_chc = $data4->debit;
        } else {
            $debit_chc = '0.00';
        }

        // Sum of Debit from MI (Market Item Purchase Transaction)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'MI');
        $data5 = $this->db->get('tblTransactions')->row();

        if (!empty($data5->userId)) {
            $debit_mi = $data5->debit;
        } else {
            $debit_mi = '0.00';
        }

        $debited = $debit + $debit_dc + $debit_gc + $debit_chc + $debit_mi;
        $balance = $credit - $debited;

        $data = array(
            'credit' => $balance,
            'debit' => "$debited"
        );

        return $data;
    } // user_balance
    */


    public function transfer_credit_to_daily_contributions($uid, $debit, $credit) {
        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $uid);
        $query = $this->db->get('tblTransactions');

        $this->db->insert('tblTransactions', $credit);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

// transfer_credit

    public function viewDcBalance($id) {

        $this->db->where('userId', $id);
        $this->db->where('type', 'TR');
        $this->db->where('mode', 'DC');
        $this->db->select('userId,
		                   SUM(cr) AS credit, 
						   SUM(dr) AS debit');
        $query = $this->db->get('tblTransactions');
		if ($query->num_rows() > 0) {
            $row = $query->row();
            if (!empty($row->credit)) {
                $data = array('userId' => $row->userId,
							  'credit' => $row->credit,
							  'debit'  => $row->debit
						     );
                return $data;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

// viewDcBalance

    public function myTransactionsDc($userId) {

        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, transaction_code');
        //$this->db->where('created >=', $fromDate);
        //$this->db->where('created <=', $toDate);
        $this->db->where('userId', $userId);
        $this->db->where('mode', "DC");
        $this->db->where('cr', "0.00");
        $this->db->where('dr IS NOT NULL');
        //$this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

// myTransactionsDc

    public function get_unique_guid() {
        $id = uniqid('DCR#');
        $guid = $this->get_dcr_number($id);
        return $guid;
    }

//get_unique_guid

    public function get_dcr_number($guid) {
        $this->db->where('transaction_code', $guid);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0) {
            $this->get_unique_guid();
        } else {
            return $guid;
        }
    }

// get_dcr_number

    public function get_unique_guid_gc() {
        $id = uniqid('GCR#');
        $guid = $this->get_gcr_number($id);
        return $guid;
    }

//get_unique_guid_gc

    public function get_gcr_number($guid) {
        $this->db->where('reference_number', $guid);
        $query = $this->db->get('tblGcAccount');
        if ($query->num_rows() > 0) {
            $this->get_unique_guid_gc();
        } else {
            return $guid;
        }
    }


    public function getGcId($gc_reference_number) {
        $this->db->select('GcAccountId');
        $this->db->where('reference_number', $gc_reference_number);
        $query = $this->db->get('tblGcAccount');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->GcAccountId;
        } else {
            return NULL;
        }
    }

    public function getDcAccountId() {
        $this->db->select('DcAccountId');
        $this->db->where('available', 1);
        $query = $this->db->get('tblDcAccount');
        if ($query->num_rows() > 0) {
            $data = $query->row();
            return $data->DcAccountId;
        } else {
            return NULL;
        }
    }

    public function createGroupContribution($userId, $title, $description, $suggested_amount) {
        $reference_number = $this->contributions_model->get_unique_guid_gc();
        $data = array('reference_number' => $reference_number, 'userId' => $userId, 'title' => $title, 'description' => $description, 'suggested_amount' => $suggested_amount, 'status' => 1);
        if ($this->db->insert('tblGcAccount', $data)) {
            $GcAccountId = $this->db->insert_id();
            $this->db->where('GcAccountId', $GcAccountId);
            $query = $this->db->get('tblGcAccount');
            if ($query->num_rows() > 0) {
                return $query->row();
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    } // End


    public function createCharity($data) 
	{
		$this->db->insert('tblCharityAccount', $data);		
        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    } // End - createCharity
    
	
	// Start Charities
    public function getCharities_bk() { // List All Active Charities on iMali
        $this->db->select("tblCharityAccount.id AS charity_id,
		                   tblCharityAccount.reference_number,
		                   tblCharityAccount.title,
						   tblCharityAccount.description,
						   tblCharityAccount.image,
						   tblCharityAccount.status,
						   tblUsers.firstName,
						   tblUsers.lastName");
        $this->db->from('tblCharityAccount');
        $this->db->join('tblUsers', 'tblCharityAccount.userId = tblUsers.userId', 'inner');
        $this->db->where('tblCharityAccount.status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }
    
    
    public function getCharities($user_id) { // List All Active Charities on iMali
        $query = $this->db->query("SELECT IF(tblCharityAccount.userId =".$user_id.", 1, 2) AS charity_flag,
				tblCharityAccount.reference_number,
				tblCharityAccount.id AS charity_id,
				tblCharityAccount.title, 
				tblCharityAccount.description, 
				tblCharityAccount.image, 
				tblCharityAccount.status, 
				tblUsers.firstName, 
				tblUsers.lastName 
			FROM 
				tblCharityAccount
			LEFT JOIN 
				tblUsers
			ON 
				tblCharityAccount.userId = tblUsers.userId 
			WHERE 
				tblCharityAccount.status = 1");
        $data = array();
        foreach($query->result() as $charity){
            $charity->invitation_flag = $this->getCharityInvitationStatus($user_id,$charity->reference_number);
            $data[] = $charity;
        }
        
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }
    
    public function getCharityInvitationStatus($user_id,$charity_ref_num){
        $query = $this->db->query("SELECT * FROM tblCharityInvitations WHERE (user_id=".$user_id." AND charity_ref_num='".$charity_ref_num."')");
        
        if($query->num_rows()==1){
            $res = $query->row_array();
            return $res['status'];
        }else{
            return 4;
        }
        
    }

//end getCharities

    public function getMyCharities($userId) { // List All Created by Me + Status = 0 and 1
        $this->db->select("tblCharityAccount.reference_number, tblCharityAccount.title, tblCharityAccount.description, tblCharityAccount.image, tblCharityAccount.status, tblUsers.firstName, tblUsers.lastName");
        $this->db->from('tblCharityAccount');
        $this->db->join('tblUsers', 'tblCharityAccount.userId = tblUsers.userId', 'inner');
        $this->db->where('tblCharityAccount.userId', $userId);
        $this->db->where('tblCharityAccount.status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

//end getMyCharities

    public function removeCharity($userId, $reference_number) { // Remove Charity Created by Me with (reference Number & token->user_id)
        $this->db->where('userId', $userId);
        $this->db->where('reference_number', $reference_number);
        $query = $this->db->get('tblCharityAccount');

        if ($query->num_rows() > 0) {
            $data = array('status' => 5);
            $this->db->where('userId', $userId);
            $this->db->where('reference_number', $reference_number);
            $update = $this->db->update('tblCharityAccount', $data);
            if (!empty($update)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

//end removeCharity

    public function getCharityId($reference_number) {
        $this->db->select('id');
        $this->db->where('reference_number', $reference_number);
        $query = $this->db->get('tblCharityAccount');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return NULL;
        }
    }

    public function charityTransaction($userId, $debit, $credit, $reference_number) {

        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblTransactions');

        $this->db->insert('tblTransactions', $credit);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

// End - charityTransaction

    public function myDonations($userId) {

        $this->db->select("tblTransactions.dr as amount, tblCharityAccount.reference_number, tblCharityAccount.title, tblCharityAccount.description
						  ");
        $this->db->from('tblTransactions');
        $this->db->join('tblCharityAccount', 'tblTransactions.refId = tblCharityAccount.id', 'inner');
        $this->db->where('tblTransactions.userId', $userId);
        $this->db->where('tblTransactions.refId !=', $userId);
        $this->db->where('tblTransactions.cr', 0.00);
        $this->db->where('tblTransactions.mode', "CHC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    // End myDonations
    public function myGroupContributions($userId) {
        $this->db->select('reference_number, title, description, suggested_amount, status');
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblGcAccount');
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }

//end myGroupContributions

    public function user_profile_by_pin($userpin) {
        $this->db->where('userPIN', $userpin);
        $query = $this->db->get('tblUsers');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

// user_profile_by_pin

    public function sendGcRequest($friend) {
        $this->db->select("*");
        $this->db->from('tblGcRequests');
        $this->db->where('userId', $friend['userId']);
        $this->db->where('friend_id', $friend['friend_id']);
        $this->db->where('gc_reference_number', $friend['gc_reference_number']);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->status;
        } else {
            ///////////////////////////////////////////////////	
            $data = array('userId' => $friend['userId'],
                'friend_id' => $friend['friend_id'],
                'gc_reference_number' => $friend['gc_reference_number'],
                'status' => 1,
                'updated' => date("Y-m-d H:i:s")
            );
            $this->db->insert('tblGcRequests', $data);
            ///////////////////////////////////////////////////
            return NULL;
        }
    }

// sendGcRequest

    public function requestResponseGc($userId, $reference_number, $status) {

        $upd_data = array('status' => $status);

        $this->db->where('friend_id', $userId);
        $this->db->where('gc_reference_number', $reference_number);
        return $this->db->update('tblGcRequests', $upd_data);
    }

// requestResponseGc

    public function gcTransaction($userId, $debit, $credit, $reference_number, $amount) {

        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblTransactions');

        $this->db->insert('tblTransactions', $credit);
        if ($query->num_rows() > 0) {
            $upd_data = array('amount' => $amount, 'status' => 4);
            $this->db->where('friend_id', $userId);
            $this->db->where('gc_reference_number', $reference_number);
            $this->db->update('tblGcRequests', $upd_data);

            return $query->row();
        } else {
            return false;
        }
    }

//gcTransaction

    public function getMyGcDetails($reference_number) {

        $query = $this->db->query("
		
								SELECT tblUsers.firstName, tblUsers.lastName,tblUsers.userPin, tblGcAccount.suggested_amount, tblGcRequests.amount, tblGcRequests.status, tblGcRequests.gc_reference_number
								
								FROM 
								tblGcRequests
								
								INNER JOIN
								tblUsers
								
								ON
								tblGcRequests.friend_id=tblUsers.userId
								
								INNER JOIN
								tblGcAccount
								
								ON
								tblGcRequests.gc_reference_number=tblGcAccount.reference_number
								
								WHERE
								tblGcRequests.gc_reference_number = '$reference_number'
		
		");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_friendlist_upd($uid, $gc_reference_number) {
        $this->db->select('id');
        $this->db->where('uid', $uid);
        $uid_data = $this->db->get('tblFriends')->result();

        $this->db->select('id');
        $this->db->where('friend_id', $uid);
        $friend_id_data = $this->db->get('tblFriends')->result();

        $data1 = array();
        $data2 = array();

        if (!empty($uid_data)) {
            foreach ($uid_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.phoneNumber,
								   tblUsers.address,
								   tblUsers.city,
								   tblUsers.country,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblUsers.currency,
								   tblStatus.name AS status		                   
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.friend_id', 'left');
                $this->db->join('tblGcRequests', 'tblUsers.userId = tblGcRequests.friend_id');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status', 'inner');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row->id);
                $this->db->where('tblGcRequests.gc_reference_number', $gc_reference_number);
                $this->db->order_by("tblUsers.firstName", "ASC");
                $data1[] = $this->db->get()->result();
            }
        }

        if (!empty($friend_id_data)) {
            foreach ($friend_id_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.phoneNumber,
								   tblUsers.address,
								   tblUsers.city,
								   tblUsers.country,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblUsers.currency,
								   tblStatus.name AS status
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.uid', 'left');
                $this->db->join('tblGcRequests', 'tblUsers.userId = tblGcRequests.userId');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status', 'inner');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row->id);
                $this->db->where('tblGcRequests.gc_reference_number', $gc_reference_number);
                $this->db->order_by("tblUsers.firstName", "ASC");
                $data2[] = $this->db->get()->result();
            }
        }
        $result = array_merge($data1, $data2);
        return $result;
    }

    public function myGcRequests($userId) {

        $this->db->select('tblGcRequests.gc_reference_number, tblUsers.firstName, tblUsers.lastName, tblGcAccount.title, tblGcAccount.description, tblGcAccount.suggested_amount, tblGcRequests.amount as paid_amount, tblGcRequests.status');
        $this->db->from('tblGcRequests');
        $this->db->join('tblGcAccount', 'tblGcAccount.reference_number = tblGcRequests.gc_reference_number', 'inner');
        $this->db->join('tblUsers', 'tblUsers.userId = tblGcRequests.userId', 'inner');
        $this->db->where('tblGcRequests.status !=', 3);
        $this->db->where('friend_id', $userId);
        $this->db->order_by("tblGcRequests.updated", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return NULL;
        }
    }

    public function get_charity($reference_number) {
        $this->db->select('id');
        $this->db->where('reference_number', $reference_number);
        $charity_id = $this->db->get('tblCharityAccount')->result();
        if (!empty($charity_id[0]->id)) {
            $this->db->select('u.firstName, u.lastName, t.dr as amount_paid, t.description, t.created');
            $this->db->from('tblTransactions as t');
            $this->db->join('tblUsers as u', 't.userId = u.userId');
            $this->db->where('t.type', "TR");
            $this->db->where('t.mode', "CHC");
            $this->db->where('t.refId', $charity_id[0]->id);
            $this->db->where('t.dr IS NOT NULL');
            $this->db->where('t.cr', 0.00);
            $result = $this->db->get()->result_array();
            if (!empty($result)) {
                return $result;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function get_total_charity_amount($reference_number) {
        $this->db->select('id');
        $this->db->where('reference_number', $reference_number);
        $charity_id = $this->db->get('tblCharityAccount')->result();
        if (!empty($charity_id[0]->id)) {
            $this->db->select('sum(t.dr) as total');
            $this->db->from('tblTransactions as t');
            $this->db->join('tblUsers as u', 't.userId = u.userId');
            $this->db->where('t.type', "TR");
            $this->db->where('t.mode', "CHC");
            $this->db->where('t.refId', $charity_id[0]->id);
            $this->db->where('t.dr IS NOT NULL');
            $this->db->where('t.cr', 0.00);
            $row = $this->db->get()->row();
            if (!empty($row)) {
                return $row->total;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }


    /*
     * Edited By ::: Adnan Haider
     */

    public function inviteToCharity($senderId,$userId,$charityId,$reference_number) 
    {
        $this->db->select("*");
        $this->db->from("tblCharityInvitations");
        $this->db->where("charity_ref_num", $reference_number);
        $this->db->where("user_id", $userId);
        $this->db->where("charity_id", $charityId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $data = array('sender_id'       => $senderId,
                          'user_id'         => $userId,
                          'charity_id'      => $charityId,
                          'charity_ref_num' => $reference_number
                         );
            $this->db->insert('tblCharityInvitations', $data);
            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }
    } // inviteToCharity



    public function getSenderId($id){
        $this->db->select('sender_id');
        $this->db->where('id',$id);
        $res = $this->db->get('tblCharityInvitations');
        $res = $res->result();
        $res = $res[0]->sender_id;        
        return $res;        
    }
    

    public function getMyCharityDetail($charity_id) 
    { 
        $this->db->select("tblCharityAccount.id,
                           tblCharityAccount.reference_number, 
                           tblCharityAccount.title,
                           tblCharityAccount.description,
                           tblCharityAccount.image, 
                           tblCharityAccount.status, 
                           tblUsers.firstName, 
                           tblUsers.lastName
                         ");
        $this->db->from('tblCharityAccount');
        $this->db->join('tblUsers', 'tblCharityAccount.userId = tblUsers.userId', 'inner');
		$this->db->where('tblCharityAccount.id', $charity_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    }

    public function getPendingInvitations($sender_id) {

        $this->db->select('tblUsers.firstName,
                           tblUsers.lastName,
                           tblCharityInvitations.sender_id,
                           tblCharityInvitations.id AS invitation_id,
                           tblCharityAccount.title,
                           tblCharityAccount.description,
                           tblCharityAccount.image,
                           tblCharityAccount.reference_number');
        $this->db->from('tblCharityInvitations');
        $this->db->join('tblUsers', 'tblCharityInvitations.sender_id = tblUsers.userId');
        $this->db->join('tblCharityAccount', 'tblCharityInvitations.charity_id = tblCharityAccount.id');
		$this->db->where('tblCharityInvitations.user_id', $sender_id);
        $this->db->where('tblCharityInvitations.status', 1);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }


    public function responseToInvitation($invitation_id,$status) 
    {
        $data = array('status' => $status);
        $this->db->where('id', $invitation_id);
        $res = $this->db->update('tblCharityInvitations', $data);
        if ($res) {
            $this->db->select('charity_id');
            $this->db->from('tblCharityInvitations');
            $this->db->where('id', $invitation_id);
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                $row = $query->row();
                return $row->charity_id;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    } // responseToInvitation

}