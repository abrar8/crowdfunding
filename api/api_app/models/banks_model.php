<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class banks_model extends CI_Model {
	function __construct() {
    	parent::__construct();
  	} 

  	
	public function get_banks($uid) 
	{
		$this->db->select('banks.id,
		                   banks.bank_name,
						   tblUsers.country
						  ');
		$this->db->from('banks');
		$this->db->join('tblUsers', 'tblUsers.country = banks.country_id', 'inner');
		$this->db->where('tblUsers.userId', $uid);
		$query =$this->db->get();
		
    	if($query->num_rows() > 0) 
			return $query->result();
		else 
			return NULL;	
  	} // get_banks
  
  
	public function save_bank($data) 
	{
    	if ($this->db->insert('tblBankAccounts', $data)) {
			return true;
    	} else {
      		return false;
    	}
  	} // save_bank
	
  
   public function banks_by_user_id($userId) 
   {
		// $this->db->where('userId', $userId);
    	// $query = $this->db->get('tblBankAccounts');
		$this->db->select('tblBankAccounts.*,
		                   banks.bank_name
						  ');
		$this->db->from('tblBankAccounts');
		$this->db->join('banks', 'banks.id = tblBankAccounts.bankId');
		$this->db->where('tblBankAccounts.userId', $userId);
		$query =$this->db->get();
    	if($query->num_rows() > 0) 
			return $query->result();
		else 
			return FALSE;	
  	} // banks_by_user_id
	
  
   	public function banks_by_bank_id($userId,$bankId) 
	{
		//$this->db->where('userId', $userId);
    	//$query = $this->db->get('tblBankAccounts');
		$this->db->select('*');
		$this->db->from('tblBankAccounts AS Accounts');
		$this->db->join('tblAvailableBanks AS Banks', 'Banks.bankId = Accounts.bankId');
		$this->db->where('Accounts.userId', $userId);
		$this->db->where('Accounts.bankId', $bankId);
		$query =$this->db->get();
    	if($query->num_rows() > 0) 
			return $query->result();
		else 
			return FALSE;
 	} // banks_by_bank_id
	
	
}

