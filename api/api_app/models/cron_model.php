<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron_model extends CI_Model {
	// constructor
	function __construct() {
    	parent::__construct();
	}
	
		public function closing_ipool_formax(){
		$this->db->select('*');
		$this->db->where('closed',0);		
        return $this->db->get('tblIpool');
		
		}
		
		
		public function getCountInviteds($ipool_id){
			
			$this->db->where('ipool_id',$ipool_id);
			$this->db->where('status',2);
			$this->db->from('tblIpoolInvitation');
			return $this->db->count_all_results();
		}
		
		public function update_ipoool($ipool_id){
			$data = array('closed' => 1);
			$this->db->where('id',$ipool_id);
			return $this->db->update('tblIpool',$data);
		}
	
	
		public function get_closed_ipools(){
			$this->db->select('*');
			$this->db->where('closed',1);		
			return $this->db->get('tblIpool');
			
		}
		public function get_random_user($ipool_id){
			$this->db->select('tblDevices.device_id,
			                   tblDevices.device_type,
							   tblUsers.userPIN,
							   tblIpool.amount,
			                   tblUsers.firstName,
							   tblUsers.lastName,
			                   tblIpoolInvitation.uid');
			$this->db->from('tblIpoolInvitation');
			$this->db->join('tblUsers','tblUsers.userId = tblIpoolInvitation.uid ','inner');
			$this->db->join('tblDevices','tblDevices.uid = tblUsers.userId ','inner');
			$this->db->join('tblIpool','tblIpool.id = tblIpoolInvitation.ipool_id ','inner');
			$this->db->where('tblIpoolInvitation.status',2);
			$this->db->where('tblIpoolInvitation.selected',0);
			$this->db->where('tblIpoolInvitation.ipool_id',$ipool_id);
			$this->db->order_by('tblIpoolInvitation.id', 'RANDOM');
    		$this->db->limit(1);	
			$query = $this->db->get();
			
			if($query->num_rows() > 0) {
				//$this->credit_transaction($query->row());
				return $query->row();	
			}else{
				return false;
			}
		}
		public function get_payee_users($ipool_id,$selected_user){
			$this->db->select('tblDevices.device_id,
			                   tblDevices.device_type,
							   tblUsers.userPIN,
							   tblIpool.amount,
			                   tblUsers.firstName,
							   tblUsers.lastName,
			                   tblIpoolInvitation.uid');
			$this->db->from('tblIpoolInvitation');
			$this->db->join('tblUsers','tblUsers.userId = tblIpoolInvitation.uid ','inner');
			$this->db->join('tblDevices','tblDevices.uid = tblUsers.userId ','inner');
			$this->db->join('tblIpool','tblIpool.id = tblIpoolInvitation.ipool_id ','inner');
			$this->db->where('tblIpoolInvitation.status',2);
			$this->db->where('tblIpoolInvitation.selected',0);
			$this->db->where_not_in('tblIpoolInvitation.uid',$selected_user);
			$this->db->where('tblIpoolInvitation.ipool_id',$ipool_id);
			$query = $this->db->get();
			
			if($query->num_rows() > 0) {
				return $query->result();	
			}else{
				return false;
			}
		}
	
	
	public function credit_transaction($user_ipool,$user_ipool_reciever) {
		
			$this->load->model('transactions_model');
			$this->load->helper('notification');
        
		
            $userPIN = $user_ipool_reciever->userPIN;
            $user = $this->transactions_model->user_balance($user_ipool->uid); // Get User Balance
            $userPinStatus = $this->transactions_model->check_userPin($userPIN, $user_ipool->uid);
            if ($userPinStatus == 1) {
                echo 'You cannot transfer credit to your own account.';
				exit;
            } else if ($user['credit'] < $this->input->post('credit')) {
                echo 'Insufficient balance. Your balance ' . $user['credit'];
            } else {
                $user_profile = $this->transactions_model->user_profile_by_pin($userPIN);
                if ($user_profile) {
                    $debit = array('userId' => $user_ipool->uid,
                        'refId' => $user_profile->userId,
                        
						'dr' => $user_ipool->amount,
                        
						'devId' => $user_ipool->device_id,
                        'type' => 'TR',
                        'mode' => 'TCP',
                        'description' => 'ipools deducation'
                    );
                    $credit = array('userId' => $user_profile->userId,
                        'refId' => $user_ipool->uid,
                        
						'cr' => $user_ipool->amount,
                        
						'devId' =>  $user_ipool->device_id,
                        'type' => 'TR',
                        'mode' => 'TCP',
                        'description' => 'ipools deducation'
                    );
                    $transaction_data = $this->transactions_model->transfer_credit($user_ipool->uid, $debit, $credit);
                    if ($transaction_data) {
                        $transfer_credit_amount = $user_ipool->amount;
                        $newCredit = $this->transactions_model->user_balance($user_profile->userId); // Get Updated Balance
                        if (!empty($newCredit)) {
                            $totalBalance = $newCredit['credit'];
                        } else {
                            $totalBalance = 0;
                        }
                        $senderPin = $this->transactions_model->getUserPin($user_ipool->uid); // Get sender's pin
                        $msg = array(
                            'notification_type' => "credit_transfer",
                            'amount' => $transfer_credit_amount,
                            'new_credit' => $totalBalance,
                            'sender_name' => $user_profile->firstName,
                            'message' => "Ipool Credit transfered successfully"
                        );

                        $msg = json_encode($msg);
                        $notification_data = array(
                            'message' => $msg
                        );
                        sent_notify($notification_data, $user_profile->userId);

                        $user = $this->transactions_model->user_balance($user_ipool->uid); // Get Updated Balance

                    } // if
					return true;
                } else {
					return false;
					//echo 'Invalid User Pin. Please try again.';
				}
            } // else
    } 
	
	
} // End - cron_model