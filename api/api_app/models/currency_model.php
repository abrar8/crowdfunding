<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Currency_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
	

	public function get_exchange_rates()
	{
		$this->db->select('*');
		$this->db->from('tblExchangeRates');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		} else {
			return NULL;
		}
	} // get_exchange_rates
	
		
	public function update_currency($data,$uid)
	{
		$this->db->where('userId', $uid);
		$this->db->update('tblUsers', $data);
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;	
	} // update_currency
	
	
} // Currency_model