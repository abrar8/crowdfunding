<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signin_model extends CI_Model {
	function __construct() {
    	parent::__construct();
  	} 

	
	public function does_user_exist($login) 
	{  
		$this->db->select('*');
		$this->db->from('tblUsers');
		$login_data = array('userPIN'      => $login['userPIN'],
		                    'userPassword' => $this->encrypt->sha1($login['userPassword'])
						   );
		$this->db->where($login_data); 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			$row = $query->row();			
			/////////////////////// Check FB ID START /////////////////////////
			$this->db->select('fb_id');
			$this->db->from('tblUsers');
			$this->db->where('userId',$row->userId); 
			$query1 = $this->db->get();
			if($query1->num_rows() == 1)
			{	
				$row1 = $query1->row();
				if($row1->fb_id == "")
				{
					$data = array('fb_id' => addslashes($login['fb_id'])); 
					$this->db->where('userId', $row->userId);
					$this->db->update('tblUsers', $data);		
				} else {
					// FB ID already exists	
				}							
			} 
			////////////////////// Check FB ID END ///////////////////////////	
			
			/////////////////////// Check Device ID START /////////////////////////
			$this->db->select('id');
			$this->db->from('tblDevices');
			$this->db->where('uid',$row->userId); 
			$query = $this->db->get();
			if($query->num_rows() == 1)
			{	
				$data = array('device_id'   => addslashes($login['device_id']),
							  'device_type' => addslashes($login['device_type'])
							  ); 
				$this->db->where('uid', $row->userId);
				$this->db->update('tblDevices', $data);				
			} else {
				$data = array('device_id'   => addslashes($login['device_id']),
						  	  'device_type' => addslashes($login['device_type']),
							  'uid'         => $row->userId
						     ); 			
				$this->db->insert('tblDevices', $data);
			}
			////////////////////// Check Device ID END ///////////////////////////	
					
			return $row;
		} else {			
			return FALSE;
		}
  	} // does_user_exist
	
	public function does_merchant_exist($login) 
	{  
		$this->db->select("*");
		$this->db->from("tblMerchantUsers");
		$login_data = array('userPIN'      => $login['userPIN'],
		                    'userPassword' => $this->encrypt->sha1($login['userPassword'])
						   );
		$this->db->where($login_data); 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			$row = $query->row();
			/////////////////////// Check Device ID START /////////////////////////
			$this->db->select('id');
			$this->db->from('tblDevices');
			$this->db->where('uid',$row->userId); 
			$query = $this->db->get();
			if($query->num_rows() == 1)
			{	
				$data = array('device_id'   => addslashes($login['device_id']),
							  'device_type' => addslashes($login['device_type'])
							  ); 
				$this->db->where('uid', $row->userId);
				$this->db->update('tblDevices', $data);				
			} else {
				$data = array('device_id'   => addslashes($login['device_id']),
						  	  'device_type' => addslashes($login['device_type']),
							  'uid'         => $row->userId
						     ); 			
				$this->db->insert('tblDevices', $data);
			}
			////////////////////// Check Device ID END ///////////////////////////	
					
			return $row;
		} else {		
			return FALSE;
		}
  	} // does_merchant_exist
	
/*	public function profile_status($userId){
		$this->db->where('userId', $userId);
		$response = $this->db->get('tblUsers')->row();
		if(!empty($response)){
			if($response->address != NULL && $response->country != NULL && $response->city != NULL && $response->currency != NULL){
				$this->db->where('userId', $userId);
				$bankAccount = $this->db->get('tblBankAccounts')->result();
				if(!empty($bankAccount)){
					return "1";
				} else {
					return "0";
				}
			} else {
				return "0";
			}
		} else {
			return "Error: User does not exist.";
		}
	}*/
	
	
	public function merchant_profile_status($userId)
	{
		$this->db->select('currency');
		$this->db->from('tblMerchantUsers');
		$this->db->where('userId', $userId);
		$query =$this->db->get();
    	if($query->num_rows() > 0) { 
			$row = $query->row();
			if(empty($row->currency))
			{
				return 0;
			} else {
				return 1;	
			}
		} else {
			return 0;
		}
	} // merchant_profile_status
	
	
	public function does_agent_exist($login) 
	{  
		$this->db->select('*');
		$this->db->from('tblAgents');
		$login_data = array('userPIN'      => $login['userPIN'],
		                    'userPassword' => $this->encrypt->sha1($login['userPassword'])
						   );
		$this->db->where($login_data); 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			$row = $query->row();			
			/////////////////////// Check Device ID START /////////////////////////
			$this->db->select('id');
			$this->db->from('tblDevices');
			$this->db->where('uid',$row->userId); 
			$query = $this->db->get();
			if($query->num_rows() == 1)
			{	
				$data = array('device_id'   => addslashes($login['device_id']),
							  'device_type' => addslashes($login['device_type'])
							  ); 
				$this->db->where('uid', $row->userId);
				$this->db->update('tblDevices', $data);				
			} else {
				$data = array('device_id'   => addslashes($login['device_id']),
						  	  'device_type' => addslashes($login['device_type']),
							  'uid'         => $row->userId
						     ); 			
				$this->db->insert('tblDevices', $data);
			}
			////////////////////// Check Device ID END ///////////////////////////
			return $row;
		} else {			
			return FALSE;
		}
  	} // does_agent_exist
	
	
	public function agent_profile_status($userId)
	{
		$this->db->where('userId', $userId);
		$response = $this->db->get('tblAgents')->row();
		if(!empty($response)){
			if($response->address != NULL && $response->country != NULL && $response->city != NULL && $response->currency != NULL){
				$this->db->where('userId', $userId);
				$bankAccount = $this->db->get('tblBankAccounts')->result();
				if(!empty($bankAccount)){
					return "1";
				} else {
					return "0";
				}
			} else {
				return "0";
			}
		} else {
			return "Error: Agent does not exist.";
		}
	} // agent_profile_status
	
	
/*	public function update_user_password($email,$password)
	{
		$data = array('userPassword' => $this->encrypt->sha1($password));
		$this->db->where('email', $email);					 
		$this->db->update('tblUsers', $data);
		
		$this->db->select("*");
		$this->db->from("tblUsers");
		$this->db->where('email', $email);
		$query = $this->db->get();
		if($query->num_rows() == 1) {
			return $query->row();
		} else {
			return FALSE;
		} 		
	} // update_user_password*/
	
	
	public function get_user_info($email)
	{		
		$this->db->select("*");
		$this->db->from("tblUsers");
		$this->db->where('email', $email);
		$query = $this->db->get();
		if($query->num_rows() == 1) {
			return $query->row();
		} else {
			return FALSE;
		} 		
	} // get_user_info

	
	public function get_merchant_info($email)
	{		
		$this->db->select("*");
		$this->db->from("tblMerchantUsers");
		$this->db->where('email', $email);
		$query = $this->db->get();
		if($query->num_rows() == 1) {
			return $query->row();
		} else {
			return FALSE;
		} 		
	} // get_merchant_info
	
	
	public function add_activation_code($uid,$activation)
	{
		$data = array('activation' => $activation,
                      'status'     => 0
			          );		
		$this->db->where('userId', $uid);
		$this->db->update('tblUsers', $data);		
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 			 
	} // add_activation_code


	public function add_merchant_activation_code($uid,$activation)
	{
		$data = array('activation' => $activation,
                      'status'     => 0
			          );		
		$this->db->where('userId', $uid);
		$this->db->update('tblMerchantUsers', $data);		
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 			 
	} // add_merchant_activation_code
		
	
	public function get_profile_status($uid) 
	{
		$this->db->select('currency');
		$this->db->from('tblUsers');
		$this->db->where('userId', $uid);
		$query =$this->db->get();
    	if($query->num_rows() > 0) { 
			$row = $query->row();
			if(empty($row->currency))
			{
				return 0;
			} else {
				return 1;	
			}
		} else {
			return 0;
		}
    } // get_profile_status
	
	


}

