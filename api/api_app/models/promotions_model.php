<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promotions_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
	
	// Functions
	public function getPromotions(){
		$promotions = $this->db->get('tblPromotions');
		if($promotions->num_rows() > 0){
			return $promotions->result_array();
		} else {
			return NULL;
		}
	} // End - Get Promotions
	
	public function getPromotionCopouns($promotion_id){
		$this->db->where('status', 1);
		$this->db->where('promotionId', $promotion_id);
		$coupons = $this->db->get('tblPromotionCoupons');
		if($coupons->num_rows() > 0){
			return $coupons->result_array();
		} else {
			return NULL;
		}
	} // End - Get Promotion Coupons
	
	public function claimPromotionCopoun($promotion_id, $coupon_code){
		$this->db->where('promotionId', $promotion_id);
		$this->db->where('couponCode', $coupon_code);
		$query = $this->db->get('tblPromotionCoupons');
		if($query->num_rows() > 0){
			return $query->row();
		} else {
			return NULL;
		}
	}
	
	// Promotion Coupon Won transaction
	public function promotionCouponTransaction($data) 
	{
    	if($this->db->insert('tblTransactions',$data)) 
		{
			$transId = $this->db->insert_id();			
			
			$this->db->where('transId',$transId);
			$query = $this->db->get('tblTransactions');
			if($query->num_rows() > 0)
			{ 
				return $query->row();
			} else {
				return FALSE;
			}
		} else {
      		return FALSE;
    	}
	} // End - promotionCouponTransaction
	
	public function getPromotionsAccountId(){
		$this->db->select('PromotionsAccountId');
		$this->db->where('available', 1);
		$this->db->where('status', 1);
		$query = $this->db->get('tblPromotionsAccount');
		if($query->num_rows() > 0){
			return $query->row();
		} else {
			return NULL;
		}
	}
} // Contributions_model