<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
	// Save Invoice as Pending
	public function save_invoice($data){
		if($this->db->insert('tblMerchantInvoices', $data)){
                    return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}
	// Get All Pending invoices
	public function get_invoices($merchant_id, $status){
		$data = array('tblMerchantInvoices.merchantId' => $merchant_id,
		              'tblMerchantInvoices.status' => $status);
		$this->db->select('tblMerchantInvoices.id as invoice_id,
		                   tblMerchantInvoices.details,
						   tblMerchantInvoices.net_total,
						   tblUsers.firstName,
						   tblUsers.lastName,
						   tblMerchantInvoices.status');
		$this->db->from('tblMerchantInvoices');
		$this->db->join('tblUsers', 'tblUsers.userPin = tblMerchantInvoices.user_pin', 'inner');
		$this->db->where($data);
        $this->db->order_by("tblMerchantInvoices.dated", "desc"); 
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return NULL;
		}
	}
//	public function get_invoice_details($invoice_id){
//		
//		$this->db->select('mi.id as invoice_id, mi.details, mi.net_total, mu.merchant_title as merchant_title, mu.firstName as merchant_first_name, mu.lastName as merchant_last_name, mu.address as merchant_address,mu.city as merchant_city,mu.country as merchant_country, mu.phoneNumber as merchant_phone_number, u.firstName as user_first_name, u.lastName as user_last_name, mi.status');
//		$this->db->from('tblMerchantInvoices as mi');
//		$this->db->join('tblUsers as u', 'u.userPin = mi.user_pin', 'inner');
//		$this->db->join('tblMerchantUsers as mu', 'mu.userId = mi.merchantID', 'inner');
//		$this->db->where('id', $invoice_id);
//		$query = $this->db->get();
//		if($query->num_rows() > 0){
//			return $query->row_array();
//		} else {
//			return NULL;
//		}
//	}
	public function get_account_balance($merchant_id){
		
		$this->db->select('SUM(dr) as total');
		$this->db->select('SUM(cr) as credit_sum');
		$this->db->where('mode', "MIB");
		$this->db->where('refId', $merchant_id);
		$this->db->where('dr IS NOT NULL');
		
		$data = $this->db->get('tblTransactions')->row();
		if(!empty($data->total)){
			return $data;
		} else {
			return false;
		}	
	}
	public function my_currency_symbol($userId){
		$this->db->select('tblCurrency.symbol');
		$this->db->from('tblMerchantUsers');
		$this->db->join('tblCurrency', 'tblCurrency.code = tblMerchantUsers.currency');
		$this->db->where('tblMerchantUsers.userId', $userId);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->symbol;
		} else {
			return NULL;
		}
	}
	public function my_currency_code($userId){
		$this->db->select('tblMerchantUsers.currency AS currency_id,
		                   tblExchangeRates.currency_type
						  ');
		$this->db->from('tblMerchantUsers');
		$this->db->join('tblExchangeRates', 'tblExchangeRates.id = tblMerchantUsers.currency', 'inner');				  
        $this->db->where('tblMerchantUsers.userId', $userId);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->currency_type;
		} else {
			return NULL;
		}
	}
	/*
	public function insert_cashout($data){
		if($this->db->insert('tblCashInHandMerchant', $data)){
			return $this->db->insert_id();
		} else {
			return false;
		}
	}
	public function get_accounts_payable_id(){
		$this->db->select('id');
		$this->db->where('status', 1);
		$row = $this->db->get('tblAccountsPayable')->row();
		if(!empty($row)){
			return $row->id;
		} else {
			return false;
		}
	}
	*/
}