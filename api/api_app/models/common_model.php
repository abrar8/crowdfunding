<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
 	
	public function getNameByPin($userPin)
	{
		$this->db->select("firstName, lastName");
		$this->db->from('tblUsers');
		$this->db->where('userPin', $userPin); 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return $query->row();
		} else {
			return FALSE;
		}		
	} // getNameByPin
	public function getBasicInfo($userId)
	{
		$this->db->select("firstName, lastName, currency");
		$this->db->from('tblUsers');
		$this->db->where('userId', $userId); 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return $query->row();
		} else {
			return FALSE;
		}		
	} // getNameByPin
	public function user_balance($id)
   	{
		$this->db->where('userId',$id);
		$this->db->where('mode !=','DC');
		$this->db->where('mode !=','GC');
		$this->db->where('mode !=','CHC');
		$this->db->where('mode !=','MI');
		$this->db->where('mode !=','UB');
		$this->db->where('mode !=','CO');
		$this->db->select('userId, 
		                   SUM(cr) AS credit, 
						   SUM(dr) AS debit');
    	$data1 = $this->db->get('tblTransactions')->row();
		
		if(!empty($data1->userId))
		{
			$credit = $data1->credit;
			$debit = $data1->debit;
			
		} else { 
			$credit = '';
			$debit = '';
		}
		
		// Sum of Debit from DC (Daily Contribution)
		$this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
		$this->db->where('userId',$id);
		$this->db->where('mode','DC');
    	$data2 = $this->db->get('tblTransactions')->row();
		
		if(!empty($data2->userId)) 
		{
    		$debit_dc = $data2->debit;
			// Daily contribution balance
			$credit_dc = $data2->credit;
			
		} else {
			$debit_dc = 0.00;
			$credit_dc = 0.00;
		}
		
		// Sum of Debit from GC (Group Contribution)
		$this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
		$this->db->where('userId',$id);
		$this->db->where('mode','GC');
    	$data3 = $this->db->get('tblTransactions')->row();
		
		if(!empty($data3->userId)) 
		{
    		$debit_gc = $data3->debit;
			// Group contribution balance
			$credit_gc = $data3->credit;
			
		} else { 
			$debit_gc = 0.00;
			$credit_gc = 0.00;
		}
		
		// Sum of Debit from CHC (Charity Contribution)
		$this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
		$this->db->where('userId',$id);
		$this->db->where('mode','CHC');
    	$data4 = $this->db->get('tblTransactions')->row();
		
		if(!empty($data4->userId)) 
		{
			// My Total Charity Contributions
    		$debit_chc = $data4->debit;
			
		} else { 
			$debit_chc = 0.00;
		}
		
		// Sum of Debit from MI (Market Item Purchase Transaction)
		$this->db->select('userId, SUM(dr) AS debit');
		$this->db->where('userId',$id);
		$this->db->where('mode','MI');
    	$data5 = $this->db->get('tblTransactions')->row();
		
		if(!empty($data5->userId)) 
		{
			// My total item purchases
    		$debit_mi = $data5->debit;
			
		} else { 
			$debit_mi = 0.00;
		}
		
		// Sum of Debit from UB (Utility Bills Transactions)
		$this->db->select('userId, SUM(dr) AS debit');
		$this->db->where('userId',$id);
		$this->db->where('mode','UB');
    	$data6 = $this->db->get('tblTransactions')->row();
		
		if(!empty($data6->userId)) 
		{
			// My total item purchases
    		$debit_ub = $data6->debit;
			
		} else { 
			$debit_ub = 0.00;
		}
		
		// Sum of Debit from CO (CashOut Transactions)
		$this->db->select('userId, SUM(dr) AS debit');
		$this->db->where('userId',$id);
		$this->db->where('mode','CO');
    	$data7 = $this->db->get('tblTransactions')->row();
		
		if(!empty($data7->userId)) 
		{
			// My total item purchases
    		$debit_co = $data7->debit;
			
		} else { 
			$debit_co = 0.00;
		}
		
		$debited = $debit + $debit_dc + $debit_gc + $debit_chc + $debit_mi + $debit_ub + $debit_co;
		$balance = $credit - $debited;
		
		// Get charity credit
		$this->db->select(" SUM(tblTransactions.cr) AS credit");
		$this->db->from('tblTransactions');
		$this->db->join('tblCharityAccount', 'tblCharityAccount.id = tblTransactions.refId');
		$this->db->where('tblTransactions.mode', "CHC");
		$this->db->where('tblCharityAccount.userId', $id); 
		$query = $this->db->get();
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$credit_chc = $row->credit;
		} else {
			$credit_chc = '0.00';
		}
		// Get Market Item sales credit
		
		$data = array(
						'credit' 							=> $balance,
			            'debit'  							=> $debited,
						'total_daily_contributions_credit' 	=> $credit_dc,
						'total_group_contributions_credit'  => $credit_gc,
						'total_charities_debit'				=> $debit_chc,
						'total_my_created_charities_credit'	=> $credit_chc
				    );

		return $data;
		
	} // user_balance
	
	public function get_all_available_currencies(){
		$data = $this->db->get('tblCurrency')->result_array();
		if(!empty($data)){
			return $data;
		} else {
			return NULL;
		}
	}
	
	public function my_currency_symbol($userId){
		$this->db->select('tblCurrency.symbol');
		$this->db->from('tblUsers');
		$this->db->join('tblCurrency', 'tblCurrency.code = tblUsers.currency');
		$this->db->where('tblUsers.userId', $userId);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$row = $query->row();
			return $row->symbol;
		} else {
			return NULL;
		}
	}
	
}