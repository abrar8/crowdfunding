<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cashout_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    // Public functions
    // Insert Cash Out
    public function insert_cashout($data) {
        if ($this->db->insert('tblDepositAccounts', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }
	
	 public function insert_cashoutagent($data) {
        if ($this->db->insert('tblCashoutPin', $data)) {
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    // Cash Out Transaction
    public function cashout_transaction($userId, $debit, $credit) {

        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblTransactions');

        $this->db->insert('tblTransactions', $credit);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    // Cash Out Transaction
    public function get_charges($type) {


        $this->db->where('charges_type', $type);
        $query = $this->db->select('start_amount_range, end_amount_range, charges');
        $query = $this->db->get('tblCharges');

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    // My Cash In Hand
    public function get_my_cash_in_hand($userId) {
        $this->db->select('SUM(dr) as total');
        $this->db->where('mode', "CO");
        $this->db->where('refId', $userId);
        $this->db->where('userId', $userId);
        $this->db->where('dr IS NOT NULL');

        $data = $this->db->get('tblTransactions')->row();
        if (!empty($data)) {
            return $data;
        } else {
            return false;
        }
    }

    // Get Accounts Payable ID
    public function get_accounts_payable_id() {
        $this->db->select('id');
        $this->db->where('status', 1);
        $row = $this->db->get('tblAccountsPayable')->row();
        if (!empty($row)) {
            return $row->id;
        } else {
            return false;
        }
    }

    public function get_accounts_payable_co() {

        /* $this->db->select('id');
          $this->db->where('status', 1);
          $this->db->where('available', 1);
          $row = $this->db->get('tblAccountsPayable')->row();
          if(!empty($row)){
          $this->db->select('id');
          $this->db->where('mode', "CO");
          $this->db->where('cr !=', 0.00);
          $this->db->where('refId', $row->id);
          }
         */
    }

}
