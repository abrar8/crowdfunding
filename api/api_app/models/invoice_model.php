<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}


	public function get_invoice_details($invoice_id){
		
		$this->db->select('mi.id as invoice_id, mi.details, mi.net_total, mu.merchant_title as merchant_title, mu.firstName as merchant_first_name, mu.lastName as merchant_last_name, mu.address as merchant_address,mu.city as merchant_city,mu.country as merchant_country, mu.phoneNumber as merchant_phone_number, u.firstName as user_first_name, u.lastName as user_last_name, mi.status');
		$this->db->from('tblMerchantInvoices as mi');
		$this->db->join('tblUsers as u', 'u.userPin = mi.user_pin', 'inner');
		$this->db->join('tblMerchantUsers as mu', 'mu.userId = mi.merchantID', 'inner');
		$this->db->where('id', $invoice_id);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return NULL;
		}
	}
	
	
}