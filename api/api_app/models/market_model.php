<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Market_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
 	// Add new Market Item (Product)
	public function add_item($data){
		$status = $this->db->insert('tblMarket', $data);
		if(!empty($status)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	public function addGallery($item_id, $gallery_image){
		
	}
	// Get all Market Items (Products)
	public function get_all_items(){
		$this->db->select('id, item_name, description, item_image, price');
		$this->db->where('deleted', NULL);
		$this->db->where('available', 1);
		$data = $this->db->get('tblMarket')->result_array();
		if(!empty($data)){
			return $data;
		} else {
			return NULL;
		}
	}
	// Get all My Added Market Items (Products)
	public function get_all_my_items($userId){
		$this->db->select('id, item_name, description, item_image, price');
		$this->db->where('deleted', NULL);
		$this->db->where('available', 1);
		$this->db->where('userId', $userId);
		$data = $this->db->get('tblMarket')->result_array();
		if(!empty($data)){
			return $data;
		} else {
			return NULL;
		}
	}
	// Remove Market Item (Product) By Id
	public function remove_item($item_id){
		$data = array('deleted' => 1);
		$this->db->where('id', $item_id);
		$status = $this->db->update('tblMarket', $data);
		if(!empty($status)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	// Get filtered search records (Market Items - Products)
	public function get_search_items_filtered_records($data){
		
		$select = ' SELECT *';
		$from = ' FROM tblMarket';
		
		if($data['price_from'] != '' || $data['price_to'] != '' || $data['keywords'] != '' || $data['category'] != ''){
			$condition = ' WHERE deleted IS NULL AND available=1 AND ';
		} else {
			$condition = '';
			$condition = ' WHERE deleted IS NULL AND available=1 ';
		}
		
		$count = 0;
		
		if($data['keywords'] != ''){
			$exploded_keywords = explode(',', $data['keywords']);
			
			$query_parts = array();
			foreach ($exploded_keywords as $val) {
				$query_parts[] = "'%".mysql_real_escape_string($val)."%'";
			}

			$string1 = implode(' OR item_name LIKE ', $query_parts);
			$string2 = implode(' OR description LIKE ', $query_parts);

			if($count > 0){
				$condition .= ' AND ';
			}
			//$condition .= ' item_name LIKE "%'.$data['keywords'].'%" OR '.'description LIKE "%'.$data['keywords'].'%"';
			$condition .= " item_name LIKE {$string1} OR description LIKE {$string2}";
			$count++;
		}
		if($data['category'] != ''){
				
			if($count > 0){
				$condition .= ' AND ';
			}
			$condition .= ' category LIKE "%'.$data['category'].'%"';
			$count++;
		}
		if($data['price_from'] != '' && $data['price_to'] != ''){
			
			if($count > 0){
				$condition .= ' AND ';
			}
			$condition .= ' price BETWEEN '.$data['price_from'].' AND '.$data['price_to'];
			$count++;
			
		} else {
			if($data['price_from'] != ''){
				
				if($count > 0){
					$condition .= ' AND ';
				}
				$condition .= ' price >= '.$data['price_from'];
				$count++;
			}
			if($data['price_to'] != ''){
				
				if($count > 0){
					$condition .= ' AND ';
				}
				$condition .= ' price <= '.$data['price_to'];
				$count++;
			}
		}
		
		$query_string = $select.$from.$condition;
		
		$result = $this->db->query($query_string)->result_array();
		return $result;
	}
	// Get Single Item (Product)
	public function get_item($item_id){
		
		$this->db->where('id', $item_id);
		$data = $this->db->get('tblMarket')->result_array();
		if(!empty($data)){
			$contact_direct = 0;
			if($data[0]['contact_direct'] == 1){
				$this->db->select('firstName as first_name, lastName as last_name, phoneNumber as phone_number, userPIN as user_pin, email');
				$this->db->where('userId', $data[0]['userId']);
				$user_contact_details = $this->db->get('tblUsers')->result_array();
				$contact_direct = 1;
			}
			$result_array = array();
			if($contact_direct == 1){
				$result_array = array('item' => $data, 'contact_details' => $user_contact_details);
			} else {
				$result_array = array('item' => $data, 'contact_details' => NULL);
			}
			return $result_array;
		} else {
			return NULL;
		}
	}
	// Get Item Price (Product)
	public function get_item_price($item_id){
		$this->db->select('price');
		$this->db->where('id', $item_id);
		$data = $this->db->get('tblMarket')->row();
		if(!empty($data)){
			return $data;
		} else {
			return NULL;
		}
	}
	// Market Item Buy Transaction
	public function itemBuyTransaction($userId, $debit, $credit){
		
		$this->db->insert('tblTransactions', $debit);
		$insert_id = $this->db->insert_id();	
		
		$this->db->where('transId', $insert_id);
		$this->db->where('userId', $userId);
		$query = $this->db->get('tblTransactions');
		
		$this->db->insert('tblTransactions', $credit);
		if($query->num_rows() > 0){
			return $query->row();					
		} else {
			return false;
		}
	} // End - itemBuyTransaction
	// Get Unique Charity Image Name
	public function get_unique_product_image_name(){
		$image_name = 'item_'.rand(1,99999).'.png';
		$unique_image_name = $this->get_product_image_name($image_name);
		return $unique_image_name;
	}
	
	public function get_product_image_name($image_name){
		$this->db->where('item_image', $image_name);
		$query = $this->db->get('tblMarket');
		if($query->num_rows() > 0){
			$this->get_unique_product_image_name();
		} else {
			return $image_name;
		}
	} // 
} // Contributions_model