<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signin_model extends CI_Model {
	function __construct() {
    	parent::__construct();
  	} 

	
	public function does_user_exist($login) 
	{  
		$this->db->select('*');
		$this->db->from('tblUsers');
		$login_data = array('userPIN'      => $login['userPIN'],
		                    'userPassword' => $this->encrypt->sha1($login['userPassword'])
						   );
		$this->db->where($login_data); 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			$row = $query->row();
			
			/////////////////////// Check Device ID START /////////////////////////
			$this->db->select('id');
			$this->db->from('tblDevices');
			$this->db->where('uid',$row->userId); 
			$query = $this->db->get();
			if($query->num_rows() == 1)
			{	
				$data = array('device_id'   => addslashes($login['device_id']),
							  'device_type' => addslashes($login['device_type'])
							  ); 
				$this->db->where('uid', $row->userId);
				$this->db->update('tblDevices', $data);				
			} else {
				$data = array('device_id'   => addslashes($login['device_id']),
						  	  'device_type' => addslashes($login['device_type']),
							  'uid'         => $row->userId
						     ); 			
				$this->db->insert('tblDevices', $data);
			}
			////////////////////// Check Device ID END ///////////////////////////	
					
			return $row;
		} else {			
			return FALSE;
		}
  	} // does_user_exist
	
	public function does_merchant_exist($login) 
	{  
		$this->db->select('*');
		$this->db->from('tblMerchantUsers');
		$login_data = array('userPIN'      => $login['userPIN'],
		                    'userPassword' => $this->encrypt->sha1($login['userPassword'])
						   );
		$this->db->where($login_data); 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			$row = $query->row();
			
			/////////////////////// Check Device ID START /////////////////////////
			$this->db->select('id');
			$this->db->from('tblDevices');
			$this->db->where('uid',$row->userId); 
			$query = $this->db->get();
			if($query->num_rows() == 1)
			{	
				$data = array('device_id'   => addslashes($login['device_id']),
							  'device_type' => addslashes($login['device_type'])
							  ); 
				$this->db->where('uid', $row->userId);
				$this->db->update('tblDevices', $data);				
			} else {
				$data = array('device_id'   => addslashes($login['device_id']),
						  	  'device_type' => addslashes($login['device_type']),
							  'uid'         => $row->userId
						     ); 			
				$this->db->insert('tblDevices', $data);
			}
			////////////////////// Check Device ID END ///////////////////////////	
					
			return $row;
		} else {			
			return FALSE;
		}
  	} // does_merchant_exist
	
	public function profile_status($userId){
		$this->db->where('userId', $userId);
		$response = $this->db->get('tblUsers')->row();
		if(!empty($response)){
			if($response->address != NULL && $response->country != NULL && $response->city != NULL && $response->currency != NULL){
				$this->db->where('userId', $userId);
				$bankAccount = $this->db->get('tblBankAccounts')->result();
				if(!empty($bankAccount)){
					return "1";
				} else {
					return "0";
				}
			} else {
				return "0";
			}
		} else {
			return "Error: User does not exist.";
		}
	}
	
	public function merchant_profile_status($userId){
		$this->db->where('userId', $userId);
		$response = $this->db->get('tblMerchantUsers')->row();
		if(!empty($response)){
			if($response->address != NULL && $response->country != NULL && $response->city != NULL && $response->currency != NULL){
				$this->db->where('tblMerchantUsers', $userId);
				$bankAccount = $this->db->get('tblBankAccounts')->result();
				if(!empty($bankAccount)){
					return "1";
				} else {
					return "0";
				}
			} else {
				return "0";
			}
		} else {
			return "Error: User does not exist.";
		}
	}


}

