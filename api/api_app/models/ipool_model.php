<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ipool_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
 			
	public function create_ipool($data) 
	{
		$this->db->insert('tblIpool', $data);
		return $this->db->insert_id();
	} // create_ipool


	public function get_ipool_list($uid) 
	{
        $this->db->select('*');
        $this->db->from('tblIpool');
        $this->db->where('uid', $uid);
		$this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // get_ipool_list 
	
	
	public function get_ipool_info($id) 
	{
        $this->db->select('*');
        $this->db->from('tblIpool');
        $this->db->where('id', $id);
		$this->db->order_by('id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    } // get_ipool_info 
	
	
	public function send_invitation($data) 
	{
		$this->db->insert('tblIpoolInvitation', $data);
		return $this->db->insert_id();
	} // send_invitation
	
		
	public function pending_invitation($uid) 
	{
        $this->db->select('tblIpoolInvitation.id,
		                   tblIpoolInvitation.status,
						   tblIpoolInvitation.datetime AS invited_date,
						   tblIpool.title,
						   tblIpool.amount,
						   tblIpool.frequency,
						   tblIpool.max_people,
						   tblIpool.start_date						   
						  ');
        $this->db->from('tblIpoolInvitation');
		$this->db->join('tblIpool', 'tblIpool.id = tblIpoolInvitation.ipool_id', 'inner');
        $this->db->where('tblIpoolInvitation.uid', $uid);
		$this->db->where('tblIpoolInvitation.status', 1);
		$this->db->order_by('tblIpoolInvitation.id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // pending_invitation
	
	
	public function requestResponse($data,$invitation_id)
	{
		$this->db->where('id', $invitation_id);			 
		$this->db->update('tblIpoolInvitation', $data);
	 	return ($this->db->affected_rows()>0) ? TRUE : FALSE;	
	} // requestResponse 
	
	
	public function participation($uid) 
	{
        $this->db->select('tblIpoolInvitation.id,
		                   tblIpoolInvitation.status,
						   tblIpoolInvitation.datetime AS invited_date,
						   tblIpool.title,
						   tblIpool.amount,
						   tblIpool.frequency,
						   tblIpool.max_people,
						   tblIpool.start_date						   
						  ');
        $this->db->from('tblIpoolInvitation');
		$this->db->join('tblIpool', 'tblIpool.id = tblIpoolInvitation.ipool_id', 'inner');
        $this->db->where('tblIpoolInvitation.uid', $uid);
		$this->db->order_by('tblIpoolInvitation.id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // participation
	
	
	public function my_ipool_participants($ipool)
	{
		$this->db->select('tblIpoolInvitation.uid,
		                   tblUsers.firstName,
						   tblUsers.lastName,
						   tblUsers.userPIN                  		   
						  ');
        $this->db->from('tblIpoolInvitation');
		$this->db->join('tblUsers', 'tblUsers.userId = tblIpoolInvitation.uid', 'inner');
		$this->db->where('tblIpoolInvitation.ipool_id', $ipool);
		$this->db->where('tblIpoolInvitation.status', 2);
		$this->db->order_by('tblIpoolInvitation.id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // my_ipool_participants
	
	
	public function get_friendlist($uid) 
	{
        $this->db->select('id');
        $this->db->where('uid', $uid);
		$this->db->where('status', 2);
        $uid_data = $this->db->get('tblFriends')->result_array();

        $this->db->select('id');
        $this->db->where('friend_id', $uid);
		$this->db->where('status', 2);
        $friend_id_data = $this->db->get('tblFriends')->result_array();

        $data1 = array();
        $data2 = array();

        if (!empty($uid_data)) {
            foreach ($uid_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblStatus.name AS status		                   
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.friend_id');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row["id"]);
                $this->db->order_by("tblUsers.firstName", "ASC");
                $data1[] = $this->db->get()->row();
            } // foreach
        }

        if (!empty($friend_id_data)) {
            foreach ($friend_id_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblStatus.name AS status		                   
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.uid');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row["id"]);
                $this->db->order_by("tblUsers.firstName", "ASC");
                $data2[] = $this->db->get()->row();
            } // foreach
        }
        $result = array_merge($data1, $data2);
        return $result;
    } // get_friendlist
	
	
	public function isIpoolAcceptedMem($ipool,$uid)
	{
		$this->db->select("id,
		                   status	                   
						  ");
		$this->db->from('tblIpoolInvitation');
		$this->db->where('ipool_id', $ipool);
		$this->db->where('uid', $uid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            $row = $query->row();
			return $row->status;			
        } else {
            return 4;
        }		
	}
	
	
	public function get_ipool_id($invitation_id) 
	{
        $this->db->select('ipool_id');
        $this->db->from('tblIpoolInvitation');
        $this->db->where('id', $invitation_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
			$row = $query->row();
			return $row->ipool_id;
        } else {
            return NULL;
        }
    } // get_ipool_id
	
	
	
	
} // Ipool_model