<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Crowdfund_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    //echo $this->db->last_query(); exit();
	
	
	public function create_crowdfund($data) 
	{
        $this->db->insert('tblCrowdfund', $data);		
        return ($this->db->affected_rows()>0) ? TRUE : FALSE;	
    } // create_crowdfund
		
	
	public function getCrowdfund($user_id) {
		$query = $this->db->query("SELECT IF(tblCrowdfund.userId =".$user_id.", 1, 2) AS crowdfund_flag,
                                   tblCrowdfund.id,
							       tblCrowdfund.reference_number,
								   tblCrowdfund.title, 
								   tblCrowdfund.description, 
								   tblCrowdfund.image, 
								   tblCrowdfund.status, 
								   tblUsers.firstName, 
								   tblUsers.lastName 
								FROM tblCrowdfund
								LEFT JOIN tblUsers ON 
									tblCrowdfund.userId = tblUsers.userId 
								WHERE tblCrowdfund.status = 1");
        $data = array();
        foreach($query->result() as $crowdfund)
		{
            $crowdfund->invitation_flag = $this->getCrowdfundInvitationStatus($user_id,$crowdfund->reference_number);
            $data[] = $crowdfund;
        }        
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // getCrowdfund
	
	
	public function getCrowdfundInvitationStatus($user_id,$crowdfund_ref_num)
	{
        $query = $this->db->query("SELECT * FROM tblCrowdfundInvitations WHERE (user_id=".$user_id." AND crowdfund_ref_num='".$crowdfund_ref_num."')");    
        if($query->num_rows()==1)
		{
            $res = $query->row_array();
            return $res['status'];
        } else {
            return 4;
        }        
    } // getCrowdfundInvitationStatus
	
	
	public function getMyCrowdfund($userId) 
	{ 
		// List All Created by Me + Status = 0 and 1
        $this->db->select("tblCrowdfund.id,
                           tblCrowdfund.reference_number,
		                   tblCrowdfund.title,
						   tblCrowdfund.description,
						   tblCrowdfund.image,
						   tblCrowdfund.status,
						   tblUsers.firstName,
						   tblUsers.lastName
						  ");
        $this->db->from('tblCrowdfund');
        $this->db->join('tblUsers','tblCrowdfund.userId = tblUsers.userId','inner');
        $this->db->where('tblCrowdfund.userId', $userId);
        $this->db->where('tblCrowdfund.status', 1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // getMyCrowdfund
	
	
	public function getPendingInvitations($sender_id) 
	{
        $this->db->select('tblUsers.firstName,
		                   tblUsers.lastName,
		                   tblCrowdfundInvitations.sender_id,
						   tblCrowdfundInvitations.id AS invitation_id,
                           tblCrowdfund.id AS crowdfund_id,
						   tblCrowdfund.title,
						   tblCrowdfund.description,
						   tblCrowdfund.image,
						   tblCrowdfund.reference_number
						  ');
        $this->db->from('tblCrowdfundInvitations');
        $this->db->join('tblUsers', 'tblCrowdfundInvitations.sender_id = tblUsers.userId');
        $this->db->join('tblCrowdfund', 'tblCrowdfundInvitations.crowdfund_id = tblCrowdfund.id');
		$this->db->where('tblCrowdfundInvitations.user_id', $sender_id);
        $this->db->where('tblCrowdfundInvitations.status', 1);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // getPendingInvitations
	
	
	public function removeCrowdfund($userId, $reference_number)
	{ 
		// Remove Crowdfund Created by Me with (reference Number & token->user_id)
        $this->db->where('userId', $userId);
        $this->db->where('reference_number', $reference_number);
        $query = $this->db->get('tblCrowdfund');
        if ($query->num_rows() > 0) 
		{
            $data = array('status' => 5);
            $this->db->where('userId', $userId);
            $this->db->where('reference_number', $reference_number);
            $update = $this->db->update('tblCrowdfund', $data);
            if (!empty($update)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    } // removeCrowdfund

	
	public function myDonations($userId) 
	{
        $this->db->select("tblTransactions.dr as amount,
                           tblCrowdfund.id,
		                   tblCrowdfund.reference_number,
						   tblCrowdfund.title,
						   tblCrowdfund.description
						  ");
        $this->db->from('tblTransactions');
        $this->db->join('tblCrowdfund', 'tblTransactions.refId = tblCrowdfund.id', 'inner');
        $this->db->where('tblTransactions.userId', $userId);
        $this->db->where('tblTransactions.refId !=', $userId);
        $this->db->where('tblTransactions.cr', 0.00);
        $this->db->where('tblTransactions.mode', "CF");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // myDonations
	
	
	public function getCrowdfundId($reference_number) 
	{
        $this->db->select('id');
        $this->db->where('reference_number', $reference_number);
        $query = $this->db->get('tblCrowdfund');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->id;
        } else {
            return NULL;
        }
    } // getCrowdfundId
	
	
	public function crowdfundTransaction($userId, $debit, $credit, $reference_number)
	{
        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();
		
        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblTransactions');

        $this->db->insert('tblTransactions', $credit);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    } // crowdfundTransaction
	
	
	public function crowdfund_detail($reference_number) 
	{
        $this->db->select('id');
        $this->db->where('reference_number', $reference_number);
        $crowdfund_id = $this->db->get('tblCrowdfund')->result();
        if (!empty($crowdfund_id[0]->id)) {
            $this->db->select('u.firstName, 
			                   u.lastName, 
							   t.dr as amount_paid,
							   t.description, 
							   t.created
							  ');
            $this->db->from('tblTransactions as t');
            $this->db->join('tblUsers as u', 't.userId = u.userId');
            $this->db->where('t.type', "TR");
            $this->db->where('t.mode', "CHC");
            $this->db->where('t.refId', $crowdfund_id[0]->id);
            $this->db->where('t.dr IS NOT NULL');
            $this->db->where('t.cr', 0.00);
            $result = $this->db->get()->result_array();
            if (!empty($result)) {
                return $result;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    } // crowdfund_detail
	
	
	public function get_total_crowdfund_amount($reference_number) 
	{
        $this->db->select('id');
        $this->db->where('reference_number', $reference_number);
        $charity_id = $this->db->get('tblCrowdfund')->result();
        if (!empty($charity_id[0]->id)) {
            $this->db->select('sum(t.dr) as total');
            $this->db->from('tblTransactions as t');
            $this->db->join('tblUsers as u', 't.userId = u.userId');
            $this->db->where('t.type', "TR");
            $this->db->where('t.mode', "CHC");
            $this->db->where('t.refId', $charity_id[0]->id);
            $this->db->where('t.dr IS NOT NULL');
            $this->db->where('t.cr', 0.00);
            $row = $this->db->get()->row();
            if (!empty($row)) {
                return $row->total;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    } // get_total_crowdfund_amount
	
	
	public function getMyCrowdfundDetail($crowdfund_id) 
	{ 
        $this->db->select("tblCrowdfund.id,
		                   tblCrowdfund.reference_number,
						   tblCrowdfund.title,
						   tblCrowdfund.description,
						   tblCrowdfund.image,
						   tblCrowdfund.status,
						   tblUsers.firstName,
						   tblUsers.lastName
						  ");
        $this->db->from("tblCrowdfund");
        $this->db->join("tblUsers", "tblCrowdfund.userId = tblUsers.userId", "inner");
		$this->db->where("tblCrowdfund.id", $crowdfund_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    } // getMyCrowdfundDetail
	
	
	public function inviteToCrowdfund($senderId,$userId,$crowdfundId,$reference_number) 
	{
        $this->db->select("*");
        $this->db->from("tblCrowdfundInvitations");
        $this->db->where("crowdfund_ref_num", $reference_number);
        $this->db->where("user_id", $userId);
        $this->db->where("crowdfund_id", $crowdfundId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return FALSE;
        } else {
            $data = array('sender_id'         => $senderId,
                          'user_id'           => $userId,
                          'crowdfund_id'      => $crowdfundId,
                          'crowdfund_ref_num' => $reference_number
                         );
            $this->db->insert('tblCrowdfundInvitations', $data);
            return ($this->db->affected_rows()>0) ? TRUE : FALSE;
        }
    } // inviteToCrowdfund
	
	
	public function responseToInvitation($invitation_id,$status) 
	{
        $data = array('status' => $status);
        $this->db->where('id', $invitation_id);
        $res = $this->db->update('tblCrowdfundInvitations', $data);
        if ($res) {
            $this->db->select('crowdfund_id');
            $this->db->from('tblCrowdfundInvitations');
			$this->db->where('id', $invitation_id);
			$query = $this->db->get();
			if ($query->num_rows() > 0) {
				$row = $query->row();
				return $row->crowdfund_id;
			} else {
				return FALSE;
			}
        } else {
            return FALSE;
        }
    } // responseToInvitation
	
	
	public function getSenderId($id)
	{
        $this->db->select('sender_id');
		$this->db->from('tblCrowdfundInvitations');
        $this->db->where('id',$id);
        $query = $this->db->get();
		if ($query->num_rows() > 0) {
			$row = $query->row();
			return $row->sender_id;
		} else {
			return FALSE;
		}       
    } // getSenderId
	
	
	public function get_friendlist($uid) 
	{
        $this->db->select('id');
        $this->db->where('uid', $uid);
		$this->db->where('status', 2);
        $uid_data = $this->db->get('tblFriends')->result_array();

        $this->db->select('id');
        $this->db->where('friend_id', $uid);
		$this->db->where('status', 2);
        $friend_id_data = $this->db->get('tblFriends')->result_array();

        $data1 = array();
        $data2 = array();

        if (!empty($uid_data)) {
            foreach ($uid_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblStatus.name AS status		                   
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.friend_id');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row["id"]);
                $this->db->order_by("tblUsers.firstName", "ASC");
                $data1[] = $this->db->get()->row();
            } // foreach
        }

        if (!empty($friend_id_data)) {
            foreach ($friend_id_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblStatus.name AS status		                   
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.uid');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row["id"]);
                $this->db->order_by("tblUsers.firstName", "ASC");
                $data2[] = $this->db->get()->row();
            } // foreach
        }
        $result = array_merge($data1, $data2);
        return $result;
    } // get_friendlist
	
	
	public function isCrowdfundAcceptedMem($crowdfund_id,$uid)
	{
		$this->db->select("id,
		                   status,
                           crowdfund_id	                   
						  ");
		$this->db->from('tblCrowdfundInvitations');
		$this->db->where('crowdfund_id', $crowdfund_id);
		$this->db->where('user_id', $uid);
		$query = $this->db->get();
		if ($query->num_rows() > 0) {
            $row = $query->row();
			return $row->status;			
        } else {
            return 4;
        }		
	} // isCrowdfundAcceptedMem
	
	
	
	

} // Crowdfund_model