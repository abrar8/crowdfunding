<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Refund_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}


	public function get_refund_list($uid)
	{
		$this->db->select('*');
		$this->db->from('tblrefund');
		$this->db->where('uid', $uid);
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row();
		} else {
			return NULL;
		}
	} // get_refund_list
	
	
} // Refund_model