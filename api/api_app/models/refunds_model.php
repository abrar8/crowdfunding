<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Refunds_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function get_user_refunds($uid) {
        $this->db->select('*');
        $this->db->from('tblrefund');
        $this->db->where('uid', $uid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

// get_user_refunds

    public function get_merchant_refunds($merchant_id) {
        $this->db->select('*');
        $this->db->from('tblrefund');
        $this->db->where('merchant_id', $merchant_id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_merchant_refunds_list($merchant_id, $status) {
        $this->db->select('*');
        $this->db->from('tblrefund');
        $this->db->where('merchant_id', $merchant_id);
        $this->db->where('status', $status);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

// get_merchant_refunds


    public function create_refund($data) {
        $this->db->insert('tblrefund', $data);
        return $this->db->insert_id();
    }

// create_refund	

    public function get_invoice_detail($id) {
        $this->db->select('*');
        $this->db->from('tblMerchantInvoices');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    }

// get_invoice_detail

    public function get_refund($id) {
        $this->db->select('*');
        $this->db->from('tblrefund');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    }

// get_refund


    public function responseToRefund($merchant_id) {
        
        $data = array(
            'status' => $this->input->post('status')
        );
        $this->db->where('id', $this->input->post('id'));
        $this->db->where('merchant_id', $merchant_id);
        $res = $this->db->update('tblrefund', $data);
        
        if ($res) {
            $this->db->select('tblrefund.*,tblMerchantUsers.merchant_title');
            $this->db->from('tblrefund');
            $this->db->join('tblMerchantUsers', 'tblrefund.merchant_id=tblMerchantUsers.userId');
            $this->db->where('tblrefund.id', $this->input->post('id'));
            $result = $this->db->get();
            $result = $result->row();

            return $result;
        } else {
            return FALSE;
        }
    }

    public function refundInvoiceBillTransaction($userId, $debit, $credit, $invoice_id) {

        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblTransactions');

        $response = $this->db->insert('tblTransactions', $credit);
        if ($response) {

            // Update status that the invoice bill is refunded
            $data = array('status' => 4);
            $this->db->where('id', $invoice_id);
            $this->db->update('tblMerchantInvoices', $data);
            return $query->row();
        } else {
            return false;
        }
    }
    
    public function getInvoiceAmount($invoice_no,$merchant_id){
        
        $this->db->select('net_total');
        $this->db->where('status',2);
        $this->db->where('merchantid',$merchant_id);
        $this->db->where('id',$invoice_no);
        $result = $this->db->get('tblMerchantInvoices');
        $re = $result->row();
        return $re->net_total;
         
    }

}

// Refunds_model