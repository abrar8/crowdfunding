<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utility_bills_model extends CI_Model {
	// constructor
	function __construct() {
    	parent::__construct();
	}
	public function getProviderTypes(){
		$this->db->select('id, type');
		$this->db->where('status', 1);
		$query = $this->db->get('tblProviderTypes');
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return NULL;
		}
	}
	
	// Providers List by type
	public function getProviderList($type){
		$this->db->where('type', $type);
		$this->db->where('status', 1);
		$query = $this->db->get('tblProviders');
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return NULL;
		}
	}
	
	public function saveBill($data){
		if($this->db->insert('tblUtilityBills', $data)){
                    return $this->db->insert_id();
		} else {
			return false;
		}
	}
	
	public function getMyBills($userId){
		
		$this->db->select('tblUtilityBills.id, tblProviders.name as provider_name, tblProviderTypes.type as provider_type, tblUtilityBills.customer_id, tblUtilityBills.comments, tblUtilityBills.dated,tblUtilityBills.paid');
		$this->db->from('tblUtilityBills');
		$this->db->join('tblProviders', 'tblProviders.id = tblUtilityBills.provider_id', 'inner');
		$this->db->join('tblProviderTypes', 'tblProviderTypes.id = tblProviders.type', 'inner');
		$this->db->where('tblUtilityBills.userId', $userId);
		$this->db->where('tblUtilityBills.paid', 0);
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return NULL;
		}
	}
	
	public function get_utility_bills_account_id(){
		$this->db->select('id');
		$this->db->where('available', 1);
		$query = $this->db->get('tblUtilityBillsAccount');
		if($query->num_rows() > 0){
			$data = $query->result();
			return $data[0]->id;
		} else {
			return null;
		}
	}
	
	public function update_bill($bill_id, $upd_data){
		$this->db->where('id', $bill_id);
		$update = $this->db->update('tblUtilityBills', $upd_data);
		if($update){
			return true;
		} else {
			return false;
		}
	}
	public function payBill($userId, $debit, $credit){
		
		$this->db->insert('tblTransactions', $debit);
		$insert_id = $this->db->insert_id();	
		
		$this->db->where('transId', $insert_id);
		$this->db->where('userId', $userId);
		$query = $this->db->get('tblTransactions');
		
		$this->db->insert('tblTransactions', $credit);
		if($query->num_rows() > 0){
			return $query->row();					
		} else {
			return false;
		}
	}
	public function varify_bill_id($bill_id){
		$this->db->where('id', $bill_id);
		$query = $this->db->get('tblUtilityBills');
		if($query->num_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
	public function myPaidBillsHistory($userId){

		$this->db->select('tblUtilityBills.id, tblProviders.name as provider_name, tblProviderTypes.type as provider_type, tblUtilityBills.customer_id, tblUtilityBills.comments, tblUtilityBills.dated,tblUtilityBills.paid, tblUtilityBills.transaction_track_code');
		$this->db->from('tblUtilityBills');
		$this->db->join('tblProviders', 'tblProviders.id = tblUtilityBills.provider_id', 'inner');
		$this->db->join('tblProviderTypes', 'tblProviderTypes.id = tblProviders.type', 'inner');
		$this->db->where('tblUtilityBills.userId', $userId);
		$this->db->where('tblUtilityBills.paid', 1);
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return NULL;
		}
	}
	public function varify_provider_id($provider_id){
		$this->db->where('id', $provider_id);
		$query = $this->db->get('tblProviders');
		if($query->num_rows() > 0){
			return true;
		} else {
			return false;
		}
	}
	/*
	// Methods
	public function list_utility_companies(){
		$query = $this->db->get('tblUtilityCompanies');
		if($query->num_rows() > 0){
			return $query->result_array();
		} else {
			return NULL;
		}
	}
	
	// Claim Utility Bill
	public function claim_utility_bill_transaction($userId, $debit, $credit){
		
		$this->db->insert('tblTransactions', $debit);
		$insert_id = $this->db->insert_id();	
		
		$this->db->where('transId', $insert_id);
		$this->db->where('userId', $userId);
		$query = $this->db->get('tblTransactions');
		
		$this->db->insert('tblTransactions', $credit);
		if($query->num_rows() > 0){
			return $query->row();					
		} else {
			return false;
		}
	}
	
	// Claim Utility Bill Entry
	public function claim_utility_bill_entry($data){
		if($this->db->insert('tblUtilityBills', $data)){
			return true;
		} else {
			return false;
		}
	}
 	*/
} // End - Utility_bills_model