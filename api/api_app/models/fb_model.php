<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Fb_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }


    public function addFbAccessToken($fbData) {
        $this->db->select("*");
        $this->db->from('tblFbToken');
        $this->db->where('uid', $fbData['uid']);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            /////////////////////////////////////////////////////////	
            $data = array('access_token' => $fbData['access_token']);
            $this->db->where('uid', $fbData['uid']);
            $this->db->update('tblFbToken', $data);
            ////////////////////////////////////////////////////////
            return TRUE;
        } else {
            /////////////////////////////////////////////////////////	
            $data = array('access_token' => $fbData['access_token'],
						  'uid'          => $fbData['uid']
						  );
            $this->db->insert('tblFbToken', $data);
            ////////////////////////////////////////////////////////
            return FALSE;
        }
    } // addFbAccessToken
	
	
	public function friendship($uid,$friend_id)
	{
		 $query = $this->db->query("SELECT * FROM tblFriends
						            WHERE (uid=$uid AND friend_id=$friend_id AND status='2') 
									OR (uid=$friend_id AND friend_id=$uid AND status='2')");
		//echo $this->db->last_query();
		//exit();							
        if ($query->num_rows() == 1) {
            return FALSE;
        } else {
            return TRUE;
        }
	} // friendship
	
	
	
	
} // Fb_model