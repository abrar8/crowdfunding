<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class authenticate_model extends CI_Model {
	function __construct() {
    	parent::__construct();
  	} 

  	public function is_valid_api_key($key) 
	{  
    	$this->db->where('api_key', $key);
    	$query = $this->db->get('tblUsers');
    	if($query->num_rows() > 0) 
			return $query->row();
		else 
			return FALSE;	
  	} // is_valid_api_key
	
	
  	public function is_valid_merchant_api_key($key) 
	{
    	$this->db->where('api_key', $key);
    	$query = $this->db->get('tblMerchantUsers');
    	if($query->num_rows() > 0) 
			return $query->row();
		else 
			return FALSE;	
  	} // is_valid_api_key
	
	
	public function is_valid_agent_api_key($key) 
	{
    	$this->db->where('api_key', $key);
    	$query = $this->db->get('tblAgents');
    	if($query->num_rows() > 0) 
			return $query->row();
		else 
			return FALSE;	
  	} // is_valid_agent_api_key
  
  
}

