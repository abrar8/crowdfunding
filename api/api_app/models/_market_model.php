<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Market_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
 	// Add new Market Item (Product)
	public function add_item($data){
		$status = $this->db->insert('tblmarket', $data);
		if(!empty($status)){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	// Get all Market Items (Products)
	public function get_all_items(){
		$this->db->select('item_name, description, item_image, price');
		$this->db->where('deleted', NULL);
		$this->db->where('available', 1);
		$data = $this->db->get('tblmarket')->result_array();
		if(!empty($data)){
			return $data;
		} else {
			return NULL;
		}
	}
	// Get all My Added Market Items (Products)
	public function get_all_my_items($userId){
		$this->db->select('item_name, description, item_image, price');
		$this->db->where('deleted', NULL);
		$this->db->where('available', 1);
		$this->db->where('userId', $userId);
		$data = $this->db->get('tblmarket')->result_array();
		if(!empty($data)){
			return $data;
		} else {
			return NULL;
		}
	}
	// Get Single Item (Product)
	public function get_item($item_id){
		$this->db->where('id', $item_id);
		$data = $this->db->get('tblmarket')->result_array();
		if(!empty($data)){
			return $data;
		} else {
			return NULL;
		}
	}
} // End Market_model (CI_Model)