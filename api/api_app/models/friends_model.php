<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Friends_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function add_friend($friend) {
        /* $this->db->select("*");
          $this->db->from('tblFriends');
          $this->db->where('uid', $friend['uid']);
          $this->db->where('friend_id', $friend['friend_id']);
          $query = $this->db->get(); */
        $uid = $friend['uid'];
        $fid = $friend['friend_id'];
        $response = $this->db->query("
			
						SELECT * FROM tblFriends
						
						WHERE
						(uid=$uid AND friend_id=$fid) OR (uid=$fid AND friend_id=$uid)
		
		")->row();

        if (!empty($response)) {
            return $response->status;
        } else {
            ///////////////////////////////////////////////////	
            $data = array('uid' => $friend['uid'],
                'friend_id' => $friend['friend_id'],
                'status' => 1,
                'request_sent' => date("Y-m-d H:i:s")
            );
            $this->db->insert('tblFriends', $data);
            ///////////////////////////////////////////////////
            return NULL;
        }
    } // add_friend

	/*
    public function get_friendlist($uid) {
        $this->db->select("tblFriends.status AS status_id,
		                   tblFriends.request_sent,
		                   tblUsers.userId,
		                   tblUsers.firstName,
						   tblUsers.lastName,
						   tblUsers.phoneNumber,
						   tblUsers.address,
						   tblUsers.city,
						   tblUsers.country,
						   tblUsers.userPIN,
						   tblUsers.email,
						   tblUsers.currency,
						   tblStatus.name AS status		                   
						  ");
        $this->db->from('tblFriends');
        $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.friend_id');
        $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
        $this->db->where('tblFriends.status', 2);
        $this->db->where('tblFriends.uid', $uid);
        $this->db->or_where('tblFriends.friend_id', $uid);
        $this->db->order_by("tblUsers.firstName", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // get_friendlist
*/

    public function get_friendlist_upd($uid) {
        $this->db->select('id');
        $this->db->where('uid', $uid);
		$this->db->where('status', 2);
        $uid_data = $this->db->get('tblFriends')->result_array();

        $this->db->select('id');
        $this->db->where('friend_id', $uid);
		$this->db->where('status', 2);
        $friend_id_data = $this->db->get('tblFriends')->result_array();

        $data1 = array();
        $data2 = array();

        if (!empty($uid_data)) {
            foreach ($uid_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.phoneNumber,
								   tblUsers.address,
								   tblUsers.city,
								   tblUsers.country,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblUsers.currency,
								   tblStatus.name AS status		                   
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.friend_id');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row["id"]);
                $this->db->order_by("tblUsers.firstName", "ASC");
                //$data1[] = $this->db->get()->result();
				$data1[] = $this->db->get()->row();
            } // End foreach 1
        }

        if (!empty($friend_id_data)) {
            foreach ($friend_id_data as $row) {
                $this->db->select("tblFriends.status AS status_id,
								   tblFriends.request_sent,
								   tblUsers.userId,
								   tblUsers.firstName,
								   tblUsers.lastName,
								   tblUsers.phoneNumber,
								   tblUsers.address,
								   tblUsers.city,
								   tblUsers.country,
								   tblUsers.userPIN,
								   tblUsers.email,
								   tblUsers.currency,
								   tblStatus.name AS status		                   
								  ");
                $this->db->from('tblFriends');
                $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.uid');
                $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
                $this->db->where('tblFriends.status', 2);
                $this->db->where('tblFriends.id', $row["id"]);
                $this->db->order_by("tblUsers.firstName", "ASC");
                //$data2[] = $this->db->get()->result();
				$data2[] = $this->db->get()->row();
            } // End foreach 2
        }
        $result = array_merge($data1, $data2);
        return $result;
    } // get_friendlist
	

    public function requests_sent($uid) {
        $this->db->select("tblFriends.status AS status_id,
		                   tblFriends.request_sent,
		                   tblUsers.userId,
		                   tblUsers.firstName,
						   tblUsers.lastName,
						   tblUsers.phoneNumber,
						   tblUsers.address,
						   tblUsers.city,
						   tblUsers.country,
						   tblUsers.userPIN,
						   tblUsers.email,
						   tblUsers.currency,
						   tblStatus.name AS status		                   
						  ");
        $this->db->from('tblFriends');
        $this->db->where('tblFriends.uid', $uid);
        $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.friend_id');
        $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
        $this->db->where('tblFriends.status', 1);
        $this->db->order_by("tblUsers.firstName", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

// requests_sent

    public function pending_requests($uid) {
        $this->db->select("tblFriends.status AS status_id,
		                   tblFriends.request_sent,
		                   tblUsers.userId,
		                   tblUsers.firstName,
						   tblUsers.lastName,
						   tblUsers.phoneNumber,
						   tblUsers.address,
						   tblUsers.city,
						   tblUsers.country,
						   tblUsers.userPIN,
						   tblUsers.email,
						   tblUsers.currency,
						   tblStatus.name AS status		                   
						  ");
        $this->db->from('tblFriends');
        $this->db->where('tblFriends.friend_id', $uid);
        $this->db->join('tblUsers', 'tblUsers.userId = tblFriends.uid');
        $this->db->join('tblStatus', 'tblStatus.id = tblFriends.status');
        $this->db->where('tblFriends.status', 1);
        $this->db->order_by("tblUsers.firstName", "ASC");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

// pending_requests	

    public function update_friend($friend) {
        $this->db->select("*");
        $this->db->from('tblFriends');
        $this->db->where('uid', $friend['uid']);
        $this->db->where('friend_id', $friend['friend_id']);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            ///////////////////////////////////////////////////	
            $data = array('status' => $friend['status'],
                'updated' => date("Y-m-d H:i:s")
            );
            $this->db->where('uid', $friend['uid']);
            $this->db->where('friend_id', $friend['friend_id']);
            $this->db->update('tblFriends', $data);
            ///////////////////////////////////////////////////
            return true;
        } else {
            return false;
        }
    }

// update_friend	

    public function userInfo_by_pin($userPin) {
        $this->db->select("tblUsers.userId,
		                   tblUsers.firstName,
						   tblUsers.lastName						   
						 ");
        $this->db->from('tblUsers');
        $this->db->where('tblUsers.userPIN', $userPin);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    }

// userInfo_by_pin

    public function unfriend($userId, $friendId) {
        $condition1 = array(
            'uid' => $userId,
            'friend_id' => $friendId,
            'status' => 2
        );

        $condition2 = array(
            'uid' => $friendId,
            'friend_id' => $userId,
            'status' => 2
        );
        $query = $this->db->query("
			
						DELETE FROM tblFriends
						
						WHERE
						(uid=$userId AND friend_id=$friendId AND status='2') OR (uid=$friendId AND friend_id=$userId AND status='2')
		
		");
        if (!empty($query)) {
            return TRUE;
        } else {
            return FALSE;
        }

        /* $this->db->where($condition1);
          $this->db->or_where($condition2);
          $this->db->delete('tblFriends');
          return ($this->db->affected_rows() > 0) ? TRUE : FALSE; */
    }

// unfriend

    public function getUserPin($userId) {

        $this->db->select('userPin');
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblUsers');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

// user_pin_by_id

    public function getUserName($userId) {

        $this->db->select('firstName, lastName');
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblUsers');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

}

// Friends_model