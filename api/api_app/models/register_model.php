<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Register_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function register_user($data) {
        if ($this->db->insert('tblUsers', $data)) {
            $insert_id = $this->db->insert_id();
            $this->db->where('userId', $insert_id);
            $query = $this->db->get('tblUsers');
            if ($query->num_rows() > 0)
                return $query->row();
            else
                return FALSE;
        } else {
            return false;
        }
    } // register_user

    public function register_merchant($data) {
        if ($this->db->insert('tblMerchantUsers', $data)) {
            $insert_id = $this->db->insert_id();
            $this->db->where('userId', $insert_id);
            $query = $this->db->get('tblMerchantUsers');
            if ($query->num_rows() > 0)
                return $query->row();
            else
                return FALSE;
        } else {
            return false;
        }
    } // register_merchant


    public function process_update_user($data, $id) {
        //$id = $this->token_user_id;
        $this->db->where('userId', $id);
        if ($this->db->update('tblUsers', $data)) {
            $this->db->where('userId', $id);
            $query = $this->db->get('tblUsers');
            if ($query->num_rows() > 0)
                return $query->row();
            else
                return FALSE;
        } else {
            return false;
        }
    } // process_update_user
	

    public function process_update_merchant($data, $id) {
        //$id = $this->token_user_id;
        $this->db->where('userId', $id);
        if ($this->db->update('tblMerchantUsers', $data)) {
            $this->db->where('userId', $id);
            $query = $this->db->get('tblMerchantUsers');
            if ($query->num_rows() > 0)
                return $query->row();
            else
                return FALSE;
        } else {
            return false;
        }
    } // process_update_user
	
	
	public function email_already_exists($email) 
	{
        $this->db->select('email');
		$this->db->from('tblUsers');
		$this->db->where('email', $email);
		$query =$this->db->get();
    	if($query->num_rows() > 0) { 
			return TRUE;
		} else {
			return FALSE;
		}
    } // email_already_exists


    public function merchant_email_already_exists($email) 
    {
        $this->db->select('email');
        $this->db->from('tblMerchantUsers');
        $this->db->where('email', $email);
        $query =$this->db->get();
        if($query->num_rows() > 0) { 
            return TRUE;
        } else {
            return FALSE;
        }
    } // merchant_email_already_exists
	
	
	public function get_countries() 
	{
        $this->db->select('*');
		$this->db->from('countries');
		$query =$this->db->get();
    	if($query->num_rows() > 0) { 
			return $query->result();
		} else {
			return NULL;
		}
    } // get_countries
	
	
	public function validate_password($table,$password) 
	{
        $this->db->select('userId');
		$this->db->from($table);
		$this->db->where('userPassword', $this->encrypt->sha1($password));
		$query =$this->db->get();
    	if($query->num_rows() > 0) { 
			return TRUE;
		} else {
			return FALSE;
		}
    } // validate_password
	
	
	public function update_user_pin($uid,$userPIN,$table) 
	{		
		$data = array('userPIN' => $userPIN);
        $this->db->where('userId', $uid);
        $this->db->update($table, $data); 
        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    } // update_user_pin
	
	
	

} // Register_model