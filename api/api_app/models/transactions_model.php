<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class transactions_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }

    public function add_credit_card($creditCard) {
        if ($this->db->insert('tblTransactions', $creditCard)) {
            $transId = $this->db->insert_id();
            $this->db->where('transId', $transId);
            $query = $this->db->get('tblTransactions');
            if ($query->num_rows() == 1) {
                return $query->row();
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    } // add_credit_card


    public function add_scratch_card($scratchCard) 
	{
        $data = array('userId' => $scratchCard['userId'],
					  'cr'     => $scratchCard['cr'],
					  'gps'    => $scratchCard['gps'],
					  'devId'  => $scratchCard['devId'],
					  'refId'  => $scratchCard['cardId'],
					  'type'   => $scratchCard['type'],
					  'mode'   => $scratchCard['mode']
				     );
        if ($this->db->insert('tblTransactions', $data)) {
            $transId = $this->db->insert_id();
            ////////////// Card Expiry START ////////////
            $data = array('available' => 0);
            $this->db->where('cardId', $scratchCard['cardId']);
            $this->db->update('tblScratchCards', $data);
            ////////////// Card Expiry END /////////////

            $this->db->where('transId', $transId);
            $query = $this->db->get('tblTransactions');
            if ($query->num_rows() == 1) {
                return $query->row();
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    } // add_scratch_card


    public function transfer_credit($uid, $debit, $credit) {
        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $uid);
        $query = $this->db->get('tblTransactions');

        $this->db->insert('tblTransactions', $credit);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

// transfer_credit

    public function user_balance($id) {
        $this->db->where('userId', $id);
        $this->db->where('mode !=', 'DC'); // Daily Contribution
        $this->db->where('mode !=', 'GC'); // Group Contributinon
        $this->db->where('mode !=', 'CHC'); //Charity Contribution
        $this->db->where('mode !=', 'CF'); //Crowdfund Contribution
        $this->db->where('mode !=', 'MI'); // Market Item Purchase Transaction
        $this->db->where('mode !=', 'UB'); // Utility Bills Transactions
        $this->db->where('mode !=', 'CO'); // Cash out
        $this->db->where('mode !=', 'MIB'); // Merchant Invoice Bill
        $this->db->select('userId, 
		                   SUM(cr) AS credit, 
						   SUM(dr) AS debit');
        $data1 = $this->db->get('tblTransactions')->row();

        if (!empty($data1->userId)) {
            $credit = $data1->credit;
            $debit = $data1->debit;
        } else {
            $credit = '';
            $debit = '';
        }

        // Sum of Debit from DC (Daily Contribution)
        $this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'DC');
        $data2 = $this->db->get('tblTransactions')->row();

        if (!empty($data2->userId)) {
            $debit_dc = $data2->debit;
            // Daily contribution balance
            $credit_dc = $data2->credit;
        } else {
            $debit_dc = 0.00;
            $credit_dc = 0.00;
        }

        // Sum of Debit from GC (Group Contribution)
        $this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'GC');
        $data3 = $this->db->get('tblTransactions')->row();

        if (!empty($data3->userId)) {
            $debit_gc = $data3->debit;
            // Group contribution balance
            $credit_gc = $data3->credit;
        } else {
            $debit_gc = 0.00;
            $credit_gc = 0.00;
        }

        // Sum of Debit from CHC (Charity Contribution)
        $this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'CHC');
        $data4 = $this->db->get('tblTransactions')->row();

        if (!empty($data4->userId)) {
            // My Total Charity Contributions
            $debit_chc = $data4->debit;
        } else {
            $debit_chc = 0.00;
        }


        // Sum of Debit from CF (Crowdfund Contribution)
        $this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'CF');
        $data4 = $this->db->get('tblTransactions')->row();
        if (!empty($data4->userId)) {
            // My Total Crowdfund Contributions
            $debit_cf = $data4->debit;
        } else {
            $debit_cf = 0.00;
        }



        // Sum of Debit from MI (Market Item Purchase Transaction)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'MI');
        $data5 = $this->db->get('tblTransactions')->row();

        if (!empty($data5->userId)) {
            // My total item purchases
            $debit_mi = $data5->debit;
        } else {
            $debit_mi = 0.00;
        }

        // Sum of Debit from UB (Utility Bills Transactions)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'UB');
        $data6 = $this->db->get('tblTransactions')->row();

        if (!empty($data6->userId)) {
            // My total item purchases
            $debit_ub = $data6->debit;
        } else {
            $debit_ub = 0.00;
        }

        // Sum of Debit from CO (CashOut Transactions)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'CO');
        $data7 = $this->db->get('tblTransactions')->row();

        if (!empty($data7->userId)) {
            // My total item purchases
            $debit_co = $data7->debit;
        } else {
            $debit_co = 0.00;
        }

        // Sum of Debit from MIB (Merchant Invoice Bill Transactions)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'MIB');
        $data8 = $this->db->get('tblTransactions')->row();

        if (!empty($data8->userId)) {
            // My total item purchases
            $debit_mib = $data8->debit;
        } else {
            $debit_mib = 0.00;
        }

        $debited = $debit + $debit_dc + $debit_gc + $debit_chc + $debit_cf + $debit_mi + $debit_ub + $debit_co + $debit_mib;
        $balance = $credit - $debited;

        // Get charity credit
        $this->db->select(" SUM(tblTransactions.cr) AS credit");
        $this->db->from('tblTransactions');
        $this->db->join('tblCharityAccount', 'tblCharityAccount.id = tblTransactions.refId');
        $this->db->where('tblTransactions.mode', "CHC");
        $this->db->where('tblCharityAccount.userId', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $credit_chc = $row->credit;
        } else {
            $credit_chc = '0.00';
        }
        // Get Market Item sales credit

        $data = array(
            'credit' => $balance,
            'debit' => $debited,
            'total_daily_contributions_credit' => $credit_dc,
            'total_group_contributions_credit' => $credit_gc,
            'total_charities_debit' => $debit_chc,
            'total_my_created_charities_credit' => $credit_chc
        );

        return $data;
    }

// user_balance

    public function my_currency_symbol($userId) {
        $this->db->select('tblCurrency.symbol');
        $this->db->from('tblUsers');
        $this->db->join('tblCurrency', 'tblCurrency.code = tblUsers.currency');
        $this->db->where('tblUsers.userId', $userId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->symbol;
        } else {
            return NULL;
        }
    }

    public function my_currency_code($userId) {
        $this->db->select('tblExchangeRates.currency_type,
		                   tblUsers.currency
						  ');
		$this->db->from('tblExchangeRates');
		$this->db->join('tblUsers', 'tblUsers.currency = tblExchangeRates.id', 'inner');
		$this->db->where('tblUsers.userId', $userId);		
        $query = $this->db->get();		
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->currency_type;
        } else {
            return NULL;
        }
    }
      
    public function user_debit_credit_entries($fromDate, $toDate, $id) {
        $this->db->where('userId', $id);
        $this->db->where('created >=', $fromDate);
        $this->db->where('created <=', $toDate);
        $this->db->select('userId,
		                   SUM(cr) AS credit, 
						   SUM(dr) AS debit');

        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $data = array('userId' => $row->userId,
                'credit' => $row->credit,
                'debit' => $row->debit
            );
            return $data;
        } else {
            return NULL;
        }
    }

// user_debit_credit_entries

    public function check_userPin($userpin, $uid) {
        $this->db->select('userId');
        $this->db->from('tblUsers');
        $this->db->where('userPIN', $userpin);
        $this->db->where('userId', $uid);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return 1;
        } else {
            return 0;
        }
    }

// check_userPin

    public function user_profile_by_pin($userpin) {
        $this->db->where('userPIN', $userpin);
        $query = $this->db->get('tblUsers');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

// user_profile_by_pin

    public function scratch_card_by_number($card_no) {
        $this->db->where('cardNumber', $card_no);
        $this->db->where('available', 1);
        $query = $this->db->get('tblScratchCards');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

    public function update_scratch_card($card_id) {
        $this->db->where('cardNumber', $card_no);
        $this->db->where('available', 1);
        $query = $this->db->get('tblScratchCards');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

// update_scratch_card

    function process_update_user($id, $data) {
        $this->db->where('userId', $id);
        if ($this->db->update('tblUsers', $data)) {
            $this->db->where('userId', $id);
            $query = $this->db->get('tblUsers');
            if ($query->num_rows() > 0)
                return $query;
            else
                return FALSE;
        } else {
            return false;
        }
    }

// process_update_user

    public function bankTransfer($bankData) {
        /////////////// Bank Transfer START ////////////////	
        $data = array('bank' => $bankData['bank'],
            'branch' => $bankData['branch'],
            'account_no' => $bankData['account_no'],
            'amount' => $bankData['amount'],
            'firstname' => $bankData['firstname'],
            'lastname' => $bankData['lastname'],
            'email' => $bankData['email'],
            'userId' => $bankData['userId'],
            'address' => $bankData['address']
        );
        $this->db->insert('tblBankTransfer', $data);
        $refId = $this->db->insert_id();
        /////////////// Bank Transfer END ////////////////		
        /////////////// Transactions Start ////////////////		
        $data = array('userId' => $bankData['userId'],
            'devId' => $bankData['devId'],
            'dr' => $bankData['amount'],
            'gps' => $bankData['gps'],
            'refId' => $refId,
            'type' => 'TR',
            'mode' => 'BT'
        );
        $this->db->insert('tblTransactions', $data);
        $transId = $this->db->insert_id();
        /////////////// Transactions END ////////////////	

        $this->db->select("*");
        $this->db->from('tblTransactions');
        $this->db->where('transId', $transId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

// bankTransfer

    public function cashTransfer($cashData) {
        /////////////// Cash Transfer START ////////////////	
        $data = array('userId' => $cashData['userId'],
            'amount' => $cashData['amount'],
            'phone' => $cashData['phone'],
            'secret_code' => $cashData['secret_code'],
            'firstname' => $cashData['firstname'],
            'lastname' => $cashData['lastname'],
            'email' => $cashData['email'],
            'address' => $cashData['address']
        );
        $this->db->insert('tblCashTransfer', $data);
        $refId = $this->db->insert_id();
        /////////////// Cash Transfer END /////////////////
        /////////////// Transactions Start ////////////////
        $data = array('userId' => $cashData['userId'],
            'devId' => $cashData['devId'],
            'dr' => $cashData['amount'],
            'gps' => $cashData['gps'],
            'refId' => $refId,
            'type' => 'TR',
            'mode' => 'CT'
        );
        $this->db->insert('tblTransactions', $data);
        $transId = $this->db->insert_id();
        /////////////// Transactions END ////////////////

        $this->db->select("*");
        $this->db->from('tblTransactions');
        $this->db->where('transId', $transId);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return FALSE;
        }
    }

// cashTransfer

    public function user_profile_by_id($uid) {
        $this->db->where('userId', $uid);
        $query = $this->db->get('tblUsers');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

// user_profile_by_id

    public function getUserPin($userId) {

        $this->db->select('userPin');
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblUsers');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    }

// user_pin_by_id

    public function myRecentTransactions($limit, $userId) {

        $fromDate = NULL;
        $toDate = NULL;
        $order_by = 'desc';
        $offset = 0;

        // Get Credit Card Transactions
        $cc_transactions = $this->get_cc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Scratch Card Transactions
        $sc_transactions = $this->get_sc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Transfer Credit by Pin Transactions
        $tcp_transactions = $this->get_tcp_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Bank Transfer Transactions
        $bt_transactions = $this->get_bt_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Cash Transfer Transactions
        $ct_transactions = $this->get_ct_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Daily Contribution Transactions
        $dc_transactions = $this->get_dc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Charity Transactions
        $charity_transactions = $this->get_charity_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get MI Transactions
        $mi_transactions = $this->get_mi_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);

        $response_array = NULL;
        if (!empty($cc_transactions)) {
            $response_array[0] = $cc_transactions;
        }
        if (!empty($sc_transactions)) {
            $response_array[1] = $sc_transactions;
        }
        if (!empty($tcp_transactions)) {
            $response_array[2] = $tcp_transactions;
        }
        if (!empty($bt_transactions)) {
            $response_array[3] = $bt_transactions;
        }
        if (!empty($ct_transactions)) {
            $response_array[4] = $ct_transactions;
        }
        if (!empty($dc_transactions)) {
            $response_array[5] = $dc_transactions;
        }
        if (!empty($charity_transactions)) {
            $response_array[6] = $charity_transactions;
        }
        if (!empty($mi_transactions)) {
            $response_array[7] = $mi_transactions;
        }
        $all_transactions = array();
        if (!empty($response_array)) {
            for ($i = 0; $i < 8; $i++) {
                if (!empty($response_array[$i]))
                    $all_transactions = array_merge($all_transactions, $response_array[$i]);
            }
        }

        $transactions = array();
        for ($i = 0; $i < $limit; $i++) {
            if (!empty($all_transactions[$i])) {
                $transactions[] = $all_transactions[$i];
            } else {
                $i = $limit;
            }
        }
usort($transactions, function($a, $b) {
    return strtotime($b->transaction_date) - strtotime($a->transaction_date);
});

        return $transactions;

        /*
          $offset = 0;

          $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date');
          $this->db->where('userId', $userId);
          $this->db->order_by("created", "desc");
          $this->db->limit($limit, $offset);
          $query = $this->db->get('tblTransactions');

          if($query->num_rows() > 0)
          return $query->result();
          else
          return FALSE;
         */
    }

// My Recent Transactions

    public function myTransactions($fromDate, $toDate, $userId, $limit, $offset) {

        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date');
        $this->db->where('created >=', $fromDate);
        $this->db->where('created <=', $toDate);
        $this->db->where('userId', $userId);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

// myTransactions
    // New Function-myTransactions
    public function new_myTransactions($fromDate, $toDate, $userId, $limit, $offset) {

        $order_by = "asc";

        // Get Credit Card Transactions
        $cc_transactions = $this->get_cc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Scratch Card Transactions
        $sc_transactions = $this->get_sc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Transfer Cash By Pin Transactions
        $tcp_transactions = $this->get_tcp_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Bank Transfer Transactions
        $bt_transactions = $this->get_bt_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Cash Transfer Transactions
        $ct_transactions = $this->get_ct_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Daily Contribution Transactions
        $dc_transactions = $this->get_dc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get Charity Transactions
        $charity_transactions = $this->get_charity_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);
        // Get MI Transactions
        $mi_transactions = $this->get_mi_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by);

        $response_array = NULL;
        if (!empty($cc_transactions)) {
            $response_array[0] = $cc_transactions;
        }
        if (!empty($sc_transactions)) {
            $response_array[1] = $sc_transactions;
        }
        if (!empty($tcp_transactions)) {
            $response_array[2] = $tcp_transactions;
        }
        if (!empty($bt_transactions)) {
            $response_array[3] = $bt_transactions;
        }
        if (!empty($ct_transactions)) {
            $response_array[4] = $ct_transactions;
        }
        if (!empty($dc_transactions)) {
            $response_array[5] = $dc_transactions;
        }
        if (!empty($charity_transactions)) {
            $response_array[6] = $charity_transactions;
        }
        if (!empty($mi_transactions)) {
            $response_array[7] = $mi_transactions;
        }
        $all_transactions = array();
        if (!empty($response_array)) {
            for ($i = 0; $i < 8; $i++) {
                if (!empty($response_array[$i]))
                    $all_transactions = array_merge($all_transactions, $response_array[$i]);
            }
        }

        return $all_transactions;
    }

// myTransactions

    public function get_cc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "CC");
        $this->db->where('cr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_sc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "SC");
        $this->db->where('cr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_tcp_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "TCP");
        $this->db->where('dr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_bt_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "BT");
        $this->db->where('dr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_ct_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "CT");
        $this->db->where('dr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    public function get_dc_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "DC");
        $this->db->where('dr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    // Get CHC Transactions (Charity Contributions)
    public function get_charity_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "CHC");
        $this->db->where('dr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    // Get MI Transactions (Market Items)
    public function get_mi_transactions($fromDate, $toDate, $userId, $limit, $offset, $order_by) {
        $this->db->select('transId, description, dr as debit, cr as credit, created as transaction_date, mode');
        if ($fromDate != NULL && $toDate != NULL) {
            $this->db->where('created >=', $fromDate);
            $this->db->where('created <=', $toDate);
        }
        $this->db->where('userId', $userId);
        $this->db->where('mode', "MI");
        $this->db->where('dr !=', "0.00");
        $this->db->order_by("created", $order_by);
        $this->db->limit($limit, $offset);
        $query = $this->db->get('tblTransactions');
        if ($query->num_rows() > 0)
            return $query->result();
        else
            return FALSE;
    }

    // Start Merchant
    // Get My Merchant Invoices
    public function get_my_invoices($userId, $status) {

        $this->db->select('userPin');
        $this->db->where('userId', $userId);
        $row = $this->db->get('tblUsers')->row();
        if (!empty($row->userPin))
            $userPin = $row->userPin;
        else
            $userPin = NULL;

        if ($userPin != NULL) {
            /*
              $this->db->select('tblMerchantInvoices.*, tblMerchantUsers.firstName, tblMerchantUsers.lastName')
              $this->db->form('tblMerchantInvoices');
              $this->db->join('tblMerchantUsers', 'tblMerchantUsers.userId = tblMerchantInvoices.merchantId', 'inner');
              $this->db->join('tblUsers', 'tblUsers.userPIN = tblMerchantInvoices.user_pin', 'inner');
              $this->db->where('tblMerchantInvoices.user_pin', $userPin);
              $this->db->where('tblMerchantInvoices.status', $status);
              $invoices = $this->db->get();
              print_r($this->db->last_query()); exit;
             */
            $this->db->select('tblMerchantInvoices.*,IFNULL(tblrefund.status,0) refund_status',false);
            $this->db->from('tblMerchantInvoices');
            $this->db->join('tblrefund','tblrefund.invoice_no=tblMerchantInvoices.id','left');
            $this->db->where('tblMerchantInvoices.user_pin', $userPin);
            $this->db->where('tblMerchantInvoices.status', $status);
            $this->db->order_by("tblMerchantInvoices.dated", "desc");
            $invoices = $this->db->get()->result_array();

            if (!empty($invoices)) {
                return $invoices;
            } else {
                return NULL;
            }
        } else {
            return NULL;
        }
    }

    public function get_invoice_price($merchant_id, $invoice_id) {
        $this->db->select('net_total');
        $this->db->where('merchantId', $merchant_id);
        $this->db->where('id', $invoice_id);
        $data = $this->db->get('tblMerchantInvoices')->row();
        if (!empty($data)) {
            return $data;
        } else {
            return NULL;
        }
    }

//	public function get_invoice_details($invoice_id, $userId){
//		
//		$this->db->select('mi.id as invoice_id, mi.details, mi.net_total, u.firstName, u.lastName, mi.status');
//		$this->db->from('tblMerchantInvoices as mi');
//		$this->db->join('tblMerchantUsers as u', 'u.userId = mi.merchantId', 'inner');
//		$this->db->where('id', $invoice_id);
//		$query = $this->db->get();
//		if($query->num_rows() > 0){
//			return $query->row_array();
//		} else {
//			return NULL;
//		}
//	}
    public function set_invoice_status($invoice_id, $status) {
        $update_data = array('status' => $status);
        $this->db->where('id', $invoice_id);
        return $this->db->update('tblMerchantInvoices', $update_data);
    }

    public function payInvoiceBillTransaction($userId, $debit, $credit, $invoice_id) {

        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $userId);
        $query = $this->db->get('tblTransactions');

        $response = $this->db->insert('tblTransactions', $credit);
        if ($response) {

            // Update status that the invoice bill is paid
            $data = array('status' => 2);
            $this->db->where('id', $invoice_id);
            $this->db->update('tblMerchantInvoices', $data);

            return $query->row();
        } else {
            return false;
        }
    }
	
	/*
	public function get_unique_charity_image_name(){
		$image_name = 'checkslip_'.rand(1,99999).'.png';
		//$unique_image_name = $this->get_charity_image_name($image_name);
		return $image_name;
	}
	*/

// End - payInvoiceBillTransaction
    // End New Function-myTransactions

    /*
      // Pending
      public function myTransactions($fromDate, $toDate, $userId){

      $this->db->select('transId, created as date, description as account_debit, description as account_credit, dr as debit_amount, cr as credit_amount');
      $this->db->where('created >=', $fromDate);
      $this->db->where('created <=', $toDate);
      $this->db->where('userId', $userId);

      $query = $this->db->get('tblTransactions');
      if($query->num_rows() > 0)
      return $query->result();
      else
      return FALSE;
      } // myTransactions
     */
	 
	public function save_deposit_slip($data) {
		$this->db->insert('tbldepositSlip', $data);
		return $this->db->insert_id();	 
	} //save_deposit_slip 
	
	
	public function manual_transactions($data) 
	{
		$this->db->insert('manual_transactions', $data);
		return $this->db->insert_id();	 
	} // manual_transactions 
	 
	 
	 
} // transactions_model
