<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register_model extends CI_Model {
	function __construct() {
    	parent::__construct();
  	}
  
  	
	public function register_user($data)
	{
    	if ($this->db->insert('tblUsers', $data)) 
		{
			$insert_id = $this->db->insert_id();	
			$this->db->where('userId', $insert_id);
    		$query = $this->db->get('tblUsers');
    		if($query->num_rows() > 0) 
				return $query->row();
			else 
				return FALSE;
		} else {
      		return false;
    	}
	} // register_user
	
	public function register_merchant($data)
	{
    	if ($this->db->insert('tblMerchantUsers', $data)) 
		{
			$insert_id = $this->db->insert_id();	
			$this->db->where('userId', $insert_id);
    		$query = $this->db->get('tblMerchantUsers');
    		if($query->num_rows() > 0) 
				return $query->row();
			else 
				return FALSE;
		} else {
      		return false;
    	}
	} // register_merchant
	
  
	function process_update_user($data, $id) 
	{
		
		//$id = $this->token_user_id;
		
    	$this->db->where('userId', $id);
    	if ($this->db->update('tblUsers', $data)) 
		{
      		$this->db->where('userId', $id);
    		$query = $this->db->get('tblUsers');
    		if($query->num_rows() > 0) 
				return $query->row();
			else 
				return FALSE;
    	} else {
      		return false;
    	}
  	} // process_update_user
	
	
	
}