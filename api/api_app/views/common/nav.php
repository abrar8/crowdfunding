<?php 
$roles =  $this->uri->segment(1);

 ?>	

<nav id="pageslide-left" class="pageslide inner">
				<div class="navbar-content">
					<!-- start: SIDEBAR -->
					<div class="main-navigation left-wrapper transition-left">
						<div class="navigation-toggler hidden-sm hidden-xs">
							<a href="#main-navbar" class="sb-toggle-left">
							</a>
						</div>
						<div class="user-profile border-top padding-horizontal-10 block">
							<div class="inline-block">
								<?php /*?><img src="<?php echo base_url(); ?>assets/images/avatar-1.jpg" alt=""><?php */?>
							</div>
							<div class="inline-block">
								<h5 class="no-margin"> Welcome </h5>
								<h4 class="no-margin"> <?php 
								echo $this->session->userdata('admin_fname');?> </h4>
								<a class="btn user-options sb_toggle">
									<i class="fa fa-cog"></i>
								</a>
							</div>
						</div>
					
						<!-- start: MAIN NAVIGATION MENU -->
                       <ul class="main-navigation-menu">
					    <?php if($roles == 'roles' || $roles=='users' ) { $class = 'active'; }else{ $class = ''; } ?>
                            <li class="<?php echo $class;?>">
								<a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> System User </span><i class="icon-arrow"></i> </a>
								<ul class="sub-menu">
														
									<li>
										<a href="<?php echo base_url().'roles/roles_management';?>">
											<span class="title"> Roles Management</span>
										</a>
									</li> 
									<li>
										<a href="<?php echo base_url().'users/users_management';?>">
											<span class="title"> Users Management</span>
										</a>
									</li>    
									   
								</ul>
							</li>
                           
                      		
						</ul>
                     
						<!-- end: MAIN NAVIGATION MENU -->
					</div>
					<!-- end: SIDEBAR -->
				</div>
				<div class="slide-tools">
					<div class="col-xs-6 text-left no-padding">
						<a class="btn btn-sm status" href="#">
							Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
						</a>
					</div>
					<div class="col-xs-6 text-right no-padding">
						<a class="btn btn-sm log-out text-right" href="<?php echo base_url().'signin/signout'; ?>">
							<i class="fa fa-power-off"></i> Log Out
						</a>
					</div>
				</div>
			</nav>
			<!-- end: PAGESLIDE LEFT -->