<html>
<head>
<style>
body
{
	font-family:Verdana, Geneva, sans-serif;
	background-color:#CCC;
}
#container {
    margin: 20px auto;
    width: 950px;
    background: #fff;
    border-radius: 5px;
    padding: 10px 20px;
}
table
{
	table-layout:fixed;
	word-wrap:break-word;
	
	width:100%;
	border:1px solid #e5e5e5;
	border-collapse:collapse;
	
}
table tr td
{
	border:1px solid #e5e5e5;
	padding:10px;
	word-wrap:
	
}

</style>
</head>
<body>
<div id="container">
<h2 align="center" style="color:#06C;">Imali API v.1</h2>
<h3>Overview</h3>
<hr>
<table width="100%">
<tr>
<td width="30%">Resource URL</td>
<td>http://10.90.1.251/imali/api/v1/</td>
</tr>
<tr>
<td>Request Method</td>
<td>REST</td>
</tr>
<tr>
<td>Response Format</td>
<td>JSON</td>
</tr>
<tr>
<td>Response Encoding</td>
<td>UTF-8</td>
</tr>
</table>
<h3>Resource</h3>
<hr>
<table>
<tbody>
<tr>
<td><strong>URL</strong></td>
<td><strong>Method</strong></td>
<td><strong>Parameters</strong></td>
<td><strong>Description</strong></td>
</tr>
<tr>
<td>api/v1/register</td>
<td>POST</td>
<td>firstName,lastName,phoneNumber,address,country,city,currency,email,password</td>
<td>User registration</td>
</tr>
<tr>
<td>api/v1/signin</td>
<td>POST</td>
<td>email,password</td>
<td>User Login</td>
</tr>
<tr>
<td>api/v1/banks/available</td>
<td>GET</td>
<td></td>
<td>Fetching Available Banks</td>
</tr>
</tbody>
</table>
</div>
</body>
</html>