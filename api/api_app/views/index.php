﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>Imali Documentation</title>
        <link rel="alternate" type="application/rss+xml" title="frittt.com" href="feed/index.html">
        <link href="http://fonts.googleapis.com/css?family=Raleway:700,300" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url('assets/docs/css/style.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/docs/css/prettify.css'); ?>">
        <style>
            body
            {
                font-family:Verdana, Geneva, sans-serif;
                background-color:#CCC;
            }
            #container {
                margin: 20px auto;
                width: 950px;
                background: #fff;
                border-radius: 5px;
                padding: 10px 20px;
            }
            table
            {
                table-layout:fixed;
                word-wrap:break-word;

                width:100%;
                border:1px solid #e5e5e5;
                border-collapse:collapse;

            }
            table tr td
            {
                border:1px solid #e5e5e5;
                padding:10px;
                word-wrap:

            }    
        </style>
    </head>
    <body>

        <div class="wrapper">
            <nav>
                <div class="pull-left">
                    <h1><a href="javascript:"><img src="<?php echo base_url('assets/docs/img/icon.png'); ?>" alt="Free Documentation Template Icon" /> <span>DocWeb</span></a></h1>
                </div>

                <div class="pull-right">
                    <a href="#" target="_blank" class="btn btn-download"><img src="<?php echo base_url('assets/docs/img/download.png'); ?>" width="25" alt="Download Free Documentation Template" /> Download Now</a>
                </div>
            </nav>


            <header>
                <div class="container">
                    <h2 class="lone-header">API Documentation</h2>
                </div>
            </header>


            <section>
                <div class="container">
                    <ul class="docs-nav" style="overflow-y:scroll; height: 800px; overflow-x: hidden;">
                        <li><strong>Api </strong></li>
                        <li><a href="#Introduction" class="cc-active">Introduction</a></li>
                        <li><strong>Authentication</strong></li>
                        <li><a href="#sign_up" class="cc-active">Sign Up - User</a></li>
                        <li><a href="#sign_up_merchant" class="cc-active">Sign Up - Merchant</a></li>
                        <li><a href="#sign_up_agent" class="cc-active">Sign Up - Agent Users</a></li>
                        <li><a href="#sign_in" class="cc-active">Sign In - User</a></li>
                        <li><a href="#validate_login" class="cc-active">Sign In - Validate Login</a></li>
                        <li><a href="#sign_in_merchant" class="cc-active">Sign In - Merchant</a></li>
                        <li><a href="#sign_in_agent" class="cc-active">Sign In - Agent</a></li>
                        <li><a href="#profile" class="cc-active">Profile</a></li>
                        <li><a href="#update_currency" class="cc-active">Update Currency</a></li>
                        <li><a href="#get_profile" class="cc-active">Get Profile</a></li>
                        <li><a href="#account_forgot_password" class="cc-active">Forgot Password</a></li>
                        <li><a href="#merchant_forgot_password" class="cc-active">Merchant Forgot Password</a></li>
                        <li><a href="#get_countries" class="cc-active">Get Countries</a></li>
                        <li><a href="#get_currency" class="cc-active">Get Currency</a></li>
                        <li><a href="#update_userPIN" class="cc-active">Change UserPIN</a></li>
                        <li><a href="#auth_password" class="cc-active">Auth Password</a></li>
                        
                        <li><strong>Bank Details</strong></li>
                        <li><a href="#available" class="cc-active">Available banks</a></li>
                        <li><a href="#save" class="cc-active">Save banks</a></li>
                        <li><a href="#my_bank_accounts" class="cc-active">My Bank Accounts</a></li>
                        <li><a href="#banks_by_bankid" class="cc-active">View banks (By bankId)</a></li>

                        <li><strong>Transactions</strong></li>
                        <li><a href="#add_creditcard" class="cc-active">Add Credit(Credit card)</a></li>
                        <li><a href="#add_scratchcard" class="cc-active">Add Credit (SC)</a></li>
                        <li><a href="#transfer_credit" class="cc-active">Transfer Credit (By PIN)</a></li>
                        <li><a href="#available_balance" class="cc-active">Available Balance</a></li>
                        <li><a href="#bank_transfer" class="cc-active">Bank Transfer</a></li>
                        <li><a href="#cash_transfer" class="cc-active">Cash Transfer</a></li>
                        <li><a href="#my_transactions" class="cc-active">My Transactions</a></li>
                        <li><a href="#my_recent_transactions" class="cc-active">My Recent Transactions</a></li>
                        <li><a href="#my_transaction_checkSlip" class="cc-active">My Transaction Check Slipons</a></li>
                        <li><a href="#my_transaction_out" class="cc-active">Transaction Out</a></li>

                        <li><strong>Friends & Family</strong></li>
                        <li><a href="#add_friend_by_pin" class="cc-active">Add Friend (By PIN)</a></li>
                        <li><a href="#friendlist" class="cc-active">Friendlist</a></li>
                        <li><a href="#requests_sent" class="cc-active">Requests Sent</a></li>
                        <li><a href="#pending_requests" class="cc-active">Pending Requests</a></li>
                        <li><a href="#update_friendlist" class="cc-active">Friend Request Response</a></li>
                        <li><a href="#unfriend" class="cc-active">Unfriend</a></li>
                        <li><a href="#add_fb_friends" class="cc-active">Add Facebook Friend (By PIN)</a></li>
                        
                        <li><strong>Common</strong></li>
                        <li><a href="#get_full_name_by_pin" class="cc-active">Get Full Name (By Pin)</a></li>
                        <li><a href="#get_basic_information" class="cc-active">Get Basic Information</a></li>
                        <li><a href="#available_currency_types" class="cc-active">Available Currency Types</a></li>
                        <!-- <li><a href="#available_balance_all" class="cc-active">Available Balance (All)</a></li> -->
                        
                        <li><strong>Daily Contributions</strong></li>
                        <li><a href="#subscribe_me_to_dc" class="cc-active">Subscribe Me</a></li>
                        <li><a href="#my_subscription_status_dc" class="cc-active">My Subscription Status</a></li>
                        <li><a href="#add_balance_to_dc" class="cc-active">Add balance (DC)</a></li>
                        <li><a href="#view_balance_dc" class="cc-active">View Balance (DC)</a></li>
                        <li><a href="#view_all_dc" class="cc-active">View Contributions (List All)</a></li>
                        
                        <li><strong>Group Contributions</strong></li>
                        <li><a href="#createGroupContribution" class="cc-active">Create Group-Contribution</a></li>
                        <li><a href="#myGroupContributions" class="cc-active">View Group-Contributions List</a></li>
                        <li><a href="#invitePeopleGc" class="cc-active">Invite People to Group-Contribution</a></li>
                        <li><a href="#requestResponseGc" class="cc-active">Respond to Group-Contribution</a></li>
                        <li><a href="#gcHistory" class="cc-active">History Group-Contribution</a></li>
                        <li><a href="#myGcRequests" class="cc-active">My Group-Contribution Requests</a></li>
                        
                        <li><strong>Charities</strong></li>
                        <li><a href="#createCharity" class="cc-active">Create Charity</a></li>
                        <li><a href="#inviteToCharity" class="cc-active">Invite To Charity</a></li>
                        <li><a href="#pendingInvitations" class="cc-active">Pending Charity Invitations</a></li>
                        <li><a href="#responseToInvitation" class="cc-active">Response To Invitations</a></li>
                        <li><a href="#getCharities" class="cc-active">Get All Charities</a></li>
                        <li><a href="#getMyCharities" class="cc-active">Get My Charities</a></li>
                        <li><a href="#removeCharity" class="cc-active">Remove My Charity</a></li>
                        <li><a href="#contributeCharity" class="cc-active">Contribute Charity</a></li>
                        <li><a href="#myDonations" class="cc-active">My Donations (List All)</a></li>
                        <li><a href="#get_charity" class="cc-active">Charity (Details)</a></li>
                        
                        <li><strong>Market</strong></li>
                        <li><a href="#add" class="cc-active">Add Item (Product)</a></li>
                        <li><a href="#get_all_items" class="cc-active">Get All Items (Products)</a></li>
                        <li><a href="#get_my_all_items" class="cc-active">Get My All Items (Products)</a></li>
                        <li><a href="#get_item" class="cc-active">Get Item (Product) Details</a></li>
                        <li><a href="#remove_item" class="cc-active">Remove Item (Product)</a></li>
                        <li><a href="#search_items" class="cc-active">Search Items (Products)</a></li>
                        <li><a href="#buy_item" class="cc-active">Buy Item (Product)</a></li>
                        
                        <li><strong>Promos</strong></li>
                        <li><a href="#getPromotions" class="cc-active">Get Promotions (List All)</a></li>
                        <li><a href="#claimPromotionCopoun" class="cc-active">Claim Promotion Coupon</a></li>
                        
                        <li><strong>Utility Bills</strong></li>
                        <li><a href="#getProviderTypes" class="cc-active">List Provider Types</a></li>
                        <li><a href="#getProviderList" class="cc-active">List All Providers (By Type)</a></li>
                        <!--<li><a href="#saveBill" class="cc-active">Save Bill</a></li>-->
                        <li><a href="#getMyBills" class="cc-active">Get My Bills</a></li>
                        <li><a href="#payBill" class="cc-active">Pay Bill</a></li>
                        <li><a href="#myPaidBillsHistory" class="cc-active">Get My Paid Bills History</a></li>
                        
                        <li><strong>Cash Out </strong></li>
                        <li><a href="#request_cashout" class="cc-active">Request (Cash Out)</a></li>
                        <li><a href="#cash_in_hand" class="cc-active">My Cash In Hand</a></li>
                        
                        <li><strong>Charges </strong></li>
                        <li><a href="#charges" class="cc-active">Charges </a></li>

                        <li><strong>Merchant</strong></li>
                        <li><a href="#save_invoice" class="cc-active">Save Invoice</a></li>
                        <li><a href="#get_invoices" class="cc-active">Get Invoices (By Status)</a></li>
                        <li><a href="#invoice_details" class="cc-active">Invoice Details (By ID)</a></li>
                        <li><a href="#get_my_invoices" class="cc-active"> Imali User My Invoices (By Status)</a></li>
                        <li><a href="#account_balance" class="cc-active">Account Balance (Merchant)</a></li>		
                        <li><a href="#pay_invoice" class="cc-active">Pay Invoice (Merchant Invoice)</a></li>
                        <!--<li><a href="#invoice_response" class="cc-active">Invoice Response (iMali User)</a></li>-->

                        <li><strong>Refund</strong></li>
                        <li><a href="#create_refund" class="cc-active">Create Refund</a></li>
                        <li><a href="#user_refunds" class="cc-active">User Refunds</a></li>
						<li><a href="#merchant_refunds" class="cc-active">Merchant Refunds</a></li>
                        <li><a href="#merchant_refunds_list" class="cc-active">Merchant Refunds List</a></li>
                        <li><a href="#refund_detail" class="cc-active">Refund Detail</a></li>
                        <li><a href="#merchant_refunds_response" class="cc-active">Response To Refunds</a></li>
                                                
                        <li><strong>Currency</strong></li>
                        <li><a href="#exchange_rates" class="cc-active">Exchange Rates</a></li>
                        
                        <li><strong>Ipool</strong></li>
                        <li><a href="#create_ipool" class="cc-active">Create Ipool</a></li>
                        <li><a href="#ipool_list" class="cc-active">Ipool List</a></li>
                        <li><a href="#send_invitation" class="cc-active">Send Invitation</a></li>
                        <li><a href="#pending_invitation" class="cc-active">Pending Invitation</a></li>
                        <li><a href="#requestResponse" class="cc-active">Request Response</a></li>
                        <li><a href="#participation" class="cc-active">Participation</a></li>
                        <li><a href="#my_ipool_participants" class="cc-active">My Ipool Participants</a></li>                        
                        <li><a href="#ipool_friendlist" class="cc-active">Friendlist</a></li> 
                        
                        <li><strong>Facebook Friends & Family</strong></li>
                        <li><a href="#fb_friendlist" class="cc-active">Facebook Friendlist</a></li>
                        
                        <li><strong>Crowdfund</strong></li>
                        <li><a href="#createCrowdfund" class="cc-active">Create Crowdfund</a></li>
                        <li><a href="#getCrowdfund" class="cc-active">Get Crowdfund</a></li>
                        <li><a href="#getMyCrowdfund" class="cc-active">Get My Crowdfund</a></li>
                        <li><a href="#cf_pending_invitation" class="cc-active">Pending Invitations</a></li>
                        <li><a href="#removeCrowdfund" class="cc-active">Remove Crowdfund</a></li>
                        <li><a href="#myDonations_crowdfund" class="cc-active">My Donations</a></li>
                        <li><a href="#contributeCrowdfund" class="cc-active">contribute to Crowdfund</a></li>
                        <li><a href="#crowdfund_detail" class="cc-active">Crowdfund Detail</a></li>
                        <li><a href="#inviteToCrowdfund" class="cc-active">Invite To Crowdfund</a></li>
                        <li><a href="#responseToInvitationCrowdfund" class="cc-active">Response To Invitation</a></li>
                        <li><a href="#crowdfund_friendlist" class="cc-active">Friendlist</a></li> 
                    </ul>
                    <div class="docs-content">
                        <h3 id="Introduction">Introduction</h3>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>http://localhost:70/imali/api/v1/</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        
                        
                        <!-- Sign up -->
                        <h3> Authentication</h3>
                        <h2 id="sign_up"><b> Sign up</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/signup</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>first_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>last_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>phone_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    user_id: "64"
    first_name: "Naveed"
    last_name: "Arshad"
    email: "naveedarshad25@gmail.com"
    phone_number: "03125572025"
    pin_code: "4251265"
    api_key: "dc72f33e36d1c7df742a9d5d31e394b5"
    message: "You are successfully registered"
}
                        </pre>
                        </p>




						<h2 id="sign_up_agent"><b> Sign UP - Agent Users Signup</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/signup/agent_users_signup</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>

                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>first_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>last_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>phone_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    user_id: "64"
    first_name: "Naveed"
    last_name: "Arshad"
    email: "naveedarshad25@gmail.com"
    phone_number: "03125572025"
    pin_code: "4251265"
    api_key: "dc72f33e36d1c7df742a9d5d31e394b5"
    message: "You are successfully registered"
}
                        </pre>
                        </p>









                        <hr>
                        <h2 id="sign_up_merchant"><b> Sign up Merchant</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/signup/merchant</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>first_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>last_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>phone_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"user_id": "2"
	"first_name": "Abbasi"
	"last_name": "Karyana Store"
	"email": "haseeb@devdesks.com"
	"phone_number": "03355477889"
	"pin_code": "8542113"
	"api_key": "0ef27f1b9c4c1f526eca00a32f1853e8"
	"message": "You are successfully registered"
}
                        </pre>
                        </p>

                        <hr>
                        <!-- Sign in -->
                        <h3 id="sign_in"><b> Sign In </b></h3>
                        <p>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/signin</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>			
                            </tbody>
                        </table>


                        <hr>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>fb_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>password</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>device_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>device_type</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    user_id: "64"
    first_name: "Naveed"
    last_name: "Arshad"
    email: "naveedarshad25@gmail.com"
    phone_number: "03125572025"
    address: "Adyala Road, Rawalpindi"
    country: "Pakistan"
    city: "Rawalpindi"
    currency: "PKR"
    pin_code: "4251265"
    api_key: "dc72f33e36d1c7df742a9d5d31e394b5"
	profile_status: "0"
    fb_id: "123456789"
}
                        </pre>

                        </p>
                        <p>


                        <h3 id="sign_in_merchant"><b> Sign In Merchant</b></h3>
                        <p>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/signin/merchant</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>			
                            </tbody>
                        </table>


                        <hr>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>password</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>device_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>device_type</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    user_id: "64"
    first_name: "I Am"
    last_name: "A Merchant"
    email: "naveedarshad25@gmail.com"
    phone_number: "03125572025"
    address: "Adyala Road, Rawalpindi"
    country: "Pakistan"
    city: "Rawalpindi"
    currency: "PKR"
    pin_code: "4251265"
    api_key: "dc72f33e36d1c7df742a9d5d31e394b5"
	profile_status: "0"
}
                        </pre>
                        </p>
                        
                        
                        
                        
                        <h3 id="sign_in_agent"><b> Sign In - Agent</b></h3>
                        <p>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/signin/agent</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>			
                            </tbody>
                        </table>
                        <hr>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>password</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>device_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>device_type</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    user_id: "64"
    first_name: "I Am"
    last_name: "A Agent"
    email: "naveedarshad25@gmail.com"
    phone_number: "03125572025"
    address: "Adyala Road, Rawalpindi"
    country: "Pakistan"
    city: "Rawalpindi"
    currency: "PKR"
    pin_code: "4251265"
    api_key: "dc72f33e36d1c7df742a9d5d31e394b5"
	profile_status: "0"
}
                        </pre>
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p>              
                        <h2 id="profile"><b> Update Profile</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/profile</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p> 		
                        <table>
                            <tbody>
                                <tr>						
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>						
                                </tr>

                                <tr>
                                    <td>merchant_title</td>
                                    <td>Required[For merchant], Optional[For user]</td>
                                </tr>
                                <tr>
                                    <td>address</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>country</td>						
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>city</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>currency</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    user_id: "64"
    first_name: "Naveed"
    last_name: "Arshad"
    email: "naveedarshad25@gmail.com"
    phone_number: "03125572025"
    address: "Adyala Road, Rawalpindi"
    country: "Pakistan"
    city: "Rawalpindi"
    currency: "PKR"
    pin_code: "4251265"
    message: "Profile Updated Successfully"
}
                        </pre>

                        </p>
                        <hr>




                        <!-- Save -->	
                        <!-- Available banks-->

                        <h3 id="available"><b> Available banks </b></h3>
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/banks/available</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Get</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint" style="text-align: left";>
{
  "error": false,
  "available_banks": [
    {
      "bank_id": "1",
      "bank_name": "Amarillo's National Bank"
    },
    {
      "bank_id": "2",
      "bank_name": "American Savings Bank California"
    }
}
                        </pre>
                        </p>
                        <!-- Save -->	
                        <p>
                        <h3 id="save"><b> Save </b></h3>
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/banks/save</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>						
                                    <td><strong>Description</strong></td>

                                </tr>
                                <tr>
                                    <td>bank_id</td>						
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>branch_name</td>						
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>account_title</td>						
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>account_number</td>					
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	message: "Bank Detail Added successfully"
}
                        </pre>
                        </p>	
                        <p>	
                        <h3 id="my_bank_accounts"><b>My Bank Accounts</b></h3>
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/banks/my_bank_accounts</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Get</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>	
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "user_id": "143",
  "accounts": [
    {
      "bank_account_id": "42",
      "user_id": "143",
      "bank_id": "3",
      "branch_name": "tt",
      "account_title": "444455",
      "account_number": "346666",
      "available": "1",
      "bank_name": "American Savings Bank Hawaii"
    }
  ]
}
			
                        </pre>

                        </p>	
                        <p>	
                        <h3 id="banks_by_bankid"><b>Banks (By bankId)</b></h3>
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/banks/by_bankId/{:userId}/{:bankId}</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Get</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>	
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	user_id: "66"
	accounts: [1]
	0:  {
		bank_account_id: "1"
		user_id: "66"
		bank_id: "3"
		branch_name: "natiib"
		account_title: "abc"
		account_number: "56784336"
		available: "0"
		bank_name: "UBL Bank"
		display_name: "Islamabad"
	}-
	-
}
                        </pre>
                        </p>

                        <!--  Add Credit (By debit/credit card)  -->

                        <h2 id="add_creditcard"><b> Add Credit (By Debit/Credit card)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/AddCreditCard</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p> 		<table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>credit</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>card_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>exp_date</td>
                                    <td>Required</td>
                                </tr>                   
                                <tr>
                                    <td>dev_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>						
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    transaction_id: "56"
    user_id: "64"
    credit: "2070"
    created: "2015-11-03 01:28:07"
    message: "601"
}
                        </pre>

                        </p>
                        <hr>

                        <!-- Add Credit (Scratch Card)-->

                        <h2 id="add_scratchcard"><b> Add Credit (By Scratch card)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/AddScratchCard</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p> 		<table>
                            <tbody>
                                <tr>						
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>						
                                </tr>					
                                <tr>
                                    <td>card_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>						
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response</h2>
                        <pre class="prettyprint">
{
    error: false
    transaction_id: "51"
    user_id: "64"
    credit: "2020"
    created: "2015-11-03 01:19:21"
    message: "601"
}

{
    error: true
    message: "Invalid Card Number."
}
                        </pre>

                        </p>
                        <hr>


                        <!-- Add Credit (Scratch Card)-->
                        <h2 id="transfer_credit"><b> Transfer Credit (By UserPIN)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/TransferCredit</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>credit</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    transaction_id: "11"
    user_id: "64"
    credit: "555"
    debit: "20.00"
    gps: "gps12"
    dev_id: "APA91bEA7vJ1TPR1eROiQMAPOGjBxNg15fvOJxsOQ5A8WRckMMSBa_cxVrXelU6j_lJPDFHbWEUX8UOhqM1hGz6Nano27zrgDeDvul6omKIVQIvTxkxbH5zLzn4Qw4lJe8Ja4XYG7_mh"
    created: "2015-11-02 02:57:52"
    message: "601"
}

{
    error: true
    message: "Invalid User Pin. Please try again."
}

{
    error: true
    message: "You cannot transfer credit to your own account."
}

                        </pre>
                        </p>
                        <hr>
                        <!-- Available Balance-->
                        <h3 id="available_balance"><b> Available Balance </b></h3>
                        <p>
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/AvailableBalance</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Get</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint" style="text-align: left";>
{
    "error": false
    "credit": "9,691.00"
    "debit": "309.00"
    "daily_contributions_credit": "0.00"
    "credit_sum": "9,691.00"
    "currency_code": "USD"
}
                        </pre>
                        </p>





                        <!-- Sign up -->
                        <h3 id="bank_transfer"><b> Bank Transfer</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/bankTransfer</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>bank</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>branch</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>account_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>first_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>last_name</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>address</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    transaction_Id: "9"
    user_id: "64"
    dev_id: "APA91bEA7vJ1TPR1eROiQMAPOGjBxNg15fvOJxsOQ5A8WRckMMSBa_cxVrXelU6j_lJPDFHbWEUX8UOhqM1hGz6Nano27zrgDeDvul6omKIVQIvTxkxbH5zLzn4Qw4lJe8Ja4XYG7_mh"
    credit: "590"
    debit: "10.00"
    gps: "gps123"
    message: "601"
}
                        </pre>
                        </p>
                        <hr>





                        <!-- Sign up -->
                        <h3 id="cash_transfer"><b>Cash Transfer</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/cashTransfer</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>phone_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>secret_code</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>first_name</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>last_name</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>address</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    transaction_Id: "10"
    user_id: "64"
    dev_id: "APA91bEA7vJ1TPR1eROiQMAPOGjBxNg15fvOJxsOQ5A8WRckMMSBa_cxVrXelU6j_lJPDFHbWEUX8UOhqM1hGz6Nano27zrgDeDvul6omKIVQIvTxkxbH5zLzn4Qw4lJe8Ja4XYG7_mh"
    credit: "575"
    debit: "15.00"
    gps: "gps12"
    message: "601"
}
                        </pre>
                        </p>			
                        <hr>            


                        <!-- Sign up -->
                        <h3 id="my_recent_transactions"><b>My Recent Transactions</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/myRecentTransactions</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>limit</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"my_transactions": [4]
	0:  {
		"transaction_id": "21"
		"description": "Balance Added via Credit Card"
		"debit": "0.00"
		"credit": "120.00"
		"transaction_date": "2015-11-30 06:35:10"
		"mode": "CC"
	}-
	1:  {
		"transaction_id": "15"
		"description": ""
		"debit": "0.00"
		"credit": "270.00"
		"transaction_date": "2015-11-27 02:38:11"
		"mode": "CC"
	}-
	2:  {
		"transaction_id": "14"
		"description": ""
		"debit": "0.00"
		"credit": "200.00"
		"transaction_date": "2015-11-27 02:36:12"
		"mode": "CC"
	}-
	3:  {
		"transaction_id": "4"
		"description": ""
		"debit": "0.00"
		"credit": "400.00"
		"transaction_date": "2015-11-27 02:15:51"
		"mode": "CC"
	}-
	-
	"message": "My Recent Transactions."
}
                        </pre>
                        </p>			
                        <hr> 

                        <!-- Sign up -->
                        <h3 id="my_transactions"><b>My Transactions</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/myTransactions</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>from_date</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>to_date</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>limit</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>offset</td>						
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    "error":false
	"my_transactions":
	[
		{
			"transaction_id":"22"
			"description":"0"
			"debit":"100.00"
			"credit":"0.00"
			"transaction_date":"2015-11-02 06:51:45"
		}
		{
			"transaction_id":"25"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 06:55:58"
		}
		{
			"transaction_id":"27"
			"description":"0"
			"debit":"0.00"
			"credit":"5.00"
			"transaction_date":"2015-11-02 07:09:23"
		}
		{
			"transaction_id":"29"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:21:50"
		}
		{
			"transaction_id":"31"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:22:37"
		}
		{
			"transaction_id":"33"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:23:07"
		}
		{
			"transaction_id":"35"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:24:34"
		}
		{
			"transaction_id":"37"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:26:59"
		}
		{
			"transaction_id":"39"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:27:16"
		}
		{
			"transaction_id":"41"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:29:12"
		}
	]
	"message":"My Transactions."
}
                        </pre>
                        </p>			
                        <hr>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <!-- Sign up -->
                        <h3 id="my_transaction_checkSlip"><b>My Transactions Slip</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/checkSlip</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>file_name</td>
                                    <td>Required</td>
                                </tr>
                                
                                
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    "error":false
	"my_transactions":
	[
		
		{
			"transaction_id":"41"
			"description":"0"
			"debit":"0.00"
			"credit":"10.00"
			"transaction_date":"2015-11-02 07:29:12"
		}
	]
	"message":"My Transactions."
}
                        </pre>
                        </p>			
                        <hr>
                        
                        
                        
                        


                        <!-- Start -->
                        <h3 id="add_friend_by_pin"><b>Add Friend (by PIN)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/friends/add_friend</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
In case of successfully Friend Request Sent.
{
    error: false
    message: "Friend request sent."
}
In case of Friend request already sent
{
	error: true
	message: "Friend request already sent."
}
In case of Friend already (Accepted Request).
{
	error: true
	message: "Friend already."
}
In case of same User (Api Key) sending request to its own account (userPIN).
{
	error: true
	message: "You cannot send request to your own account."
}
In case of Invalid Pin Number Entered
{
    error: true
    message: "Invalid User Pin. Please try again."
}
                        </pre>
                        </p>			
                        <hr>  
                        <!-- End -->
                        <h3 id="friendlist"><b>Friendlist</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/friends/get_friendlist</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
error: false
friends: [2]
    0:  {
        status_id: "2"
        request_sent: "2015-10-28 15:50:42"
        user_id: "55"
        first_name: "abrar"
        last_name: "ahmed"
        phone_number: "12346789"
        address: "0"
        city: "0"
        country: "0"
        user_pin: "7511274"
        email: "abrar@devdesks.com"
        currency: "0"
        status: "approved"
    }
    1:  {
        status_id: "2"
        request_sent: "2015-10-28 08:09:10"
        user_id: "21"
        first_name: "faisal"
        last_name: "fa"
        phone_number: "432"
        address: "stp"
        city: "islamabad"
        country: "pakistan"
        user_pin: "1211196"
        email: "a@y.com"
        currency: "usd"
        status: "approved"
    }
}	</pre>
                        </p>			
                        <hr>  




                        <!-- Sign up -->
                        <h3 id="requests_sent"><b>Requests Sent</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/friends/requests_sent</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
error: false
requests: [3]
    0:  {
        status_id: "1"
        request_sent: "2015-10-28 17:02:54"
        user_id: "20"
        first_name: "abrar"
        last_name: "ahmed"
        phone_number: "123"
        address: "F7 markaz"
        city: "Islamabad"
        country: "Pakistan"
        user_pin: "7767331"
        email: "abrar1@devdesks.com"
        currency: "PKR"
        status: "pending"
    }
	1:  {
        status_id: "1"
        request_sent: "2015-10-28 17:03:53"
        user_id: "22"
        first_name: "Muhammad"
        last_name: "Faisal"
        phone_number: "1234"
        address: "qwerty"
        city: "wah"
        country: "Pakistan"
        user_pin: "6917939"
        email: "mfaisal@devdesks.com"
        currency: "rs"
        status: "pending"
    }
	2:  {
        status_id: "1"
        request_sent: "2015-10-28 17:03:59"
        user_id: "23"
        first_name: "Muhammad"
        last_name: "Bilal"
        phone_number: "3325124361"
        address: "F-5 Islamabad"
        city: "Islamabad"
        country: "Pakistan"
        user_pin: "8753425"
        email: "muhammad.bilal@devdesks.com"
        currency: "Rs"
        status: "pending"
    }
}	</pre>
                        </p>			
                        <hr>     
                        .
                        <!-- Sign up -->
                        <h3 id="pending_requests"><b>Pending Requests</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/friends/pending_requests</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
error: false
requests: [3]
    0:  {
        status_id: "1"
        request_sent: "2015-10-28 17:02:54"
        user_id: "20"
        first_name: "abrar"
        last_name: "ahmed"
        phone_number: "123"
        address: "F7 markaz"
        city: "Islamabad"
        country: "Pakistan"
        user_pin: "7767331"
        email: "abrar1@devdesks.com"
        currency: "PKR"
        status: "pending"
    }
	1:  {
        status_id: "1"
        request_sent: "2015-10-28 17:03:53"
        user_id: "22"
        first_name: "Muhammad"
        last_name: "Faisal"
        phone_number: "1234"
        address: "qwerty"
        city: "wah"
        country: "Pakistan"
        user_pin: "6917939"
        email: "mfaisal@devdesks.com"
        currency: "rs"
        status: "pending"
    }
	2:  {
        status_id: "1"
        request_sent: "2015-10-28 17:03:59"
        user_id: "23"
        first_name: "Muhammad"
        last_name: "Bilal"
        phone_number: "3325124361"
        address: "F-5 Islamabad"
        city: "Islamabad"
        country: "Pakistan"
        user_pin: "8753425"
        email: "muhammad.bilal@devdesks.com"
        currency: "Rs"
        status: "pending"
    }
}	</pre>
                        </p>			
                        <hr>        



                        <!-- Sign up -->
                        <h3 id="update_friendlist"><b>Friend Request Response</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/friends/requestResponse</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>status_id</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    message: "Friend request approved."
}

{
    error: false
    message: "Friend request rejected."
}

{
    error: true
    message: "Invalid User Pin. Please try again."
}

                        </pre>
                        </p>			
                        <hr>

                        <!-- Sign up -->
                        <h3 id="unfriend"><b>Un-friend Imali User</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/friends/unfriend</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    error: false
    message: "Khurram Shehzad removed from your friends list."
}
                        </pre>
                        </p>			
                        <hr>          
                        <!-- common/getUserNameByPin -->
                        <h3 id="get_full_name_by_pin"><b>Get Full Name (By Pin)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/common/getNameByPin</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	full_name: "Muhammad Bilal"
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end common/getUserNameByPin -->
                        <!-- common/getUserNameByPin -->
                        <h3 id="get_basic_information"><b>Get Basic Information</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/common/basicInfo</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	first_name: "Khurram"
	last_name: "Shehzad"
	currency: "Pakistani Rupee (PKR)"
	credit: "16992"
}
                        </pre>
                        </p>			
                        
                        <!--<hr>
                        <h3 id="available_currencies"><b>List Available Currencies</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">List Available Currencies</td>
                                    <td>api/v1/common/available_currencies</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>			
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"data": [2]
	0:  {
		"id": "1"
		"currency": "US Dollar"
		"symbol": "$"
		"code": "USD"
		"status": "1"
	}-
	1:  {
		"id": "2"
		"currency": "Naira"
		"symbol": "₦"
		"code": "NGN"
		"status": "1"
	}-
	-
	"message": "All available currency types."
}
                        </pre>
                        </p>
                        
                        
                        
                        			
-->                        <hr>			
                        <!-- end common/getUserNameByPin -->
                        <!-- Daily Contributions / getUserNameByPin -->
                        <h3 id="subscribe_me_to_dc"><b>Subscribe to Daily Contributions</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/subscribeMe</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "message": "You are now subscribed to Daily Contributions."
}

{
  "error": true,
  "message": "You are already subscribed to Daily Contributions."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end contributions/subscribeMe -->
                        <!-- Daily Contributions / mySubscriptionStatus -->
                        <h3 id="my_subscription_status_dc"><b>My Subscription Status (Daily Contributions)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/mySubscriptionStatus</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "subscription_status": 0
}

{
  "error": false,
  "subscription_status": 1
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end contributions/mySubscriptionStatus -->
                        <!-- Daily Contributions/addDailyContribution -->
                        <h3 id="add_balance_to_dc"><b>Add Balance (Daily Contributions)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/addBalance</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>dc_amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>
                                    <td>(Optional)</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>(Optional)</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	transaction_id: "12"
	user_id: "95"
	credit: "900"
	debit: "20.00"
	gps: ""
	dev_id: "asdq23qeaw123wae123"
	created: "2015-11-27 02:25:01"
	message: "Balance added to your Daily Contribution Account."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end Daily Contributions /addDailyContribution -->
                        <!-- Daily Contributions / getUserNameByPin -->
                        <h3 id="view_balance_dc"><b>View Balance (Daily Contributions)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/viewBalance</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>

                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	credit: "130.00"
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end common/getUserNameByPin -->
                        <!-- Daily Contributions / getUserNameByPin -->
                        <h3 id="view_all_dc"><b>View Contributions (List All)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/viewAll</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                            <!--
                            <table>
                                                <tbody>
                                                <tr>
                                                        <td><strong>Parameter</strong></td>
                                                        <td><strong>Description</strong></td>
                                                </tr>
                                                <tr>
                                                        <td>from_date</td>
                                                        <td>Required</td>
                                                </tr>
                                                <tr>
                                                        <td>to_date</td>
                                                        <td>Required</td>
                                                </tr>
                                                <tr>
                                                        <td>limit</td>
                                                        <td>Required</td>
                                                </tr>
                                                <tr>
                                                        <td>offset</td>
                                                        <td>Required</td>
                                                </tr>
                                                </tbody>
                                        </table>            
                                        <hr>
                            -->
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	my_dc_transactions: [8]
	0:  {
		transaction_id: "5"
		description: "Daily Contribution Transaction Test."
		debit: "50.00"
		credit: "0.00"
		transaction_date: "2015-11-27 02:18:30"
	}-
	1:  {
		transaction_id: "6"
		description: "Daily Contribution Transaction Test."
		debit: "0.00"
		credit: "50.00"
		transaction_date: "2015-11-27 02:18:30"
	}-
	2:  {
		transaction_id: "8"
		description: "Daily Contribution Transaction Test."
		debit: "50.00"
		credit: "0.00"
		transaction_date: "2015-11-27 02:18:50"
	}-
	3:  {
		transaction_id: "9"
		description: "Daily Contribution Transaction Test."
		debit: "0.00"
		credit: "50.00"
		transaction_date: "2015-11-27 02:18:50"
	}-
	4:  {
		transaction_id: "10"
		description: "Daily Contribution Transaction Test."
		debit: "10.00"
		credit: "0.00"
		transaction_date: "2015-11-27 02:22:50"
	}-
	5:  {
		transaction_id: "11"
		description: "Daily Contribution Transaction Test."
		debit: "0.00"
		credit: "10.00"
		transaction_date: "2015-11-27 02:22:50"
	}-
	6:  {
		transaction_id: "12"
		description: "Daily Contribution Transaction Test."
		debit: "20.00"
		credit: "0.00"
		transaction_date: "2015-11-27 02:25:01"
	}-
	7:  {
		transaction_id: "13"
		description: "Daily Contribution Transaction Test."
		debit: "0.00"
		credit: "20.00"
		transaction_date: "2015-11-27 02:25:01"
	}-
	-
	message: "My Daily Contribution Transactions."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end Daily Contributions/viewAll -->

                        <!-- Start Group Contributions -->
                        <!-- Daily Contributions / getUserNameByPin -->
                        <h3 id="createGroupContribution"><b>Create Group-Contributions</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/createGroupContribution</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>

                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>title</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>suggested_amount</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>

                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	title: "New Title"
	description: "New Test Description..."
	suggested_amount: "150.00"
	message: "Group-Contribution created successfully."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end Daily Contributions/viewAll -->
                        <!-- Start contributions/myGroupContributions -->
                        <h3 id="myGroupContributions"><b>View Contributions (List All)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/myGroupContributions</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	
	error: false
	my_gc_list: [2]
	0:  {
		reference_number: "GCR#565d4b7f87f5e"
		title: "The Title"
		description: "The Description..."
		suggested_amount: "200.00"
		status: "1"
	}-
	1:  {
		reference_number: "GCR#565d4ba3131a9"
		title: "Sample Title"
		description: "Sample Description..."
		suggested_amount: "200.00"
		status: "1"
	}-
	-
	message: "List my Group-Contributions."
	
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end contributions/myGroupContributions -->
                        <!-- Start contributions/invitePeopleGc -->
                        <h3 id="invitePeopleGc"><b>Invite People to Group-Contribution</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/invitePeopleGc</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>

                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_pin</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gc_reference_number</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>

                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	message: "Group-Contribution request successfully sent."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end contributions/invitePeopleGc -->
                        <!-- Start contributions/invitePeopleGc -->
                        <h3 id="requestResponseGc"><b>Respond to Group-Contribution</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/requestResponseGc</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>

                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>gc_reference_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>status</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>--------- If status is 4 --------</td>
                                    <td>-------------</td>
                                </tr>
                                <tr>
                                    <td>gc_amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>gps</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>

                            </tbody>
                        </table>            
                        <hr>

                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	transaction_id: "106"
	user_id: "98"
	credit: "1508"
	debit: "108.00"
	gps: ""
	dev_id: "APA91bGX8Jd-biGeY3SsiHtr5hMQpO42hKq6zvERE9t_VujrtCg1-sYGf_mU3d0MXCGyaDUa-tLjkQi2_DB-QV4JlymC2cfw_9FemDqNXcJMutnMiXZfqD3WhqWEFwgciKYZ9ctm_-5-"
	created: "2015-12-04 02:07:07"
	message: "You have credited 108 to Group-Contributions Account."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end contributions/invitePeopleGc -->
                        <!-- Start contributions/gcHistory -->
                        <h3 id="gcHistory"><b>View Group-Contribution History</b></h3>
                        <p>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/gcHistory</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>

                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>gc_reference_number</td>
                                    <td>Required</td>
                                </tr>				
                            </tbody>
                        </table>            
                        <hr>

                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	error: false
	gc_all_requests: [1]
	0:  {
		first_name: "Haseeb"
		last_name: "Ur Rehman"
		pin_number: "7983292"
		suggested_amount: "80.00"
		paid_amount: "108.00"
		status: "4"
		gc_reference_number: "GCR#565d507621b6b"
	}-
	-
	message: "Group-Contribution all requests."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- end contributions/invitePeopleGc -->

                        <!-- Start - myGcRequests -->
                        <h3 id="myGcRequests"><b>My Group-Contributions Requests</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/myGcRequests</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"my_gc_requests": [5]
	0:  {
		"gc_reference_number": "GCR#565d507621b6b"
		"suggested_amount": "80.00"
		"paid_amount": "108.00"
		"status": "4"
	}-
	1:  {
		"gc_reference_number": "GCR#565d4fb8a8637"
		"suggested_amount": "200.00"
		"paid_amount": null
		"status": "1"
	}-
	2:  {
		"gc_reference_number": "GCR#566a735da77d2"
		"suggested_amount": "10.00"
		"paid_amount": null
		"status": "1"
	}-
	3:  {
		"gc_reference_number": "GCR#565d4b7f87f5e"
		"suggested_amount": "200.00"
		"paid_amount": null
		"status": "1"
	}-
	4:  {
		"gc_reference_number": "GCR#566eaa3103f6c"
		"suggested_amount": "15.00"
		"paid_amount": null
		"status": "1"
	}-
	-
	"message": "My Group-Contribution Requests."
}
                        </pre>
                        </p>			
                        <hr>  
                        <!-- End - myGcRequests -->
                        <!-- End Group Contributions -->
                        <!-- Start-Charities -->
                        <!-- Contributions/createCharity -->
                        <h3 id="createCharity"><b>Create Charity</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/createCharity</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>

                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>title</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>file_name</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>

                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"title": "Test Title"
	"description": "Test Description."
	"message": "Charity was successfully created."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End Contributions/createCharity -->
                        
                        
                        <!-- Contributions/inviteToCharity -->
                        <h3 id="inviteToCharity"><b>Invite To Charity</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/inviteToCharity</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>

                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>id (charity)</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>user_list</td>
                                    <td>Required</td>
                                </tr>
                                
                            </tbody>
                        </table>            
                        <hr>

                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "message": "Invitation sent successfully."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End Contributions/inviteToCharity -->
                        
                        <!-- Start Contributions/pendingInvitations -->
                        <h3 id="pendingInvitations"><b>Pending Charity's Invitations</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/pendingInvitations</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "invitations": [
    {
      "invitation_id": "1",
      "title": "devdesks title",
      "description": "devdesks charity",
      "image": "http://dev-web.devdesks.com/imali/uploads/charity/1409608946.png",
      "reference_number": "vkv3PRkqrTd6",
      "sender_id": "143",
      "firstName": "Naveed",
      "lastName": "Arshad"
    }
  ]
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End Contributions/pendingInvitations -->
                        
                        
                        
                        
                        <!-- Start Contributions/responseToInvitations -->
                        <h3 id="responseToInvitation"><b>Response To Invitation</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/responseToInvitation</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                            </table>
                        <hr>
                        </p>
                        <p>
                            

                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>status</td>
                                    <td>Required</td>
                                </tr>
                                
                            </tbody>
                        </table>            
                        <hr>

                            

                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "reference_number": "CR#5722ffee4828a",
  "message": "Pending."
}

{
  "error": false,
  "reference_number": "CR#5722ffee4828a",
  "message": "Accepted."
}

{
  "error": false,
  "reference_number": "CR#5722ffee4828a",
  "message": "Rejected."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End Contributions/responseToInvitations -->
                        
                        
                        
                        
                        <!-- Start Contributions/getCharities -->
                        <h3 id="getCharities"><b>Get All Charities</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/getCharities</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "active_charities_list_all": [
    {
      "charity_id": "1",
      "charity_flag": "1",
      "invitation_flag": 4,
      "reference_number": "vkv3PRkqrTd6",
      "title": "devdesks title",
      "description": "devdesks charity",
      "image": "1409608946.png",
      "created_by": "Naveed Arshad"
    },
    {
      "charity_id": "2",
      "charity_flag": "2",
      "invitation_flag": 4,
      "reference_number": "b3hegJ2EVpOy",
      "title": "one",
      "description": "two",
      "image": "2030047012.png",
      "created_by": "Nauman Malik"
    },
    {
      "charity_id": "3",
      "charity_flag": "2",
      "invitation_flag": 4,
      "reference_number": "PPxIl60zCoK5",
      "title": "TS4A",
      "description": "Africa",
      "image": "1326240467.png",
      "created_by": " "
    },
    {
      "charity_id": "4",
      "charity_flag": "2",
      "invitation_flag": 4,
      "reference_number": "VIRt7ATqiP3V",
      "title": "TS4A",
      "description": "Africa",
      "image": "328011767.png",
      "created_by": " "
    }
  ]
}</pre>
                        </p>			
                        <hr>
                        <!-- End Contributions/getCharities -->
                        <!-- Start Contributions/getMyCharities -->
                        <h3 id="getCharities"><b>Get My Charities</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/getMyCharities</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "my_charities_list_all": [
    {
      "reference_number": "vkv3PRkqrTd6",
      "title": "devdesks title",
      "description": "devdesks charity",
      "image": "http://dev-web.devdesks.com/imali/uploads/charity/1409608946.png",
      "status": "1",
      "created_by": "Naveed Arshad"
    }
  ]
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End Contributions/getMyCharities -->
                        <!-- Start Contributions/removeCharity -->
                        <h3 id="add_friend_by_pin"><b>Remove My Charity</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/removeCharity</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>

                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>reference_number</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"message": "Charity was removed successfully."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End-removeCharity -->
                        <!-- Start-contributions/contributeCharity -->
                        <h2 id="contributeCharity"><b> Contribute to charity</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/contributeCharity</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>reference_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>gps</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"transaction_id": "199"
	"user_id": "97"
	"credit": "843"
	"debit": "99.00"
	"gps": "92123,32192"
	"dev_id": "dbf4a5b95dab7b48800255b091e407d7204d222ba2b509468946bf22aab3691e"
	"created": "2015-12-29 02:49:07"
	"message": "You have successfully donated 99."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- Start contributions/myDonations -->
                        <h3 id="myDonations"><b>My Donations (List All)</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/myDonations</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"my_donations_list": [2]
	0:  {
		"donated_amount": "55.00"
		"reference_number": "CR#568234866602b"
		"title": "Title"
		"description": "Description."
	}-
	1:  {
		"donated_amount": "99.00"
		"reference_number": "CR#568234866602b"
		"title": "Title"
		"description": "Description."
	}-
	-
	"message": "List all my charity donations."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- Start - get_charity -->
                        <h2 id="get_charity"><b>Get Charity (Details)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/contributions/get_charity</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>					
                                <tr>
                                    <td>reference_number</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"charity_data": [3]
	0:  {
		"firstName": "Zohaib"
		"lastName": "Rehmani"
		"amount_paid": "22.00"
		"description": "my cgarity"
		"created": "2016-02-19 07:06:14"
	}-
	1:  {
		"firstName": "Haseeb"
		"lastName": "Ur Rehman"
		"amount_paid": "100.00"
		"description": "Good initiative."
		"created": "2016-02-19 07:07:06"
	}-
	2:  {
		"firstName": "Haseeb"
		"lastName": "Ur Rehman"
		"amount_paid": "24.00"
		"description": "Enjoy..."
		"created": "2016-02-22 02:53:23"
	}-
	-
	"total_charity_amount": "146.00"
	"message": "Specific charity details."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End- get_charity -->
                        <!-- End-Charities -->
                        <!-- Start-Market -->

                        <!-- Add new item to Market -->
                        <h2 id="add"><b>Add new item to market</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/market/add</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>					
                                <tr>
                                    <td>category</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>item_name</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>description</td>						
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>item_image</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gallery_images</td>
                                    <td>Optional</td>
                                </tr>			
                                <tr>
                                    <td>price</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>contact_direct</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"message": "New market item was added successfully."
}

                        </pre>
                        </p>
                        <hr>
                        <!-- Start Get All Market Items -->
                        <h3 id="get_all_items"><b>Get All Market Items</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/market/get_all_items</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"all_market_items": [3]
	0:  {
		"id": "1"
		"item_name": "Dell Audi 15"
		"description": "This is a used laptop for sale on 50% price only."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"price": "50.00"
	}-
	1:  {
		"id": "2"
		"item_name": "HP Pavilion G6"
		"description": "This is a used laptop for sale on 70% price only."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"price": "80.00"
	}-
	2:  {
		"id": "3"
		"item_name": "Denim Jeans"
		"description": "100% pure denim jeans by Nike."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"price": "255.00"
	}-
	-
	"message": "Market items list all."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- start - get_my_all_items -->
                        <h3 id="get_my_all_items"><b>Get All My Market Items</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/market/get_my_all_items</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"my_all_market_items": [3]
	0:  {
		"id": "1"
		"item_name": "Dell Audi 15"
		"description": "This is a used laptop for sale on 50% price only."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"price": "50.00"
	}-
	1:  {
		"id": "2"
		"item_name": "HP Pavilion G6"
		"description": "This is a used laptop for sale on 70% price only."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"price": "80.00"
	}-
	2:  {
		"id": "3"
		"item_name": "Denim Jeans"
		"description": "100% pure denim jeans by Nike."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"price": "255.00"
	}-
	-
	"message": "My market items list all."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- start - get_item -->
                        <h2 id="get_item"><b>Get Item (Product) Details</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/market/get_item</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>					
                                <tr>
                                    <td>item_id</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"item_data": {
		"id": "1"
		"userId": "98"
		"category": "Electronics"
		"item_name": "Dell Audi 15"
		"description": "This is a used laptop for sale on 50% price only."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"gallery_images": "123e12qeq1234e1q23e2tr3f24rt52r3r,123qwsedq24123ed1q3werwer,123wse14eeeeeq23r45,sdfw53swerfr243we4rf"
		"price": "50.00"
		"contact_direct": "1"
		"available": "1"
		"deleted": null
	}-
	"contact_details": {
		"first_name": "Haseeb"
		"last_name": "Ur Rehman"
		"phone_number": "03355477889"
		"user_pin": "7983292"
		"email": "haseeb@devdesks.com"
	}-
	"message": "Item details."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- Start remove_item -->
                        <h2 id="remove_item"><b>Remove Market Item (Product)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/market/remove_item</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>					
                                <tr>
                                    <td>item_id</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"message": "Item was successfully removed."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- start - search_items -->
                        <h2 id="search_items"><b>Search Market Items (Products)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/market/search_items</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>					
                                <tr>
                                    <td>price_from</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>price_to</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>keywords (i.e. laptop,jeans,others) </td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>category</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"items": [3]
	0:  {
		"id": "1"
		"userId": "98"
		"category": "Electronics"
		"item_name": "Dell Audi 15"
		"description": "This is a used laptop for sale on 50% price only."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"gallery_images": "123e12qeq1234e1q23e2tr3f24rt52r3r,123qwsedq24123ed1q3werwer,123wse14eeeeeq23r45,sdfw53swerfr243we4rf"
		"price": "50.00"
		"contact_direct": "1"
		"available": "1"
		"deleted": null
	}-
	1:  {
		"id": "2"
		"userId": "98"
		"category": "Electronics"
		"item_name": "HP Pavilion G6"
		"description": "This is a used laptop for sale on 70% price only."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"gallery_images": "123e12qeq1234e1q23e2tr3f24rt52r3r,123qwsedq24123ed1q3werwer,123wse14eeeeeq23r45,sdfw53swerfr243we4rf"
		"price": "80.00"
		"contact_direct": "0"
		"available": "1"
		"deleted": "1"
	}-
	2:  {
		"id": "3"
		"userId": "98"
		"category": "Clothing"
		"item_name": "Denim Jeans"
		"description": "100% pure denim jeans by Nike."
		"item_image": "qewqweqweqweqe12312312weq124e2q3r23qrdq2r"
		"gallery_images": "123e12qeq1234e1q23e2tr3f24rt52r3r,123qwsedq24123ed1q3werwer,123wse14eeeeeq23r45,sdfw53swerfr243we4rf"
		"price": "255.00"
		"contact_direct": "1"
		"available": "1"
		"deleted": null
	}-
	-
	"message": "Search results market items."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- start - buy_item -->
                        <h2 id="buy_item"><b>Buy Market Item (Product)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/market/buy_item</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>					
                                <tr>
                                    <td>item_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>gps</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"transaction_id": "220"
	"user_id": "98"
	"credit": "320"
	"debit": "50.00"
	"gps": "1312312313,3123123123"
	"dev_id": "sfdw4r2w3324rq23rq245r2w3r2w35r24q3"
	"created": "2015-12-30 05:55:29"
	"message": "Your request for buy item is received."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End-Market -->
                        <!-- Start-Promotions -->

                        <!-- Start-getPromotions-->
                        <h3 id="getPromotions"><b>Get Promotions</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/promotions/getPromotions</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"promotions": [1]
	0:  {
		"promotionId": "1"
		"title": "Coke"
		"description": "Sample description for coke promotion."
		"image": "coke_promo.png"
		"status": "1"
		"created": "2016-01-15 02:10:27"
	}-
	-
	"message": "Promotions list all."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- -->
                        <h2 id="claimPromotionCopoun"><b>Claim Promo Coupon</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/promotions/claimPromotionCopoun</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p> 		
                        <table>
                            <tbody>
                                <tr>						
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>						
                                </tr>					
                                <tr>
                                    <td>promotion_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>coupon_code</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>						
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response</h2>
                        <pre class="prettyprint">
{
	"error": false
	"transaction_id": "328"
	"user_id": "97"
	"credit": "7412"
	"created": "2016-01-15 06:05:59"
	"message": "Congratulations! You have won prize amount of 10.00 $."
}
                        </pre>					
                        </p>
                        <hr>

                        <!-- Start Utility Bills -->
                        <h3 id="getProviderTypes"><b>List Provider Types</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/utility_bills/getProviderTypes</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>


                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"types": [3]
	0:  {
		"id": "1"
		"type": "utility"
	}-
	1:  {
		"id": "2"
		"type": "telco"
	}-
	2:  {
		"id": "3"
		"type": "isp"
	}-
	-
	"message": "List all provider types."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- Start getProviderList -->
                        <h2 id="getProviderList"><b> List All Providers (By Type)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/utility_bills/getProviderList</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>type</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"providers": [1]
	0:  {
		"id": "3"
		"type": "3"
		"name": "C Company"
		"status": "1"
	}-
	-
	"message": "List all provider."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End getProviderList -->
                        <!-- Start saveBill -->
<!--                        <h2 id="saveBill"><b> Save Bill</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/utility_bills/saveBill</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>provider_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>customer_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>comments</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"message": "Your bill details are successfully saved."
}
                        </pre>
                        </p>
                        <hr>-->
                        <!-- End saveBill -->
                        <!-- Start getMyBills -->
                        <h3 id="getMyBills"><b>Get My Bills</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/utility_bills/getMyBills</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"types": [6]
	0:  {
		"id": "1"
		"provider_name": "Company A"
		"provider_type": "utility"
		"customer_id": "123"
		"comments": null
		"dated": "2016-01-28 06:22:47"
		"paid": "0"
	}-
	1:  {
		"id": "12"
		"provider_name": "Company B"
		"provider_type": "telco"
		"customer_id": "12"
		"comments": "12"
		"dated": "2016-02-01 04:26:20"
		"paid": "0"
	}-
	2:  {
		"id": "5"
		"provider_name": "C Company"
		"provider_type": "isp"
		"customer_id": "abc123"
		"comments": null
		"dated": "2016-02-01 01:16:37"
		"paid": "0"
	}-
	3:  {
		"id": "6"
		"provider_name": "C Company"
		"provider_type": "isp"
		"customer_id": "abc123"
		"comments": null
		"dated": "2016-02-01 01:17:29"
		"paid": "0"
	}-
	4:  {
		"id": "7"
		"provider_name": "C Company"
		"provider_type": "isp"
		"customer_id": "abc123"
		"comments": null
		"dated": "2016-02-01 01:18:03"
		"paid": "0"
	}-
	5:  {
		"id": "9"
		"provider_name": "C Company"
		"provider_type": "isp"
		"customer_id": "98"
		"comments": null
		"dated": "2016-02-01 02:38:14"
		"paid": "0"
	}-
	-
	"message": "List all my payable bills."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End getMyBills -->
                        <!-- Start payBill -->
                        <h2 id="payBill"><b> Pay Bill</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/utility_bills/payBill</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>provider_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>customer_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>gps</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"transaction_id": "514"
	"transaction_track_code": "77410"
	"user_id": "97"
	"paid_amount": "10"
	"credit": "5990"
	"debit": "10.00"
	"gps": "123123,312312"
	"dev_id": "qwe123123qwe123"
	"created": "2016-02-01 03:02:10"
        "currency_code": "USD"
	"message": "You have charged 10 for utility bill."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End payBill -->
                        <!-- Start myPaidBillsHistory -->
                        <h3 id="myPaidBillsHistory"><b>Get My Paid Bills History</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/utility_bills/myPaidBillsHistory</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"types": [2]
	0:  {
		"id": "8"
		"provider_name": "C Company"
		"provider_type": "isp"
		"customer_id": "abc123"
		"comments": null
		"dated": "2016-02-01 01:18:40"
		"paid": "1"
		"transaction_track_code": "19981"
	}-
	1:  {
		"id": "11"
		"provider_name": "C Company"
		"provider_type": "isp"
		"customer_id": "98"
		"comments": null
		"dated": "2016-02-01 02:51:47"
		"paid": "1"
		"transaction_track_code": "77410"
	}-
	-
	"message": "List all provider types."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End myPaidBillsHistory -->
                        <!-- Start Request Cash Out -->
                        <h2 id="request_cashout"><b> Request (Cash Out)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/cashout/request_cashout</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>cashout_amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>cashout_charges</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>bank_account_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>gps</td>						
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"transaction_id": "722"
	"user_id": "97"
	"credit": "5516"
        "currency_code":"USD"
	"created": "2016-02-15 00:48:13"
	"message": "Your cash out request is received and will be processed in 3 working days."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End request cash out -->
                        <!-- Start My Cash in hand -->
                        <h3 id="cash_in_hand"><b>My Cash In Hand</b></h3>
                        <p>
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/cashout/cash_in_hand</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>	
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": true
	"total": "₦ 578.00"
	"message": "Your cash in hand."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- End My Cash In Hand -->
                        <!-- End Cash Out -->
                        <!-- Start Charges -->
                        <h2 id="charges"><b> Charges</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/cashout/get_charges</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>cashout_type ( value = user cashout|merchant cashout )</td>
                                    <td>Required</td>
                                </tr>
                                

                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
    {
        "error": false
        "charges": [3]
            0:  {
            "start_amount_range": "0"
            "end_amount_range": "1000"
            "charges": "10"
            }-
            1:  {
            "start_amount_range": "1001"
            "end_amount_range": "2000"
            "charges": "15"
            }-
            2:  {
            "start_amount_range": "2001"
            "end_amount_range": "3000"
            "charges": "20"
            }-
        -
        "message": "List of charges"
    }
                        </pre>
                        </p>
                        <hr>
                        <!-- End  charges -->

                        <!-- Start-Merchant -->
                        <!-- save_invoice -->
                        <h2 id="save_invoice"><b> Save Invoice</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/merchant/save_invoice</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>merchant_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>details</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>net_total</td>						
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>imali_account_user_pin</td>
                                    <td>Required</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"message": "Invoice saved successfully.."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End-save_invoice -->
                        <!-- Start get_invoices -->
                        <h2 id="get_invoices"><b> Get Invoices (By Status)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/merchant/get_invoices</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>merchant_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>status [(1) = Pending (2) = Accepted (3) = Rejected (4) Cleared]</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"data": [2]
	0:  {
		"invoice_id": "1"
		"details": "[{"item_name":"rice","item_quantity":"4","unit_price":"88","item_total_price":352},{"item_name":"vegetable","item_quantity":"7","unit_price":"91","item_total_price":637},{"item_name":"lays","item_quantity":"13","unit_price":"40","item_total_price":520}]"
		"net_total": "120.00"
		"firstName": "Haseeb"
		"lastName": "Ur Rehman"
		"status": "1"
	}-
	1:  {
		"invoice_id": "2"
		"details": "[{"item_name":"rice","item_quantity":"4","unit_price":"88","item_total_price":352},{"item_name":"vegetable","item_quantity":"7","unit_price":"91","item_total_price":637},{"item_name":"lays","item_quantity":"13","unit_price":"40","item_total_price":520}]"
		"net_total": "120.00"
		"firstName": "Haseeb"
		"lastName": "Ur Rehman"
		"status": "1"
	}-
	-
	"message": "List Invoices."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End get_invoices -->
                        <!-- Start invoice_details -->
                        <h2 id="invoice_details"><b> Invoice Details</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/invoice/invoice_details</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>invoice_id</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
                    {
                        "error": false
                        "data": {
                        "invoice_id": "97"
                        "details": "[{"unit_price":"250","item_quantity":"1","item_name":"notify"}]"
                        "net_total": "250.00"
                        "merchant_title": "ABC Merchant"
                        "merchant_first_name": "Nauman"
                        "merchant_last_name": "Malik"
                        "merchant_address": "test address1"
                        "merchant_city": "Islamabad1"
                        "merchant_country": "Pakistan1"
                        "merchant_phone_number": "03335007726"
                        "user_first_name": "Khurram"
                        "user_last_name": "Shehzad"
                        "status": "4"
                        }-
                	"message": "Invoice details."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End invoice_details -->
                        <!-- Start Get-Imali User Invoices -->
                        <h2 id="get_my_invoices"><b> Imali User Get My Invoices (By Status)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/get_my_invoices</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>status [(1) = Pending (2) = Accepted (3) = Rejected (4) Cleared]</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "data": [
    {
      "id": "312",
      "merchantId": "6",
      "details": "[{\"unit_price\":\"10\",\"item_quantity\":\"4\",\"item_name\":\"dsfdsf\"},{\"unit_price\":\"20\",\"item_quantity\":\"6\",\"item_name\":\"coffee\"}]",
      "dated": "2016-05-10 02:39:09",
      "net_total": "160.00",
      "user_pin": "9199681",
      "status": "2",
      "refund_status": null
    },
    {
      "id": "311",
      "merchantId": "16",
      "details": "[{\"item_name\":\"pen\",\"item_quantity\":\"3\",\"unit_price\":\"10\"}]",
      "dated": "2016-05-09 04:11:37",
      "net_total": "30.00",
      "user_pin": "9199681",
      "status": "2",
      "refund_status": "1"
    }
  ],
  "message": "List invoices."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End- Get-Imali User Invoices -->
                        <!-- Start -->
                        <h2 id="account_balance"><b> Account Balance (Merchant)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/merchant/account_balance</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>merchant_id</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": true
	"total": "823.00"
        "credit_sum": "7,029"
        "currency_code": "USD"
	"message": "Your balance."
}
                        </pre>
                        </p>
                        <hr>
                        <!-- End -->


                        <!-- Start -->
                        <h2 id="pay_invoice"><b> Pay Bill (Merchant Invoice)</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/transactions/pay_invoice</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>

                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>

                                </tr>

                                <tr>
                                    <td>merchant_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>invoice_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>invoice_status [1=pending, 2=paid, 3=rejected]</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>gps</td>
                                    <td>Optional</td>
                                </tr>
                                <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
	"error": false
	"transaction_id": "117"
	"user_id": "114"
	"credit": "2682"
	"debit": "100.00"
	"gps": "123123,31231"
	"dev_id": "qer2134qrwqe231423qwer1234"
	"created": "2016-02-26 06:07:11"
	"message": "You have successfully paid the invoice bill."
}
                        </pre>
                        </p>
                        <hr>





                        <!-- User Refunds Start -->
                        <h2 id="user_refunds"><b> User Refunds</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/refunds/user_refunds</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Get</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
 
{
    "error": false
    "refunds": [2]
    0:  {
        "refund_id": "1"
        "invoice_no": "22"
        "message": "Please refund my money"
        "datetime": "2016-04-22 12:14:53"
    }
    1:  {
        "refund_id": "2"
        "invoice_no": "21"
        "message": "Please refund my money back"
        "datetime": "2016-04-22 12:42:26"
    }
}                    </pre>
                        </p>
                        <hr>
                        <!-- User Refunds END -->
                        
                        
                        <!-- Merchant Refunds Start -->
                        <h2 id="merchant_refunds"><b> Merchant Refunds</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/refunds/merchant_refunds</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Get</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
 
{
    "error": false
    "refunds": [2]
    0:  {
        "refund_id": "1"
        "invoice_no": "22"
        "message": "Please refund my money"
        "datetime": "2016-04-22 12:14:53"
    }
    1:  {
        "refund_id": "2"
        "invoice_no": "21"
        "message": "Please refund my money back"
        "datetime": "2016-04-22 12:42:26"
    }
}                    </pre>
                        </p>
                        <hr>
                        <!-- Merchant Refunds END -->
                        
                        
                        <!-- Merchant Refunds List Start adnan -->
                        <h2 id="merchant_refunds_list"><b> Merchant Refunds List</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/refunds/merchant_refunds_list</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>refund_status</td>
                                    <td>Required</td>
                                </tr>
                                                                
                            </tbody>
                        </table>
                        
                        
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
 
{
    "error": false
    "refunds": [2]
    0:  {
        "refund_id": "1"
        "invoice_no": "22"
        "message": "Please refund my money"
        "datetime": "2016-04-22 12:14:53"
    }
    1:  {
        "refund_id": "2"
        "invoice_no": "21"
        "message": "Please refund my money back"
        "datetime": "2016-04-22 12:42:26"
    }
}                    </pre>
                        </p>
                        <hr>
                        <!-- Merchant Refunds List END -->
                        
                        
                        
                        <!-- Create Refund Start -->
                        <h2 id="create_refund"><b> Create Refund</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/refunds/create_refund</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>user_invoice</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>user_text</td>
                                    <td>Required</td>
                                </tr>                                
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    "error": false
    "refund_id": 1
    "invoice_no": "21"
    "message": "Email sent"
}
                     </pre>
                        </p>
                        <hr>
                        <!-- Create Refund END -->
                        
                        
                        <!-- Refund Detail Start -->
                        <h2 id="refund_detail"><b> Refund Detail</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/refunds/refund_detail</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>refund_id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>is_merchant</td>
                                    <td>Required (1 for Merchant/0 for User)</td>
                                </tr>                                
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    "error": false
    "refund": {
        "refund_id": "1"
        "message": "Please refund my money"
        "datetime": "2016-04-22 3:11 PM"
    }
    "invoice": {
        "invoice_no": "21"
        "details": "[{"item_name":"best items","item_quantity":"31","unit_price":"5"}]"
        "datetime": "2016-02-29 1:48 AM"
        "net_total": "155.00"
        "status": "4"
    }
    "user": {
        "id": "4"
        "first_name": "zohaib"
        "last_name": "devdesk"
        "merchant_title": ""
    }
}

                     </pre>
                        </p>
                        <hr>
                        <!-- Refund Detail END -->
                        
                        <!-- Response To Refund Start -->
                        <h2 id="merchant_refunds_response"><b> Response To Refunds</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/refunds/responseToRefund</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Post</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>

                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>status</td>
                                    <td>Required</td>
                                </tr>                                
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "total": "110.00",
  "credit_sum": "80.00",
  "currency": null,
  "action_status": "2",
  "message": "Refund Request is Accepted."
}

                     </pre>
                        </p>
                        <hr>
                        <!-- Response to Refund END -->
                        
                        
                        
                        
                        <!-- Exchange Rates Start -->
                        <h2 id="exchange_rates"><b> Exchange Rates</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/currency/exchange_rates</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>Get</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    "error": false
    "exchange_rates": [3]
    0:  {
        "id": "1"
        "currency_name": "Nigerian Naira"
        "currency_type": "NGN"
        "rate": "199.20"
    }
    1:  {
        "id": "2"
        "currency_name": "Pakistani Rupee"
        "currency_type": "PKR"
        "rate": "104.73"
    }
    2:  {
        "id": "3"
        "currency_name": "Euro"
        "currency_type": "EUR"
        "rate": "0.89"
    }
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Exchange Rates END -->
                        
                        
					
                        <!-- Update Currency Start -->
                        <h2 id="update_currency"><b> Update Currency</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody><tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/currency/update_currency</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>currency</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>is_merchant</td>
                                    <td>Required (1 for Merchant, 0 for User)</td>
                                </tr>                                                            
                            </tbody>
                        </table>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
    "error": false
    "currency": "USD"
    "message": "Currency updated successfully."
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Update Currency END -->
                    
                    
                    
                        <!-- Get Profile START -->
                        <h2 id="get_profile"><b> Get Profile</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/profile/get_profile</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>Get</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "userId": "143",
  "first_name": "Naveed",
  "last_name": "Arshad",
  "address": "sggf",
  "city": "dghh",
  "country_id": "156",
  "country": "Nigeria",
  "currency": "USD"
}

{
  "error": false,
  "userId": "4",
  "merchant_title": "Naveed",
  "first_name": "Arshad",
  "last_name": "naveed",
  "address": "F5",
  "city": "Islamabad",
  "country_id": "156",
  "country": "Nigeria",
  "currency": "USD"
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Get Profile END -->
                    
                    
                     <!-- Forgot User Password START -->
                        <h2 id="account_forgot_password"><b> Forgot User Password</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/signin/account_forgot_password</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>POST</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "message": "New Password have been send to your email address."
}

{
  "error": true,
  "message": "Sorry Unable to send you email."
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Forgot User Password END -->
                    


                    <!-- Forgot Merchant Password START -->
                        <h2 id="merchant_forgot_password"><b> Forgot Merchant Password</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/signin/merchant_account_forgot_password</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>POST</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>email</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "message": "New Password have been send to your email address."
}

{
  "error": true,
  "message": "Sorry Unable to send you email."
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Forgot Merchant Password END -->
                    
                    
                    <!-- Get Countries START -->
                        <h2 id="get_countries"><b> Get Countries</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/profile/get_countries</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>GET</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "countries": [
    {
      "id": "1",
      "name": "Afghanistan",
      "code": "AFG"
    },
    {
      "id": "2",
      "name": "Albania",
      "code": "ALB"
    }
  ]
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Get Countries END -->

                    
                    <!-- Get Currency START -->
                        <h2 id="get_currency"><b> Get Currency</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/profile/get_currency</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>GET</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "currency": [
    {
      "id": "1",
      "name": "Nigerian Naira",
      "type": "NGN",
      "symbol": "N"
    },
    {
      "id": "2",
      "name": "Pakistani Rupee",
      "type": "PKR",
      "symbol": "Rs"
    },
    {
      "id": "3",
      "name": "Euro",
      "type": "EUR",
      "symbol": "E"
    },
    {
      "id": "4",
      "name": "Indian",
      "type": "USD",
      "symbol": "$"
    }
  ]
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Get Currency END -->

                    
                    <!-- Update UserPIN START -->
                        <h2 id="update_userPIN"><b>Update UserPIN</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/profile/update_userpin</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>POST</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>userPIN</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>usertype</td>
                                    <td>Required (2 for User/3 for Merchant/4 for Agent</td>
                                </tr>
                            </tbody>
                        </table>
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "message": "UserPIN updated successfully."
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Update UserPIN END -->

                    
                        <!-- Auth Password START -->
                        <h2 id="auth_password"><b>Auth Password</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/profile/auth_password</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>POST</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>password</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>usertype</td>
                                    <td>Required (2 for User/3 for Merchant/4 for Agent</td>
                                </tr>
                            </tbody>
                        </table>
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "message": "Password Match."
}

{
  "error": true,
  "message": "Invalid Password."
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Auth Password END -->
                    
                    
                    
                    
					
                    <!-- Create Ipool START -->
                        <h2 id="create_ipool"><b> Create Ipool</b></h2>
                        <p> 
                        <table width="100%">
                            <tbody>
                            <tr>
                                <td width="30%">Resource URL</td>
                                <td>api/v1/ipool/create</td>
                            </tr>
                            <tr>
                                <td>Request Method</td>
                                <td>REST</td>
                            </tr>
                            <tr>
                                <td>Type</td>
                                <td>POST</td>
                            </tr>
                            <tr>
                                <td>Response Format</td>
                                <td>JSON</td>
                            </tr>
                            </tbody>
                        </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>title</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>amount</td>
                                    <td>Required</td>
                                </tr> 
                                 <tr>
                                    <td>frequency</td>
                                    <td>Required</td>
                                </tr>  
                                 <tr>
                                    <td>max_people</td>
                                    <td>Required</td>
                                </tr>  
                                 <tr>
                                    <td>start_date</td>
                                    <td>Required</td>
                                </tr>                                                   
                            </tbody>
                        </table>
                    <hr>
                    <h2>JSON Response </h2>
                    <pre class="prettyprint">
{
  "error": false,
  "message": "Ipool Created Successfully.",
  "amount": "10",
  "uid": "143",
  "frequency": "2",
  "max_people": "25",
  "start_date": "2015-04-27",
  "created": "2016-04-28 12:14 PM"
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Create Ipool END -->

				
                    <!-- Ipool List START -->
                    <h2 id="ipool_list"><b> Ipool List</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/ipool/ipool_list</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>GET</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p>                        
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "ipools": [
    {
      "id": "1",
      "userId": "143",
      "title": "Ipool Title",
      "amount": "10",
      "frequency": "2",
      "max_people": "25",
      "start_date": "2015-04-27",
      "created": "2016-04-28 12:14 PM"
    }
  ]
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Ipool List END -->


					<!-- Send Invitation START -->
                    <h2 id="send_invitation"><b> Send Invitation</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/ipool/send_invitation</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>ipool_id</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>users</td>
                            <td>Required (Json Array)</td>
                        </tr>                      
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "message": "Invitation send to all users."
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Send Invitation END -->
                    
                    
                    <!-- Pending Invitation START -->
                    <h2 id="pending_invitation"><b> Pending Invitation</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/ipool/pending_invitation</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>GET</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "ipools": [
    {
      "id": "1",
      "userId": "134",
      "title": "abc",
      "amount": "200",
      "frequency": "3",
      "max_people": "10",
      "start_date": "2016-07-29",
      "invited_date": "2016-04-29 7:46 PM"
    }
  ]
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Pending Invitation END -->
                    
                    
                    <!-- Request Response START -->
                    <h2 id="requestResponse"><b> Request Response</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/ipool/requestResponse</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p>
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>invitation_id</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>Required</td>
                        </tr>                      
                    </tbody>
                </table>                        
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "message": Request is Pending.
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Request Response END -->                
                    
                    
                    
                    <!-- Participation START -->
                    <h2 id="participation"><b> Participation</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/ipool/participation</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>GET</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p>
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "participation": [
    {
      "id": "4",
      "userId": "143",
      "title": "abc",
      "amount": "200",
      "frequency": "3",
      "max_people": "10",
      "start_date": "2016-07-29",
      "invited_date": "2016-04-29 8:32 PM"
    }
  ]
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Participation END -->   
                    
                    
                    
                    <!-- My Ipool Participants START -->
                    <h2 id="my_ipool_participants"><b> My Ipool Participants</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/ipool/my_ipool_participants</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p>
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>ipool_id</td>
                            <td>Required</td>
                        </tr>
                    </tbody>
                </table>                        
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "users": [
    {
      "uid": "125",
      "firstName": "Khurram",
      "lastName": "Shehzad",
      "userPIN": "1829971"
    }
  ]
}
                     </pre>
                    </p>
                    <hr>
                    <!-- My Ipool Participants END -->
                    
                    
                    <!-- Ipool Friendlist START -->
                    <h2 id="ipool_friendlist"><b> Ipool Friendlist</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/ipool/friendlist</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>ipool_id</td>
                            <td>Required</td>
                        </tr>
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "users": [
    {
      "id": "134",
      "firstName": "Marrium",
      "lastName": "Abid",
      "userPIN": "6785189",
      "invitation_status": "1"
    },
    {
      "id": "125",
      "firstName": "Khurram",
      "lastName": "Shehzad",
      "userPIN": "1829971",
      "invitation_status": "1"
    }
  ]
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Ipool Friendlist END -->

                    
                    <!-- Signin - Validate Login START -->
                    <h2 id="validate_login"><b> Signin - Validate Login</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/signin/validate_login</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>user_pin</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>password</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>device_id</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>device_type</td>
                            <td>Required</td>
                        </tr>
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "status": true,
  "message": "Login Successfully."
}

{
  "error": true,
  "status": false,
  "message": "Invalid UserPin or Password."
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Signin - Validate Login END -->
                    
                    
                    
                    <!-- Add Facebook Friend (BY UserPin) START -->
                    <h2 id="add_fb_friends"><b> Add Facebook Friend (BY UserPin)</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/friends/add_fb_friends</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>users</td>
                            <td>Required</td>
                        </tr>
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
// Input Data                
$users = '[
    {
   	 	"user_pin": "486577"
    },
    {
   	 	"user_pin": "486577"
    },
    {
    	"user_pin": "486577"
    }
]';

In case of successfully Friend Request Sent.
{
    error: false
    message: "Friend request sent."
}
In case of Friend request already sent
{
	error: true
	message: "Friend request already sent."
}
In case of Friend already (Accepted Request).
{
	error: true
	message: "Friend already."
}
In case of same User (Api Key) sending request to its own account (userPIN).
{
	error: true
	message: "You cannot send request to your own account."
}
In case of Invalid Pin Number Entered
{
    error: true
    message: "Invalid User Pin. Please try again."
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Add Facebook Friend (BY UserPin) END -->
                    
                    
                    

                    <!-- Facebook Friend & Family START -->
                    <h2 id="fb_friendlist"><b> Facebook Friend & Family</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/fb/friendlist</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>access_token</td>
                            <td>Required</td>
                        </tr>
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "friendlist": [
    {
      "fb_id": "1117287984990306",
      "firstName": "Sultan",
      "lastName": "Shaukat",
      "status": 0,
      "uid": "135",
      "user_pin": "7444789"
    },
    {
      "fb_id": "485242458328267",
      "firstName": "Adnan",
      "lastName": "Haider",
      "status": 0,
      "uid": "141",
      "user_pin": "9199681"
    }
  ]
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Facebook Friend & Family END -->


                    
                    <!-- Transaction Out START -->
                    <h2 id="my_transaction_out"><b> Transaction Out</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/transactions/transactions_out</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>amount</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>secret_code</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>dev_id</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>receiver_name</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>receiver_phone</td>
                            <td>Optional</td>
                        </tr>
                        <tr>
                            <td>receiver_email</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>receiver_address</td>
                            <td>Optional</td>
                        </tr>
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
    error: false
    transaction_id: "11"
    user_id: "64"
    credit: "555"
    debit: "20.00"
    gps: "gps12"
    dev_id: "APA91bEA7vJ1TPR1eROiQMAPOGjBxNg15fvOJxsOQ5A8WRckMMSBa_cxVrXelU6j_lJPDFHbWEUX8UOhqM1hGz6Nano27zrgDeDvul6omKIVQIvTxkxbH5zLzn4Qw4lJe8Ja4XYG7_mh"
    created: "2015-11-02 02:57:52"
    message: "601"
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Transaction Out END -->

                    
                    
                    <!-- Create Crowdfund START -->
                    <h2 id="createCrowdfund"><b> Create Crowdfund</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/crowdfund/create_Crowdfund</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>title</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>description</td>
                            <td>Required</td>
                        </tr>
                        <tr>
                            <td>file_name</td>
                            <td>Required</td>
                        </tr>
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
    error: false
    message: "Crowdfund was successfully created."
}

{
    error: true
    message: "Unable to save crowdfund record in database."
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Create Crowdfund END -->
                    
                    
                    
                    <!-- Get Crowdfund START -->
                    <h2 id="getCrowdfund"><b> Get Crowdfund</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/crowdfund/getCrowdfund</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>GET</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "active_crowdfund_list_all": [
    {
      "id": "1",
      "crowdfund_flag": "1",
      "invitation_flag": 4,
      "reference_number": "QJ2YdC7KPgKJ",
      "title": "DevDesks",
      "description": "Developer Desks",
      "image": "http://dev-web.devdesks.com/imali/uploads/crowdfund/1638321072.png",
      "created_by": "Naveed Arshad"
    }
  ],
  "message": "List all active Crowdfund."
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Get Crowdfund END -->
                    
                    
                    <!-- Get My Crowdfund START -->
                    <h2 id="getMyCrowdfund"><b> Get My Crowdfund</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/crowdfund/getMyCrowdfund</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>GET</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "my_crowdfund_list_all": [
    {
      "id": "1",
      "reference_number": "QJ2YdC7KPgKJ",
      "title": "DevDesks",
      "description": "Developer Desks",
      "image": "http://dev-web.devdesks.com/imali/uploads/crowdfund/1638321072.png",
      "status": "1",
      "created_by": "Naveed Arshad"
    }
  ],
  "message": "My Crowdfund list all."
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Get My Crowdfund END -->
                    
                    
                    <!-- Pending Invitations Crowdfund START -->
                    <h2 id="cf_pending_invitation"><b> Pending Invitations</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/crowdfund/pendingInvitations</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>GET</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": true,
  "message": "No record found."
}


{
  "error": false,
  "invitations": [
    {
      "firstName": "Khurram",
      "lastName": "Shehzad",
      "invitation_id": "36",
      "crowdfund_id": "4",
      "title": "unique project",
      "description": "sdfsdfdsf",
      "image": "http://dev-web.devdesks.com/imali/uploads/crowdfund/946006743.png",
      "reference_number": "BajGHniNHktX"
    },
    {
      "firstName": "Khurram",
      "lastName": "Shehzad",
      "invitation_id": "52",
      "crowdfund_id": "6",
      "title": "testing crowd fund",
      "description": "dsfsdfsdf",
      "image": "http://dev-web.devdesks.com/imali/uploads/crowdfund/533928324.png",
      "reference_number": "kDhlOEiAt9hB"
    },
    {
      "firstName": "Khurram",
      "lastName": "Shehzad",
      "invitation_id": "54",
      "crowdfund_id": "2",
      "title": "First time",
      "description": "test",
      "image": "http://dev-web.devdesks.com/imali/uploads/crowdfund/261898680.png",
      "reference_number": "7GPxtY8rtwKJ"
    },
    {
      "firstName": "Khurram",
      "lastName": "Shehzad",
      "invitation_id": "58",
      "crowdfund_id": "9",
      "title": "nauman ipool",
      "description": "test",
      "image": "http://dev-web.devdesks.com/imali/uploads/crowdfund/356042940.png",
      "reference_number": "imTsMlKfVBLQ"
    }
  ]
}

                     </pre>
                    </p>
                    <hr>
                    <!-- Pending Invitations Crowdfund END -->
                    
                    
                    
                       <!-- Remove Crowdfund START -->
                        <h3 id="removeCrowdfund"><b>Remove Crowdfund</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/crowdfund/removeCrowdfund</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                            </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>reference_number</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "message": "Crowdfund was removed successfully."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- Remove Crowdfund END --> 	
                        
                        
                        
                        <!-- My Donations Crowdfund START -->
                        <h3 id="myDonations_crowdfund"><b>My Donations</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/crowdfund/myDonations</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>GET</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                            </table>
                        <hr>
                        </p>
                        <p>
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "my_donations_list": [
    {
      "donated_amount": "10.00",
      "reference_number": "QJ2YdC7KPgKJ",
      "id": "1",
      "title": "DevDesks",
      "description": "Developer Desks"
    },
    {
      "donated_amount": "20.00",
      "reference_number": "QJ2YdC7KPgKJ",
      "id": "1",
      "title": "DevDesks",
      "description": "Developer Desks"
    }
  ],
  "message": "List all my Crowdfund donations."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- My Donations Crowdfund END --> 	
                        
                    

						<!-- Contribute to Crowdfund START -->
                        <h3 id="contributeCrowdfund"><b>Contribute to Crowdfund</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/crowdfund/contributeCrowdfund</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                            </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>reference_number</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>amount</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>dev_id</td>
                                    <td>Optional</td>
                                </tr>
                                 <tr>
                                    <td>gps</td>
                                    <td>Optional</td>
                                </tr>
                                 <tr>
                                    <td>description</td>
                                    <td>Optional</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "transaction_id": "992",
  "user_id": "143",
  "credit": "50229",
  "debit": "10.00",
  "gps": "0",
  "dev_id": "0",
  "created": "2016-05-20 07:07:17",
  "message": "You have successfully donated 10"
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- Contribute to Crowdfund END --> 


						<!-- Crowdfund Detail START -->
                        <h3 id="crowdfund_detail"><b>Crowdfund Detail</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/crowdfund/crowdfund_detail</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                            </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>reference_number</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "crowdfund_data": [
    {
      "firstName": "Naveed",
      "lastName": "Arshad",
      "amount_paid": "10.00",
      "description": "desc here",
      "created": "2016-05-20 07:07:17"
    }
  ],
  "total_crowdfund_amount": "10.00",
  "message": "Specific Crowdfund details."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- Crowdfund Detail END -->
                        
                        
                        
                        <!-- Invite To Crowdfund START -->
                        <h3 id="inviteToCrowdfund"><b>Invite To Crowdfund</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/crowdfund/inviteToCrowdfund</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                            </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>id (crowdfund)</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>user_list</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "message": "Invitation sent successfully."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- Invite To Crowdfund END -->
                        
                        
                        <!-- Response To Invitation Crowdfund START -->
                        <h3 id="responseToInvitationCrowdfund"><b>Response To Invitation</b></h3>
                        <p> 
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td width="30%">Resource URL</td>
                                    <td>api/v1/crowdfund/responseToInvitation</td>
                                </tr>
                                <tr>
                                    <td>Request Method</td>
                                    <td>REST</td>
                                </tr>
                                <tr>
                                    <td>Type</td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td>Response Format</td>
                                    <td>JSON</td>
                                </tr>
                            </tbody>
                            </table>
                        <hr>
                        </p>
                        <p>
                        <table>
                            <tbody>
                                <tr>
                                    <td><strong>Parameter</strong></td>
                                    <td><strong>Description</strong></td>
                                </tr>
                                <tr>
                                    <td>id</td>
                                    <td>Required</td>
                                </tr>
                                <tr>
                                    <td>status</td>
                                    <td>Required</td>
                                </tr>
                            </tbody>
                        </table>            
                        <hr>
                        <h2>JSON Response </h2>
                        <pre class="prettyprint">
{
  "error": false,
  "message": "Invitation sent successfully."
}
                        </pre>
                        </p>			
                        <hr>
                        <!-- Response To Invitation Crowdfund END -->
                        
                        
                    <!-- Crowdfund Friendlist START -->
                    <h2 id="crowdfund_friendlist"><b> Crowdfund Friendlist</b></h2>
                    <p> 
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td width="30%">Resource URL</td>
                            <td>api/v1/crowdfund/friendlist</td>
                        </tr>
                        <tr>
                            <td>Request Method</td>
                            <td>REST</td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>POST</td>
                        </tr>
                        <tr>
                            <td>Response Format</td>
                            <td>JSON</td>
                        </tr>
                        </tbody>
                    </table>
                    <hr>
                    </p>
                    <p> 
                    <table>
                    <tbody>
                        <tr>
                            <td><strong>Parameter</strong></td>
                            <td><strong>Description</strong></td>
                        </tr>
                        <tr>
                            <td>crowdfund_id</td>
                            <td>Required</td>
                        </tr>
                    </tbody>
                </table>                       
                <hr>
                <h2>JSON Response </h2>
                <pre class="prettyprint">
{
  "error": false,
  "users": [
    {
      "id": "170",
      "firstName": "Umer",
      "lastName": "Afzal",
      "userPIN": "4529454",
      "invitation_status": 4
    },
    {
      "id": "125",
      "firstName": "Khurram",
      "lastName": "Shehzad",
      "userPIN": "1829971",
      "invitation_status": 4
    }
  ]
}
                     </pre>
                    </p>
                    <hr>
                    <!-- Crowdfund Friendlist END -->

                        
                        
                        
                        
                        
                        


                    </div>
                </div>
            </section>

            <footer>
                <div class="">
                    <p> &copy; Copyright IMALI 2015. All Rights Reserved.</p>
                </div>
            </footer>
        </div>
        <script src="<?php echo base_url('assets/docs/js/jquery.min.js'); ?>"></script> 
        <script type="text/javascript" src="<?php echo base_url('assets/docs/js/prettify/prettify.js'); ?>"></script> 
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css&skin=sunburst"></script>
        <script src="<?php echo base_url('assets/docs/js/layout.js'); ?>"></script>
    </body>
</html>