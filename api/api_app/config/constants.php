<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

// Custom Constants 
date_default_timezone_set('Asia/Karachi');
define('SITE_NAME', 'Imali');
define('SITE_EMAIL', 'no-reply@devdesks.com');
define('UPLOADS_DIR', '../uploads/');
define('FB_APP_ID', '1210382095662364');
define('FB_APP_SECRET', '6e4e3e5ba4dd539f43db298a6dc66027');
define('API_URL', 'http://'.$_SERVER['HTTP_HOST'].'/imali/api/');
define('ADMIN_URL', 'http://'.$_SERVER['HTTP_HOST'].'/imali/');
define("GOOGLE_GCM_URL", "https://android.googleapis.com/gcm/send");

/* End of file constants.php */
/* Location: ./application/config/constants.php */