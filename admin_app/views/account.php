<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class  Account extends CI_Controller {

  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('Account_model');
    $this->load->library('form_validation');
  }
  public function accounts_view() {
	   
    ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1); 
	
		$config = array();
        $config["base_url"] = base_url() . "account/accounts_view";
		
		$total_row = $this->Account_model->count_account_view();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Account_model->fetch_accounts_view($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		
//////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('accounts_view',$data);
        $this->load->view('common/footer',$data);   
    }

  public function accounts_view_edit() {
		$config = array();
        $config["base_url"] = base_url() . "account/accounts_view_edit";
		
		$total_row = $this->Account_model->count_account_view_edit();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Account_model->fetch_accounts_view_edit($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		
//////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('accounts_view_edit',$data);
        $this->load->view('common/footer',$data);   
    }
	
  public function accounts_add(){
	    $this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('accounts_add');
        $this->load->view('common/footer');   
	  }	
  public function add_deposit_accounts_val(){
		$this->form_validation->set_rules('accountTitle', 'Account Title', 'required');
		$this->form_validation->set_rules('accountNumber', 'Account Number', 'required');
		$this->form_validation->set_rules('bankName', 'Bank Name', 'required');
		$this->form_validation->set_rules('branchName', 'Branch Name', 'required');
		$this->form_validation->set_rules('swiftCode', 'Swift Code', 'required');
		$this->form_validation->set_rules('available', 'Available', 'required');
		
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('accounts_add');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->Account_model->add_deposit_accounts_val();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'account/accounts_add');
			   }
	  }	  
	 
  public function accounts_edit(){
	  $depositAccountId = $this->uri->segment(3);
	  $data['query'] = $this->Account_model->edit_deposit_accounts_val($depositAccountId);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('accounts_edit',$data);
      $this->load->view('common/footer',$data);
	  }	
	
  public function edit_deposit_accounts(){
	 $this->form_validation->set_rules('accountTitle', 'Account Title', 'required');
		$this->form_validation->set_rules('accountNumber', 'Account Number', 'required');
		$this->form_validation->set_rules('bankName', 'Bank Name', 'required');
		$this->form_validation->set_rules('branchName', 'Branch Name', 'required');
		$this->form_validation->set_rules('swiftCode', 'Swift Code', 'required');
		$this->form_validation->set_rules('available', 'Available', 'required');
				
	  $depositAccountId = $this->input->post('depositAccountId');
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'account/accounts_edit/'.$depositAccountId);
			$this->load->view('common/footer');  
			}
		    else{
			   $this->Account_model->update_deposti_accounts();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'account/accounts_view_edit');
			   }
	  }	
  
   public function deposit_account_delete(){
	   $depositAccountId = $this->uri->segment(3);
	   $this->Account_model->deposit_account_delete($depositAccountId);
	   
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'account/accounts_view_edit');
	   }
 
 }