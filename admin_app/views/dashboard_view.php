	<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h1>Dashboard <small>overview &amp; stats </small></h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12">
								<a href="#" class="back-subviews">
									<i class="fa fa-chevron-left"></i> BACK
								</a>
								<a href="#" class="close-subviews">
									<i class="fa fa-times"></i> CLOSE
								</a>
								<div class="toolbar-tools pull-right">
									<!-- start: TOP NAVIGATION MENU -->
									<ul class="nav navbar-right">
										<!-- start: TO-DO DROPDOWN -->
										<li class="dropdown">
											<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
												<i class="fa fa-plus"></i> SUBVIEW
												<div class="tooltip-notification hide">
													<div class="tooltip-notification-arrow"></div>
													<div class="tooltip-notification-inner">
														<div>
															<div class="semi-bold">
																HI THERE!
															</div>
															<div class="message">
																Try the Subview Live Experience
															</div>
														</div>
													</div>
												</div>
											</a>
											<ul class="dropdown-menu dropdown-light dropdown-subview">
												<li class="dropdown-header">
													Notes
												</li>
												<li>
													<a href="#newNote" class="new-note"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new note</a>
												</li>
												<li>
													<a href="#readNote" class="read-all-notes"><span class="fa-stack"> <i class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Read all notes</a>
												</li>
												<li class="dropdown-header">
													Calendar
												</li>
												<li>
													<a href="#newEvent" class="new-event"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new event</a>
												</li>
												<li>
													<a href="#showCalendar" class="show-calendar"><span class="fa-stack"> <i class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Show calendar</a>
												</li>
												<li class="dropdown-header">
													Contributors
												</li>
												<li>
													<a href="#newContributor" class="new-contributor"><span class="fa-stack"> <i class="fa fa-user fa-stack-1x fa-lg"></i> <i class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span> Add new contributor</a>
												</li>
												<li>
													<a href="#showContributors" class="show-contributors"><span class="fa-stack"> <i class="fa fa-user fa-stack-1x fa-lg"></i> <i class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span> Show all contributor</a>
												</li>
											</ul>
										</li>
										<li class="dropdown">
											<a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
												<span class="messages-count badge badge-default hide">3</span> <i class="fa fa-envelope"></i> MESSAGES
											</a>
											<ul class="dropdown-menu dropdown-light dropdown-messages">
												<li>
													<span class="dropdown-header"> You have 9 messages</span>
												</li>
												<li>
													<div class="drop-down-wrapper ps-container">
														<ul>
															<li class="unread">
																<a href="javascript:;" class="unread">
																	<div class="clearfix">
																		<div class="thread-image">
																			<img src="assets/images/avatar-2.jpg" alt="">
																		</div>
																		<div class="thread-content">
																			<span class="author">Nicole Bell</span>
																			<span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
																			<span class="time"> Just Now</span>
																		</div>
																	</div>
																</a>
															</li>
															<li>
																<a href="javascript:;" class="unread">
																	<div class="clearfix">
																		<div class="thread-image">
																			<img src="assets/images/avatar-3.jpg" alt="">
																		</div>
																		<div class="thread-content">
																			<span class="author">Steven Thompson</span>
																			<span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
																			<span class="time">8 hrs</span>
																		</div>
																	</div>
																</a>
															</li>
															<li>
																<a href="javascript:;">
																	<div class="clearfix">
																		<div class="thread-image">
																			<img src="assets/images/avatar-5.jpg" alt="">
																		</div>
																		<div class="thread-content">
																			<span class="author">Kenneth Ross</span>
																			<span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
																			<span class="time">14 hrs</span>
																		</div>
																	</div>
																</a>
															</li>
														</ul>
													</div>
												</li>
												<li class="view-all">
													<a href="pages_messages.html">
														See All
													</a>
												</li>
											</ul>
										</li>
										<li class="menu-search">
											<a href="#">
												<i class="fa fa-search"></i> SEARCH
											</a>
											<!-- start: SEARCH POPOVER -->
											<div class="popover bottom search-box transition-all">
												<div class="arrow"></div>
												<div class="popover-content">
													<!-- start: SEARCH FORM -->
													<form class="" id="searchform" action="#">
														<div class="input-group">
															<input type="text" class="form-control" placeholder="Search">
															<span class="input-group-btn">
																<button class="btn btn-main-color btn-squared" type="button">
																	<i class="fa fa-search"></i>
																</button> </span>
														</div>
													</form>
													<!-- end: SEARCH FORM -->
												</div>
											</div>
											<!-- end: SEARCH POPOVER -->
										</li>
									</ul>
									<!-- end: TOP NAVIGATION MENU -->
								</div>
							</div>
						</div>
						<!-- end: TOOLBAR -->
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="#">
											Dashboard
										</a>
									</li>
									<li class="active">
										Dashboard
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-7 col-lg-4">
								<div class="panel panel-dark">
									<div class="panel-heading">
										<h4 class="panel-title">Browser</h4>
										<div class="panel-tools">
											<div class="dropdown">
												<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-white">
													<i class="fa fa-cog"></i>
												</a>
												<ul class="dropdown-menu dropdown-light pull-right" role="menu">
													<li>
														<a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
													</li>
													<li>
														<a class="panel-refresh" href="#">
															<i class="fa fa-refresh"></i> <span>Refresh</span>
														</a>
													</li>
													<li>
														<a class="panel-config" href="#panel-config" data-toggle="modal">
															<i class="fa fa-wrench"></i> <span>Configurations</span>
														</a>
													</li>
													<li>
														<a class="panel-expand" href="#">
															<i class="fa fa-expand"></i> <span>Fullscreen</span>
														</a>
													</li>
												</ul>
											</div>
											<a class="btn btn-xs btn-link panel-close" href="#">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
									<div class="panel-body no-padding">
										<div class="partition-green padding-15 text-center">
											<h4 class="no-margin">Monthly Statistics</h4>
											<span class="text-light">based on the major browsers</span>
										</div>
										<div id="accordion" class="panel-group accordion accordion-white no-margin">
											<div class="panel no-radius">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle padding-15">
														<i class="icon-arrow"></i>
														This Month <span class="label label-danger pull-right">3</span>
													</a></h4>
												</div>
												<div class="panel-collapse collapse in" id="collapseOne">
													<div class="panel-body no-padding partition-light-grey">
														<table class="table">
															<tbody>
																<tr>
																	<td class="center">1</td>
																	<td>Google Chrome</td>
																	<td class="center">4909</td>
																	<td><i class="fa fa-caret-down text-red"></i></td>
																</tr>
																<tr>
																	<td class="center">2</td>
																	<td>Mozilla Firefox</td>
																	<td class="center">3857</td>
																	<td><i class="fa fa-caret-up text-green"></i></td>
																</tr>
																<tr>
																	<td class="center">3</td>
																	<td>Safari</td>
																	<td class="center">1789</td>
																	<td><i class="fa fa-caret-up text-green"></i></td>
																</tr>
																<tr>
																	<td class="center">4</td>
																	<td>Internet Explorer</td>
																	<td class="center">612</td>
																	<td><i class="fa fa-caret-down text-red"></i></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="panel no-radius">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle padding-15 collapsed">
														<i class="icon-arrow"></i>
														Last Month
													</a></h4>
												</div>
												<div class="panel-collapse collapse" id="collapseTwo">
													<div class="panel-body no-padding partition-light-grey">
														<table class="table">
															<tbody>
																<tr>
																	<td class="center">1</td>
																	<td>Google Chrome</td>
																	<td class="center">5228</td>
																	<td><i class="fa fa-caret-up text-green"></i></td>
																</tr>
																<tr>
																	<td class="center">2</td>
																	<td>Mozilla Firefox</td>
																	<td class="center">2853</td>
																	<td><i class="fa fa-caret-up text-green"></i></td>
																</tr>
																<tr>
																	<td class="center">3</td>
																	<td>Safari</td>
																	<td class="center">1948</td>
																	<td><i class="fa fa-caret-up text-green"></i></td>
																</tr>
																<tr>
																	<td class="center">4</td>
																	<td>Internet Explorer</td>
																	<td class="center">456</td>
																	<td><i class="fa fa-caret-down text-red"></i></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<div class="panel no-radius">
												<div class="panel-heading">
													<h4 class="panel-title">
													<a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle padding-15 collapsed">
														<i class="icon-arrow"></i>
														Two Months Ago
													</a></h4>
												</div>
												<div class="panel-collapse collapse" id="collapseThree">
													<div class="panel-body no-padding partition-light-grey">
														<table class="table">
															<tbody>
																<tr>
																	<td class="center">1</td>
																	<td>Google Chrome</td>
																	<td class="center">4256</td>
																	<td><i class="fa fa-caret-down text-red"></i></td>
																</tr>
																<tr>
																	<td class="center">2</td>
																	<td>Mozilla Firefox</td>
																	<td class="center">3557</td>
																	<td><i class="fa fa-caret-up text-green"></i></td>
																</tr>
																<tr>
																	<td class="center">3</td>
																	<td>Safari</td>
																	<td class="center">1435</td>
																	<td><i class="fa fa-caret-up text-green"></i></td>
																</tr>
																<tr>
																	<td class="center">4</td>
																	<td>Internet Explorer</td>
																	<td class="center">423</td>
																	<td><i class="fa fa-caret-down text-red"></i></td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-5">
								<div class="row">
									<div class="col-md-6">
										<div class="panel panel-blue">
											<div class="panel-body padding-20 text-center">
												<div class="space10">
													<h5 class="text-white semi-bold no-margin p-b-5">Today</h5>
													<h3 class="text-white no-margin"><span class="text-small">&#36;</span>1,450</h3>
													253 Sales
												</div>
												<div class="sparkline-4 space10">
													<span ></span>
												</div>
												<span class="text-light"><i class="fa fa-clock-o"></i> 1 hour ago</span>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="panel panel-green">
											<div class="panel-body padding-20 text-center">
												<div class="space10">
													<h5 class="text-white semi-bold no-margin p-b-5">Yesterday</h5>
													<h3 class="text-white no-margin"><span class="text-small">&#36;</span>1,250</h3>
													198 Sales
												</div>
												<div class="sparkline-5 space10">
													<span></span>
												</div>
												<span class="text-light"><i class="fa fa-clock-o"></i> 1 hour ago</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-8 col-md-7">
								<div class="panel panel-white">
									<div class="panel-heading border-light">
										<h4 class="panel-title">Site <span class="text-bold">Visits</span></h4>
										<ul class="panel-heading-tabs border-light">
											<li>
												<div class="pull-right">
													<div class="btn-group">
														<a class="btn btn-green dropdown-toggle" data-toggle="dropdown" href="#">
															Tools <span class="caret"></span>
														</a>
														<ul role="menu" class="dropdown-menu">
															<li class="dropdown-header" role="presentation">
																Dropdown header
															</li>
															<li>
																<a href="#">
																	Action
																</a>
															</li>
															<li>
																<a href="#">
																	Another action
																</a>
															</li>
															<li>
																<a href="#">
																	Something else here
																</a>
															</li>
															<li class="divider"></li>
															<li class="dropdown-header" role="presentation">
																Dropdown header
															</li>
															<li>
																<a href="#">
																	Separated link
																</a>
															</li>
														</ul>
													</div>
												</div>
											</li>
											<li>
												<div class="rate">
													<i class="fa fa-caret-up text-green"></i><span class="value">11</span><span class="percentage">%</span>
												</div>
											</li>
											<li class="panel-tools">
												<div class="dropdown">
													<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
														<i class="fa fa-cog"></i>
													</a>
													<ul class="dropdown-menu dropdown-light pull-right" role="menu">
														<li>
															<a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
														</li>
														<li>
															<a class="panel-refresh" href="#">
																<i class="fa fa-refresh"></i> <span>Refresh</span>
															</a>
														</li>
														<li>
															<a class="panel-config" href="#panel-config" data-toggle="modal">
																<i class="fa fa-wrench"></i> <span>Configurations</span>
															</a>
														</li>
														<li>
															<a class="panel-expand" href="#">
																<i class="fa fa-expand"></i> <span>Fullscreen</span>
															</a>
														</li>
													</ul>
												</div>
												<a class="btn btn-xs btn-link panel-close" href="#">
													<i class="fa fa-times"></i>
												</a>
											</li>
										</ul>
									</div>
									<div class="panel-body partition-green">
										<div class="col-md-12">
											<div class="height-350">
												<div id="chart4" class='with-3d-shadow with-transitions'>
													<svg></svg>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-5">
								<div class="panel">
									<div class="panel-heading">
										<i class="clip-bars"></i>
										<h4 class="panel-title">Pageviews <span class="text-bold">real-time</span></h4>
										<div class="panel-tools">
											<div class="dropdown">
												<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
													<i class="fa fa-cog"></i>
												</a>
												<ul class="dropdown-menu dropdown-light pull-right" role="menu">
													<li>
														<a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
													</li>
													<li>
														<a class="panel-refresh" href="#">
															<i class="fa fa-refresh"></i> <span>Refresh</span>
														</a>
													</li>
													<li>
														<a class="panel-config" href="#panel-config" data-toggle="modal">
															<i class="fa fa-wrench"></i> <span>Configurations</span>
														</a>
													</li>
													<li>
														<a class="panel-expand" href="#">
															<i class="fa fa-expand"></i> <span>Fullscreen</span>
														</a>
													</li>
												</ul>
											</div>
											<a class="btn btn-xs btn-link panel-close" href="#">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
									<div class="panel-body">
										<h3 class="inline">26</h3> visitors on-line
										<div class="progress progress-xs transparent-black no-radius">
											<div aria-valuetransitiongoal="12" class="progress-bar progress-bar-success partition-green animate-progress-bar" ></div>
										</div>
										<div class="row">
											<div class="col-sm-4">
												<h4>15</h4>
												<div class="progress progress-xs transparent-black no-margin no-radius">
													<div aria-valuetransitiongoal="37" class="progress-bar progress-bar-success partition-green animate-progress-bar" ></div>
												</div>
												Direct
											</div>
											<div class="col-sm-4">
												<h4>7</h4>
												<div class="progress progress-xs transparent-black no-margin no-radius">
													<div aria-valuetransitiongoal="23" class="progress-bar progress-bar-success partition-green animate-progress-bar" ></div>
												</div>
												Sites
											</div>
											<div class="col-sm-4">
												<h4>4</h4>
												<div class="progress progress-xs transparent-black no-margin no-radius">
													<div aria-valuetransitiongoal="13" class="progress-bar progress-bar-success partition-green animate-progress-bar" ></div>
												</div>
												Search
											</div>
										</div>
										<div class="row space10">
											<div class="col-sm-4 text-center">
												<div class="rate">
													<i class="fa fa-caret-up text-green"></i><span class="value">26</span><span class="percentage">%</span>
												</div>
												Mac OS X
											</div>
											<div class="col-sm-4 text-center">
												<div class="rate">
													<i class="fa fa-caret-up text-green"></i><span class="value">62</span><span class="percentage">%</span>
												</div>
												Windows
											</div>
											<div class="col-sm-4 text-center">
												<div class="rate">
													<i class="fa fa-caret-down text-red"></i><span class="value">12</span><span class="percentage">%</span>
												</div>
												Other OS
											</div>
										</div>
										<div class="height-155">
											<div id='chart2' class='chart half with-transitions'>
												<svg></svg>
												<!--
												<button id='start-stop-button'>
												Start/Stop Stream
												</button>
												!-->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->