<!DOCTYPE html>
<html>
<head>    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>imali</title>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link href="http://getbootstrap.com/dist/css/bootstrap.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">   
</head>
<body>
    <div class="go-live">
        <img class="logo" src="<?php echo base_url(); ?>assets/images/new-logo2.png">
        <br />
    <?php if($this->session->flashdata('message') != "") : ?>    
    <form class="login">
  		<fieldset><?php echo $this->session->flashdata('message'); ?></fieldset>
    </form>
    <?php endif; ?>
    </div>
</body>
</html>