
			
			<div id="pageslide-right" class="pageslide slide-fixed inner">
				<div class="right-wrapper">
					<ul class="nav nav-tabs nav-justified" id="sidebar-tab">
						<li class="active">
							<a href="#users" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a>
						</li>
						<li>
							<a href="#notifications" role="tab" data-toggle="tab"><i class="fa fa-bookmark "></i></a>
						</li>
						<li>
							<a href="#settings" role="tab" data-toggle="tab"><i class="fa fa-gear"></i></a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="users">
							<div class="users-list">
								<h5 class="sidebar-title">On-line</h5>
								<ul class="media-list">
									<li class="media">
										<a href="#">
											<i class="fa fa-circle status-online"></i>
											<img alt="..." src="assets/images/avatar-2.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Nicole Bell</h4>
												<span> Content Designer </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<div class="user-label">
												<span class="label label-default">3</span>
											</div>
											<i class="fa fa-circle status-online"></i>
											<img alt="..." src="assets/images/avatar-3.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Steven Thompson</h4>
												<span> Visual Designer </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<i class="fa fa-circle status-online"></i>
											<img alt="..." src="assets/images/avatar-4.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Ella Patterson</h4>
												<span> Web Editor </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<i class="fa fa-circle status-online"></i>
											<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Kenneth Ross</h4>
												<span> Senior Designer </span>
											</div>
										</a>
									</li>
								</ul>
								<h5 class="sidebar-title">Off-line</h5>
								<ul class="media-list">
									<li class="media">
										<a href="#">
											<img alt="..." src="assets/images/avatar-6.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Nicole Bell</h4>
												<span> Content Designer </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<div class="user-label">
												<span class="label label-default">3</span>
											</div>
											<img alt="..." src="assets/images/avatar-7.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Steven Thompson</h4>
												<span> Visual Designer </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<img alt="..." src="assets/images/avatar-8.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Ella Patterson</h4>
												<span> Web Editor </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<img alt="..." src="assets/images/avatar-9.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Kenneth Ross</h4>
												<span> Senior Designer </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<img alt="..." src="assets/images/avatar-10.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Ella Patterson</h4>
												<span> Web Editor </span>
											</div>
										</a>
									</li>
									<li class="media">
										<a href="#">
											<img alt="..." src="assets/images/avatar-5.jpg" class="media-object">
											<div class="media-body">
												<h4 class="media-heading">Kenneth Ross</h4>
												<span> Senior Designer </span>
											</div>
										</a>
									</li>
								</ul>
							</div>
							<div class="user-chat">
								<div class="sidebar-content">
									<a class="sidebar-back" href="#"><i class="fa fa-chevron-circle-left"></i> Back</a>
								</div>
								<div class="user-chat-form sidebar-content">
									<div class="input-group">
										<input type="text" placeholder="Type a message here..." class="form-control">
										<div class="input-group-btn">
											<button class="btn btn-blue no-radius" type="button">
												<i class="fa fa-chevron-right"></i>
											</button>
										</div>
									</div>
								</div>
								<ol class="discussion sidebar-content">
									<li class="other">
										<div class="avatar">
											<img src="assets/images/avatar-4.jpg" alt="">
										</div>
										<div class="messages">
											<p>
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
											</p>
											<span class="time"> 51 min </span>
										</div>
									</li>
									<li class="self">
										<div class="avatar">
											<img src="assets/images/avatar-1.jpg" alt="">
										</div>
										<div class="messages">
											<p>
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
											</p>
											<span class="time"> 37 mins </span>
										</div>
									</li>
									<li class="other">
										<div class="avatar">
											<img src="assets/images/avatar-4.jpg" alt="">
										</div>
										<div class="messages">
											<p>
												Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
											</p>
										</div>
									</li>
								</ol>
							</div>
						</div>
						<div class="tab-pane" id="notifications">
							<div class="notifications">
								<div class="pageslide-title">
									You have 11 notifications
								</div>
								<ul class="pageslide-list">
									<li>
										<a href="javascript:void(0)">
											<span class="label label-primary"><i class="fa fa-user"></i></span> <span class="message"> New user registration</span> <span class="time"> 1 min</span>
										</a>
									</li>
									<li>
										<a href="javascript:void(0)">
											<span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 7 min</span>
										</a>
									</li>
									<li>
										<a href="javascript:void(0)">
											<span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 8 min</span>
										</a>
									</li>
									<li>
										<a href="javascript:void(0)">
											<span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 16 min</span>
										</a>
									</li>
									<li>
										<a href="javascript:void(0)">
											<span class="label label-primary"><i class="fa fa-user"></i></span> <span class="message"> New user registration</span> <span class="time"> 36 min</span>
										</a>
									</li>
									<li>
										<a href="javascript:void(0)">
											<span class="label label-warning"><i class="fa fa-shopping-cart"></i></span> <span class="message"> 2 items sold</span> <span class="time"> 1 hour</span>
										</a>
									</li>
									<li class="warning">
										<a href="javascript:void(0)">
											<span class="label label-danger"><i class="fa fa-user"></i></span> <span class="message"> User deleted account</span> <span class="time"> 2 hour</span>
										</a>
									</li>
								</ul>
								<div class="view-all">
									<a href="javascript:void(0)">
										See all notifications <i class="fa fa-arrow-circle-o-right"></i>
									</a>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="settings">
							<h5 class="sidebar-title">General Settings</h5>
							<ul class="media-list">
								<li class="media">
									<div class="checkbox sidebar-content">
										<label>
											<input type="checkbox" value="" class="green" checked="checked">
											Enable Notifications
										</label>
									</div>
								</li>
								<li class="media">
									<div class="checkbox sidebar-content">
										<label>
											<input type="checkbox" value="" class="green" checked="checked">
											Show your E-mail
										</label>
									</div>
								</li>
								<li class="media">
									<div class="checkbox sidebar-content">
										<label>
											<input type="checkbox" value="" class="green">
											Show Offline Users
										</label>
									</div>
								</li>
								<li class="media">
									<div class="checkbox sidebar-content">
										<label>
											<input type="checkbox" value="" class="green" checked="checked">
											E-mail Alerts
										</label>
									</div>
								</li>
								<li class="media">
									<div class="checkbox sidebar-content">
										<label>
											<input type="checkbox" value="" class="green">
											SMS Alerts
										</label>
									</div>
								</li>
							</ul>
							<div class="sidebar-content">
								<button class="btn btn-success">
									<i class="icon-settings"></i> Save Changes
								</button>
							</div>
						</div>
					</div>
					<div class="hidden-xs" id="style_selector">
						<div id="style_selector_container">
							<div class="pageslide-title">
								Style Selector
							</div>
							<div class="box-title">
								Choose Your Layout Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="layout" class="form-control">
										<option value="default">Wide</option><option value="boxed">Boxed</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								Choose Your Header Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="header" class="form-control">
										<option value="fixed">Fixed</option><option value="default">Default</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								Choose Your Sidebar Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="sidebar" class="form-control">
										<option value="fixed">Fixed</option><option value="default">Default</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								Choose Your Footer Style
							</div>
							<div class="input-box">
								<div class="input">
									<select name="footer" class="form-control">
										<option value="default">Default</option><option value="fixed">Fixed</option>
									</select>
								</div>
							</div>
							<div class="box-title">
								10 Predefined Color Schemes
							</div>
							<div class="images icons-color">
								<a href="#" id="default"><img src="assets/images/color-1.png" alt="" class="active"></a>
								<a href="#" id="style2"><img src="assets/images/color-2.png" alt=""></a>
								<a href="#" id="style3"><img src="assets/images/color-3.png" alt=""></a>
								<a href="#" id="style4"><img src="assets/images/color-4.png" alt=""></a>
								<a href="#" id="style5"><img src="assets/images/color-5.png" alt=""></a>
								<a href="#" id="style6"><img src="assets/images/color-6.png" alt=""></a>
								<a href="#" id="style7"><img src="assets/images/color-7.png" alt=""></a>
								<a href="#" id="style8"><img src="assets/images/color-8.png" alt=""></a>
								<a href="#" id="style9"><img src="assets/images/color-9.png" alt=""></a>
								<a href="#" id="style10"><img src="assets/images/color-10.png" alt=""></a>
							</div>
							<div class="box-title">
								Backgrounds for Boxed Version
							</div>
							<div class="images boxed-patterns">
								<a href="#" id="bg_style_1"><img src="assets/images/bg.png" alt=""></a>
								<a href="#" id="bg_style_2"><img src="assets/images/bg_2.png" alt=""></a>
								<a href="#" id="bg_style_3"><img src="assets/images/bg_3.png" alt=""></a>
								<a href="#" id="bg_style_4"><img src="assets/images/bg_4.png" alt=""></a>
								<a href="#" id="bg_style_5"><img src="assets/images/bg_5.png" alt=""></a>
							</div>
							<div class="style-options">
								<a href="#" class="clear_style">
									Clear Styles
								</a>
								<a href="#" class="save_style">
									Save Styles
								</a>
							</div>
						</div>
						<div class="style-toggle open"></div>
					</div>
				</div>
			</div>
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<!-- start: PANEL CONFIGURATION MODAL FORM -->
					<div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title">Panel Configuration</h4>
								</div>
								<div class="modal-body">
									Here will be a configuration form
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">
										Close
									</button>
									<button type="button" class="btn btn-primary">
										Save changes
									</button>
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
					<div class="container">
						<!-- start: PAGE HEADER -->
						<!-- start: TOOLBAR -->
						<div class="toolbar row">
							<div class="col-sm-6 hidden-xs">
								<div class="page-header">
									<h2>Users Eidt</h2>
								</div>
							</div>
                          
							<div class="col-sm-6 col-xs-12">
								<a href="#" class="back-subviews">
									<i class="fa fa-chevron-left"></i> BACK
								</a>
								<a href="#" class="close-subviews">
									<i class="fa fa-times"></i> CLOSE
								</a>
								
							</div>
						</div>
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="alert">
				<?php if($this->session->flashdata('message')){?> 
                 <div class="alert alert-success">
                   <button data-dismiss="alert" class="close">&times;</button>
                  <strong><?php  echo $this->session->flashdata('message');?></strong> 
                    </div>
                   <?php }?>         
								</div>
								<!-- start: RESPONSIVE TABLE PANEL -->
								<div class="panel panel-white">
									<div class="panel-heading">
										<i class="fa fa-external-link-square"></i>
										Users Edit
										<div class="panel-tools">										
											<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu">
												<li>
													<a class="panel-collapse collapses" href="#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
												</li>
												<li>
													<a class="panel-refresh" href="#"> <i class="fa fa-refresh"></i> <span>Refresh</span> </a>
												</li>
												<li>
													<a class="panel-config" href="#panel-config" data-toggle="modal"> <i class="fa fa-wrench"></i> <span>Configurations</span></a>
												</li>
												<li>
													<a class="panel-expand" href="#"> <i class="fa fa-expand"></i> <span>Fullscreen</span></a>
												</li>										
											</ul>
											</div>
										</div>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-bordered table-hover" id="sample-table-1">
												<thead>
                                                        <th>Admin Name</th>
                                                        <th>User Name</th>
														<th>Email</th>
                                                       	<th>Action</th>
													</tr>
												</thead>
												<tbody>
         <?php 
		  
			if ($results!='') {?>
			
			<?php foreach ($results as $row) {?>
			 <tr>
                <td><?php echo $row->admin_fname." ".$row->admin_lname;?> </td>
                <td><?php echo $row->admin_username;?></td>
                <td><?php echo $row->admin_email; ?></td>
                <td><?php if($this->session->userdata('role_update') == 1){?>
				<a href="<?php echo base_url().'users/users_edit/'.$row->admin_id;?>" > <i class="fa fa-pencil fa-fw" title="Edit Record"></i> </a> | <?php }  if($this->session->userdata('role_delete') == 1){?>
				<a href="<?php echo base_url().'users/users_delete/'.$row->admin_id;?>" title="Delete Record" onclick="return confirm('Are you sure you want to delete this record?')"> X </a> <?php } ?>
                </td> 
            </tr>
			<?php  } } 
			 ?>
												</tbody>
											</table>
                                            
                                          <div>
											<ul class="pagination pagination-blue margin-bottom-10">
										       <li> <?php echo $links; ?></li>
											</ul>
										 </div>
                                             
										</div>
									</div>
								</div>
								<!-- end: RESPONSIVE TABLE PANEL -->
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			