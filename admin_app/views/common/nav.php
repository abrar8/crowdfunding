<nav id="pageslide-left" class="pageslide inner">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <div class="main-navigation left-wrapper transition-left">
            <div class="navigation-toggler hidden-sm hidden-xs">
                <a href="#main-navbar" class="sb-toggle-left">
                </a>
            </div>
            <div class="user-profile border-top padding-horizontal-10 block">
                <div class="inline-block">
                    <img src="<?php echo base_url(); ?>assets/images/avatar-1.jpg" alt="">
                </div>
                <div class="inline-block">
                    <h5 class="no-margin"> Welcome </h5>
                    <h4 class="no-margin"> <?php echo ucfirst($this->session->userdata('admin_fname')); ?> </h4>
                    <a class="btn user-options sb_toggle">
                        <i class="fa fa-cog"></i>
                    </a>
                </div>
            </div>

            <!-- start: MAIN NAVIGATION MENU -->
            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'dashboard') ? 'active' : ''; ?>">
                    <a href="<?php echo base_url(); ?>">
                        <i class="fa fa-home"></i> 
                        <span class="title"> Dashboard </span>
                        <!--<span class="label label-default pull-right ">LABEL</span>--> 
                    </a>
                </li>
                

                <li class="<?php echo ($this->uri->segment(1) == 'roles') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Roles</span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() . 'roles/roles_add'; ?>">
                                <span class="title"> Create </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'roles/roles_view_edit'; ?>">
                                <span class="title"> Edit</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url() . 'roles/roles_view'; ?>">
                                <span class="title"> View </span>
                            </a>
                        </li> 
                    </ul>
                </li>
            </ul>


            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'users') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Users</span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() . 'users/users_add'; ?>">
                                <span class="title"> Create </span>
                            </a>
                        </li> 
                        <li>
                            <a href="<?php echo base_url() . 'users/users_view_edit'; ?>">
                                <span class="title"> Edit </span>
                            </a>
                        </li>    
                        <li>
                            <a href="<?php echo base_url() . 'users/users_view'; ?>">
                                <span class="title"> View </span>
                            </a>
                        </li>    

                    </ul>
                </li>
            </ul>
			
            
             <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'banks') ? 'active' : ''; ?>">
                    <a href="javascript:void(0);"><i class="fa fa-cogs"></i> <span class="title"> Bank</span><i class="icon-arrow"></i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url().'banks/add_bank'; ?>">
                                <span class="title"> Add Bank </span>
                            </a>
                        </li> 
                        <li>
                            <a href="<?php echo base_url().'banks/list_banks'; ?>">
                                <span class="title">Bank Lists</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'banks/add_bank_branches'; ?>">
                                <span class="title">Add Bank Branches</span>
                            </a>
                        </li> 
                        <li>
                            <a href="<?php echo base_url().'banks/list_bank_branches'; ?>">
                                <span class="title">List Bank Branches</span>
                            </a>
                        </li>   
                    </ul>
                </li>
            </ul>            


            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'currency_rates') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Currency Rates</span><i class="icon-arrow"></i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url().'exchangeRates/add_currency_rates'; ?>">
                                <span class="title"> Add Currency Rates </span>
                            </a>
                        </li> 
                        <li>
                            <a href="<?php echo base_url().'exchangeRates/exchangerates_list'; ?>">
                                <span class="title"> Currency Rates List </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'exchangeRates/base_currency'; ?>">
                                <span class="title"> Base Currency </span>
                            </a>
                        </li>        
                    </ul>
                </li>
            </ul>


            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'promostions') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Promotions</span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url() . 'promostions/add'; ?>">
                                <span class="title"> Create </span>
                            </a>
                        </li> 
                     <!--   <li>
                            <a href="<?php //echo base_url() . 'promostions/edit'; ?>">
                                <span class="title"> Edit </span>
                            </a>
                        </li>   --> 
                        <li>
                            <a href="<?php echo base_url() . 'promostions/view'; ?>">
                                <span class="title"> View </span>
                            </a>
                        </li>    

                    </ul>
                </li>
            </ul>
            
            
            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'manualtransactions') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Manual Transactions</span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url().'manualtransactions/add_transaction_in'; ?>">
                                <span class="title"> Add Manual Transactions (IN) </span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url().'manualtransactions/transactions_in'; ?>">
                                <span class="title"> View Manual Transactions (IN) </span>
                            </a>
                        </li>  
                         <li>
                            <a href="<?php echo base_url().'manualtransactions/transactions_out'; ?>">
                                <span class="title"> View Manual Transactions (OUT) </span>
                            </a>
                        </li>                         
                    </ul>
                </li>
            </ul>
                        

            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'transactions') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Transactions </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url('transactions/view/all'); ?>">
                                <span class="title"> All</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/credit-card'); ?>">
                                <span class="title"> Credit Card</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/scratch-card'); ?>">
                                <span class="title"> Scratch Card</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/bank-transfer'); ?>">
                                <span class="title"> Bank Transfer</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/pin-transfer'); ?>">
                                <span class="title"> Pin Transfer</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/cash-transfer'); ?>">
                                <span class="title"> Cash Transfer</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/daily-contributions'); ?>">
                                <span class="title"> Daily Contributions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/group-contributions'); ?>">
                                <span class="title"> Group Contributions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/charity-contributions'); ?>">
                                <span class="title"> Charity Contributions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('transactions/view/market-items'); ?>">
                                <span class="title"> Market Items</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'newTransactions') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> New Transactions </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/all'); ?>">
                                <span class="title"> All</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/credit-card'); ?>">
                                <span class="title"> Credit Card</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/scratch-card'); ?>">
                                <span class="title"> Scratch Card</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/bank-transfer'); ?>">
                                <span class="title"> Bank Transfer</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/pin-transfer'); ?>">
                                <span class="title"> Pin Transfer</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/cash-transfer'); ?>">
                                <span class="title"> Cash Transfer</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/daily-contributions'); ?>">
                                <span class="title"> Daily Contributions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/group-contributions'); ?>">
                                <span class="title"> Group Contributions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/charity-contributions'); ?>">
                                <span class="title"> Charity Contributions</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url('newTransactions/view/market-items'); ?>">
                                <span class="title"> Market Items</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>

            <ul class="main-navigation-menu">
                <li class="<?php echo ($this->uri->segment(1) == 'disputes') ? 'active' : ''; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Disputes </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="<?php //echo base_url().'app/app_management';   ?>">
                                <span class="title"> View  </span>
                            </a>
                        </li>   
                        <li>
                            <a href="<?php //echo base_url().'app/app_management';   ?>">
                                <span class="title"> Assign </span>
                            </a>
                        </li>    
                        <li>
                            <a href="<?php //echo base_url().'app/app_management';  ?>">
                                <span class="title"> Update </span>
                            </a>
                        </li>     

                    </ul>
                </li>
            </ul>

            <ul class="main-navigation-menu">
                <?php
                if ($this->uri->segment(1) == 'account' || $this->uri->segment(1) == 'app' || $this->uri->segment(1) == 'card') {
                    $class = 'active';
                } else {
                    $class = '';
                }
                ?>
                <li class="<?php echo $class; ?>">
                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Settings </span><i class="icon-arrow"></i> </a>
                    <ul class="sub-menu">
                        <li>

                            <ul class="main-navigation-menu">
                                <li class="<?php echo ($this->uri->segment(1) == 'app')? 'active': ''; ?>">
                                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> User App </span><i class="icon-arrow"></i> </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="<?php echo base_url() . 'app/app_add'; ?>">
                                                <span class="title"> Create </span>
                                            </a>
                                        </li>	
                                        <li>
                                            <a href="<?php echo base_url() . 'app/app_edit_view'; ?>">
                                                <span class="title"> Edit</span>
                                            </a>
                                        </li>	
                                        <li>
                                            <a href="<?php echo base_url() . 'app/app_view'; ?>">
                                                <span class="title"> View</span>
                                            </a>
                                        </li>	

                                    </ul>
                                </li>
                            </ul>

                        </li>   
                        <li>
                            <a href="<?php //echo base_url().'app/deposit_accounts_add';  ?>">
                                <span class="title"> Merchant Types </span>
                            </a>
                        </li>    
                        <li>
                            <a href="<?php //echo base_url().'app/app_management';   ?>">
                                <span class="title"> Merchant Views </span>
                            </a>
                        </li>     
                        <li>

                            <ul class="main-navigation-menu">
                                
                                <li class="<?php echo ($this->uri->segment(1) == 'card')? 'active': ''; ?>">
                                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Scratch Cards </span><i class="icon-arrow"></i> </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="<?php echo base_url() . 'card/card_add'; ?>">
                                                <span class="title"> Create </span>
                                            </a>
                                        </li>	
                                        <li>
                                            <a href="<?php echo base_url() . 'card/card_view_edit'; ?>">
                                                <span class="title"> Edit</span>
                                            </a>
                                        </li>	
                                        <li>
                                            <a href="<?php echo base_url() . 'card/card_view/1'; ?>">
                                                <span class="title"> View (Available) </span>
                                            </a>
                                        </li>	
                                        <li>
                                            <a href="<?php echo base_url() . 'card/card_view/0'; ?>">
                                                <span class="title"> View (Used) </span>
                                            </a>
                                        </li>	

                                    </ul>
                                </li>
                            </ul>
                        </li>	
                        <li>

                            <ul class="main-navigation-menu">
                                <li class="<?php echo ($this->uri->segment(1) == 'account')? 'active': ''; ?>">
                                    <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> Imali Accounts </span><i class="icon-arrow"></i> </a>
                                    <ul class="sub-menu">
                                        <li>
                                            <a href="<?php echo base_url() . 'account/accounts_add'; ?>">
                                                <span class="title"> Create </span>
                                            </a>
                                        </li>	
                                        <li>
                                            <a href="<?php echo base_url() . 'account/accounts_view_edit'; ?>">
                                                <span class="title"> Edit</span>
                                            </a>
                                        </li>	
                                        <li>
                                            <a href="<?php echo base_url() . 'account/accounts_view'; ?>">
                                                <span class="title"> View</span>
                                            </a>
                                        </li>	

                                    </ul>
                                </li>
                            </ul>

                        </li>							   
                    </ul>
                </li>
            </ul>

            <!-- end: MAIN NAVIGATION MENU -->
        </div>
        <!-- end: SIDEBAR -->
    </div>
    <div class="slide-tools">
        <div class="col-xs-6 text-left no-padding">
            <a class="btn btn-sm status" href="#">
                Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
            </a>
        </div>
        <div class="col-xs-6 text-right no-padding">
            <a class="btn btn-sm log-out text-right" href="<?php echo base_url() . 'signin/signout'; ?>">
                <i class="fa fa-power-off"></i> Log Out
            </a>
        </div>
    </div>
</nav>
<!-- end: PAGESLIDE LEFT -->