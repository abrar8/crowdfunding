<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
	<!--<![endif]-->
	<!-- start: HEAD -->
	
<!-- Mirrored from www.cliptheme.com/demo/rapido/login_login.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 01 Dec 2014 06:48:26 GMT -->
<head>
		<title>Rapido - Responsive Admin Template</title>
		<!-- start: META -->
		<meta charset="utf-8" />
		<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta content="" name="description" />
		<meta content="" name="author" />
		<!-- end: META -->
		<!-- start: MAIN CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/animate.css/animate.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/styles.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/styles-responsive.css">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/skins/all.css">
		<!--[if IE 7]>
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
		<![endif]-->
		<!-- end: MAIN CSS -->
		<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
		<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body class="login">
		<div class="row">
			<div class="main-login col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
				<div class="logo">
					<img src="<?php echo base_url();?>assets/images/logo.png">
				</div>
				<!-- start: LOGIN BOX -->
				<div class="box-login">
					<h3>Set New Password</h3>
					
                     
                <?php if(validation_errors()){ ?>                               
                <div class="alert alert-danger">
                   <button data-dismiss="alert" class="close">&times; </button>
                      <strong><?php echo validation_errors(); ?></strong>
                    </div>
                    <?php }?>  
                     
				 <?php if($this->session->flashdata('message')){?> 
                  <div class="alert alert-success">
                   <button data-dismiss="alert" class="close">&times;</button>
                   <strong><?php  echo $this->session->flashdata('message');?></strong> 
                    </div>
                   <?php }?>   
                    
                    
                     <form action="<?php echo base_url(); ?>forgetpass/reset_pass" method="post" class="form-login">
                     <input type="hidden" name="type" value="<?php echo $this->uri->segment(3)?>">
                     <input type="hidden" name="id" value="<?php echo $this->uri->segment(4)?>">

						<div class="errorHandler alert alert-danger no-display">
							<i class="fa fa-remove-sign"></i> You have some form errors. Please check below.
						</div>
						<fieldset>
                         <div class="form-group">
                        <div class="col-sm-12">
                        <input type="password" name="new_pass" class="form-control" placeholder="Enter new password"><br>
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <div class="col-sm-12">
                        <input type="password" name="conf_pass" class="form-control" placeholder="Confirm new password"><br>
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-sm-2 control-label" for="form-field-1">
                        </label>                                  
                        <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary">Change Password </button>
                        </div>
                        </div>
         
						</fieldset>
					</form>
					<!-- start: COPYRIGHT -->
					<div class="copyright">
						2014 &copy; Rapido by cliptheme.
					</div>
					<!-- end: COPYRIGHT -->
				</div>
				<!-- end: LOGIN BOX -->
				<!-- start: FORGOT BOX -->
				
				<!-- end: FORGOT BOX -->
				<!-- start: REGISTER BOX -->
				
				<!-- end: REGISTER BOX -->
			</div>
		</div>
		<!-- start: MAIN JAVASCRIPTS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<script type="text/javascript" src="assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
		<!--<![endif]-->
		<script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/jquery.transit/jquery.transit.js"></script>
		<script src="<?php echo base_url();?>assets/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="<?php echo base_url();?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
			});
		</script>
	</body>
	<!-- end: BODY -->

<!-- Mirrored from www.cliptheme.com/demo/rapido/login_login.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 01 Dec 2014 06:48:26 GMT -->
</html>