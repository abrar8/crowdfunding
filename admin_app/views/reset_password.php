<!DOCTYPE html>
<html>
<head>    
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>imali</title>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link href="http://getbootstrap.com/dist/css/bootstrap.css" type="text/css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>    -->
    <!--<script>
	$(document).ready(function(){
		$(".submit").click(function(){
			$(".alert-danger").show();
		});
	});
    </script>  -->  
</head>
<body>
<?php if($account_type == 1) {
    $action = ADMIN_URL.'signin/update_password';
} elseif($account_type == 2) {
    $action = ADMIN_URL.'signin/update_merchant_password';
} ?>
    <div class="go-live">
        <img class="logo" src="<?php echo base_url(); ?>assets/images/new-logo2.png">
        <br /> 
        <form name="reset_password" id="reset_password" action="<?php echo $action; ?>" method="post" class="login">
          <fieldset>    
            <legend class="legend">Please enter your New Password</legend>
              <!--<div class="alert alert-danger" role="alert">Wrong Password</div>-->
            <div class="input">
                <input type="password" name="password" id="password" value="" placeholder="Password" required maxlength="4" />
            </div>    
            <div class="input">
                <input type="password" name="confirm_password" id="confirm_password" value="" placeholder="Confirm Password" required maxlength="4" />
            </div>      
                <input type="hidden" name="uid" value="<?php echo $uid; ?>">
              <button type="submit" class="submit">Submit</button>
          </fieldset>
        </form>        
    </div>
    <!--<script type="text/javascript" src="<?php //echo base_url(); ?>assets/js/custom.js"></script>-->
</body>
</html>