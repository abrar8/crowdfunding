<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Banks extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != true) {
            redirect('signin');
        }
       $this->load->model('banks_model');
       $this->load->helper(array('form', 'url'));
       $this->load->library('form_validation');
       $this->load->library('session');
       $this->load->library('pagination');
        
    }

    /*

     * this is method show All Transactions which ever done
     * 
     */

   public function view() { 

        if($this->input->post()){
            $default_perPage    =   $this->input->post('recordPerpage');
        }else if($this->uri->segment(4)){
            $default_perPage    =   $this->uri->segment(4);
        }
        else{
            $default_perPage    =   20;
        }
        $data['js'][]           =   'Banks';
        $data['page_title']     =   'Banks';
        $data['breadcrum']      =   array(
            'dashboard' =>  base_url('dashboard'),
            'Banks' =>  base_url('Banks/view'),
            $data['page_title'] =>  'active'
        );
        
        ///////////   Paginaton Start Here     ////////////
        
        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = base_url() . "Banks/view";     
        $total_row = $this->banks_model->count_banks(); 
        $config["total_rows"]    = $total_row;
        $config["per_page"]      = $default_perPage;
        $config['num_links']     = $total_row;
        $config['cur_tag_open']  = '&nbsp;<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link']     = 'Next';
        $config['prev_link']     = 'Previous';
        $config['uri_segment']   = 3;
        $this->pagination->initialize($config); 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->banks_model->get_banks($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        
        
        $data['total_row']  =   $total_row;
        $data['to']         =   (($this->uri->segment(3)) ? $this->uri->segment(3) : 0)+1;
        $data['from']       =   ($config["per_page"]+$this->uri->segment(4)>$total_row)?$total_row:$config["per_page"]+$this->uri->segment(3);
        
        $data['current_url']=   $config["base_url"];
        $data['per_page']   =   $config['per_page'];  
        $data['recordPerpage'] = array(
            5    => 5,
            10   => 10,
            15   => 15,
            20   => 20,
            1000 => 'all'
        );
        //echo "<pre>"; print_r($data); exit;
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('bank_view',$data);
        $this->load->view('common/footer');
     }
    public function add(){ 
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('bnks_add');
        $this->load->view('common/footer');
    }
    public function add_bank_val(){
        $this->form_validation->set_rules('bankName', 'Bank Name', 'required');
        $this->form_validation->set_rules('displayName', 'Display Name', 'required');
        
        if($this->form_validation->run()=== false){
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('bnks_add');
            $this->load->view('common/footer');  
            }
            else{ 
            $bankName       = $this->input->post('bankName');
            $displayName = $this->input->post('displayName');
            $this->banks_model->add_banks($bankName,$displayName);
            $this->session->set_flashdata('message', 'Record Inserted Successfuly');
            redirect(base_url().'Banks/add');
               
      }  
      } 

      public function edit($bankId){ 
        $data['result'] = $this->banks_model->selectBanks($bankId);
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('bnk_edit',$data);
        $this->load->view('common/footer');
    }

     public function update_bank_val(){
        $this->form_validation->set_rules('bankName', 'Bank Name', 'required');
        $this->form_validation->set_rules('displayName', 'Display Name', 'required');
        
        $bankId          = $this->input->post('bankId');
        if($this->form_validation->run()=== false){
            $data['result'] = $this->banks_model->selectBanks($bankId);
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('bnk_edit',$data);
            $this->load->view('common/footer');  
            }
            else{ 
            $bankName       = $this->input->post('bankName');
            $displayName    = $this->input->post('displayName');
            $this->banks_model->update_banks($bankName,$displayName,$bankId);
            $this->session->set_flashdata('message', 'Record Updated Successfuly');
            redirect(base_url().'Banks/edit/'.$bankId);
               
      }  
      } 

      public function addCoupons(){  
        $data["res"] = $this->promotions_model->get_promos();
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('addCoupons',$data);
        $this->load->view('common/footer');

      }

      public function add_coupon_val(){ 
        $this->form_validation->set_rules('couponCode',  'Coupon Code', 'required');
        $this->form_validation->set_rules('couponPrize', 'Coupon Prize', 'required');
        $this->form_validation->set_rules('promotion',   'Promotion', 'required');
        $data["res"] = $this->promotions_model->get_promos();
        if($this->form_validation->run()=== false){
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('addCoupons',$data);
            $this->load->view('common/footer');  
            }
        else{ 
              $couponCode       = $this->input->post('couponCode');
              $couponPrize      = $this->input->post('couponPrize');
              $promotion      = $this->input->post('promotion');
              $this->promotions_model->add_coupon($couponCode,$couponPrize,$promotion);
              $this->session->set_flashdata('message', 'Record Inserted Successfuly');
              redirect(base_url().'promostions/addCoupons');
        
            }  
      } 

       public function status($bankId,$available){ 
       $returnData = $this->banks_model->changeStatus($bankId,$available);
       echo $returnData; 
    }
        
   

}
