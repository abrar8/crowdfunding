<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles_Model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->library('encrypt');
  } 

////////////////  Pagination Start Here   /////////////////////

 public function roles_view_count() {
        return $this->db->count_all("tblRoles");
    }
 
    public function fetch_roles_view($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblRoles");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
   
   
   public function roles_edit_count() {
        return $this->db->count_all("tblRoles");
    }
 
    public function fetch_roles_edit($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblRoles");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
  ///////////////  Pagination End Her     //////////////////////
  
  public function add_roles(){
		 
		 $data = array(
		 'role_name' => $this->input->post('role_name'),
		 'role_create' => $this->input->post('role_create'),
		 'role_update' => $this->input->post('role_update'),
		 'role_delete' => $this->input->post('role_delete')
		 );
		 return $this->db->insert('tblRoles', $data);	
			//echo $this->db->last_query(); exit;		 
	  }
	  
  public function roles_val_edit($role_id){
		 $query = $this->db->get_where('tblRoles', array('role_id' => $role_id));
		 return $query->row_array();
	  }	  
	  
  public function update_roles(){
		  $role_id = $this->input->post('role_id');
		  $data = array(
		 'role_name' => $this->input->post('role_name'),
		 'role_create' => $this->input->post('role_create'),
		 'role_update' => $this->input->post('role_update'),
		 'role_delete' => $this->input->post('role_delete')
		 );
	  
	    $this->db->where('role_id',$role_id);
		return $this->db->update('tblRoles',$data); 
	  }	
	  
  public function delete_role($role_id){
	  $this->db->where('role_id',$role_id);
	  return $this->db->delete('tblRoles',$data);
	  }	
	  
  public function user_view_model($user_id){
	  $this->db->where('user_id',$user_id);
	  $query = $this->db->get('tblAdmins');
	  return $query->row_array();
	  //echo $this->db->last_query(); exit;
	  }	  
	  
}


