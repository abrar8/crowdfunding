<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_Model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->library('encrypt');
  } 

////////////////  Pagination Start Here   /////////////////////

 public function count_users_veiw() {
        return $this->db->count_all("tblAdmins");
    }
 
    public function fetch_users_view($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAdmins");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
    public function count_users_edit_veiw() {
        return $this->db->count_all("tblAdmins");
    }
 
    public function fetch_users_view_edit($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAdmins");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
  ///////////////      Inactive Pagination      ////////////////// 
   public function inactive_record_count() {
        return $this->db->count_all("tblAdmins");
    }
 
    public function fetch_inactive($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAdmins");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
  ///////////////  Pagination End Her     //////////////////////
  
  public function add_users(){
		 $user_hash = $this->encrypt->sha1($this->input->post('password'));
		 $data = array(
		 'admin_fname' => $this->input->post('admin_fname'),
		 'admin_lname' => $this->input->post('admin_lname'),
		 'admin_username' => $this->input->post('admin_username'),
		 'admin_email' => $this->input->post('admin_email'),
		 'role_id' => $this->input->post('role_id'),
		 'admin_hash' => $user_hash
		 );
			return $this->db->insert('tblAdmins', $data);			  
	  }
	  
  public function user_edit($admin_id){
		 $query = $this->db->get_where('tblAdmins', array('admin_id' => $admin_id));
		 return $query->row_array();
	  }	  
	  
  public function updat_users_val(){
		  $admin_id = $this->input->post('admin_id');
		  $data = array(
		  'admin_fname' => $this->input->post('admin_fname'),
		  'admin_lname' => $this->input->post('admin_lname'),
		  'admin_username' => $this->input->post('admin_username'),
		  'admin_email' => $this->input->post('admin_email'),
		  'role_id' => $this->input->post('role_id')
		  );
	  
	    $this->db->where('admin_id',$admin_id);
		return $this->db->update('tblAdmins',$data); 
	  }	
	  
  public function del_users_val($admin_id){
	  $this->db->where('admin_id',$admin_id);
	  return $this->db->delete('tblAdmins',$data);
	  }	
	  
 
}


