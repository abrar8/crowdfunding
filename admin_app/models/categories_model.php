<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories_Model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
  } 


////////////////  Pagination Start Here   /////////////////////

 public function record_count() {
        return $this->db->count_all("certification_category");
    }
 
    public function fetch_countries($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("certification_category");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
  ///////////////  Pagination End Her     //////////////////////

  public function add_cat_val(){
	 $data = array('cat_name' => $this->input->post('cat_name'));
		return $this->db->insert('certification_category', $data);			  
	  }
	  
  public function edit_cat_val($cat_id){
	   $query = $this->db->get_where('certification_category', array('cat_id' => $cat_id));
  	   return $query->row_array();
	  }	  
	  
  public function edit_cat_form_val(){
	  $cat_id = $this->input->post('cat_id');
	  $data = array(
	  'cat_id' => $this->input->post('cat_id'),
	  'cat_name' => $this->input->post('cat_name')
	  );
	  
	    $this->db->where('cat_id',$cat_id);
		return $this->db->update('certification_category',$data); 
	  }	
  public function del_cat_val($cat_id){
	  $this->db->where('cat_id',$cat_id);
	  return $this->db->delete('certification_category',$data);
	  }	    
}


