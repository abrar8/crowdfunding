<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App_Model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	  $this->load->library('encrypt');
  } 

 public function app_count_view() {
        return $this->db->count_all("tblUsers");
    }
 
    public function fetch_app_view($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblUsers");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
  public function app_count_view_edit() {
        return $this->db->count_all("tblUsers");
    }
 
    public function fetch_app_view_edit($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblUsers");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   } 
   
   
  public function add_app(){
   $password = $this->encrypt->sha1($this->input->post('password'));
	 $data = array(
	 'firstName' => $this->input->post('firstName'),
	 'lastName' => $this->input->post('lastName'),
	 'phoneNumber' => $this->input->post('phoneNumber'),
	 'address' => $this->input->post('address'),
	 'city' => $this->input->post('city'),
	 'country' => $this->input->post('country'),
	 'email' => $this->input->post('email'),
	 'userPassword' => $password,
	 'available' => $this->input->post('available'),
	 'role_id' => $this->input->post('role_id')
	 );
		return $this->db->insert('tblUsers', $data);			  
	 }
	  
  public function edit_app_val($userId){
  	   $query = $this->db->get_where('tblUsers', array('userId' => $userId));
	   return $query->row_array();
	  }	  
	  
  public function update_app__val(){
	  $userId = $this->input->post('userId');
	  $data = array(
	  'firstName' => $this->input->post('firstName'),
	  'lastName' => $this->input->post('lastName'),
	  'phoneNumber' => $this->input->post('phoneNumber'),
	  'address' => $this->input->post('address'),
	  'city' => $this->input->post('city'),
	  'country' => $this->input->post('country'),
	  'email' => $this->input->post('email'),
	  'available' => $this->input->post('available'),
	  'role_id' => $this->input->post('role_id')
	  );
	  
	    $this->db->where('userId',$userId);
		return $this->db->update('tblUsers',$data); 
	  }	
  public function delete_app($userId){
	  $this->db->where('userId',$userId);
	  return $this->db->delete('tblUsers',$data);
	  }	  
	  
}


