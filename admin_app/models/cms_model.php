<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Cms_model extends CI_Model {
	function __construct() {
		parent::__construct();
	 	if($this->session->userdata('logged_in') != true){
	 		redirect('signin');
		}
	}
	
	
	public function get_pages($id)
	{
	    $query = $this->db->get_where("certification_pages",array('id' => $id));
		return $query->row_array();
	}


  	public function exchangerates_list()
	{
        $this->db->select('*');
        $this->db->from('tblExchangeRates');
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // exchangerates_list	


    public function save_currency_rates($data) 
	{
        $this->db->insert('tblExchangeRates', $data);
        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    } // save_currency_rates


    public function get_currency_rates_info($id)
	{
        $this->db->select('*');
        $this->db->from('tblExchangeRates');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    } // get_currency_rates_info	

	
	public function update_currency_rates($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('tblExchangeRates',$data); 
	} // update_currency_rates 	

	
	public function del_currency($id) 
	{
		$this->db->where('id',$id);
        $this->db->delete('tblExchangeRates');
        return ($this->db->affected_rows()>0) ? TRUE : FALSE;
    } // del_currency

  
 	public function base_currency()
	{
        $this->db->select('base_currency.id,
		                   tblExchangeRates.currency_name,
						   tblExchangeRates.currency_type
						  ');
        $this->db->from('base_currency');
		$this->db->join('tblExchangeRates', 'tblExchangeRates.id = base_currency.currency_id', 'inner');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return NULL;
        }
    } // base_currency		
 
 
	public function get_base_currency()
	{
        $this->db->select('currency_id');
        $this->db->from('base_currency');
		$this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
			return $row->currency_id;
        } else {
            return NULL;
        }
    } // get_base_currency	 
 
 	
	public function get_exchange_rates()
	{
		$this->db->select('*');
		$this->db->from('tblExchangeRates');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->result();
		} else {
			return NULL;
		}
	} // get_exchange_rates
 
 
 	public function update_base_currency($data)
	{
		$this->db->where('id', 1);
		$this->db->update('base_currency',$data); 
		return ($this->db->affected_rows()>0) ? TRUE : FALSE;
	} // update_base_currency 	
 
 
 
 
}

