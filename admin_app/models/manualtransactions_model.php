<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manualtransactions_model extends CI_Model {
	function __construct() {
    	parent::__construct();
  	} 
	
	
	public function user_balance($id) 
	{
        $this->db->where('userId', $id);
        $this->db->where('mode !=', 'DC'); // Daily Contribution
        $this->db->where('mode !=', 'GC'); // Group Contributinon
        $this->db->where('mode !=', 'CHC'); //Charity Contribution
        $this->db->where('mode !=', 'MI'); // Market Item Purchase Transaction
        $this->db->where('mode !=', 'UB'); // Utility Bills Transactions
        $this->db->where('mode !=', 'CO'); // Cash out
        $this->db->where('mode !=', 'MIB'); // Merchant Invoice Bill
        $this->db->select('userId, 
		                   SUM(cr) AS credit, 
						   SUM(dr) AS debit');
        $data1 = $this->db->get('tblTransactions')->row();

        if (!empty($data1->userId)) {
            $credit = $data1->credit;
            $debit = $data1->debit;
        } else {
            $credit = '';
            $debit = '';
        }

        // Sum of Debit from DC (Daily Contribution)
        $this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'DC');
        $data2 = $this->db->get('tblTransactions')->row();

        if (!empty($data2->userId)) {
            $debit_dc = $data2->debit;
            // Daily contribution balance
            $credit_dc = $data2->credit;
        } else {
            $debit_dc = 0.00;
            $credit_dc = 0.00;
        }

        // Sum of Debit from GC (Group Contribution)
        $this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'GC');
        $data3 = $this->db->get('tblTransactions')->row();

        if (!empty($data3->userId)) {
            $debit_gc = $data3->debit;
            // Group contribution balance
            $credit_gc = $data3->credit;
        } else {
            $debit_gc = 0.00;
            $credit_gc = 0.00;
        }

        // Sum of Debit from CHC (Charity Contribution)
        $this->db->select('userId, SUM(dr) AS debit, SUM(cr) AS credit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'CHC');
        $data4 = $this->db->get('tblTransactions')->row();

        if (!empty($data4->userId)) {
            // My Total Charity Contributions
            $debit_chc = $data4->debit;
        } else {
            $debit_chc = 0.00;
        }

        // Sum of Debit from MI (Market Item Purchase Transaction)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'MI');
        $data5 = $this->db->get('tblTransactions')->row();

        if (!empty($data5->userId)) {
            // My total item purchases
            $debit_mi = $data5->debit;
        } else {
            $debit_mi = 0.00;
        }

        // Sum of Debit from UB (Utility Bills Transactions)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'UB');
        $data6 = $this->db->get('tblTransactions')->row();

        if (!empty($data6->userId)) {
            // My total item purchases
            $debit_ub = $data6->debit;
        } else {
            $debit_ub = 0.00;
        }

        // Sum of Debit from CO (CashOut Transactions)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'CO');
        $data7 = $this->db->get('tblTransactions')->row();

        if (!empty($data7->userId)) {
            // My total item purchases
            $debit_co = $data7->debit;
        } else {
            $debit_co = 0.00;
        }

        // Sum of Debit from MIB (Merchant Invoice Bill Transactions)
        $this->db->select('userId, SUM(dr) AS debit');
        $this->db->where('userId', $id);
        $this->db->where('mode', 'MIB');
        $data8 = $this->db->get('tblTransactions')->row();

        if (!empty($data8->userId)) {
            // My total item purchases
            $debit_mib = $data8->debit;
        } else {
            $debit_mib = 0.00;
        }

        $debited = $debit + $debit_dc + $debit_gc + $debit_chc + $debit_mi + $debit_ub + $debit_co + $debit_mib;
        $balance = $credit - $debited;

        // Get charity credit
        $this->db->select(" SUM(tblTransactions.cr) AS credit");
        $this->db->from('tblTransactions');
        $this->db->join('tblCharityAccount', 'tblCharityAccount.id = tblTransactions.refId');
        $this->db->where('tblTransactions.mode', "CHC");
        $this->db->where('tblCharityAccount.userId', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $credit_chc = $row->credit;
        } else {
            $credit_chc = '0.00';
        }
        // Get Market Item sales credit

        $data = array(
            'credit' => $balance,
            'debit' => $debited,
            'total_daily_contributions_credit' => $credit_dc,
            'total_group_contributions_credit' => $credit_gc,
            'total_charities_debit' => $debit_chc,
            'total_my_created_charities_credit' => $credit_chc
        );

        return $data;
    } // user_balance
	
		
	public function check_userPin($userpin, $uid)
	{
        $this->db->select('userId');
        $this->db->from('tblUsers');
        $this->db->where('userPIN', $userpin);
        $this->db->where('userId', $uid);
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            return 1;
        } else {
            return 0;
        }
    } // check_userPin	
		
		
	public function user_profile_by_pin($userpin) 
	{
        $this->db->where('userPIN', $userpin);
        $query = $this->db->get('tblUsers');
        if ($query->num_rows() > 0)
            return $query->row();
        else
            return FALSE;
    } // user_profile_by_pin	
		
		
	public function transfer_credit($uid, $debit, $credit) 
	{
        $this->db->insert('tblTransactions', $debit);
        $insert_id = $this->db->insert_id();

        $this->db->where('transId', $insert_id);
        $this->db->where('userId', $uid);
        $query = $this->db->get('tblTransactions');

        $this->db->insert('tblTransactions', $credit);
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    } // transfer_credit
	
	
	public function get_transactions_out()
	{
        $this->db->select('manual_transactions.id,
		                   manual_transactions.fullname,
						   manual_transactions.email,
						   manual_transactions.phone,
						   manual_transactions.address,
						   manual_transactions.secret_code,
						   manual_transactions.datetime,
						   manual_transactions.amount,
						   tblUsers.firstName,
						   tblUsers.lastName
						  ');
        $this->db->from('manual_transactions');
		$this->db->join('tblUsers', 'tblUsers.userId = manual_transactions.uid');
		$this->db->where('manual_transactions.type',2); // 1 for IN and 2 for OUT
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    } // get_transactions_out		
	
	
	public function get_user_profile($receiver_email,$receiver_userpin)
	{
		$this->db->select('*');
        $this->db->from('tblUsers');
		if(!empty($receiver_email))
		{
			$this->db->where('email',$receiver_email);	
		}
		if(!empty($receiver_userpin))
		{
			$this->db->where('userPIN',$receiver_userpin);	
		}
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    } // get_user_profile
	
	
	public function add_manual_transaction_in($data) 
	{
        $this->db->insert('manual_transactions', $data);
        return $this->db->insert_id();
    } // add_manual_transaction_in		
	
	
	public function get_transactions_in()
	{
        $this->db->select('manual_transactions.id,
		                   manual_transactions.fullname,
						   manual_transactions.email,
						   manual_transactions.phone,
						   manual_transactions.address,
						   manual_transactions.secret_code,
						   manual_transactions.datetime,
						   manual_transactions.amount,
						   tblUsers.firstName,
						   tblUsers.lastName
						  ');
        $this->db->from('manual_transactions');
		$this->db->join('tblUsers', 'tblUsers.userId = manual_transactions.uid');
		$this->db->where('manual_transactions.type',1); // 1 for IN and 2 for OUT
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    } // get_transactions_in	
	
	
	
} // Manualtransactions_model