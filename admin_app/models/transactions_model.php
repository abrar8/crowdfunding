<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class transactions_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
	public function get_transactions_users($arg,$limit, $start){
            
            /*
             * user table 
             * [userId] => 97
            [firstName] => nauman
            [lastName] => malik
            [phoneNumber] => 03335007726
            [address] => taxila
            [city] => taxila
            [country] => pakistan
            [userPIN] => 2783348
            [email] => nouman.malik@devdesks.com
            [userPassword] => 42b0aef1f53bddc6c8d238a5d1f2b59e9fe7edec
            [available] => 1
            [api_key] => 27828b61fd7a0ccd47f0f344227d2b2c
            [role_id] => 0
            [currency] => US Dollar ($)*
             */
		$this->db->select('ur.firstName,tr.transId, tr.description, tr.dr as debit, tr.cr as credit, tr.created as transaction_date, tr.mode,tr.description');
		$this->db->from('tblTransactions as tr');
                $this->db->join('tblUsers as ur','ur.userId=tr.userId');
                
                switch ($arg) {
                    case 'all':                        
                        break;
                    case 'credit-card':
                        $this->db->where('tr.mode','CC');
                        break;
                    case 'scratch-card';
                        $this->db->where('tr.mode','SC');
                        break;
                    case 'bank-transfer';
                        $this->db->where('tr.mode','BT');
                        break;
                    case 'pin-transfer';
                        $this->db->where('tr.mode','TCP');
                        break;
                    case 'cash-transfer';
                        $this->db->where('tr.mode','CT');
                        break;
                    case 'daily-contributions';
                        $this->db->where('tr.mode','DC');
                        break;  
                    case 'group-contributions';
                        $this->db->where('tr.mode','GC');
                        break; 
                    case 'charity-contributions';
                        $this->db->where('tr.mode','CC');
                        break;  
                    case 'market-items';
                        $this->db->where('tr.mode','MI');
                        break;                      
                }
                
                $this->db->order_by("tr.created", 'created'); 
                $this->db->limit($limit, $start);
		$query = $this->db->get();
		
                if($query->num_rows() > 0) 
			return $query->result();
		else 
			return FALSE;
	}
        public function count_transactions_users($arg) {
            $this->db->select('');
            $this->db->from('tblTransactions as tr');
            $this->db->join('tblUsers as ur','ur.userId=tr.userId');
            switch ($arg) {
                    case 'all':                        
                        break;
                    case 'credit-card':
                        $this->db->where('tr.mode','CC');
                        break;
                    case 'scratch-card';
                        $this->db->where('tr.mode','SC');
                        break;
                    case 'bank-transfer';
                        $this->db->where('tr.mode','BT');
                        break;
                    case 'pin-transfer';
                        $this->db->where('tr.mode','TCP');
                        break;
                    case 'cash-transfer';
                        $this->db->where('tr.mode','CT');
                        break;
                    case 'daily-contributions';
                        $this->db->where('tr.mode','DC');
                        break;  
                    case 'group-contributions';
                        $this->db->where('tr.mode','GC');
                        break; 
                    case 'charity-contributions';
                        $this->db->where('tr.mode','CC');
                        break;  
                    case 'market-items';
                        $this->db->where('tr.mode','MI');
                        break;                           
                }
            $query = $this->db->count_all_results();
            return $query;            
        }
        
        
        public function fetch_users_view($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAdmins");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
	
}