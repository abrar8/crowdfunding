<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages_Model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->library('encrypt');
  } 

////////////////  Pagination Start Here   /////////////////////

 public function record_count() {
        return $this->db->count_all("certification_pages");
    }
 
    public function fetch_countries($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("certification_pages");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
    
  public function add_pages(){
	  
		 $data = array(
		 'title' => $this->input->post('title'),
		 'sub_title' => $this->input->post('sub_title'),
		 'description' => $this->input->post('description'),
		 'show_on_menu' => $this->input->post('show_on_menu')
		 );
		  return $this->db->insert('certification_pages', $data);	
         // echo $this->db->last_query(); exit;			
	  }
	  
  public function page_edit($id){
		 $query = $this->db->get_where('certification_pages', array('id' => $id));
		 return $query->row_array();
	  }	  
	  
  public function updat_page_val(){
		  $id = $this->input->post('id');
		  $data = array(
		  'title' => $this->input->post('title'),
		  'sub_title' => $this->input->post('sub_title'),
		  'description' => $this->input->post('description'),
		  'show_on_menu' => $this->input->post('show_on_menu')
		  );
	  
	    $this->db->where('id',$id);
		return $this->db->update('certification_pages',$data); 
	  }	
	  
  public function del_pages_val($id){
	  $this->db->where('id',$id);
	  return $this->db->delete('certification_pages',$data);
	  }	
	    
}


