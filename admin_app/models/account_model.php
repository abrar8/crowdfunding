<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account_model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->helper('date');
  } 

    public function count_account_view() {
			return $this->db->count_all("tblDepositAccounts");
			
    }
 
    public function fetch_accounts_view($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblDepositAccounts");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
 public function count_account_view_edit() {
			return $this->db->count_all("tblDepositAccounts");
			
    }
 
  public function fetch_accounts_view_edit($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblDepositAccounts");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   } 
   
  public function add_deposit_accounts_val(){
	 $data = array(
	 'accountTitle' => $this->input->post('accountTitle'),
	 'accountNumber' => $this->input->post('accountNumber'),
	 'bankName' => $this->input->post('bankName'),
	 'branchName' => $this->input->post('branchName'),
	 'swiftCode' => $this->input->post('swiftCode'),
	 'available' => $this->input->post('available')
	 );
		return $this->db->insert('tblDepositAccounts', $data);			  
	  }
	  
  public function edit_deposit_accounts_val($depositAccountId){
	   $query = $this->db->get_where('tblDepositAccounts', array('depositAccountId' => $depositAccountId));
  	   return $query->row_array();
	  }	  
	  
  public function update_deposti_accounts(){
	  $depositAccountId = $this->input->post('depositAccountId');
	  $data = array(
	     'accountTitle' => $this->input->post('accountTitle'),
		 'accountNumber' => $this->input->post('accountNumber'),
		 'bankName' => $this->input->post('bankName'),
		 'branchName' => $this->input->post('branchName'),
		 'swiftCode' => $this->input->post('swiftCode'),
		 'available' => $this->input->post('available')
	  );
	    $this->db->where('depositAccountId',$depositAccountId);
		return $this->db->update('tblDepositAccounts',$data); 
	  }	
	  
  public function deposit_account_delete($depositAccountId){
	  $this->db->where('depositAccountId',$depositAccountId);
	  return $this->db->delete('tblDepositAccounts',$data);
	  }	  
	  
		
		////////////////////    Scratch Card     /////////////////////
		
		
 public function card_count() {
			return $this->db->count_all("tblScratchCards");
			
    }
 
    public function fetch_card($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblScratchCards");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   	
  public function scratch_card__val_add(){
		 $data = array(
		 'cardNumber' => $this->input->post('cardNumber'),
		 'cardAmount' => $this->input->post('cardAmount'),
		 'available' => $this->input->post('available')
		 );
		return $this->db->insert('tblScratchCards', $data);			  
	  }
		
	  public function scratch_card_update($cardId){
	   $query = $this->db->get_where('tblScratchCards', array('cardId' => $cardId));
  	   return $query->row_array();
	  }	
	  
	  
 public function update_scratch_card_val(){
		$cardId = $this->input->post('cardId');
		$data = array(
			 'cardNumber' => $this->input->post('cardNumber'),
			 'cardAmount' => $this->input->post('cardAmount'),
			 'available' => $this->input->post('available'),
		);
			$this->db->where('cardId',$cardId);
			return $this->db->update('tblScratchCards',$data); 
	}	
	
 public function scratch_card_delete($cardId){
	 	 $this->db->where('cardId',$cardId);
	 	 return $this->db->delete('tblScratchCards',$data);
	  }	  
}


