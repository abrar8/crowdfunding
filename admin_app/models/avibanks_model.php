<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Avibanks_model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->library('encrypt');
  } 

////////////////  Pagination Start Here   /////////////////////

 public function count_bank_view() {
        return $this->db->count_all("tblAvailableBanks");
    }
 
    public function fetch_view_bank($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAvailableBanks");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
 public function count_bank_view_edit() {
        return $this->db->count_all("tblAvailableBanks");
    }
 
    public function fetch_view_bank_edit($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAvailableBanks");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
  
  public function add_abibanks(){
		 $data = array(
		 'bankName' => $this->input->post('bankName'),
		 'displayName' => $this->input->post('displayName'),
		 'available' => $this->input->post('available')
		 );
			return $this->db->insert('tblAvailableBanks', $data);			  
	  }
	  
  public function avibanks_edit($bankId){
		 $query = $this->db->get_where('tblAvailableBanks', array('bankId' => $bankId));
		 return $query->row_array();
	  }	  
	  
  public function update_avibanks_val(){
		 $bankId = $this->input->post('bankId');
		 $data = array(
		 'bankName' => $this->input->post('bankName'),
		 'displayName' => $this->input->post('displayName'),
		 'available' => $this->input->post('available')
		 );
	  
	    $this->db->where('bankId',$bankId);
		return $this->db->update('tblAvailableBanks',$data); 
	  }	
	  
  public function del_avibanks_val($bankId){
	  $this->db->where('bankId',$bankId);
	  return $this->db->delete('tblAvailableBanks');
	  }	
	  
}


