<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }

    public function card_count($status) {

        $this->db->where('available', $status);
        $this->db->from('tblScratchCards');
        return $this->db->count_all_results();
        
    }

    public function fetch_card($limit, $start, $status) {
        $this->db->limit($limit, $start);
        $this->db->where('available', $status);
        $query = $this->db->get("tblScratchCards");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function card_count_edit() {
        return $this->db->count_all("tblScratchCards");
    }

    public function fetch_card_edit($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblScratchCards");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function scratch_card__val_add() {
        $data = array(
            'cardNumber' => $this->input->post('cardNumber'),
            'cardAmount' => $this->input->post('cardAmount'),
            'available' => $this->input->post('available')
        );
        return $this->db->insert('tblScratchCards', $data);
    }

    public function scratch_card_update($cardId) {
        $query = $this->db->get_where('tblScratchCards', array('cardId' => $cardId));
        return $query->row_array();
    }

    public function update_scratch_card_val() {
        $cardId = $this->input->post('cardId');
        $data = array(
            'cardNumber' => $this->input->post('cardNumber'),
            'cardAmount' => $this->input->post('cardAmount'),
            'available' => $this->input->post('available'),
        );
        $this->db->where('cardId', $cardId);
        return $this->db->update('tblScratchCards', $data);
    }

    public function scratch_card_delete($cardId) {
        $this->db->where('cardId', $cardId);
        return $this->db->delete('tblScratchCards', $data);
    }

}
