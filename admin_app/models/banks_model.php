<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class banks_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
	
	/*
	public function get_banks($limit, $start){
            
		$this->db->select('tr.bankId,tr.bankName,tr.displayName, tr.available');
		$this->db->from('tblAvailableBanks as tr');  
        $this->db->order_by('tr.bankId','DESC');     
        $this->db->limit($limit, $start);
		$query = $this->db->get();
		
        if($query->num_rows() > 0) 
			return $query->result();
		else 
			return FALSE;
	}
        public function count_banks() { 
            $this->db->select('*');
            $this->db->from('tblAvailableBanks as tr');
            $query = $this->db->count_all_results();
            return $query;            
        }
        
        
        public function fetch_users_view($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAdmins");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }

    public function add_coupon($couponCode,$couponPrize,$promotion){

    $data = array(
         'couponCode'       => $couponCode,
         'couponPrize'      => $couponPrize,
         'status'           => 0,
         'promotionId'      => $promotion
         );
            return $this->db->insert('tblPromotionCoupons', $data);

   }	

   public function get_promos(){

        $this->db->select('tr.bankId,tr.bankName,tr.displayName, tr.available');
        $this->db->from('tblPromotions as tr');      
        $this->db->order_by("tr.created", 'created'); 
        $query = $this->db->get();
        
        if($query->num_rows() > 0) 
            return $query->result();
        else 
            return FALSE;
   }

    public function selectBanks($bankId){
    
        $this->db->select('tr.bankId,tr.bankName,tr.displayName, tr.available');
        $this->db->from('tblAvailableBanks as tr');      
        $this->db->where("tr.bankId", $bankId); 
        $query = $this->db->get();
        
        if($query->num_rows() > 0) 
            return $query->result();
        else 
            return FALSE;
    
   }

   public function update_banks($bankName,$displayName,$bankId){
    
        $data = array(
               'bankName' => $bankName,
               'displayName' => $displayName
            );

        $this->db->where('bankId', $bankId);
        $this->db->update('tblAvailableBanks', $data); 
    
   } 

   public function changeStatus($bankId){

     $this->db->select('tr.available');
     $this->db->from('tblAvailableBanks as tr');      
     $this->db->where("tr.bankId", $bankId); 
     $query = $this->db->get();
     $res = $query->result();
     $available = $res[0]->available;
    if($available == 1){
    $data = array(
               'available' => 0
            );
        $returnData = "Inactive";
    }else{
        $data = array(
               'available' => 1
            );
      $returnData = "Active";
    }

    $this->db->where('bankId', $bankId);
    $this->db->update('tblAvailableBanks', $data);
    return $returnData;
   }
   */
   
   
   	/////////////////////////////////////////////////////////////////////////////////////////
	public function get_countries()
	{
		$this->db->select("*");
		$this->db->from("countries");      
		$this->db->order_by("printable_name", 'ASC'); 
		$query = $this->db->get();		
		if($query->num_rows() > 0) { 
			return $query->result();
		} else { 
			return FALSE;
		}
	} // get_countries
   
   
	public function add_bank($data)
	{
		$this->db->insert('banks',$data);
		return ($this->db->affected_rows()>0) ? TRUE : FALSE;
	} // add_bank
   
   
   	public function get_banks()
	{
		$this->db->select("banks.id,
		                   banks.bank_name,
		                   countries.printable_name AS country
						  ");
		$this->db->from("banks");
		$this->db->join("countries", "countries.id = banks.country_id", "inner");      
		$this->db->order_by("banks.country_id", 'ASC'); 
		$query = $this->db->get();		
		if($query->num_rows() > 0) { 
			return $query->result();
		} else { 
			return FALSE;
		}
	} // get_countries
   
   
	public function get_bank_list($country_id)
	{
		$this->db->select("*");
		$this->db->from("banks");
		$this->db->where("country_id", $country_id);  
		$this->db->order_by("bank_name", 'ASC'); 
		$query = $this->db->get();		
		if($query->num_rows() > 0) { 
			return $query->result();
		} else { 
			return NULL;
		}
	} // get_bank_list
   
   
	public function add_bank_branches($data)
	{
		$this->db->insert('bank_branches',$data);
		return ($this->db->affected_rows()>0) ? TRUE : FALSE;
	} // add_bank_branches
   
   	
	public function list_bank_branches()
	{
		$this->db->select("bank_branches.id,
		                   bank_branches.branch_name,
						   banks.bank_name,
						   countries.printable_name AS country
						  ");
		$this->db->from("bank_branches");
  		$this->db->join("banks", "banks.id = bank_branches.bank_id", "inner");
		$this->db->join("countries", "countries.id = banks.country_id", "inner");
		$this->db->order_by("banks.country_id", 'ASC');
		$query = $this->db->get();		
		if($query->num_rows() > 0) { 
			return $query->result();
		} else { 
			return NULL;
		}
	} // list_bank_branches
   
   
   
   
   
} // banks_model