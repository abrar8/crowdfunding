<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgetpass_model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->library('encrypt');
  } 

  public function get_user(){
		 $login_type = $this->input->post('login_type');
		 $email = $this->input->post('email');
		 if($login_type == 1){
			 return $query = $this->db->get_where('certification_admin', array('admin_email' => $email));
			 }
			 else{
				return $query = $this->db->get_where('certification_certifier', array('certifier_email' => $email));
			 }
	    }
	  
  public function change_pass(){
	  $id = $this->input->post('id');
	  $type = $this->input->post('type');
	  $pass = $this->encrypt->sha1($this->input->post('new_pass'));
    // echo $id.'<br>'.$type.'<br>'.$pass."<br>";
    
	  if($type == 1){ 
		   $data = array(
		   'admin_hash' => $pass
		   );
		     $this->db->where('admin_id', $id);
		     $this->db->update('certification_admin', $data);
	  }
	  else{ 
		    $data = array(
		    'certifier_hash' => $pass
		    ); 
			  $this->db->where('certifier_id', $id);
		      $this->db->update('certification_certifier', $data);
			//echo   $this->db->last_query(); exit;
		  }
	  }	  
	  
}


