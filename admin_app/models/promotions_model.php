<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class promotions_model extends CI_Model {
	function __construct() {
    	parent::__construct();
	}
	public function get_promostions($limit, $start){
            
		$this->db->select('tr.promotionId,tr.title,tr.description, tr.description, tr.image, tr.status, tr.created');
		$this->db->from('tblPromotions as tr');      
        $this->db->order_by("tr.created", 'created'); 
        $this->db->limit($limit, $start);
		$query = $this->db->get();
		
        if($query->num_rows() > 0) 
			return $query->result();
		else 
			return FALSE;
	}
        public function count_promotions() { 
            $this->db->select('*');
            $this->db->from('tblPromotions as tr');
            $query = $this->db->count_all_results();
            return $query;            
        }
        
        
        public function fetch_users_view($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get("tblAdmins");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   public function add_promostions($title,$description,$file_name){

    $data = array(
         'title'       => $title,
         'description' => $description,
         'image'       => $file_name,
         'status'      => 0,
         'created'     => date("Y-m-d H:i:s")
         );
            return $this->db->insert('tblPromotions', $data);

   }
   
   public function edit_promostions($id,$title,$description,$file_name){

    $data = array(
         'title'       => $title,
         'description' => $description,
         'image'       => $file_name,
         'status'      => 0,
         'created'     => date("Y-m-d H:i:s")
         );
            
			$this->db->where('promotionId', $id);
          return  $this->db->update('tblPromotions', $data);
			

   }
   
   
    public function add_coupon($couponCode,$couponPrize,$promotion){

    $data = array(
         'couponCode'       => $couponCode,
         'couponPrize'      => $couponPrize,
         'status'           => 0,
         'promotionId'      => $promotion
         );
            return $this->db->insert('tblPromotionCoupons', $data);

   }	

   public function get_promos(){

    $this->db->select('tr.promotionId,tr.title,tr.description, tr.description, tr.image, tr.status, tr.created');
        $this->db->from('tblPromotions as tr');      
        $this->db->order_by("tr.created", 'created'); 
        $query = $this->db->get();
        
        if($query->num_rows() > 0) 
            return $query->result();
        else 
            return FALSE;
   }
}