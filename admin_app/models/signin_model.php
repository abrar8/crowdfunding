<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signin_model extends CI_Model {
  function __construct() {
     parent::__construct();
  } 

  public function does_user_exist($username) {
    $this->db->where('admin_username', $username);
    $query = $this->db->get('tblAdmins');
	//echo $this->db->last_query(); exit;
    return $query;
  }
  
  public function does_user_exist_certifier($email){
	    $this->db->where('certifier_email', $email);
		$query = $this->db->get('tblAdmins');
		//echo $this->db->last_query(); exit;
		return $query;
	  }
	  
	  
	public function activation_forgotpass($uid,$activation)
	{
		$this->db->select('userId');
		$this->db->from('tblUsers');
		$this->db->where('userId',$uid);
		$this->db->where('activation',$activation); 		 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			$row = $query->row();
			return $row->userId;
		} else {	
			return FALSE;
		}		 
	} // activation_forgotpass


	public function merchant_activation_forgotpass($uid,$activation)
	{
		$this->db->select('userId');
		$this->db->from('tblMerchantUsers');
		$this->db->where('userId',$uid);
		$this->db->where('activation',$activation); 		 
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			$row = $query->row();
			return $row->userId;
		} else {	
			return FALSE;
		}		 
	} // merchant_activation_forgotpass	

	  
	public function validate_user_id($uid)
	{
		$this->db->select('userId');
		$this->db->from('tblUsers');
		$this->db->where('userId', $uid);
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return TRUE;
		} else {
			return FALSE;			
		}		
	} // validate_user_id  


	public function validate_merchant_id($uid)
	{
		$this->db->select('userId');
		$this->db->from('tblMerchantUsers');
		$this->db->where('userId', $uid);
		$query = $this->db->get();
		if($query->num_rows() == 1)
		{
			return TRUE;
		} else {
			return FALSE;			
		}		
	} // validate_merchant_id  
	  
	  
	public function updatepassword($user)
	{
		$data = array('userPassword'   => $this->encrypt->sha1($user['password']),		             
					  'activation' => '',
					  'status'     => 1
					 );		
		$this->db->where('userId', $user['uid']);
		$this->db->update('tblUsers', $data);		
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 			 
	} // updatepassword	  


	public function update_merchant_password($user)
	{
		$data = array('userPassword' => $this->encrypt->sha1($user['password']),		             
					  'activation'   => '',
					  'status'       => 1
					 );		
		$this->db->where('userId', $user['uid']);
		$this->db->update('tblMerchantUsers', $data);		
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 			 
	} // update_merchant_password	  
	  
	  
	  
}

