<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
  } 
	
	////////////////  Pagination Start Here   /////////////////////

 public function record_count() {
	     $this->db->select('*');
	    $this->db->from('certification_payments');
	 
	   $this->db->join('certification_users', 'certification_users.user_id = certification_payments.certification_users_user_id');
        return $this->db->count_all_results();
    }
 
    public function fetch_countries($limit, $start) {
        $this->db->limit($limit, $start);
		 $this->db->select('*');
	 $this->db->from('certification_payments');
	 
	 $this->db->join('certification_users', 'certification_users.user_id = certification_payments.certification_users_user_id');
        $query = $this->db->get();
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
   
  ///////////////  Pagination End Her     //////////////////////

	
  
  public function add_payment(){
	 $data = array(
	 'certification_users_user_id' => $this->input->post('certification_users_user_id'),
	 'payment_amount' => $this->input->post('payment_amount'),
	 'email' => $this->input->post('email'),
	 'payment_status' => $this->input->post('payment_status'),
	 'payment_log' => $this->input->post('payment_log'),
	 'payment_date' => date('Y-m-d H:i:s')
	 );
		return $this->db->insert('certification_payments', $data);			  
	  }
	  
  public function edit_paymnet_val($payment_id){
	   $query = $this->db->get_where('certification_payments', array('payment_id' => $payment_id));
  	   return $query->row_array();
	  }	  
	  
  public function update_payment(){
	  $payment_id = $this->input->post('payment_id');
	   $data = array(
		 'certification_users_user_id' => $this->input->post('certification_users_user_id'),
		 'payment_amount' => $this->input->post('payment_amount'),
		 'email' => $this->input->post('email'),
		 'payment_status' => $this->input->post('payment_status'),
		 'payment_log' => $this->input->post('payment_log'),
		 'payment_date' => date('Y-m-d H:i:s')
		 );
	  
	    $this->db->where('payment_id',$payment_id);
		return $this->db->update('certification_payments',$data); 
	  }	
  public function del_payment($payment_id){
	  $this->db->where('payment_id',$payment_id);
	  return $this->db->delete('certification_payments',$data);
	  }	  
	  
}


