<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup_model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->library('encrypt');
  } 

  public function add_user(){
		 $admin_hash = $this->encrypt->sha1($this->input->post('password'));
		 $data = array(
		 'admin_fname' => $this->input->post('admin_fname'),
		 'admin_lname' => $this->input->post('admin_lname'),
		 'admin_username' => $this->input->post('admin_username'),
		 'admin_email' => $this->input->post('admin_email'),
		 'admin_hash' => $admin_hash
		 );
			return $this->db->insert('certification_admin', $data);			  
	  }
}


