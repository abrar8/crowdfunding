<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Exchange_rates_model extends CI_Model {
	function __construct() {
    	parent::__construct();
  	} 

		
	public function exchangerates_list()
	{
        $this->db->select('*');
        $this->db->from('tblExchangeRates');
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    } // exchangerates_list	
		
	

	
	
	
} // Exchange_rates_model