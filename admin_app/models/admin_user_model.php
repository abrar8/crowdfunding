<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_user_model extends CI_Model {
  function __construct() {
     parent::__construct();
	 $this->load->database();
	 $this->load->library('encrypt');
  } 

  public function get_admin_val(){
	  $admin_id = $this->session->userdata('admin_id');
	
	 $query = $this->db->get_where('tblAdmins', array('admin_id' => $admin_id));
  	  return $query->row_array();
	  
	  }
	  
	  
  public function admin_user_profile(){
		// $user_hash = $this->encrypt->sha1($this->input->post('password'));
		$admin_id = $this->input->post('admin_id');
		 $data = array(
		 'admin_fname' => $this->input->post('admin_fname'),
		 'admin_lname' => $this->input->post('admin_lname'),
		 'admin_email' => $this->input->post('admin_email')
		 );
		 $this->db->where('admin_id',$admin_id);
		 return $this->db->update('tblAdmins', $data);			  
	  }
	  
 public function checkpass(){
   	 $admin_id = $this->session->userdata('admin_id');

	 if($this->session->userdata('certifier_login')== FALSE){
		 	$query = $this->db->get_where("certification_admin", array('admin_id' =>$admin_id));
			return $query;
	    }else{
			$query = $this->db->get_where("certification_certifier", array('certifier_id' =>$admin_id));
			return $query;
			}
	 }	  
	  
	  
   public function changepass(){
	   $admin_id = $this->session->userdata('admin_id');

	   if($this->session->userdata('certifier_login')== FALSE){
		    $data = array(
		    'admin_hash'=> $this->encrypt->sha1($this->input->post('new_pass'))
		    );
		    $this->db->where("admin_id",$admin_id);
		   return $this->db->update("certification_admin",$data);
		   
	   }else{
		    $data = array(
		   'certifier_hash'=> $this->encrypt->sha1($this->input->post('new_pass'))
		   );
		   $this->db->where("certifier_id",$admin_id);
		   return $this->db->update("certification_certifier",$data);
		  }
	}	  
	  
}


