<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adminuser extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('admin_user_model');
	$this->load->helper('form');
    //$this->lang->load('en_admin', 'english'); 
    $this->load->library('form_validation');
	$this->load->library('session');
	
  }
  
    public function admin_update(){
		$data['query'] = $this->admin_user_model->get_admin_val();
		
	    $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
		$this->load->view('adminuser_profile',$data);
		$this->load->view('common/footer',$data); 
	  }
	  
	  
   public function admin_update_val(){
	    $this->form_validation->set_rules('admin_fname', 'first name', 'required');
		$this->form_validation->set_rules('admin_lname', 'last name', 'required');
		$this->form_validation->set_rules('admin_email', 'email', 'required');
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'adminuser/admin_update');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->admin_user_model->admin_user_profile();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'adminuser/admin_update');
			   }
	   }
	   
  public function change_pass(){
	    $this->load->view('common/header');
		$this->load->view('common/nav');
		$this->load->view('change_pass');
		$this->load->view('common/footer'); 
	  }   
	  
  public function change_pass_val(){
	  
	  $this->form_validation->set_rules('old_pass', 'old password', 'required');
	  $this->form_validation->set_rules('new_pass', 'new password' , 'required');
	  $this->form_validation->set_rules('conf_new_pass', 'confirm new password', 'required');
	  $this->form_validation->set_rules('new_pass', 'new password', 'trim|min_length[6]|matches[conf_new_pass]');
	  if($this->form_validation->run()===false){
		  $this->load->view('common/header');
		  $this->load->view('common/nav');
		  $this->session->set_flashdata('errors',validation_errors());
		  redirect(base_url().'adminuser/change_pass'); 
		  $this->load->view('common/footer');
		  }else{
			   $query = $this->admin_user_model->checkpass();
			   if($query->num_rows() ==1){
				   foreach($query->result() as $row){
					     $this->load->library('encrypt');
						  $hash = $this->encrypt->sha1($this->input->post('old_pass'));
						   if($this->session->userdata('certifier_login')== FALSE){
							   $check_pass = $row->admin_hash;
							   }else{
								    $check_pass = $row->certifier_hash;
								   }
						   if($hash != $check_pass ){
							  $this->session->set_flashdata('errors',"The entered old password does nto match");
							  redirect(base_url().'adminuser/change_pass'); 
						   }
					   }
			   }
			   
			   $this->admin_user_model->changepass();
			   $this->session->set_flashdata('message', 'Password changed Successfuly');
			   redirect(base_url().'adminuser/change_pass');
			  }
	  }	  
  
 }
