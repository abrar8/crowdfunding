<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('users_model');
	$this->load->helper('form');
    //$this->lang->load('en_admin', 'english'); 
    $this->load->library('form_validation');
	$this->load->library('session');
	$this->load->library('pagination');
  }

  public function users_view() {

	 ///////////   Paginaton Start Here     ////////////
		$config = array();
        $config["base_url"] = base_url() . "users/users_view";
		$total_row = $this->users_model->count_users_veiw();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->users_model->fetch_users_view($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 
		
      //////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
	$this->load->view('common/nav',$data);
        $this->load->view('users_view',$data);
        $this->load->view('common/footer',$data);   
    }
	
   public function users_view_edit() {

	 ///////////   Paginaton Start Here     ////////////
		$config = array();
        $config["base_url"] = base_url() . "users/users_view_edit";
		$total_row = $this->users_model->count_users_edit_veiw();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->users_model->fetch_users_view_edit($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 
		
      //////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('users_view_edit',$data);
        $this->load->view('common/footer',$data);   
    }
			
  public function users_add(){
	  	
	    $this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('users_add');
        $this->load->view('common/footer');   
	  }	
  public function add_user_val(){
 		$this->form_validation->set_rules('admin_fname', 'first name', 'required');
		$this->form_validation->set_rules('admin_lname', 'last name', 'required');
		$this->form_validation->set_rules('admin_username', 'user name', 'required|is_unique[tblAdmins.admin_username]');
		$this->form_validation->set_rules('admin_email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('role_id', 'roles name', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('conf_password', 'confirm password', 'required');
		$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|matches[conf_password]');
		
		if($this->form_validation->run()=== false){
		    $this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('users_add');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->users_model->add_users();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'users/users_add');
			   }
	  }	  
	 
  public function users_edit(){
	  $admin_id = $this->uri->segment(3);
	  $data['roles'] = $this->db->get("tblRoles")->result_array();
	  $data['query'] = $this->users_model->user_edit($admin_id);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('users_edit',$data);
      $this->load->view('common/footer',$data);
	  }	
	
  public function update_user(){
  
		$this->form_validation->set_rules('admin_fname', 'first name', 'required');
		$this->form_validation->set_rules('admin_lname', 'last name', 'required');
		$this->form_validation->set_rules('admin_username', 'user name', 'required');
		$this->form_validation->set_rules('admin_email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('role_id', 'roles name', 'required');
				
	    $admin_id = $this->input->post('admin_id');
	 
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'users/users_edit/'.$admin_id);
			$this->load->view('common/footer');  
			}
		    else{
			   $this->users_model->updat_users_val();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'users/users_view_edit');
			   }
	  }	
  
   public function users_delete(){
	   $admin_id = $this->uri->segment(3);
	   $this->users_model->del_users_val($admin_id);
	   
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'users/users_view_edit');
	   }
	   
   public function users_profile(){
	  $this->load->view('common/header');
	  $this->load->view('common/nav');
      $this->load->view('user_profile');
      $this->load->view('common/footer');
	  }	   
  
   public function update_adminuser(){
	    $this->form_validation->set_rules('user_fname', 'first name', 'required');
		$this->form_validation->set_rules('user_lname', 'last name', 'required');
		$this->form_validation->set_rules('user_email', 'email', 'required');
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('user_profile');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->users_model->user_profile();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'users/users_profile');
			   }
	   
	   }
	   
    
	   
 }
