<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Signin extends CI_Controller {
	function __construct() {
    parent::__construct();
	
    $this->load->helper('form');
    $this->load->helper('security');
    $this->lang->load('en_admin', 'english'); 
    $this->load->library('form_validation');
	$this->load->library('session');
	$this->load->model('signin_model');
	$this->load->library('encrypt');  
    $this->form_validation->set_error_delimiters('<div class="alert alert-warning" role="alert">', '</div>'); 
  }

  public function index() {
	 	  
  if ($this->session->userdata('logged_in') == true) {
        redirect('dashboard');
      } else {
       //Set validation rules for view filters
    
	  $this->form_validation->set_rules('username', $this->lang->line('email'), 'required');
      $this->form_validation->set_rules('password', $this->lang->line('password'), 'required');

      if ($this->form_validation->run() == FALSE) {
        $this->load->view('login_view');
      } else {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
		//$login_type = $this->input->post('login_type');
		
       
        $query = $this->signin_model->does_user_exist($username);

        if ($query->num_rows() == 1) { // One matching row found
          foreach ($query->result() as $row) {
            // Call Encrypt library
            $this->load->library('encrypt');

            // Generate hash from a their password
            $hash = $this->encrypt->sha1($password);

            if ($row->admin_level != 0) { // See if the user is active or not
              // Compare the generated hash with that in the database
              if ($hash != $row->admin_hash) {
                // Didn't match so send back to login
                $data['login_fail'] = "OPPS! Password Not Matched";
                //echo "admin@admin.com";
				$this->load->view('login_view',$data);
              } else {
                $data = array(
                    'admin_id' => $row->admin_id,
                    'admin_fname' => $row->admin_fname,
					'admin_username' => $row->admin_username,
                    'admin_lname' => $row->admin_lname,
					'admin_email' => $row->admin_email,
					'admin_level' => $row->admin_level,
					'role_id'     => $row->role_id,    
                    'logged_in' => TRUE
                );
                // Save data to session
                $this->session->set_userdata($data);
				
				//Set roles for admin opertaion
				$role_row = $this->db->get_where('tblRoles' ,array('role_id'=>$this->session->userdata('role_id')))->row_array() ;
				$admin = array(
					'role_create' => $role_row['role_create'],
					'role_update' => $role_row['role_update'],
					'role_delete' => $role_row['role_delete']
					);
				$this->session->set_userdata($admin);
				
				redirect('dashboard');
              }
            } else {
              // User currently inactive
              $data['login_fail'] = "User Not Active";
			  $this->load->view('login_view',$data);
            }
          }
        } else {
		 	$data['login_fail'] = "Admin With Email Not Exists!";
			$this->load->view('login_view',$data);
		}
	 	}
   		} 
 	}

	
	public function signout() 
	{
    	$this->session->sess_destroy();
    	redirect('signin');
  	} 
	  
	  
	public function forgot_pass_activation()
	{	
		$uid = $this->uri->segment(3);
		$activation = $this->uri->segment(4);
		$user_id = $this->signin_model->activation_forgotpass($uid,$activation);
		if($user_id)
		{	
			redirect(ADMIN_URL.'signin/reset_password/'.$user_id);
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Invalid Activation Code</div>');
			redirect(ADMIN_URL.'signin/message');
		}		
	} // forgot_pass_activation


	public function merchant_forgot_pass_activation()
	{	
		$uid = $this->uri->segment(3);
		$activation = $this->uri->segment(4);
		$user_id = $this->signin_model->merchant_activation_forgotpass($uid,$activation);
		if($user_id)
		{	
			redirect(ADMIN_URL.'signin/merchant_reset_password/'.$user_id);
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Invalid Activation Code</div>');
			redirect(ADMIN_URL.'signin/message');
		}		
	} // merchant_forgot_pass_activation


	public function reset_password()
	{	
		$uid = $this->uri->segment(3);
		$status = $this->signin_model->validate_user_id($uid);
		if($status == FALSE) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Invalid User ID</div>');
			redirect(ADMIN_URL.'signin/message');		
		} else {
			$data = array();
			$data['uid'] = $uid;
			$data['account_type'] = 1;	
			$this->load->view('reset_password',$data);
		}		
	} // reset_password


	public function merchant_reset_password()
	{	
		$uid = $this->uri->segment(3);
		$status = $this->signin_model->validate_merchant_id($uid);
		if($status == FALSE) {
			$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Invalid Merchant ID</div>');
			redirect(ADMIN_URL.'signin/message');		
		} else {
			$data = array();
			$data['uid'] = $uid;
			$data['account_type'] = 2;	
			$this->load->view('reset_password',$data);
		}		
	} // merchant_reset_password

				
	public function update_password()
	{	
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
		if($this->form_validation->run() == FALSE)
		{ 
			redirect(ADMIN_URL.'signin/reset_password/'.$this->input->post('uid'));
		} else {
			$user = $this->input->post();
			$status = $this->signin_model->updatepassword($user);		
			if($status == TRUE)
			{	
				$this->session->set_flashdata('message', '<legend class="legend">Password Updated Successfully. Please login</legend>');
				redirect(ADMIN_URL.'signin/message');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Updated Failed.</div>');
				redirect(ADMIN_URL.'signin/message');
			}
		}	
	} // update_password


	public function update_merchant_password()
	{	
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[password]');
		if($this->form_validation->run() == FALSE)
		{ 
			redirect(ADMIN_URL.'signin/merchant_reset_password/'.$this->input->post('uid'));
		} else {
			$user = $this->input->post();
			$status = $this->signin_model->update_merchant_password($user);		
			if($status == TRUE)
			{	
				$this->session->set_flashdata('message', '<legend class="legend">Password Updated Successfully. Please login</legend>');
				redirect(ADMIN_URL.'signin/message');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password Updated Failed.</div>');
				redirect(ADMIN_URL.'signin/message');
			}
		}	
	} // update_merchant_password
  
  
  	public function message()
	{
		$this->load->view('message');		
	} // message
  
  
  
   
} // Signin