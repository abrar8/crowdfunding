<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Signup extends CI_Controller {
  function __construct() {
		parent::__construct();
		$this->load->model('signup_model');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
  }

  public function index(){
	   $this->signup_model->add_user();
	   $this->session->set_flashdata('message', 'SignUp Completed Successfuly');
		  redirect(base_url().'signin');
	   }
  
 }
