<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Forgetpass extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('forgetpass_model');
    $this->load->helper('form');
    $this->load->helper('security');
    $this->load->library('form_validation');
	$this->load->library('email');
  }

  public function index() {
	    
	  $query = $this->forgetpass_model->get_user();
	  $login_type = $this->input->post('login_type');
	  if($query->num_rows()>0){ 
	  
			foreach ($query->result() as $row)
			{
				if($login_type == 1){
					 $email = $row->admin_email;
					 $id  = base64_encode($row->admin_id);
				}else{
					 $email = $row->certifier_email;
					 $id  = $row->certifier_id;
					}
			}
	  }else{ 
		  $this->session->set_flashdata('errors', 'User not registered with this email');
		  	redirect(base_url().'signin');
		  }
		$login_type = $this->input->post('login_type');
		$email = $this->input->post('email');
		$this->email->set_mailtype("html");
		$this->email->from('admin@certification.com', 'Certification');
		$this->email->to($email);
		$this->email->subject('Forget Password');
		$this->email->message('Please click the following link to reset password<br><a href="'.base_url().'forgetpass/resetpass/'.$login_type.'/'.$id.'" target="_blank">'.base_url().'forgetpass/resetpass/'.$login_type.'/'.$id.'</a>');
		$this->email->send();
		//echo $this->email->print_debugger(); exit;
		$this->session->set_flashdata('message', 'An email sent to your email.please login.');
		  redirect(base_url().'signin');
   }
 
 public function resetpass(){
	    $this->load->view('resetpass');
	 }
	 
  public function reset_pass(){
	   $this->form_validation->set_rules('new_pass', 'new password', 'required');
	   $this->form_validation->set_rules('conf_pass', 'new password', 'required');
	   $this->form_validation->set_rules('new_pass', 'confirm password', 'trim|min_length[6]|matches[conf_pass]');
	   if($this->form_validation->run()=== false){
		
		   $this->load->view('resetpass');
		   }
		     else{
			   $this->forgetpass_model->change_pass();
			   $this->session->set_flashdata('message', 'Password Changed Successfuly');
				  redirect(base_url().'forgetpass/resetpass');
			   }
	  }	 
}
