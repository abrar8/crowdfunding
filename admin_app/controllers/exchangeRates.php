<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class ExchangeRates extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('cms_model');
		$this->load->library('form_validation');
	}


	public function exchangerates_list()
  	{
  		$data = array();
  		$data['exchangeRates'] = $this->cms_model->exchangeRates_list();
  		$data['title'] = 'Exchange Rates List';
		$data['page'] = 'exchangeRates_list';
        $this->load->view('template',$data);
	} // exchangerates_list


	public function add_currency_rates()
  	{
  		$data = array();
  		$data['title'] = 'Add Currency Rates';
		$data['page'] = 'add_currency_rates';
        $this->load->view('template',$data);
	} // add_currency_rates


	public function save_currency_rates()
	{
 		$this->form_validation->set_rules('currency_name', 'Currency Name', 'trim|required');
		$this->form_validation->set_rules('currency_type', 'Currency Type', 'trim|required');
		$this->form_validation->set_rules('buying', 'Buying Rate', 'trim|required');
		$this->form_validation->set_rules('selling', 'Selling Rate', 'trim|required');
		$this->form_validation->set_rules('symbol', 'Currency Symbol', 'trim');
		if($this->form_validation->run()=== false)
		{
		    $this->add_currency_rates();  
		} else {		
			$data = array("currency_name" => addslashes($this->input->post('currency_name')),
						  "currency_type" => $this->input->post('currency_type'),
						  "symbol"        => $this->input->post('symbol'),
						  "buying"        => $this->input->post('buying'),
						  "selling"       => $this->input->post('selling')
						 );
			$status = $this->cms_model->save_currency_rates($data);
		    if($status) 
			{                   
                $this->session->set_flashdata('message', 'Currency Rates added successfully.');
				redirect(base_url().'exchangeRates/exchangerates_list');
            } else {
				$this->session->set_flashdata('message', 'Unable to add currency rates.');
				redirect(base_url().'exchangeRates/add_currency_rates');
            }
		}
	} // save_currency_rates


	public function edit_currency()
  	{
  		$data = array();
  		$data['currency_rates'] = $this->cms_model->get_currency_rates_info($this->uri->segment(3));
  		$data['title'] = 'Edit Currency Rates';
		$data['page'] = 'edit_currency_rates';
        $this->load->view('template',$data);
	} // edit_currency

	
	public function update_currency_rates()
	{
 		$this->form_validation->set_rules('currency_name', 'Currency Name', 'trim|required');
		$this->form_validation->set_rules('currency_type', 'Currency Type', 'trim|required');
		$this->form_validation->set_rules('buying', 'Buying Rate', 'trim|required');
		$this->form_validation->set_rules('selling', 'Selling Rate', 'trim|required');
		$this->form_validation->set_rules('symbol', 'Currency Symbol', 'trim');
		if($this->form_validation->run()=== false)
		{
		    redirect(base_url().'exchangeRates/edit_currency/'.$this->input->post('id')); 
		} else {
			$id = $this->input->post('id');		
			$data = array("currency_name" => addslashes($this->input->post('currency_name')),
						  "currency_type" => $this->input->post('currency_type'),
						  "symbol"        => $this->input->post('symbol'),
						  "buying"        => $this->input->post('buying'),
						  "selling"       => $this->input->post('selling')
						 );
			$status = $this->cms_model->update_currency_rates($data,$id);
		    if($status)
			{                   
                $this->session->set_flashdata('message', 'Currency Rates updated successfully.');
				redirect(base_url().'exchangeRates/exchangerates_list');
            } else {
				$this->session->set_flashdata('message', 'Failed to update currency rates.');
				redirect(base_url().'exchangeRates/edit_currency/'.$id);
            }
		}
	} // update_currency_rates
	
	
	public function del_currency()
	{
		$id = $this->uri->segment(3);	
		$status = $this->cms_model->del_currency($id);
		if($status)
		{                   
			$this->session->set_flashdata('message', 'Currency deleted successfully.');
			redirect(base_url().'exchangeRates/exchangerates_list');
		} else {
			$this->session->set_flashdata('message', 'Failed to delete currency.');
			redirect(base_url().'exchangeRates/exchangerates_list');
		}
	} // del_currency
	

	public function base_currency()
  	{
  		$data = array();
  		$data['base_currency'] = $this->cms_model->base_currency();
  		$data['title'] = 'Base Currency';
		$data['page'] = 'base_currency';
        $this->load->view('template',$data);
	} // base_currency

	
	public function edit_base_currency()
  	{
  		$data = array();
  		$data['base_currency'] = $this->cms_model->get_base_currency();
		$data['currencys'] = $this->cms_model->get_exchange_rates();
  		$data['title'] = 'Edit Base Currency';
		$data['page'] = 'edit_base_currency';
        $this->load->view('template',$data);
	} // edit_base_currency
	
	
	public function update_base_currency()
	{
 		$this->form_validation->set_rules('base_currency', 'Base Currency', 'trim|required');
		if($this->form_validation->run()=== false)
		{
		    redirect(base_url().'exchangeRates/edit_base_currency'); 
		} else {
			$data = array("currency_id" => $this->input->post('base_currency'));
			$this->cms_model->update_base_currency($data);
		   	redirect(base_url().'exchangeRates/base_currency');
        }
	} // update_base_currency
	
	
	
  
} // ExchangeRates
