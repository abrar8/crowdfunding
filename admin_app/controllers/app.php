<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class App extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('App_Model');
	$this->load->library('form_validation');
	
  }

  public function app_view() {
  
       	$config = array();
        $config["base_url"] = base_url() . "app/app_view";
		$total_row = $this->App_Model->app_count_view();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->App_Model->fetch_app_view($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 		
       // $data['query'] = $this->App_Model->get_all_app_val();
        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('app_view',$data);
        $this->load->view('common/footer',$data);   
    }
	
 public function app_edit_view() {
  
       	$config = array();
        $config["base_url"] = base_url() . "app/app_edit_view";
		$total_row = $this->App_Model->app_count_view_edit();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->App_Model->fetch_app_view_edit($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 		
       // $data['query'] = $this->App_Model->get_all_app_val();
        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('app_edit_view',$data);
        $this->load->view('common/footer',$data);   
    }
	
  public function app_add(){
	  	$this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('app_add');
        $this->load->view('common/footer');   
	  }	
  public function add_app_val(){
		$this->form_validation->set_rules('firstName', 'first name', 'required');
		$this->form_validation->set_rules('lastName', 'last name', 'required');
		$this->form_validation->set_rules('phoneNumber', 'phone number', 'required');
		$this->form_validation->set_rules('address', 'address', 'required');
		$this->form_validation->set_rules('city', 'city', 'required');
		$this->form_validation->set_rules('country', 'country', 'required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('available', 'available', 'required');
		$this->form_validation->set_rules('role_id', 'role', 'required');
		$this->form_validation->set_rules('conf_password', 'confirm password', 'required');
		$this->form_validation->set_rules('password', 'password', 'trim|min_length[6]|matches[conf_password]');
		
		if($this->form_validation->run()=== false){
			
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'app/app_add');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->App_Model->add_app();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'app/app_add');
			   }
	  }	  
	 
  public function app_edit(){
    ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1);
	
      $userId = $this->uri->segment(3);
	  $data['query'] = $this->App_Model->edit_app_val($userId);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('app_edit',$data);
      $this->load->view('common/footer',$data);
	  }	
	
  public function edit_app_val(){
	    $this->form_validation->set_rules('firstName', 'first name', 'required');
		$this->form_validation->set_rules('lastName', 'last name', 'required');
		$this->form_validation->set_rules('phoneNumber', 'phone number', 'required');
		$this->form_validation->set_rules('address', 'address', 'required');
		$this->form_validation->set_rules('city', 'city', 'required');
		$this->form_validation->set_rules('country', 'country', 'required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('role_id', 'role', 'required');
		$this->form_validation->set_rules('available', 'available', 'required');
			
	   $userId = $this->input->post('userId');
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'app/app_edit/'.$userId);
			$this->load->view('common/footer');  
			}
		    else{
			   $this->App_Model->update_app__val();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'app/app_edit_view');
			   }
	  }	
  
   public function app_delete(){
	   $userId = $this->uri->segment(3);
	   $this->App_Model->delete_app($userId);
	   
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'app/app_edit_view');
	   }
 }
