<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Banks extends CI_Controller {   
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != true) {
            redirect('signin');
        }
       $this->load->model('banks_model');
       $this->load->library('form_validation');
       $this->load->library('pagination');
        
    }


   public function view() { 

        if($this->input->post()){
            $default_perPage    =   $this->input->post('recordPerpage');
        } else if($this->uri->segment(4)){
            $default_perPage    =   $this->uri->segment(4);
        } else {
            $default_perPage    =   20;
        }
        $data['js'][]           =   'Banks';
        $data['page_title']     =   'Banks';
        $data['breadcrum']      =   array(
            'dashboard' =>  base_url('dashboard'),
            'Banks' =>  base_url('Banks/view'),
            $data['page_title'] =>  'active'
        );
        
        ///////////   Paginaton Start Here     ////////////
        
        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = base_url() . "Banks/view";     
        $total_row = $this->banks_model->count_banks(); 
        $config["total_rows"]    = $total_row;
        $config["per_page"]      = $default_perPage;
        $config['num_links']     = $total_row;
        $config['cur_tag_open']  = '&nbsp;<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link']     = 'Next';
        $config['prev_link']     = 'Previous';
        $config['uri_segment']   = 3;
        $this->pagination->initialize($config); 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->banks_model->get_banks($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        
        
        $data['total_row']  =   $total_row;
        $data['to']         =   (($this->uri->segment(3)) ? $this->uri->segment(3) : 0)+1;
        $data['from']       =   ($config["per_page"]+$this->uri->segment(4)>$total_row)?$total_row:$config["per_page"]+$this->uri->segment(3);
        
        $data['current_url']=   $config["base_url"];
        $data['per_page']   =   $config['per_page'];  
        $data['recordPerpage'] = array(
            5    => 5,
            10   => 10,
            15   => 15,
            20   => 20,
            1000 => 'all'
        );
        //echo "<pre>"; print_r($data); exit;
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('bank_view',$data);
        $this->load->view('common/footer');
     }



      public function edit($bankId){ 
        $data['result'] = $this->banks_model->selectBanks($bankId);
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('bnk_edit',$data);
        $this->load->view('common/footer');
    }

     public function update_bank_val(){
        $this->form_validation->set_rules('bankName', 'Bank Name', 'required');
        $this->form_validation->set_rules('displayName', 'Display Name', 'required');
        
        $bankId          = $this->input->post('bankId');
        if($this->form_validation->run()=== false){
            $data['result'] = $this->banks_model->selectBanks($bankId);
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('bnk_edit',$data);
            $this->load->view('common/footer');  
            }
            else{ 
            $bankName       = $this->input->post('bankName');
            $displayName    = $this->input->post('displayName');
            $this->banks_model->update_banks($bankName,$displayName,$bankId);
            $this->session->set_flashdata('message', 'Record Updated Successfuly');
            redirect(base_url().'Banks/view');
               
      }  
      } 

      public function addCoupons(){  
        $data["res"] = $this->promotions_model->get_promos();
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('addCoupons',$data);
        $this->load->view('common/footer');

      }

      public function add_coupon_val(){ 
        $this->form_validation->set_rules('couponCode',  'Coupon Code', 'required');
        $this->form_validation->set_rules('couponPrize', 'Coupon Prize', 'required');
        $this->form_validation->set_rules('promotion',   'Promotion', 'required');
        $data["res"] = $this->promotions_model->get_promos();
        if($this->form_validation->run()=== false){
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('addCoupons',$data);
            $this->load->view('common/footer');  
            }
        else{ 
              $couponCode       = $this->input->post('couponCode');
              $couponPrize      = $this->input->post('couponPrize');
              $promotion      = $this->input->post('promotion');
              $this->promotions_model->add_coupon($couponCode,$couponPrize,$promotion);
              $this->session->set_flashdata('message', 'Record Inserted Successfuly');
              redirect(base_url().'promostions/addCoupons');
        
            }  
      }        

	/////////////////////////////////////////////////////////////////////////////////////////////////
    public function add_bank() 
	{
		$data = array();
		$data['countries'] = $this->banks_model->get_countries();
        $data['title'] = 'Add Bank';
        $data['page'] = 'add_bank';
        $this->load->view('template',$data);   
    } // add_bank
	
	
	public function save_bank()
	{
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('bank_name', 'Bank Name', 'trim|required');
		if($this->form_validation->run()=== false)
		{ 
			$this->add_bank();
		} else {
			$data = array('bank_name'  => addslashes($this->input->post('bank_name')),
			              'country_id' => $this->input->post('country') 
						  );
			$status = $this->banks_model->add_bank($data);
			if($status)
			{
				$this->session->set_flashdata('message', 'Bank added successfuly');
				redirect(base_url().'banks/list_banks');
			} else {
				$this->session->set_flashdata('message', 'Failed to add bank details.');
				redirect(base_url().'banks/add_bank');
			}
		}
	} // save_bank 


	public function list_banks() 
	{
		$data = array();
		$data['banks'] = $this->banks_model->get_banks();
        $data['title'] = 'List of all Banks';
        $data['page'] = 'list_banks';
        $this->load->view('template',$data);   
    } // add_bank

	
	public function add_bank_branches() 
	{
		$data = array();
		$data['countries'] = $this->banks_model->get_countries();
        $data['title'] = 'Add Bank Branches';
        $data['page'] = 'add_bank_branches';
        $this->load->view('template',$data);   
    } // add_bank_branches


	public function get_bank_list() 
	{
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		if ($this->form_validation->run() == FALSE) 
		{
			$this->session->set_flashdata('message',validation_errors());
			redirect(base_url().'banks/add_bank_branches');
		} else {
			$country_id = $this->input->post("country");
			$banks = $this->banks_model->get_bank_list($country_id);
			echo json_encode($banks);
		}
    } // get_bank_list


	public function save_bank_branches()
	{
		$this->form_validation->set_rules('country', 'Country', 'trim|required');
		$this->form_validation->set_rules('bank', 'Bank Name', 'trim|required');
		$this->form_validation->set_rules('branch_name', 'Branch Name', 'trim|required');
		if($this->form_validation->run()=== false)
		{ 
			$this->add_bank();
		} else {
			$data = array('bank_id'     => $this->input->post('bank'),
			              'branch_name' => addslashes($this->input->post('branch_name'))						   
						  );
			$status = $this->banks_model->add_bank_branches($data);
			if($status)
			{
				$this->session->set_flashdata('message', 'Bank Branch added successfuly');
				redirect(base_url().'banks/list_bank_branches');
			} else {
				$this->session->set_flashdata('message', 'Failed to add bank branch.');
				redirect(base_url().'banks/add_bank_branches');
			}
		}
	} // save_bank_branches 


	public function list_bank_branches() 
	{
		$data = array();
		$data['bank_branches'] = $this->banks_model->list_bank_branches();
        $data['title'] = 'List Bank Branches';
        $data['page'] = 'list_bank_branches';
        $this->load->view('template',$data);   
    } // list_bank_branches
   

} // Banks