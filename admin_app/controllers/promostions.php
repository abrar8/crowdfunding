<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Promostions extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != true) {
            redirect('signin');
        }
       $this->load->model('promotions_model');
       $this->load->helper(array('form', 'url'));
       //$this->lang->load('en_admin', 'english'); 
       $this->load->library('form_validation');
       $this->load->library('session');
       $this->load->library('pagination');
        
    }

    /*

     * this is method show All Transactions which ever done
     * 
     */

   public function view() {  

        if($this->input->post()){
            $default_perPage    =   $this->input->post('recordPerpage');
        }else if($this->uri->segment(4)){
            $default_perPage    =   $this->uri->segment(4);
        }
        else{
            $default_perPage    =   20;
        }
        $data['js'][]           =   'Promotions';
        $data['page_title']     =   'Promotions';
        $data['breadcrum']      =   array(
            'dashboard' =>  base_url('dashboard'),
            'Promotions' =>  base_url('Promostions/view'),
            $data['page_title'] =>  'active'
        );
        
        ///////////   Paginaton Start Here     ////////////
        
        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = base_url() . "Promostions/view";     
        $total_row = $this->promotions_model->count_promotions();            
        $config["total_rows"]    = $total_row;
        $config["per_page"]      = $default_perPage;
        $config['num_links']     = $total_row;
        $config['cur_tag_open']  = '&nbsp;<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link']     = 'Next';
        $config['prev_link']     = 'Previous';
        $config['uri_segment']   = 4;
        $this->pagination->initialize($config); 
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data["results"] = $this->promotions_model->get_promostions($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        
        
        $data['total_row']  =   $total_row;
        $data['to']         =   (($this->uri->segment(4)) ? $this->uri->segment(4) : 0)+1;
        $data['from']       =   ($config["per_page"]+$this->uri->segment(4)>$total_row)?$total_row:$config["per_page"]+$this->uri->segment(4);
        
        $data['current_url']=   $config["base_url"];
        $data['per_page']   =   $config['per_page'];  
        $data['recordPerpage'] = array(
            5    => 5,
            10   => 10,
            15   => 15,
            20   => 20,
            1000 => 'all'
        );
        //echo "<pre>"; print_r($data); exit;
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('promostions_view',$data);
        $this->load->view('common/footer');
     }
    public function add(){
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('promotions_add');
        $this->load->view('common/footer');
    }
	 public function edit($id){
        $this->load->view('common/header');
		$this->db->select('*');
		$this->db->where('promotionId',$id); 
		$record = $this->db->get('tblPromotions')->result_array();
		$data['result'] = $record;
        $this->load->view('common/nav');
        $this->load->view('promotions_edit',$data);
        $this->load->view('common/footer');
    }
	
	
    public function add_promostion_val(){
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        //$this->form_validation->set_rules('userfile', 'image', 'required');
        
        if($this->form_validation->run()=== false){
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('promotions_add');
            $this->load->view('common/footer');  
            }
            else{ 
               $config['upload_path'] = './uploads/promotions';
               $config['allowed_types'] = 'gif|jpg|png';
               $config['max_size'] = '1000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());

            echo "<pre>"; print_r($error); exit;
        }
        else
        {
               $data        = array('upload_data' => $this->upload->data());
               $title       = $this->input->post('title');
               $description = $this->input->post('description');
               $file_name   = $data['upload_data']['file_name'];
               $this->promotions_model->add_promostions($title,$description,$file_name);
               $this->session->set_flashdata('message', 'Record Inserted Successfuly');
               redirect(base_url().'promostions/add');
               }
      }  
      } 

	  public function edit_promostion_val(){
         $id       = $this->input->post('id');
		 $old_image       = $this->input->post('old_image');
		$this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        //$this->form_validation->set_rules('userfile', 'image', 'required');
        
        if($this->form_validation->run()=== false){
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('promotions_edit');
            $this->load->view('common/footer');  
            }
            else{ 
               $config['upload_path'] = './uploads/promotions';
               $config['allowed_types'] = 'gif|jpg|png';
               $config['max_size'] = '1000';

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
           $file_name=$old_image;
		       $title = $this->input->post('title');
               $description = $this->input->post('description');
               $this->promotions_model->edit_promostions($id,$title,$description,$file_name);
               $this->session->set_flashdata('message', 'Record Update Successfuly');
               redirect(base_url().'promostions/view');
		   
		   
		   
        }
        else
        {
               $data        = array('upload_data' => $this->upload->data());
               $title       = $this->input->post('title');
               $description = $this->input->post('description');
               $file_name   = $data['upload_data']['file_name'];
			   $this->promotions_model->edit_promostions($id,$title,$description,$file_name);
               $this->session->set_flashdata('message', 'Record Update Successfuly');
               redirect(base_url().'promostions/view');
               }
      }  
      } 
	   
      public function addCoupons(){  
        $data["res"] = $this->promotions_model->get_promos();
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('addCoupons',$data);
        $this->load->view('common/footer');

      }

      public function add_coupon_val(){ 
        $this->form_validation->set_rules('couponCode',  'Coupon Code', 'required');
        $this->form_validation->set_rules('couponPrize', 'Coupon Prize', 'required');
        $this->form_validation->set_rules('promotion',   'Promotion', 'required');
        $data["res"] = $this->promotions_model->get_promos();
        if($this->form_validation->run()=== false){
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('addCoupons',$data);
            $this->load->view('common/footer');  
            }
        else{ 
              $couponCode       = $this->input->post('couponCode');
              $couponPrize      = $this->input->post('couponPrize');
              $promotion      = $this->input->post('promotion');
              $this->promotions_model->add_coupon($couponCode,$couponPrize,$promotion);
              $this->session->set_flashdata('message', 'Record Inserted Successfuly');
              redirect(base_url().'promostions/addCoupons');
        
            }  
      } 
        
   

}
