<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('roles_model');
    $this->load->library('form_validation');
	$this->load->library('pagination');
  }

  public function roles_view() {

	 ///////////   Paginaton Start Here     ////////////
		$config = array();
        $config["base_url"] = base_url() . "roles/roles_view";
		$total_row = $this->roles_model->roles_view_count();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->roles_model->fetch_roles_view($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 		
      //////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('roles_view',$data);
        $this->load->view('common/footer',$data);   
    }
	
  public function roles_view_edit() {
	 ///////////   Paginaton Start Here     ////////////
		$config = array();
        $config["base_url"] = base_url() . "roles/roles_view_edit";
		$total_row = $this->roles_model->roles_edit_count();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->roles_model->fetch_roles_edit($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 		
      //////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('roles_view_edit',$data);
        $this->load->view('common/footer',$data);   
    }
	
	
  public function roles_add(){
	  	
		//$data['categories'] = $this->db->get("certification_category")->result_array();
	  
	    $this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('roles_add');
        $this->load->view('common/footer');   
	  }	
  public function add_roles_val(){
		$this->form_validation->set_rules('role_name', 'role name', 'required');
	    if($this->form_validation->run()=== false){
		     
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('roles_add');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->roles_model->add_roles();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'roles/roles_add');
			   }
	  }	  
	 
  public function roles_edit(){
	  $id = $this->uri->segment(3);
	  $data['query'] = $this->roles_model->roles_val_edit($id);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('roles_edit',$data);
      $this->load->view('common/footer',$data);
	  }	
	
  public function edit_roles_val(){
		$this->form_validation->set_rules('role_name', 'role name', 'required');
	    $role_id = $this->input->post('role_id');
	 
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'roles/roles_edit/'.$role_id);
			$this->load->view('common/footer');  
			}
		    else{
			   $this->roles_model->update_roles();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'roles/roles_view_edit');
			   }
	  }	
  
   public function roles_delete(){
	   $role_id = $this->uri->segment(3);
	   $this->roles_model->delete_role($role_id);
	   
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'roles/roles_view_edit');
	   }
	   
   public function users_profile(){
	  $this->load->view('common/header');
	  $this->load->view('common/nav');
      $this->load->view('user_profile');
      $this->load->view('common/footer');
	  }	   
  
   public function update_adminuser(){
	    $this->form_validation->set_rules('user_fname', 'first name', 'required');
		$this->form_validation->set_rules('user_lname', 'last name', 'required');
		$this->form_validation->set_rules('user_email', 'email', 'required');
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('user_profile');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->users_model->user_profile();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'users/users_profile');
			   }
	   
	   }
	   
  public function user_view(){
	  $user_id = $this->uri->segment(3);
	  $data['query'] = $this->users_model->user_view_model($user_id);
	  $this->load->view('common/header');
	  $this->load->view('common/nav');
      $this->load->view('user_view',$data);
      $this->load->view('common/footer');
	  }	   
	   
 }
