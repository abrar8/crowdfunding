<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Card extends CI_Controller {

    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != true) {
            redirect('signin');
        }
        $this->load->model('Card_model');
        $this->load->library('form_validation');
        $this->load->library('pagination');
    }

    public function card_view() {

        $status = 1;//$this->uri->segment(3);
        $config = array();
        $config["base_url"] = base_url() . "card/card_view/";// . $status;
        $total_row = $this->Card_model->card_count($status);
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '&nbsp;<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Card_model->fetch_card($config["per_page"], $page, $status);
        $data["links"] = $this->pagination->create_links();
        $this->load->view('common/header', $data);
        $this->load->view('common/nav', $data);
        $this->load->view('card_view', $data);
        $this->load->view('common/footer', $data);
    }

    public function card_view_edit() {

        $config = array();
        $config["base_url"] = base_url() . "card/card_view_edit";
        $total_row = $this->Card_model->card_count_edit();
        $config["total_rows"] = $total_row;
        $config["per_page"] = 10;
        $config['num_links'] = $total_row;
        $config['cur_tag_open'] = '&nbsp;<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Previous';
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Card_model->fetch_card_edit($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        
        $this->load->view('common/header', $data);
        $this->load->view('common/nav', $data);
        $this->load->view('card_view_edit', $data);
        $this->load->view('common/footer', $data);
    }

    public function card_add() {
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('card_add');
        $this->load->view('common/footer');
    }

    public function card_add_val() {
        $this->form_validation->set_rules('cardNumber', 'Card Number', 'required|is_unique[tblScratchCards.cardNumber]');
        $this->form_validation->set_rules('cardAmount', 'Card Amount', 'required');
        $this->form_validation->set_rules('available', 'Available', 'required');

        if ($this->form_validation->run() === false) {
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->load->view('card_add');
            $this->load->view('common/footer');
        } else {
            $this->Card_model->scratch_card__val_add();
            $this->session->set_flashdata('message', 'Record Inserted Successfuly');
            redirect(base_url() . 'card/card_add');
        }
    }

    public function card_edit() {
        $cardId = $this->uri->segment(3);
        $data['query'] = $this->Card_model->scratch_card_update($cardId);
        $this->load->view('common/header', $data);
        $this->load->view('common/nav', $data);
        $this->load->view('card_edit', $data);
        $this->load->view('common/footer', $data);
    }

    public function card_update_val() {
        $this->form_validation->set_rules('cardNumber', 'Card Number', 'required');
        $this->form_validation->set_rules('cardAmount', 'Card Amount', 'required');
        $this->form_validation->set_rules('available', 'Available', 'required');

        $cardId = $this->input->post('cardId');
        if ($this->form_validation->run() === false) {
            $this->load->view('common/header');
            $this->load->view('common/nav');
            $this->session->set_flashdata('errors', validation_errors());
            redirect(base_url() . 'card/card_edit/' . $cardId);
            $this->load->view('common/footer');
        } else {
            $this->Card_model->update_scratch_card_val();
            $this->session->set_flashdata('message', 'Record Updated Successfuly');
            redirect(base_url() . 'card/card_view_edit');
        }
    }

    public function card_delete() {
        $cardId = $this->uri->segment(3);
        $this->Card_model->scratch_card_delete($cardId);

        $this->session->set_flashdata('message', 'Record Deleted Successfuly');
        redirect(base_url() . 'card/card_view_edit');
    }

    public function cvs_card_add() {
        $this->load->view('common/header');
        $this->load->view('common/nav');
        $this->load->view('cvs_card_add');
        $this->load->view('common/footer');
    }

    public function importcsv() {
        $path = $_FILES['userfile']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        if ($_FILES['userfile']['name'] == '' or $ext != 'csv') {
            $this->session->set_flashdata('errors', 'Please select CVS file');
            redirect(base_url() . 'card/cvs_card_add');
        } else {
            $file = fopen($_FILES['userfile']['tmp_name'], "r");
            $csv_file = fgetcsv($file);
            //print_r($csv_file); exit;
            $i = 0;
            while (!feof($file)) {
                $csv_data[] = fgets($file, 1024);
                $csv_array = explode(",", $csv_data[$i]);

                $cardNumber = $csv_array[0];
                $cardAmount = $csv_array[1];
                $available = $csv_array[2];
                if ($cardNumber && $cardAmount && $available) {
                    $query = "INSERT INTO tblScratchCards SET 
												cardNumber     ='$cardNumber',
												cardAmount     ='$cardAmount',	
												available      ='$available'";
                    mysql_query($query);
                }
                $i++;
            }
            fclose($csvfile);
            $this->session->set_flashdata('message', 'Record Updated Successfuly');
            redirect(base_url() . 'card/cvs_card_add');
        }
    }

}
