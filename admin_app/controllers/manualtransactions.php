<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Manualtransactions extends CI_Controller {
	function __construct() {
    	parent::__construct();
	 	if($this->session->userdata('logged_in') != true)
		{
	 		redirect('signin');
	 	}
		$this->load->model('manualtransactions_model');
    	$this->load->library('form_validation');
		$this->load->library('pagination');
	}

			
	public function add_transaction_in()
  	{
		$data['title'] = 'Add Transaction IN';
		$data['page'] = 'add_manual_transactions_in';
        $this->load->view('template',$data);   
	} // add_transaction_in
	
		
	public function save_transaction_in()
	{
 		$this->form_validation->set_rules('sender_name', 'Sender Name', 'trim|required');
		$this->form_validation->set_rules('sender_phone', 'Sender Phone', 'trim');
		$this->form_validation->set_rules('sender_email', 'Sender Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('sender_address', 'Sender Address', 'trim');
		$this->form_validation->set_rules('amount', 'Amount', 'trim|required');
		$this->form_validation->set_rules('receiver_userpin', 'Receiver UserPin', 'trim');
		$this->form_validation->set_rules('receiver_email', 'Receiver Email', 'trim');
		//$this->form_validation->set_rules('status', 'status', 'trim|required');
		if($this->form_validation->run()=== false)
		{
		    $this->add_transaction_in();  
		} else {		
            $userPIN = $this->input->post('receiver_userpin');
			$amount = $this->input->post('amount');
			$admin_id = $this->session->userdata('admin_id');
			$description = $this->input->post('description');
            $user = $this->manualtransactions_model->user_balance($admin_id); // Get User Balance
            $userPinStatus = $this->manualtransactions_model->check_userPin($userPIN, $admin_id);
			$receiver_email = $this->input->post('receiver_email');
			$receiver_userpin = $this->input->post('receiver_userpin');
			$receiver = $this->manualtransactions_model->get_user_profile($receiver_email,$receiver_userpin);			
						
			$data = array("fullname"    => $this->input->post('sender_name'),
						  "email"       => $this->input->post('sender_email'),
						  "phone"       => $this->input->post('sender_phone'),
						  "address"     => $this->input->post('sender_address'),
						  "secret_code" => '',
						  "uid"         => $receiver->userId,
						  "type"        => 1, // 1 for IN and 2 for OUT
						  "amount"      => $this->input->post('amount'),
						  "datetime"    => date('Y-m-d H:i:s')
						 );
			$this->manualtransactions_model->add_manual_transaction_in($data);
			
			if ($userPinStatus == 1) {
                $this->session->set_flashdata('message', 'You cannot transfer credit to your own account.');
				redirect(base_url().'manualtransactions/add_transaction_in');	
				
            } elseif($user['credit'] < $amount) {				
                $this->session->set_flashdata('message', 'Insufficient balance. Your balance '.$user['credit']);
				redirect(base_url().'manualtransactions/add_transaction_in');				
            } else {
                $user_profile = $this->manualtransactions_model->user_profile_by_pin($userPIN);
                if($user_profile) 
				{
                    $debit = array('userId'      => $admin_id,
								   'refId'       => $user_profile->userId,
								   'dr'          => $amount,
								   'gps'         => 0,
								   'devId'       => 0,
								   'type'        => 'TR',
								   'mode'        => 'TCP',
								   'description' => $description
								  );
                    $credit = array('userId'      => $user_profile->userId,
									'refId'       => $admin_id,
									'cr'          => $amount,
									'gps'         => 0,
									'devId'       => 0,
									'type'        => 'TR',
									'mode'        => 'TCP',
									'description' => $description
								);
                    $transaction_data = $this->manualtransactions_model->transfer_credit($admin_id, $debit, $credit);
                    if ($transaction_data) {
						$this->session->set_flashdata('message', 'Transaction added successfuly');
						redirect(base_url().'manualtransactions/add_transaction_in');
	                } // if
                } else {
					$this->session->set_flashdata('message', 'Invalid User Pin. Please try again.');
					redirect(base_url().'manualtransactions/add_transaction_in');
                }
            } // else
		}
	} // save_transaction_in
	   
	   
	public function transactions_out()
	{
		$data['transactions'] = $this->manualtransactions_model->get_transactions_out();
		$data['title'] = "Manual Transaction (Out)";
		$data['page'] = 'transaction_out';
		$this->load->view('template',$data);   
	} // transactions_out	   
	   
	   
	public function transactions_in()
	{
		$data['transactions'] = $this->manualtransactions_model->get_transactions_in();
		$data['title'] = "Manual Transaction (IN)";
		$data['page'] = 'transaction_in';
		$this->load->view('template',$data);   
	} // transactions_in	      
    
	   
} // Manualtransactions