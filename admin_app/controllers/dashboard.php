<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	 function __construct() {
    parent::__construct();
	 if ($this->session->userdata('logged_in')!=TRUE) {
       redirect('signin');
      }  
    
  }
	public function index()
	{
		
		$this->load->view('common/header');
		$this->load->view('common/nav');
		$this->load->view('dashboard_view');
		$this->load->view('common/footer');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */