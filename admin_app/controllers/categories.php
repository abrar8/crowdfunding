<style type="text/css">
.pagination-blue li.active {
    background-color: red ;
}
</style>    

<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('categories_model');
	$this->load->helper('form');
    //$this->lang->load('en_admin', 'english'); 
    $this->load->library('form_validation');
	$this->load->library('session');
	$this->load->library('pagination');
  }

  public function listing() {
        //$data['query'] = $this->categories_model->get_all_cat();
		
///////////   Paginaton Start Here     ////////////

		$config = array();
        $config["base_url"] = base_url() . "categories/listing";
		
		$total_row = $this->categories_model->record_count();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->categories_model->
            fetch_countries($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		
//////////   Paginaton End Here     ////////////	

		$this->load->view('common/header', $data);
		$this->load->view('common/nav', $data);
		$this->load->view("categories_management", $data);
		$this->load->view('common/footer',$data);		
     }
	
  public function addcategory(){
	    $this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('categories_add');
        $this->load->view('common/footer');   
	  }	
  public function add_cat_value(){
		$this->form_validation->set_rules('cat_name', 'category name', 'required');
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('categories_add');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->categories_model->add_cat_val();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'categories/addcategory');
			   }
	  }	  
	 
  public function category_edit(){
	  $cat_id = $this->uri->segment(3);
	  if(empty($cat_id)){
		  show_404();
		}
		
	  $data['query'] = $this->categories_model->edit_cat_val($cat_id);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('categories_edit',$data);
      $this->load->view('common/footer',$data);
	  }	
	
  public function edit_cat_value(){
	  $this->form_validation->set_rules('cat_name', 'category name', 'required');
	  $cat_id = $this->input->post('cat_id');
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'categories/category_edit/'.$cat_id);
			$this->load->view('common/footer');  
			}
		    else{
			   $this->categories_model->edit_cat_form_val();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'categories/listing');
			   }
	  }	
  
   public function category_delete(){
	   $cat_id = $this->uri->segment(3);
	   $this->categories_model->del_cat_val($cat_id);
	   
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'categories/listing');
	   }
	       
}
