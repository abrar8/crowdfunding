<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('payment_model');
	$this->load->helper('form');
    //$this->lang->load('en_admin', 'english'); 
    $this->load->library('form_validation');
	$this->load->library('session');
	$this->load->library('pagination');
	
  }

  public function payment_management() {
	  //  $data['query'] = $this->payment_model->get_all_payment();
	  ///////////   Paginaton Start Here     ////////////

		$config = array();
        $config["base_url"] = base_url() . "categories/listing";
		
		$total_row = $this->payment_model->record_count();
		$config["total_rows"] = $total_row; 
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->payment_model->
            fetch_countries($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
		
//////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('payments_management',$data);
        $this->load->view('common/footer',$data);   
    }

  public function paymentadd(){
		$data['users'] = $this->db->get("certification_users")->result_array();
	    $this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('payment_add',$data);
        $this->load->view('common/footer');   
	  }	
	  
 public function add_payment_val(){
	    $this->form_validation->set_rules('certification_users_user_id', 'user name', 'trim|required');
	    $this->form_validation->set_rules('payment_amount', 'payment amount', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('payment_status', 'payment status', 'trim|required');
		$this->form_validation->set_rules('payment_log', 'payment log', 'trim|required');
		
		if($this->form_validation->run()=== false){
			
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'payment/paymentadd');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->payment_model->add_payment();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'payment/paymentadd');
			  }
	 }  
	   
  public function payment_edit(){
	  $payment_id = $this->uri->segment(3);
	  $data['users'] = $this->db->get("certification_users")->result_array();
	  
	  $data['query'] = $this->payment_model->edit_paymnet_val($payment_id);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('payment_edit',$data);
      $this->load->view('common/footer',$data);
	  }	   
	  
  public function update_payment(){
	    $payment_id = $this->input->post('payment_id');
	    $this->form_validation->set_rules('certification_users_user_id', 'user name', 'trim|required');
	    $this->form_validation->set_rules('payment_amount', 'payment amount', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('payment_status', 'payment status', 'trim|required');
		$this->form_validation->set_rules('payment_log', 'payment log', 'trim|required');
		
		if($this->form_validation->run()=== false){
			
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'payment/payment_edit/'.$payment_id);
			$this->load->view('common/footer');  
			}
		    else{
			   $this->payment_model->update_payment();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'payment/payment_management');
			  }
	  }	  
	
   public function payment_delete(){
	   $payment_id = $this->uri->segment(3);
	   $this->payment_model->del_payment($payment_id); 
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'payment/payment_management');
	   }	  
	  
 }
