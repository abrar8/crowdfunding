<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class newTransactions extends CI_Controller {

    
    function __construct() {
        parent::__construct();
        if ($this->session->userdata('logged_in') != true) {
            redirect('signin');
        }
        $this->load->model('transactions_model');
        
    }

    /*

     * this is method show All Transactions which ever done
     * 
     */

    public function view($arg) {

        if($this->input->post()){
            $default_perPage    =   $this->input->post('recordPerpage');
        }else if($this->uri->segment(4)){
            $default_perPage    =   $this->uri->segment(4);
        }
        else{
            $default_perPage    =   20;
        }
        
        $data['js'][]           =   'transactions';
        $data['page_title']     =   ucwords(str_replace('-', ' ', $arg));
        $data['breadcrum']      =   array(
            'dashboard' =>  base_url('dashboard'),
            'New Transactions' =>  base_url('newTransactions/view/all'),
            $data['page_title'] =>  'active'
        );
        
        
        ///////////   Paginaton Start Here     ////////////
        
        $this->load->library('pagination');
        $config = array();
        $config["base_url"] = base_url() . "newTransactions/view/".$arg;     
        
        if($arg=='all' || $arg=='credit-card' || $arg=='scratch-card' || $arg=='bank-transfer' || $arg=='pin-transfer' || $arg=='cash-transfer' || $arg=='daily-contributions' || $arg=='group-contributions' || $arg=='charity-contributions' || $arg=='market-items'){
            $total_row = $this->transactions_model->count_transactions_users($arg);            
        }
        
        $config["total_rows"]    = $total_row;
        $config["per_page"]      = $default_perPage;
        $config['num_links']     = $total_row;
        $config['cur_tag_open']  = '&nbsp;<a class="active">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link']     = 'Next';
        $config['prev_link']     = 'Previous';
        $config['uri_segment']   = 4;
        $this->pagination->initialize($config); 
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        if($arg=='all' || $arg=='credit-card' || $arg=='scratch-card' || $arg=='bank-transfer' || $arg=='pin-transfer' || $arg=='cash-transfer' || $arg=='daily-contributions' || $arg=='group-contributions' || $arg=='charity-contributions' || $arg=='market-items'){
            $data["results"] = $this->transactions_model->get_transactions_users($arg,$config["per_page"], $page);
        }
        $data["links"] = $this->pagination->create_links();
        
        
        $data['total_row']  =   $total_row;
        $data['to']         =   (($this->uri->segment(4)) ? $this->uri->segment(4) : 0)+1;
        $data['from']       =   ($config["per_page"]+$this->uri->segment(4)>$total_row)?$total_row:$config["per_page"]+$this->uri->segment(4);
        
        $data['current_url']=   $config["base_url"];
        $data['per_page']   =   $config['per_page'];  
        $data['recordPerpage'] = array(
            5    => 5,
            10   => 10,
            15   => 15,
            20   => 20,
            1000 => 'all'
        );
        //echo "<pre>"; print_r($data); exit;
        $this->load->view('common/header');
	    $this->load->view('common/nav');
        $this->load->view('newTransactions_view',$data);
        $this->load->view('common/footer');
        
    }

}
