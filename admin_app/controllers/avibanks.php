<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Avibanks extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('Avibanks_model'); 
    $this->load->library('form_validation');
	}

  public function banks_veiw() { 
   // ini_set('display_errors',1);
	//ini_set('display_startup_errors',1);
	//error_reporting(-1);
  
  	 ///////////   Paginaton Start Here     ////////////
		$config = array();
        $config["base_url"] = base_url() . "avibanks/banks_veiw";
		$total_row = $this->Avibanks_model->count_bank_view();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Avibanks_model->fetch_view_bank($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

      //////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('banks_veiw',$data);
        $this->load->view('common/footer',$data);   
    }

  public function banks_veiw_eidt() { 
  	 ///////////   Paginaton Start Here     ////////////
		$config = array();
        $config["base_url"] = base_url() . "avibanks/banks_veiw_eidt";
		$total_row = $this->Avibanks_model->count_bank_view_edit();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->Avibanks_model->fetch_view_bank_edit($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
 
      //////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('banks_veiw_eidt',$data);
        $this->load->view('common/footer',$data);   
    }	
	
	
  public function banks_add(){
	  	
		//$data['categories'] = $this->db->get("certification_category")->result_array();
	  
	    $this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('banks_add');
        $this->load->view('common/footer');   
	  }	
  public function add_bank_value(){
		$this->form_validation->set_rules('bankName', 'bank vame', 'required');
		$this->form_validation->set_rules('displayName', 'display name', 'required');
		$this->form_validation->set_rules('available', 'available', 'required');
				
		if($this->form_validation->run()=== false){
		
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('banks_add');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->Avibanks_model->add_abibanks();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'avibanks/banks_add');
			   }
	  }	  
	 
  public function avibanks_edit(){
	  $bankId = $this->uri->segment(3);
	  $data['query'] = $this->Avibanks_model->avibanks_edit($bankId);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('banks_edit',$data);
      $this->load->view('common/footer',$data);
	  }	
	
  public function update_avibanks(){
		$this->form_validation->set_rules('bankName', 'bank name', 'required');
		$this->form_validation->set_rules('displayName', 'display name', 'required');
		$this->form_validation->set_rules('available', 'available', 'required');
	    $bankId = $this->input->post('bankId');
	 
		if($this->form_validation->run()=== false){
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'avibanks/avibanks_edit/'.$bankId);
			$this->load->view('common/footer');  
			}
		    else{
			   $this->Avibanks_model->update_avibanks_val();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'avibanks/banks_veiw_eidt');
			   }
	  }	
  
   public function avibanks_delete(){
	   $bankId = $this->uri->segment(3);
	   $this->Avibanks_model->del_avibanks_val($bankId);
	   
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'avibanks/banks_veiw_eidt');
	   }
	   

 }
