<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {
  function __construct() {
    parent::__construct();
	 if($this->session->userdata('logged_in') != true){
	 	redirect('signin');
	 }
	$this->load->model('pages_model');
	$this->load->helper('form');
    $this->load->library('form_validation');
	$this->load->library('session');
	$this->load->library('pagination');
  }

  public function pages_management() {
   
	 ///////////   Paginaton Start Here     ////////////
		
		$config = array();
        $config["base_url"] = base_url() . "users/users_management";
		$total_row = $this->pages_model->record_count();
		$config["total_rows"] = $total_row;
		$config["per_page"] = 10;
		$config['num_links'] = $total_row;
		$config['cur_tag_open'] = '&nbsp;<a class="active">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = 'Next';
		$config['prev_link'] = 'Previous';		
 
        $this->pagination->initialize($config);
 
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->pages_model->fetch_countries($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
      //////////   Paginaton End Here     ////////////	

        $this->load->view('common/header',$data);
		$this->load->view('common/nav',$data);
        $this->load->view('pages_management',$data);
        $this->load->view('common/footer',$data);   
    }
		
	
  public function pages_add(){
	  	
		//$data['categories'] = $this->db->get("certification_category")->result_array();
	  
	    $this->load->view('common/header');
		$this->load->view('common/nav');
        $this->load->view('pages_add');
        $this->load->view('common/footer');   
	  }	
  public function add_page_val(){
	  
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('sub_title', 'sub title', 'required');
		$this->form_validation->set_rules('description', 'description', 'required');
		$this->form_validation->set_rules('show_on_menu', 'show on menu', 'required');
			
		if($this->form_validation->run()=== false){
			
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->load->view('pages_add');
			$this->load->view('common/footer');  
			}
		    else{
			   $this->pages_model->add_pages();
			   $this->session->set_flashdata('message', 'Record Inserted Successfuly');
				  redirect(base_url().'pages/pages_add');
			   }
	  }	  
	 
  public function pages_edit(){
	  $id = $this->uri->segment(3);
	  $data['query'] = $this->pages_model->page_edit($id);
	  $this->load->view('common/header',$data);
	  $this->load->view('common/nav',$data);
      $this->load->view('pages_edit',$data);
      $this->load->view('common/footer',$data);
	  }	
	
  public function update_pages(){
	    $id = $this->input->post('id');
		$this->form_validation->set_rules('title', 'title', 'required');
		$this->form_validation->set_rules('sub_title', 'sub title', 'required');
		$this->form_validation->set_rules('description', 'description', 'required');
		$this->form_validation->set_rules('show_on_menu', 'show on menu', 'required');
	 
		if($this->form_validation->run()=== false){
						
			$this->load->view('common/header');
			$this->load->view('common/nav');
			$this->session->set_flashdata('errors', validation_errors());
			redirect(base_url().'pages/pages_edit/'.$id);
			$this->load->view('common/footer');  
			}

		    else{
			   $this->pages_model->updat_page_val();
			   $this->session->set_flashdata('message', 'Record Updated Successfuly');
				  redirect(base_url().'pages/pages_management');
			   }
	  }	
  
   public function pages_delete(){
	   $id = $this->uri->segment(3);
	   $this->pages_model->del_pages_val($id);
	   
	   $this->session->set_flashdata('message', 'Record Deleted Successfuly');
	      redirect(base_url().'pages/pages_management');
	   }
	 
	     
 }
