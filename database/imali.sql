-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 13, 2016 at 07:04 PM
-- Server version: 5.5.46
-- PHP Version: 5.6.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imali`
--

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `bank_name`, `country_id`) VALUES
(1, 'Amarillo\\\'s National Bank', 226),
(2, 'American Savings Bank California', 226),
(3, 'American Savings Bank Hawaii', 226),
(4, 'AmSouth Bank', 226),
(5, 'Apollo Trust Company', 226),
(6, 'Bank of America', 226),
(7, 'Bank of Elk River', 226),
(8, 'Bank of Galveston, N.A', 226),
(9, 'Bank of Petaluma', 226),
(10, 'Bank of Stockton', 226),
(11, 'Bank of the Commonwealth', 226),
(12, 'Bank One', 226),
(13, 'Bank United', 226),
(14, 'Bankers Trust', 226),
(15, 'BankFirst', 226),
(16, 'Black River Country Bank', 226),
(17, 'Brown Brothers Harriman & Co', 226),
(19, 'California Credit Union League', 226),
(20, 'California Federal Bank', 226),
(21, 'Capital One Bank', 226),
(22, 'Carlsbad National Bank', 226),
(23, 'Central Bank', 226),
(25, 'Centura Banks', 226),
(26, 'Chartway Federal Credit Union', 226),
(27, 'Chase Manhattan Corporation', 226),
(28, 'CitiCorp/CitiBank', 226),
(29, 'Citizens Bank (Corpus Christi)', 226),
(30, 'Citizens Bank Online', 226),
(31, 'Citizens Federal Bank', 226),
(32, 'City National Bank of Taylor', 226),
(33, 'Colonial Bank', 226),
(34, 'Colorado Credit Union Center', 226),
(35, 'Compass Bank', 226),
(36, 'Country Club Bank of Kansas City', 226),
(37, 'Credit Union Page', 226),
(38, 'Crown Bank', 226),
(39, 'Dime Savings Bank of Williamsburgh', 226),
(41, 'Enterprise National Bank of Sarasota', 226),
(42, 'Farmers Bank & Trust Co.', 226),
(43, 'Federal Reserve Bank of Minneapolis', 226),
(44, 'Federal Reserve Bank of St. Louis', 226),
(45, 'Federal Reserve Bank of Philadelphia', 226),
(46, 'Fifth Third Bank', 226),
(47, 'First American Bank', 226),
(48, 'First Capital Bank', 226),
(49, 'First Citizens Bank', 226),
(50, 'First Commercial Bank', 226),
(51, 'First Farmers & Merchants', 226),
(52, 'First Federal Savings Bank of Indiana', 226),
(53, 'First Federal S&L of Rochester', 226),
(54, 'First Guaranty Bank of Florida', 226),
(55, 'First Hawaiian Bank', 226),
(56, 'First Interstate', 226),
(57, 'First Line Direct', 226),
(58, 'First Michigan Bank', 226),
(60, 'First Security Bank', 226),
(61, 'First Source Bank', 226),
(62, 'First State Bank of California', 226),
(64, 'First Union Corporation', 226),
(65, 'First United', 226),
(66, 'First USA Bank', 226),
(67, 'FloridaFirst Bank', 226),
(68, 'Franklin Bank', 226),
(69, 'Friedman, Billings, Ramsey & Co. Inc.', 226),
(70, 'Georgia State Bank', 226),
(71, 'Glacier Bank', 226),
(72, 'Glendale Federal Bank', 226),
(73, 'Grand Bank', 226),
(75, 'Gulf Coast Bank & Trust Co.', 226),
(76, 'Heritage Bank of Commerce', 226),
(77, 'Hudson National Bank', 226),
(78, 'Humboldt Bank', 226),
(79, 'Huntington Bancshares Inc.', 226),
(81, 'Inter-American Development Bank', 226),
(82, 'J.P. Morgan', 226),
(83, 'J.P. Morgan RiskMetrics(tm)', 226),
(84, 'Kaw Valley State Bank', 226),
(85, 'Kingfield Bank', 226),
(86, 'La Jolla Bank', 226),
(87, 'Lone Star Bank', 226),
(88, 'Marine National Bank', 226),
(89, 'Mark Twain Bank', 226),
(90, 'Mercantile Bank Lawrence', 226),
(91, 'Metropolitan Bank for Savings', 226),
(92, 'Mellon Bank', 226),
(93, 'Merchants National Bank', 226),
(94, 'Meridian Bank', 226),
(95, 'Mid-States Corporate Federal Credit Union', 226),
(96, 'Nantucket Bank', 226),
(97, 'National Bank of Blacksburg', 226),
(98, 'Naval Air Federal Credit Union', 226),
(99, 'Nevada First Holdings, Inc.', 226),
(100, 'Northrim Bank', 226),
(101, 'NORWEST Corporation', 226),
(102, 'Ohio Credit Union League', 226),
(103, 'Omni Banks', 226),
(104, 'Owensboro National Bank', 226),
(105, 'Oxford Bank', 226),
(106, 'Pacific Continental Bank', 226),
(107, 'Patelco Credit Union', 226),
(108, 'Patriot National Bank', 226),
(109, 'People\\\'s Bank', 226),
(110, 'Perpetual Savings Bank', 226),
(111, 'Premier Bank', 226),
(112, 'Presidential Savings Bank', 226),
(113, 'Provident Bank', 226),
(114, 'Quincy State Bank', 226),
(115, 'Republic Bank and Trust Company', 226),
(116, 'Salem Five Cents Savings Bank', 226),
(117, 'Savings Bank of Rockville', 226),
(118, 'Seafirst Bank', 226),
(119, 'Six Rivers National Bank', 226),
(120, 'Stanford Federal Credit Union', 226),
(121, 'Star Bank', 226),
(122, 'Sterling Payot Company', 226),
(123, 'Stillwater National Bank\\\'s E-Bank', 226),
(124, 'Treasury Bank', 226),
(125, 'UMB Bank', 226),
(126, 'University Federal Credit Union', 226),
(127, 'U.S. Bank', 226),
(128, 'USTrust of Boston', 226),
(129, 'Vermont National Bank', 226),
(131, 'West Suburban Bank', 226),
(132, 'Wilton Bank (the)', 226),
(133, 'Winona National and Savings Bank', 226),
(134, 'Wells Fargo Bank', 226),
(135, 'Hnh', 2),
(136, 'dasd', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bank_branches`
--

CREATE TABLE `bank_branches` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `branch_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `base_currency`
--

CREATE TABLE `base_currency` (
  `id` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `base_currency`
--

INSERT INTO `base_currency` (`id`, `currency_id`) VALUES
(1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) NOT NULL,
  `iso` char(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `printable_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `iso3` char(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `printable_name`, `iso3`, `numcode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152),
(44, 'CN', 'CHINA', 'China', 'CHN', 156),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352),
(99, 'IN', 'INDIA', 'India', 'IND', 356),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600),
(168, 'PE', 'PERU', 'Peru', 'PER', 604),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716);

-- --------------------------------------------------------

--
-- Table structure for table `manual_transactions`
--

CREATE TABLE `manual_transactions` (
  `id` bigint(20) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `type` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `secret_code` varchar(100) DEFAULT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manual_transactions`
--

INSERT INTO `manual_transactions` (`id`, `fullname`, `email`, `phone`, `address`, `type`, `uid`, `amount`, `secret_code`, `datetime`) VALUES
(1, 'Faisal', 'faisal@devdesks.com', '090078601', 'Islamabad', 2, 143, '10', '123456', '2016-05-16 16:44:33'),
(6, 'Ali', 'senderemail@gmail.com', '090078625', 'UK', 1, 143, '213', '', '2016-05-16 17:41:26'),
(7, 'Sultan Ali', 'sultan@devdesks.com', '3335949626', '11111', 1, 135, '100', '', '2016-05-16 17:42:27'),
(8, 'Naveed', 'sultan@devdesks.com', '3335949626', '11111', 1, 135, '100', '', '2016-05-16 17:45:47'),
(9, 'Ali', 'Ali@yahoo.com', '029564', 'Rwp', 2, 134, '20', '256', '2016-05-18 11:22:22'),
(10, 'nm', 'abc@abc.com', '123', 'test', 2, 145, '10', '1234', '2016-05-18 15:35:32'),
(11, 'umer afzal', 'umer@devdesks.com', '03335007726', 'Islamabad', 2, 145, '100', '1235', '2016-05-18 16:46:49'),
(12, 'nauman', 'nouman.malik@devdesks.com', '123', 'txt', 2, 145, '12', '134', '2016-05-23 13:27:27'),
(13, 'frauw', 'frauw@gmail.com', '76555688999', 'Rwp', 2, 145, '100', '301', '2016-06-07 09:06:12');

-- --------------------------------------------------------

--
-- Table structure for table `tblAccountsPayable`
--

CREATE TABLE `tblAccountsPayable` (
  `id` bigint(20) NOT NULL,
  `accountTitle` varchar(255) DEFAULT NULL,
  `accountNumber` varchar(255) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblAccountsPayable`
--

INSERT INTO `tblAccountsPayable` (`id`, `accountTitle`, `accountNumber`, `available`, `status`) VALUES
(1, 'Accounts Payable', '00123456789', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblAdmins`
--

CREATE TABLE `tblAdmins` (
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `admin_fname` varchar(45) DEFAULT NULL,
  `admin_lname` varchar(45) DEFAULT NULL,
  `admin_username` varchar(255) NOT NULL,
  `admin_email` varchar(45) DEFAULT NULL,
  `admin_hash` text,
  `admin_level` int(10) UNSIGNED DEFAULT NULL,
  `role_id` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblAdmins`
--

INSERT INTO `tblAdmins` (`admin_id`, `admin_fname`, `admin_lname`, `admin_username`, `admin_email`, `admin_hash`, `admin_level`, `role_id`) VALUES
(1, 'admin', 'admin', 'admin', 'admin@gmail.com', 'f865b53623b121fd34ee5426c792e5c33af8c227', 1, 1),
(3, 'arshad', 'khan', 'arshad', 'arshad@devdeskc.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 1),
(5, 'test', 'test', 'test', 'admin', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 4),
(7, 'Muhammad Adas Sabir', 'Sabir', 'adas', 'adassabir@gmail.com', '43a6b7600043785afcce0f826df03c562f169b46', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblAgents`
--

CREATE TABLE `tblAgents` (
  `userId` bigint(20) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `fb_id` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `userPIN` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `api_key` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblAgents`
--

INSERT INTO `tblAgents` (`userId`, `firstName`, `lastName`, `fb_id`, `phoneNumber`, `address`, `city`, `country`, `userPIN`, `email`, `userPassword`, `available`, `api_key`, `user_type`, `currency`, `agent_id`) VALUES
(122, 'Ugochukwu', 'Mbanugo', '', '9732890544', '33 Bryant ave', 'Bloomfield', 'Usa', '6327186', 'saosimeon@gmail.com', '5c6d9f025873ec66d3401dfbdb78d9f009ecd306', 1, 'e0e32e60531cfedd15f229a9ac0bb265', 2, 'NGN', NULL),
(123, 'Ugochukwu', 'Mbanugo', '', '9732078142', '33 Bryant', 'Bloomfield', 'Usa', '5411214', 'saosimeongroupllc@gmail.com', '995f4b9052d1b243aa970ff748c313834a3c43bf', 1, 'ccd91d4f5f6f7d3bd6426b18bab8ce04', 2, 'USD', NULL),
(124, 'Obiora', 'Okoye', '', '08030998180', '8551949', 'Lagos', 'Nigeria', '8551949', 'okoyechukwulozie@yahoo.com', '4e1f8d31d0a3e99d2dc21fd76037ff3d429a1401', 1, 'd7bb9e5b177ed15239452659851e64ab', 2, 'NGN', NULL),
(125, 'Khurram', 'Shehzad', '228635874173371', '03438541418', 'F-5 Islamabad', 'Islamabad', 'Pakistan', '1829971', 'Khurram@devdesks.com', '61a951ad7e1194d907fc84f558e04df1ca6c774f', 1, 'e1bc56a5b57aa8924d25b840bb422d6f', 2, 'USD', NULL),
(126, 'Ugochukwu', 'Ifebi', '', '2348033941713', 'No 76 Randle Avenue Surulere', 'Lagos', 'Nigeria', '9246269', 'ugosucceed@gmail.com', 'd892229a4b224bd077941152ae8f73836f5066fc', 1, '258f1bc28e13c96b99d32564032ee6f6', 2, 'USD', NULL),
(129, 'Ugo', 'Mbanugo', '', '9732890544', '33 Bryant Ave', 'Bloomfield', 'Usa', '5179118', 'e.osaji@yahoo.com', '381631d09634d0993e8ec6deecc820ad5707ce14', 1, '1d81f7c0a7bcf5c13356fe410f64a81d', 2, 'USD', NULL),
(130, 'Test', 'Testing', '', '2188463494494', NULL, NULL, NULL, '8383844', 'test@mailinator.com', '5e443c003151351b220521f436b12a2ccdd82f6e', 1, 'd2047d8357afdaf9402682965a0a8410', 2, 'USD', NULL),
(131, 'Jim', 'Honk', '', '6649181464', 'Address', 'City', 'Pakistan', '8386844', 'uvslicad@sharklasers.com', 'e2aba1aa60461778182481103430b4420bedb62a', 1, 'd4fb6633ca48a4fc81f12a47d85cfe25', 2, 'USD', NULL),
(132, 'Osita', 'Nwankwo', '', '9088843069', '1588 Edmund Terrance', 'Union', 'Usa', '1763684', 'nwankwo908@gmail.com', 'b5e96d31c0fa692e1db3e69eb2815ef873b93e8d', 1, 'd48601fadf10ec0bc11e3ab7d9601970', 2, 'USD', NULL),
(135, 'Sultan', 'Shaukat', '1117287984990306', '3335943626', 'G.13', 'Islamabad', 'Pakistan', '7444789', 'sultan@devdesks.com', 'c2c64287ee45fbb2727ec36b47256f77f17d977f', 1, '3c8ae35923068d8bcaffae3b64e12a87', 2, 'USD', NULL),
(136, 'Hina', 'Kiran', '', '84353569501', NULL, NULL, NULL, '2385959', 'hina@devdesks.com', '9531158d87c3f8269e8e181b9f337b634e6ed5af', 1, '3529ad6d6a4f7124557bd827ece5653d', 2, 'USD', NULL),
(137, 'lee', 'brown', '', '065319450', 'street 123', 'Islamabad', 'Pakistan', '9943795', 'lee.brown32145@gmail.com', '5abf5b53c561c1d8ffb28c08fa318dbf1ed10982', 1, '261ddfdcb87295fa44ccdec623cb289a', 2, 'USD', NULL),
(138, 'Osita', 'Nwankwo', '', '9088843069', NULL, NULL, NULL, '2411455', 'nhbmediagroup@gmail.com', 'db1e8b83ded5fd9e592f8e338f3e255c63cbeccf', 1, 'd6a4d595e820dcf4974f9c8d1d08f688', 2, 'USD', NULL),
(139, 'Osita', 'Nwankwo', '', '9088843069', NULL, NULL, NULL, '9168579', 'otbpictures12@gmail.com', 'eba45513a56ab4ec0f1c958995fa4a5cca29b8bc', 1, '381b3c9ee6e31fc94630e0366af7332e', 2, 'USD', NULL),
(140, 'Osita', 'Nwankwo', '', '9088843069', NULL, NULL, NULL, '8983595', 'nwankwo908@hotmail.com', '02eb9d91dbc3522d06fbb5130335da51479ba908', 1, 'bc2c752a7fe8649dd36b53ad39898bfc', 2, 'USD', NULL),
(141, 'Adnan', 'Haider', '485242458328267', '046535356', 'Islamabad', 'Islamabad', 'Pakistan', '9199681', 'adnan@devdesks.com', '1a33779d57abe280e7d0e9e8f7b60f5c8488411e', 1, '337ec3008b41cc32d038d80357fcfff3', 2, 'PKR', NULL),
(142, 'Sultan', 'Ali', '', '03335949626', 'G13', 'Islamabad', 'Pakistan', '1177942', 'alisultan482@gmail.com', '792eed93e471be02b2efbef737e7c795c655a564', 1, '783b7dec7fc67c3cdcba1c20c9a0f28b', 2, 'NGN', NULL),
(143, 'Naveed', 'Arshad', '123456789', '090078601', 'sggf', 'dghh', 'Pakistan', '9435946', 'naveed@devdesks.com', '2655db9710e289dd73f320da554ba0229c23d9e9', 1, 'e95544476f2d71210e1e0f02a647f920', 2, 'USD', NULL),
(144, 'dev', 'adnan', '', '923454825847', NULL, NULL, NULL, '4741277', 'adi908242@gmail.com', '827ac818d422f57b93682a8478afa2d4f026513d', 1, '825399b89a56f4532957c8b8f6ea1da1', 2, 'USD', NULL),
(145, 'Nauman', 'Malik', '227396064290683', '03335007726', 'F 5/1', 'Islamabad', 'Pakistan', '6489288', 'nouman.malik@devdesks.com', '0afa8337da176610d3c6b9962c32f6454084bc08', 1, 'c263f161f93bdcaf3031d982ffd3bef2', 2, 'USD', NULL),
(146, 'Umer', 'Afzal', '291240197874278', '03335007726', 'F5 Islamabad', 'Islamabad', 'Pakistan', '4444389', 'umer@devdesks.con', '4f4b185423001397274200c83bf6189157174ba1', 1, 'd1401af61c46134d3a54d6f7d8ca45c8', 2, 'USD', NULL),
(147, 'Danail', 'Zafar', '291240197874278', '03335007726', 'F5 devdesks tech', 'Islamabad', 'Pakistan', '1873369', 'danial@devdesks.com', 'ea4730ea72c5cd33517ca70f4d84e1fed3c5af9c', 1, 'fa9d958f9d88693f5f3745aca56fd3ba', 2, 'USD', NULL),
(148, 'Umar', 'Devdesks', '', '034352154879', NULL, NULL, NULL, '1658158', 'umer@devdesks.com', 'e81de1a5939d1f8df32f06d1e5357fff601096b4', 1, 'ff74766c029784114bebed7e8b243e71', 2, 'USD', NULL),
(149, 'Sultan', 'Ali', '', '3335949626', NULL, NULL, NULL, '7716846', 'Mudasir.nazir@devdesks.com', '364f5a1d50a658ff06ade29daf12c25c21de114d', 1, '0763659832e4cc4bd6bb3a0649b63cc7', 2, 'USD', NULL),
(150, 'Sultan', 'Ali', '', '03335949626', 'G13', 'Islamabad', 'Pakistan', '7266222', 'Mudasir@devdesks.com', 'ae504127be4fdbd5a07adb43505b306ec4f867dc', 1, 'e2f970ab2322d92fb36d9375a2d015e6', 0, 'USD', NULL),
(151, 'Obi', 'Abuchi', '', '4136246626', NULL, NULL, NULL, '9364324', 'hmbadugha@gmail.com', '9c2e79b57d0c4d70045d080062aa0918524ac0f7', 1, 'a859a2af66a1be7a8b7f87dfda13789a', 0, NULL, NULL),
(152, 'Obi', 'Agent', '', '9083378251', '123 main streey', 'Hoboken', 'USA', '4112559', 'henri084@gmail.com', '6119911aeb69582df95e21c2fb2b082b70efd282', 1, 'ebc3251c4379ee1e7134ba94ad1a0b14', 0, 'USD', NULL),
(153, 'Ahsan', 'Ali', '', '3335949626', NULL, NULL, NULL, '8356129', 'ahsan@devdesks.com', 'e9f8027c6d1ac0f16703285b89215342657016a9', 1, 'd8b1ade921d45aff9d4e4957af50e786', 0, NULL, NULL),
(154, 'D', 'D', '', '55', NULL, NULL, NULL, '7363752', 'ddddd@gmail.com', '54a8331f9d80efa2e72b8779c304f61b135ddd63', 1, '59472198b73bad3d6b84cc3145f5a2d5', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblBankAccounts`
--

CREATE TABLE `tblBankAccounts` (
  `bankAccountId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `bankId` bigint(20) NOT NULL,
  `branchName` varchar(255) DEFAULT NULL,
  `accountTitle` varchar(255) DEFAULT NULL,
  `accountNumber` varchar(255) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblBankAccounts`
--

INSERT INTO `tblBankAccounts` (`bankAccountId`, `userId`, `bankId`, `branchName`, `accountTitle`, `accountNumber`, `available`) VALUES
(7, 122, 2, 'onitsha', 'checking', '909333378', 1),
(14, 5, 7, 'testing', 'test', '123', 1),
(15, 6, 3, 'Abc branch', 'Abc acc title', '123456789', 1),
(16, 7, 2, 'onitsha', 'checking', '90737389', 1),
(17, 123, 3, 'lagos', 'checking', '876540987654', 1),
(18, 124, 12, 'surulere', 'obi', '0001234567', 1),
(19, 125, 3, 'Abc branch', 'Khurram Shehzad', '123456789', 1),
(20, 125, 2, 'Islamabad branch', 'Khurram Shehzad', '123456789852', 1),
(21, 125, 5, 'Abc', 'Def', 'Fhjjjjj', 1),
(22, 125, 6, 'Sdghb', 'Don\'t know', '147258369', 1),
(23, 9, 2, 'Umer Bank Branch', 'Umer Afzal', '123456', 1),
(24, 126, 6, 'Seme', 'Ugochukwu Ifebi', '0064442711', 1),
(25, 129, 2, 'lagos branch', 'checking', '998877728', 1),
(26, 10, 3, 'lagos', 'checking', '901888849200', 1),
(27, 131, 21, 'fahhj', 'Jim jom', '7377372772722', 1),
(28, 11, 3, 'branch', 'title', '6262662262', 1),
(29, 132, 7, 'facebook', 'facebook for you', '828284892020', 1),
(30, 134, 24, 'main', 'testing', '04849499937475.', 1),
(31, 135, 5, 'enterprise bank', 'hhhhjj', '221111111111', 1),
(32, 137, 2, 'bbaje', 'leebrown', '23456778899009', 1),
(33, 132, 3, 'blah', 'blah', '5646899', 1),
(34, 15, 2, 'blah', 'blah', '78866544', 1),
(35, 14, 2, 'test', 'yes', '11111111111111', 1),
(36, 16, 2, 'hdhd', 'MA', '17384895', 1),
(38, 118, 15, 'Islamabad', 'M.Bilal', '987654321', 1),
(39, 118, 17, 'Rawalpindi', 'M Bilal', '12356788', 1),
(40, 142, 2, 'test', 'sultan', '111111111', 1),
(41, 141, 2, 'test', 'adi', '23456', 1),
(42, 143, 3, 'tt', '444455', '346666', 1),
(43, 145, 2, 'Islamabad', 'Nauman Malik', '123456789', 1),
(44, 147, 2, 'Test', 'Dainal Zafar', '1234567890', 1),
(45, 146, 3, 'F5 Islamabad Branch', 'Umer Afzal', '123456789123456789', 1),
(46, 150, 2, 'test', 'sultan', '1111111¹', 1),
(47, 17, 6, 'Ikeja', 'trade account', '1010072409615', 1),
(48, 152, 3, 'ketu', 'imports', '123467899', 1),
(49, 161, 3, 'xfg', 'dghhj', '3467788', 1),
(50, 19, 3, 'isb', 'sultan', '1111111111', 1),
(51, 145, 2, 'Test', 'me', '12345', 1),
(52, 134, 3, 'xyz', 'ty', '6377383899999', 1),
(53, 145, 3, 'rest', 'gh', '123456', 1),
(54, 145, 2, 'tets', 'ee', 'dddff', 1),
(55, 168, 3, 'ggg', 'vghhh', 'ghhhj', 1),
(56, 134, 7, 'vhh', 'hjjjj', '5678999999', 1),
(57, 171, 16, 'test', 'abc', '1234', 1),
(58, 171, 4, 'u', 'u', '123', 1),
(59, 171, 22, 'u', 'u', '12', 1),
(60, 171, 3, 'b', 'b', '1', 1),
(61, 171, 3, 'b', 'b', '1', 1),
(62, 172, 3, 'hn', 'ghh', '124455', 1),
(63, 172, 4, 'fhj', 'vvb', 'ccc', 1),
(64, 172, 4, 'u', 'y', 'h', 1),
(65, 172, 5, 'crge', 'eveg', '24678', 1),
(66, 174, 3, 'gdhhd', 'gdhdh', '563637', 1),
(67, 170, 4, 'jk', '123', '12345', 1),
(68, 176, 12, 'ojuelegba', 'checking', '2627262723', 1),
(69, 20, 4, 'Test', 'Nauman Malik', '123456', 1),
(70, 21, 3, 'lagos', 'checking account', '9087625292', 1),
(71, 22, 2, 'lagos', 'checking', '9076639391', 1),
(72, 177, 4, 'ikorodu', 'checking', '9825371097465', 1),
(73, 23, 3, 'linden', 'checking', '9027374748', 1),
(74, 24, 2, 'tesy', 'tesy', '1111111', 1),
(75, 6, 2, 'abc branch', 'Khurram SHehzqad', '123456789741', 1),
(76, 20, 8, 'test', 'me', '1234', 1),
(77, 20, 19, 'tesre', '1244', '1234', 1),
(78, 20, 4, 'Islamabad', 'test account', '4321', 1),
(79, 20, 7, 'asdf', 'rew', '1234', 1),
(80, 20, 5, 'test', 'tyui', '1234', 1),
(81, 145, 33, 'Test', 'nm', '1235', 1),
(82, 187, 28, 'tesr', '1233', '123456', 1),
(83, 182, 6, 'Palo Alto', 'trade', '1234567890', 1),
(84, 184, 1, 'dnkld', 'dfd', '234', 1),
(85, 184, 4, 'ghfgh', 'fghfg', '5634', 1),
(86, 184, 6, 'tght', 'trhrt', '564', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblBankTransfer`
--

CREATE TABLE `tblBankTransfer` (
  `id` bigint(11) NOT NULL,
  `bank` varchar(50) NOT NULL,
  `branch` varchar(50) NOT NULL,
  `account_no` varchar(50) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `userId` int(11) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblBankTransfer`
--

INSERT INTO `tblBankTransfer` (`id`, `bank`, `branch`, `account_no`, `amount`, `firstname`, `lastname`, `email`, `userId`, `address`) VALUES
(1, '2', 'hsj', '27394959599999', '200', 'lee', 'bro', 'lee.bro@gmail.com', 134, '0'),
(2, '5', 'test123', '123456', '100', 'Marrium', 'Shah', 'marrium@devdesks.com', 134, '0'),
(3, '6', 'abc', '374858599', '10', 'Mairee', 'Shah', 'mairee@gmail.com', 134, '0'),
(4, '2', 'test', '1111111111111', '100', 'marrium', 'abid', 'twst@gmail.com', 135, '0'),
(5, 'First City Monument Bank Plc', 'gh', '123455667788888', '20', 'vgh', 'ggh', 'vb@yahoo.com', 134, '0'),
(6, '5', 'bnk', '73848488899999', '20', 'be', 'mo', 'bo@yahoo.com', 134, '0'),
(7, '4', 'fg', 'fgg', '50', 'fft', 'fg', 'test@test.com', 134, '0'),
(8, '3', 'test', '11111111', '100', 'sultan', 'ali', 'sultan@devdesks.com', 135, '0');

-- --------------------------------------------------------

--
-- Table structure for table `tblCashInHand`
--

CREATE TABLE `tblCashInHand` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCashInHand`
--

INSERT INTO `tblCashInHand` (`id`, `userId`, `amount`, `status`) VALUES
(8, 122, 1000, 0),
(9, 122, 100, 0),
(12, 123, 10000, 0),
(13, 123, 1000, 0),
(14, 123, 1000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblCashInHandMerchant`
--

CREATE TABLE `tblCashInHandMerchant` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblCashoutPin`
--

CREATE TABLE `tblCashoutPin` (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `transaction_id` bigint(20) NOT NULL,
  `cashout_amount` int(11) NOT NULL,
  `pin` bigint(20) NOT NULL,
  `requestDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCashoutPin`
--

INSERT INTO `tblCashoutPin` (`id`, `userId`, `transaction_id`, `cashout_amount`, `pin`, `requestDate`) VALUES
(11, 125, 476, 10, 11191465239971, '2016-04-07 05:46:15'),
(12, 125, 478, 10, 93820832106284, '2016-04-07 05:46:23'),
(13, 125, 480, 10, 88180204089730, '2016-04-07 05:47:56'),
(14, 125, 484, 100, 36816723090596, '2016-04-14 12:02:56'),
(15, 125, 494, 566, 37275369474664, '2016-04-14 03:12:53'),
(16, 125, 496, 566, 30419095894321, '2016-04-14 03:13:02'),
(17, 125, 498, 100, 55465930299833, '2016-04-14 04:35:02'),
(18, 134, 501, 400, 62190267234109, '2016-04-15 12:48:01'),
(19, 134, 503, 400, 67082427591085, '2016-04-15 12:48:32'),
(20, 134, 649, 200, 82793974378146, '2016-04-28 10:57:47'),
(21, 135, 660, 100, 40942038674838, '2016-04-29 06:07:11'),
(22, 135, 724, 10, 24710690132342, '2016-05-06 04:29:41'),
(23, 131, 897, 100, 76658756323158, '2016-05-14 01:46:09'),
(24, 131, 933, 100, 45430444055236, '2016-05-16 07:19:31'),
(25, 131, 972, 100, 17471872735768, '2016-05-19 03:45:34'),
(26, 131, 974, 100, 33259249930270, '2016-05-19 04:39:02'),
(27, 152, 1119, 200, 40922715202905, '2016-05-28 06:17:57'),
(28, 125, 1121, 6000, 57746371268294, '2016-06-03 11:42:39'),
(29, 125, 1131, 80, 15088608991354, '2016-06-03 05:22:32'),
(30, 180, 1137, 100, 14646104602143, '2016-06-03 07:45:35'),
(31, 182, 1139, 10, 10565624530427, '2016-06-03 07:47:03'),
(32, 145, 1147, 12, 98219402260147, '2016-06-06 02:52:12'),
(33, 145, 1179, 10, 21609439016319, '2016-06-07 10:57:56'),
(34, 145, 1183, 12, 88163216486573, '2016-06-07 11:05:58'),
(35, 145, 1187, 2, 42269983510486, '2016-06-07 11:07:35'),
(36, 145, 1189, 20, 77693313299678, '2016-06-07 12:53:21'),
(37, 145, 1191, 23, 56245121490210, '2016-06-07 12:55:43'),
(38, 145, 1193, 12, 47304133097641, '2016-06-07 01:00:07'),
(39, 145, 1197, 34, 12777446126565, '2016-06-07 01:02:26'),
(40, 200, 1202, 50, 44994549048133, '2016-06-08 10:57:56'),
(41, 145, 1204, 12, 16888349866494, '2016-06-08 10:58:50'),
(42, 200, 1206, 50, 84163732463493, '2016-06-08 10:59:11'),
(43, 184, 1209, 100, 81371466210111, '2016-06-08 11:34:36'),
(44, 184, 1211, 100, 57537808311171, '2016-06-08 11:35:47'),
(45, 184, 1213, 20, 46605216776952, '2016-06-08 11:39:02'),
(46, 184, 1215, 10, 54235839280299, '2016-06-08 11:40:20'),
(47, 184, 1217, 10, 73618978965096, '2016-06-08 11:43:42'),
(48, 184, 1219, 10, 29389280853793, '2016-06-08 11:48:26'),
(49, 184, 1221, 12, 87894455790519, '2016-06-08 11:51:46'),
(50, 184, 1223, 11, 79917304916307, '2016-06-08 11:53:34'),
(51, 184, 1225, 11, 54347177860327, '2016-06-08 11:57:21'),
(52, 184, 1227, 11, 82766508683562, '2016-06-08 11:58:54'),
(53, 184, 1229, 11, 98578266697004, '2016-06-08 12:20:20'),
(54, 184, 1231, 11, 55782233444042, '2016-06-08 12:30:10'),
(55, 184, 1233, 11, 28898043660447, '2016-06-08 12:33:55'),
(56, 184, 1235, 11, 44299227390438, '2016-06-08 12:35:31'),
(57, 184, 1237, 12, 71234128484502, '2016-06-08 12:35:56'),
(58, 184, 1239, 11, 68712062980048, '2016-06-08 12:38:33'),
(59, 184, 1241, 12, 69183978587388, '2016-06-08 12:38:46'),
(60, 184, 1243, 1000, 42710371678695, '2016-06-08 12:51:18'),
(61, 184, 1247, 1000, 82407062710262, '2016-06-08 01:02:14'),
(62, 184, 1249, 200, 92160609308630, '2016-06-08 01:51:14'),
(63, 204, 1266, 12, 17250768984667, '2016-06-08 02:51:22'),
(64, 145, 1268, 12, 25523091261275, '2016-06-08 02:54:37'),
(65, 182, 1270, 5, 89165591523051, '2016-06-09 12:22:48'),
(66, 145, 1274, 10, 56427615010179, '2016-06-09 02:37:48'),
(67, 145, 1276, 10, 76601034514606, '2016-06-09 02:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `tblCashTransfer`
--

CREATE TABLE `tblCashTransfer` (
  `id` bigint(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `secret_code` varchar(50) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCashTransfer`
--

INSERT INTO `tblCashTransfer` (`id`, `userId`, `amount`, `phone`, `secret_code`, `firstname`, `lastname`, `email`, `address`) VALUES
(2, 134, '20', '0356889618', '20', '0', '0', '0', '0'),
(3, 135, '100', '3335949626', '100', '0', '0', '0', '0'),
(4, 135, '100', '3335949626', '100', '0', '0', '0', '0'),
(5, 135, '100', '3335949626', '100', '0', '0', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tblCharges`
--

CREATE TABLE `tblCharges` (
  `charges_id` int(11) NOT NULL,
  `charges_type` varchar(100) NOT NULL,
  `start_amount_range` int(11) NOT NULL,
  `end_amount_range` int(11) NOT NULL,
  `charges` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCharges`
--

INSERT INTO `tblCharges` (`charges_id`, `charges_type`, `start_amount_range`, `end_amount_range`, `charges`) VALUES
(1, 'user cashout', 0, 1000, 10),
(2, 'user cashout', 1001, 2000, 15),
(3, 'user cashout', 2001, 3000, 20),
(4, 'user cashout', 3001, 4000, 25),
(5, 'user cashout', 4001, 5000, 30),
(6, 'user cashout', 5001, 999999999, 35),
(7, 'merchant cashout', 0, 1000, 5),
(8, 'merchant cashout', 1001, 2000, 10),
(9, 'merchant cashout', 2001, 3000, 15),
(10, 'merchant cashout', 3001, 4000, 20),
(11, 'merchant cashout', 4001, 5000, 25),
(12, 'merchant cashout', 5001, 999999999, 30);

-- --------------------------------------------------------

--
-- Table structure for table `tblCharityAccount`
--

CREATE TABLE `tblCharityAccount` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `reference_number` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` text,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCharityAccount`
--

INSERT INTO `tblCharityAccount` (`id`, `userId`, `reference_number`, `title`, `description`, `image`, `status`) VALUES
(1, 143, 'vkv3PRkqrTd6', 'devdesks title', 'devdesks charity', '1409608946.png', 1),
(2, 145, 'b3hegJ2EVpOy', 'one', 'two', '2030047012.png', 1),
(3, 152, 'PPxIl60zCoK5', 'TS4A', 'Africa', '1326240467.png', 1),
(4, 152, 'VIRt7ATqiP3V', 'TS4A', 'Africa', '328011767.png', 1),
(5, 184, 'GtSeMid2Q8Ex', 'ghfgh', 'ghg', '562869483.png', 1),
(6, 184, 'rM9x7tupCQMP', 'gjhg', 'gvbjhgjk', '2123622944.png', 1),
(7, 184, '5sDBYphq8bbY', 'rtrt', 'rtr', '680114267.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblCharityInvitations`
--

CREATE TABLE `tblCharityInvitations` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL COMMENT 'Invitation sender''s ID',
  `user_id` int(11) NOT NULL COMMENT 'user id from tblusers',
  `charity_id` int(11) NOT NULL COMMENT 'charity id from tblcharityaccount',
  `charity_ref_num` varchar(255) NOT NULL COMMENT 'refrence_number from tblcharityaccount',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=pending,2=accepted,3=rejected'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCharityInvitations`
--

INSERT INTO `tblCharityInvitations` (`id`, `sender_id`, `user_id`, `charity_id`, `charity_ref_num`, `status`) VALUES
(1, 143, 125, 1, 'vkv3PRkqrTd6', 1),
(2, 143, 145, 1, 'vkv3PRkqrTd6', 1),
(3, 143, 134, 1, 'vkv3PRkqrTd6', 1),
(4, 125, 143, 2, 'b3hegJ2EVpOy', 1),
(5, 125, 145, 2, 'b3hegJ2EVpOy', 1),
(6, 125, 135, 2, 'b3hegJ2EVpOy', 1),
(7, 125, 134, 2, 'b3hegJ2EVpOy', 1),
(8, 125, 143, 1, 'vkv3PRkqrTd6', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblCrowdfund`
--

CREATE TABLE `tblCrowdfund` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `reference_number` varchar(255) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCrowdfund`
--

INSERT INTO `tblCrowdfund` (`id`, `userId`, `reference_number`, `title`, `description`, `image`, `status`) VALUES
(1, 143, 'QJ2YdC7KPgKJ', 'DevDesks', 'Developer Desks', '1638321072.png', 1),
(2, 145, '7GPxtY8rtwKJ', 'First time', 'test', '261898680.png', 1),
(3, 145, 'XOObZiS1Ued2', 'Second', 'sec', '165902553.png', 1),
(4, 125, 'BajGHniNHktX', 'unique project', 'sdfsdfdsf', '946006743.png', 1),
(5, 145, '5cNlV0gMyH8w', 'tree', 'tree', '2042422100.png', 1),
(6, 125, 'kDhlOEiAt9hB', 'testing crowd fund', 'dsfsdfsdf', '533928324.png', 1),
(7, 145, '7YVggESCKTgC', 'reee', 'ree', '573744518.png', 1),
(8, 135, 'Bfihyk3byLpb', 'sultan project', 'test', '1333615679.png', 1),
(9, 145, 'imTsMlKfVBLQ', 'nauman ipool', 'test', '356042940.png', 1),
(10, 145, '5axXXZVHbFmi', 'nauman ipool', 'test', '1423437652.png', 1),
(11, 145, 'LyVxtkRvULbW', 'nauman second', 'test 2', '114472891.png', 1),
(12, 134, 'uq28LIVE9C0m', 'newonepro', 'jejep', '611533374.png', 1),
(13, 135, '5zt7BsolHS4V', 'test', 'this is a test', '530302311.png', 1),
(14, 145, 'd5eK9aaVdeDW', 'test', 're test', '516049809.png', 1),
(15, 125, 'VPsSC3W5pWxd', ';lkl;l;klkl;', 'ikikj', '289189947.png', 1),
(16, 145, 'zdjEIk8FaEAY', 'devdesks', 'funds', '2061178090.png', 1),
(17, 145, 'om2b8WL9RONS', 'devdesks', 'funds', '1164347247.png', 1),
(18, 182, 'R0nBOMbjmFPn', 'Ogo Netflix', 'for membership payment', '1699748727.png', 1),
(19, 182, '09fvL4WsuQSo', 'Ogo Netflix', 'for membership payment', '7348044.png', 1),
(20, 184, 'HZ5G2yDhK2rC', 'Ramzan', 'djfgbjksd', '1301056849.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblCrowdfundInvitations`
--

CREATE TABLE `tblCrowdfundInvitations` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL COMMENT 'Invitation sender''s ID',
  `user_id` int(11) NOT NULL COMMENT 'user id from tblusers',
  `crowdfund_id` int(11) NOT NULL COMMENT 'charity id from tblcharityaccount',
  `crowdfund_ref_num` varchar(255) NOT NULL COMMENT 'refrence_number from tblcharityaccount',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '1=pending,2=accepted,3=rejected'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblCrowdfundInvitations`
--

INSERT INTO `tblCrowdfundInvitations` (`id`, `sender_id`, `user_id`, `crowdfund_id`, `crowdfund_ref_num`, `status`) VALUES
(36, 125, 143, 4, 'BajGHniNHktX', 1),
(37, 125, 145, 4, 'BajGHniNHktX', 3),
(38, 135, 125, 8, 'Bfihyk3byLpb', 2),
(39, 135, 145, 8, 'Bfihyk3byLpb', 2),
(40, 135, 125, 1, 'QJ2YdC7KPgKJ', 2),
(41, 135, 145, 1, 'QJ2YdC7KPgKJ', 2),
(42, 135, 145, 3, 'XOObZiS1Ued2', 3),
(43, 135, 125, 4, 'BajGHniNHktX', 2),
(44, 135, 125, 6, 'kDhlOEiAt9hB', 2),
(45, 135, 145, 6, 'kDhlOEiAt9hB', 2),
(46, 135, 125, 2, '7GPxtY8rtwKJ', 2),
(47, 135, 145, 2, '7GPxtY8rtwKJ', 3),
(48, 135, 125, 5, '5cNlV0gMyH8w', 3),
(49, 135, 145, 5, '5cNlV0gMyH8w', 3),
(50, 145, 135, 11, 'LyVxtkRvULbW', 1),
(51, 125, 135, 4, 'BajGHniNHktX', 1),
(52, 125, 143, 6, 'kDhlOEiAt9hB', 1),
(53, 125, 135, 6, 'kDhlOEiAt9hB', 1),
(54, 125, 143, 2, '7GPxtY8rtwKJ', 1),
(55, 125, 135, 2, '7GPxtY8rtwKJ', 1),
(56, 125, 145, 11, 'LyVxtkRvULbW', 3),
(57, 125, 134, 11, 'LyVxtkRvULbW', 1),
(58, 125, 143, 9, 'imTsMlKfVBLQ', 1),
(59, 125, 145, 9, 'imTsMlKfVBLQ', 2),
(60, 134, 125, 12, 'uq28LIVE9C0m', 2),
(61, 145, 125, 14, 'd5eK9aaVdeDW', 1),
(62, 125, 143, 7, '7YVggESCKTgC', 1),
(63, 125, 145, 7, '7YVggESCKTgC', 2),
(64, 125, 145, 10, '5axXXZVHbFmi', 2),
(65, 125, 135, 10, '5axXXZVHbFmi', 1),
(66, 125, 143, 13, '5zt7BsolHS4V', 1),
(67, 125, 145, 13, '5zt7BsolHS4V', 2),
(68, 125, 135, 13, '5zt7BsolHS4V', 1),
(69, 125, 134, 13, '5zt7BsolHS4V', 1),
(70, 125, 143, 14, 'd5eK9aaVdeDW', 1),
(71, 125, 145, 14, 'd5eK9aaVdeDW', 2),
(72, 125, 135, 14, 'd5eK9aaVdeDW', 1),
(73, 125, 134, 14, 'd5eK9aaVdeDW', 1),
(74, 125, 143, 12, 'uq28LIVE9C0m', 1),
(75, 125, 145, 12, 'uq28LIVE9C0m', 2),
(76, 125, 135, 12, 'uq28LIVE9C0m', 1),
(77, 125, 134, 12, 'uq28LIVE9C0m', 1),
(78, 125, 143, 5, '5cNlV0gMyH8w', 1),
(79, 125, 135, 5, '5cNlV0gMyH8w', 1),
(80, 125, 134, 5, '5cNlV0gMyH8w', 1),
(81, 125, 143, 3, 'XOObZiS1Ued2', 1),
(82, 125, 135, 3, 'XOObZiS1Ued2', 1),
(83, 125, 134, 3, 'XOObZiS1Ued2', 1),
(84, 125, 134, 2, '7GPxtY8rtwKJ', 1),
(85, 125, 145, 15, 'VPsSC3W5pWxd', 2),
(86, 145, 125, 11, 'LyVxtkRvULbW', 1),
(87, 125, 134, 6, 'kDhlOEiAt9hB', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblDcAccount`
--

CREATE TABLE `tblDcAccount` (
  `DcAccountId` bigint(20) NOT NULL,
  `accountTitle` varchar(255) DEFAULT NULL,
  `accountNumber` varchar(255) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblDcAccount`
--

INSERT INTO `tblDcAccount` (`DcAccountId`, `accountTitle`, `accountNumber`, `available`) VALUES
(1, 'Daily Contributions', '02156789', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblDcSubscriptions`
--

CREATE TABLE `tblDcSubscriptions` (
  `subId` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `subStatus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblDcSubscriptions`
--

INSERT INTO `tblDcSubscriptions` (`subId`, `userId`, `subStatus`) VALUES
(1, 143, 1),
(2, 145, 1),
(3, 134, 1),
(4, 135, 1),
(5, 131, 1),
(6, 125, 1),
(7, 152, 1),
(8, 182, 1),
(9, 188, 1),
(10, 207, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblDepositAccounts`
--

CREATE TABLE `tblDepositAccounts` (
  `depositAccountId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `bankAccountId` bigint(20) NOT NULL,
  `requestDate` datetime NOT NULL,
  `cashout_amount` decimal(10,2) NOT NULL,
  `charges` decimal(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblDepositAccounts`
--

INSERT INTO `tblDepositAccounts` (`depositAccountId`, `userId`, `bankAccountId`, `requestDate`, `cashout_amount`, `charges`, `status`) VALUES
(2, 125, 0, '0000-00-00 00:00:00', '500.00', '10.00', 0),
(3, 125, 147258369, '0000-00-00 00:00:00', '500.00', '10.00', 0),
(4, 125, 0, '0000-00-00 00:00:00', '500.00', '10.00', 0),
(5, 125, 0, '0000-00-00 00:00:00', '500.00', '10.00', 0),
(6, 125, 147258369, '0000-00-00 00:00:00', '100.00', '10.00', 0),
(7, 125, 0, '0000-00-00 00:00:00', '100.00', '10.00', 0),
(8, 125, 147258369, '0000-00-00 00:00:00', '100.00', '10.00', 0),
(23, 125, 0, '2016-04-04 11:06:34', '100.00', '10.00', 0),
(25, 116, 4, '2016-04-06 12:14:56', '1.00', '10.00', 0),
(44, 125, 0, '2016-04-07 12:21:01', '100.00', '0.00', 0),
(45, 125, 0, '2016-04-07 12:21:03', '100.00', '0.00', 0),
(46, 125, 0, '2016-04-07 12:31:28', '10.00', '0.00', 0),
(47, 125, 0, '2016-04-07 12:33:40', '10.00', '0.00', 0),
(48, 125, 0, '2016-04-07 12:33:47', '10.00', '0.00', 0),
(49, 125, 0, '2016-04-07 12:35:36', '10.00', '0.00', 0),
(60, 125, 0, '2016-04-07 05:46:15', '10.00', '0.00', 0),
(61, 125, 0, '2016-04-07 05:46:22', '10.00', '0.00', 0),
(62, 125, 0, '2016-04-07 05:47:56', '10.00', '0.00', 0),
(63, 125, 0, '2016-04-14 12:02:56', '100.00', '0.00', 0),
(64, 125, 0, '2016-04-14 03:12:53', '566.00', '0.00', 0),
(65, 125, 0, '2016-04-14 03:13:02', '566.00', '0.00', 0),
(66, 125, 0, '2016-04-14 04:35:02', '100.00', '0.00', 0),
(67, 134, 0, '2016-04-15 12:48:01', '400.00', '0.00', 0),
(68, 134, 0, '2016-04-15 12:48:32', '400.00', '0.00', 0),
(69, 135, 221111111111, '2016-04-22 07:20:54', '0.00', '10.00', 0),
(70, 134, 0, '2016-04-28 10:57:46', '200.00', '0.00', 0),
(71, 135, 221111111111, '2016-04-29 06:06:50', '100.00', '10.00', 0),
(72, 135, 0, '2016-04-29 06:07:11', '100.00', '0.00', 0),
(73, 135, 221111111111, '2016-04-29 06:07:22', '100.00', '10.00', 0),
(74, 135, 221111111111, '2016-05-02 05:46:16', '10.00', '10.00', 0),
(75, 135, 0, '2016-05-06 04:29:41', '10.00', '0.00', 0),
(76, 135, 221111111111, '2016-05-06 04:30:10', '10.00', '10.00', 0),
(77, 145, 43, '2016-05-13 10:36:52', '12.00', '10.00', 0),
(78, 131, 0, '2016-05-14 01:46:09', '100.00', '0.00', 0),
(79, 131, 0, '2016-05-16 07:19:30', '100.00', '0.00', 0),
(80, 131, 0, '2016-05-19 03:45:34', '100.00', '0.00', 0),
(81, 131, 0, '2016-05-19 04:39:02', '100.00', '0.00', 0),
(82, 152, 0, '2016-05-28 06:17:57', '200.00', '0.00', 0),
(83, 125, 0, '2016-06-03 11:42:39', '6000.00', '0.00', 0),
(84, 125, 0, '2016-06-03 05:22:32', '80.00', '0.00', 0),
(85, 180, 0, '2016-06-03 07:45:35', '100.00', '0.00', 0),
(86, 182, 0, '2016-06-03 07:47:03', '10.00', '0.00', 0),
(87, 145, 0, '2016-06-06 02:52:11', '12.00', '0.00', 0),
(88, 145, 53, '2016-06-07 10:53:59', '1.00', '10.00', 0),
(89, 145, 0, '2016-06-07 10:57:56', '10.00', '0.00', 0),
(90, 145, 53, '2016-06-07 11:05:44', '12.00', '10.00', 0),
(91, 145, 0, '2016-06-07 11:05:58', '12.00', '0.00', 0),
(92, 145, 51, '2016-06-07 11:07:24', '1.00', '10.00', 0),
(93, 145, 0, '2016-06-07 11:07:35', '2.00', '0.00', 0),
(94, 145, 0, '2016-06-07 12:53:21', '20.00', '0.00', 0),
(95, 145, 0, '2016-06-07 12:55:42', '23.00', '0.00', 0),
(96, 145, 0, '2016-06-07 01:00:07', '12.00', '0.00', 0),
(97, 145, 53, '2016-06-07 01:02:09', '12.00', '10.00', 0),
(98, 145, 0, '2016-06-07 01:02:26', '34.00', '0.00', 0),
(99, 200, 0, '2016-06-08 10:57:56', '50.00', '0.00', 0),
(100, 145, 0, '2016-06-08 10:58:49', '12.00', '0.00', 0),
(101, 200, 0, '2016-06-08 10:59:11', '50.00', '0.00', 0),
(102, 184, 0, '2016-06-08 11:34:36', '100.00', '0.00', 0),
(103, 184, 0, '2016-06-08 11:35:47', '100.00', '0.00', 0),
(104, 184, 0, '2016-06-08 11:39:02', '20.00', '0.00', 0),
(105, 184, 0, '2016-06-08 11:40:20', '10.00', '0.00', 0),
(106, 184, 0, '2016-06-08 11:43:41', '10.00', '0.00', 0),
(107, 184, 0, '2016-06-08 11:48:26', '10.00', '0.00', 0),
(108, 184, 0, '2016-06-08 11:51:46', '12.00', '0.00', 0),
(109, 184, 0, '2016-06-08 11:53:34', '11.00', '0.00', 0),
(110, 184, 0, '2016-06-08 11:57:21', '11.00', '0.00', 0),
(111, 184, 0, '2016-06-08 11:58:54', '11.00', '0.00', 0),
(112, 184, 0, '2016-06-08 12:20:20', '11.00', '0.00', 0),
(113, 184, 0, '2016-06-08 12:30:10', '11.00', '0.00', 0),
(114, 184, 0, '2016-06-08 12:33:55', '11.00', '0.00', 0),
(115, 184, 0, '2016-06-08 12:35:31', '11.00', '0.00', 0),
(116, 184, 0, '2016-06-08 12:35:56', '12.00', '0.00', 0),
(117, 184, 0, '2016-06-08 12:38:33', '11.00', '0.00', 0),
(118, 184, 0, '2016-06-08 12:38:46', '12.00', '0.00', 0),
(119, 184, 0, '2016-06-08 12:51:18', '1000.00', '0.00', 0),
(120, 184, 0, '2016-06-08 01:02:14', '1000.00', '0.00', 0),
(121, 184, 0, '2016-06-08 01:51:14', '200.00', '0.00', 0),
(122, 204, 0, '2016-06-08 02:51:21', '12.00', '0.00', 0),
(123, 145, 0, '2016-06-08 02:54:37', '12.00', '0.00', 0),
(124, 182, 0, '2016-06-09 12:22:47', '5.00', '0.00', 0),
(125, 145, 0, '2016-06-09 02:37:48', '10.00', '0.00', 0),
(126, 145, 0, '2016-06-09 02:38:09', '10.00', '0.00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbldepositSlip`
--

CREATE TABLE `tbldepositSlip` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `user_pin` varchar(50) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldepositSlip`
--

INSERT INTO `tbldepositSlip` (`id`, `uid`, `user_pin`, `image`, `created`) VALUES
(9, 145, '6489288', '126649622157308203df39a.png', '2016-05-09 17:26:44'),
(10, 145, '6489288', '2329699875730830e69d71.png', '2016-05-09 17:31:10'),
(11, 145, '6489288', '1811856814573083a5802d0.png', '2016-05-09 17:33:41'),
(12, 145, '6489288', '237577579573172f256eac.png', '2016-05-10 10:34:42'),
(13, 145, '6489288', '5906753145731747c5dd17.png', '2016-05-10 10:41:16'),
(14, 145, '6489288', '1112547940573175eaa249c.png', '2016-05-10 10:47:22'),
(15, 145, '6489288', '2622009035731789576d6c.png', '2016-05-10 10:58:45'),
(16, 145, '6489288', '1513883026573191809135f.png', '2016-05-10 12:45:04'),
(17, 145, '6489288', '176163746957319215e7c0b.png', '2016-05-10 12:47:34'),
(18, 145, '6489288', '9300554595732c340544fb.png', '2016-05-11 10:29:36'),
(19, 145, '23468', '3844354465732c42193f8e.png', '2016-05-11 10:33:21'),
(20, 143, '9199681', '20024170125732c695bdc79.jpg', '2016-05-11 10:43:50'),
(21, 145, '1234', '10832054625732fab519f90.png', '2016-05-11 14:26:13'),
(22, 145, '1234567', '20900195375732fad3854c9.png', '2016-05-11 14:26:43'),
(23, 145, '2344222', '6902617805732ff9a4cd9e.png', '2016-05-11 14:47:06'),
(24, 145, '5668667', '11478992245733133240b51.png', '2016-05-11 16:10:42'),
(25, 145, '6785189', '768677487573313ddd8288.png', '2016-05-11 16:13:34'),
(26, 145, '1234567', '183649865757341d231246d.png', '2016-05-12 11:05:23'),
(27, 145, '3445677', '1272958354573470a67dd38.png', '2016-05-12 17:01:42'),
(28, 145, '1123344', '51444113573472f3743b6.png', '2016-05-12 17:11:31'),
(29, 145, '6489288', '2019772899573476921743e.png', '2016-05-12 17:26:58'),
(30, 145, '5566778', '1285555668573476c622933.png', '2016-05-12 17:27:50'),
(31, 135, '7444789', '17383769315739c24156848.jpg', '2016-05-16 17:51:13'),
(32, 135, '7444789', '6760336415739c24f38e29.jpg', '2016-05-16 17:51:27'),
(33, 135, '7444789', '1124299095739c2fcc1c1e.jpg', '2016-05-16 17:54:20'),
(34, 135, '7444789', '17536300305739c3447db60.jpg', '2016-05-16 17:55:32'),
(35, 131, '8386488', '1954684435739d88b7415a.png', '2016-05-16 19:26:19'),
(36, 145, '9199681', '1116727152575645c954361.png', '2016-06-07 08:55:53'),
(37, 145, '7788897', '708251330575645f44c770.png', '2016-06-07 08:56:36');

-- --------------------------------------------------------

--
-- Table structure for table `tblDevices`
--

CREATE TABLE `tblDevices` (
  `id` int(11) NOT NULL,
  `device_id` text NOT NULL,
  `device_type` varchar(50) NOT NULL,
  `uid` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblDevices`
--

INSERT INTO `tblDevices` (`id`, `device_id`, `device_type`, `uid`, `created`) VALUES
(2, '2893cf8d5448f5906dc6911027f9e896407f0bfd4bcba402591efe47fb1d2686', 'ios', 145, '2016-05-26 11:47:19'),
(3, 'fG7boX_JmdA:APA91bH1s5-W2WLdSjIdZddTwk_qqJTmYWU6DKpYgnShMdUkb672XZJl_u_oJLT7I7DC8MJXFH2gdaApqWz9l3TP5AGkM8f6geCim9ncR9faqLuq93jJtUUBzX6uhlxHLiJj0VRjtrFl', 'android', 6, '2016-05-26 12:16:17'),
(4, 'cf74504856fa6c2313459d6f8002b13aa423ecbfde7d2a41490e98345bd94913', 'ios', 180, '2016-05-26 15:51:13'),
(7, '', '', 27, '2016-05-27 05:55:55'),
(8, '', '', 28, '2016-05-27 07:18:46'),
(9, '', '', 26, '2016-05-27 14:53:37'),
(10, '', '', 20, '2016-05-30 05:54:58'),
(11, '0a62b1a7d6870b68c8c56ed8d37242c9b8e1ec648d224690fcd57332cc2b7f0c', 'ios', 181, '2016-05-30 13:03:16'),
(12, 'a02fbcea7fc83356ef07cfab7e47f0dda17dfdbdf87f48d8ebf2001f35e241f1', 'ios', 182, '2016-05-30 15:57:29'),
(13, 'f86e79782dfd28a65e121347156b23c6b3b8f323968a91f9e0ed19b98e6e8cee', 'ios', 183, '2016-05-30 20:06:04'),
(14, '', '', 29, '2016-05-30 20:12:12'),
(15, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', 'android', 125, '2016-06-02 06:59:05'),
(16, '3dsfdsfsdgsdvdsvdsfds', 'IOS', 146, '2016-06-03 07:47:21'),
(17, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', 'ios', 185, '2016-06-03 10:02:16'),
(18, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', 'ios', 187, '2016-06-03 10:43:14'),
(19, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', 'ios', 188, '2016-06-03 10:59:22'),
(20, 'fsFcjfZgx4w:APA91bEhUOPW3xHY2f8-g8DmPXsFM2JEUSKZnWUvcCcUq2cQ9Xw19fc2zmHNRQAIbtFWdiywVX6nxHvcT1ltUNejBgM58hpA1u1ZRfOANuGFBYl-f3hORjV-uNw2dX6tbdz-zhw6upKz', 'android', 184, '2016-06-06 07:39:18'),
(21, 'fEmX-LBLd6c:APA91bHwAGUE89cEBkigGWwmnNMGIc6njdk5E3schEQLsf9XmPt3gOUW4tMMtGKcXYZMVTwTqduxeGJHMldsAurYTPR2qFLzhj_4bEPHflyvqdCvtTfULgdN_Nx0AhHXCsTdQbi5eWIW', 'android', 190, '2016-06-07 05:02:41'),
(22, 'cO7dVO3Eh8s:APA91bH521N6kV1_sGasy67GzjMksVItzNNhXYouxkDg4QvPC2kpBdIexc2j-xDV-kf0uo_pjimA9lO704FVCMPnB9Su5yy_zn9sPG3NNa5FSmzXt1YdVcsbavHpMKlsjnWu1s6g4jcz', 'android', 191, '2016-06-07 05:08:19'),
(23, 'cO7dVO3Eh8s:APA91bH521N6kV1_sGasy67GzjMksVItzNNhXYouxkDg4QvPC2kpBdIexc2j-xDV-kf0uo_pjimA9lO704FVCMPnB9Su5yy_zn9sPG3NNa5FSmzXt1YdVcsbavHpMKlsjnWu1s6g4jcz', 'android', 192, '2016-06-07 05:14:50'),
(24, 'dgUKA0725_4:APA91bGDrB4S0zATgT-yek_esVdaiBpsfNPXjh4CUw7i8P579RnApnp8dGVSLhqfYc4-ldQocBuErg7QkOHDuYVQBsFeClYH6ucva5ONJ5WMsZ8rOOB8boRn2Q9MkdvRVscDbcwd-epi', 'android', 193, '2016-06-07 07:33:42'),
(25, 'eabaa969330072ef458300413d613243041ce82dd53810747954b060a24a4f97', 'ios', 195, '2016-06-07 11:46:58'),
(26, '9d3141c625193502d929dd463a2f4278c478007964e490c81daba8be4cbb0774', 'ios', 197, '2016-06-08 04:42:09'),
(27, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 198, '2016-06-08 05:31:38'),
(28, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 199, '2016-06-08 05:46:42'),
(29, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 200, '2016-06-08 05:55:30'),
(30, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 201, '2016-06-08 06:09:09'),
(31, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 202, '2016-06-08 06:12:02'),
(32, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 203, '2016-06-08 08:58:18'),
(33, '', '', 30, '2016-06-08 09:24:26'),
(34, 'dO_L6k1Mz-I:APA91bEilPCBpoEDawzbMH4FYB_R_hZUAlTMkuH9HqJ7UX5ypfuEwfgkIpX1_AVWl2rCK4zU0STGDyMA9bGZuw3FrGZHpmkuPj1tS_I5fHU6_CyB3pYLX9mtQQjAInoxj_fqq3PZoSgX', 'android', 204, '2016-06-08 09:39:48'),
(35, '', '', 31, '2016-06-09 06:09:50'),
(36, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 205, '2016-06-09 06:48:39'),
(37, '9d3141c625193502d929dd463a2f4278c478007964e490c81daba8be4cbb0774', 'ios', 206, '2016-06-09 17:01:10'),
(38, '9d3141c625193502d929dd463a2f4278c478007964e490c81daba8be4cbb0774', 'ios', 207, '2016-06-10 06:58:04'),
(39, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 208, '2016-06-10 09:06:03'),
(40, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 209, '2016-06-10 10:05:12'),
(41, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 210, '2016-06-10 10:12:08'),
(42, '', '', 32, '2016-06-10 10:28:47'),
(43, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', 'android', 213, '2016-06-13 03:47:06'),
(44, 'fsFcjfZgx4w:APA91bEhUOPW3xHY2f8-g8DmPXsFM2JEUSKZnWUvcCcUq2cQ9Xw19fc2zmHNRQAIbtFWdiywVX6nxHvcT1ltUNejBgM58hpA1u1ZRfOANuGFBYl-f3hORjV-uNw2dX6tbdz-zhw6upKz', 'android', 214, '2016-06-13 08:01:40'),
(45, '', '', 33, '2016-06-13 13:46:23');

-- --------------------------------------------------------

--
-- Table structure for table `tblExchangeRates`
--

CREATE TABLE `tblExchangeRates` (
  `id` int(11) NOT NULL,
  `currency_name` varchar(255) NOT NULL,
  `currency_type` varchar(10) NOT NULL,
  `symbol` varchar(5) NOT NULL,
  `buying` varchar(10) NOT NULL,
  `selling` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblExchangeRates`
--

INSERT INTO `tblExchangeRates` (`id`, `currency_name`, `currency_type`, `symbol`, `buying`, `selling`) VALUES
(1, 'Nigerian Naira', 'NGN', 'N', '105', '107'),
(2, 'Pakistani Rupee', 'PKR', 'Rs', '100', '101'),
(3, 'Euro', 'EUR', 'E', '118', '118.5'),
(4, 'Indian', 'IN', 'IN', '105.6', '105.85'),
(8, 'US Dollar', 'USD', '$', '105', '105.6');

-- --------------------------------------------------------

--
-- Table structure for table `tblFbToken`
--

CREATE TABLE `tblFbToken` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `access_token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblFbToken`
--

INSERT INTO `tblFbToken` (`id`, `uid`, `access_token`) VALUES
(2, 141, 'CAALFndbclR4BAHsfuflMHEMIA5WpZB8FFzIa1u10I3dQQeDLYch3BEz8JM0MVhjOoa9jH5ITGsUZAaJetH4dPz7FucrvS3R3ZCvIq9mCT2N0S1GXvLuKj4neDZAzWbSPVrth6ZAcQ3os8zP3uipcZBEAYpo0B8ZAIukTGByHz0f9mDjGX92tBsPyvWSIcYIanNhadzxbPzWshtFJTfpWNqJ');

-- --------------------------------------------------------

--
-- Table structure for table `tblFriends`
--

CREATE TABLE `tblFriends` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `friend_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `request_sent` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblFriends`
--

INSERT INTO `tblFriends` (`id`, `uid`, `friend_id`, `status`, `request_sent`, `updated`) VALUES
(2, 178, 131, 2, '2016-05-24 15:30:14', '2016-05-24 19:31:07'),
(4, 125, 143, 2, '2016-05-24 15:48:32', '2016-05-24 10:48:32'),
(5, 125, 145, 2, '2016-05-24 15:49:15', '2016-05-24 10:49:15'),
(6, 145, 135, 2, '2016-05-24 16:39:28', '2016-05-24 20:39:45'),
(7, 135, 125, 2, '2016-05-24 16:44:19', '2016-05-24 20:44:38'),
(8, 134, 125, 2, '2016-05-25 14:52:02', '2016-05-25 18:52:41'),
(10, 145, 134, 2, '2016-06-07 09:12:45', '2016-06-07 13:13:35'),
(11, 180, 195, 1, '2016-06-07 18:14:26', '2016-06-07 13:14:26'),
(12, 206, 180, 1, '2016-06-09 22:05:03', '2016-06-09 17:05:03'),
(13, 207, 204, 2, '2016-06-10 12:11:01', '2016-06-10 16:12:15'),
(14, 208, 143, 1, '2016-06-10 14:06:48', '2016-06-10 09:06:48'),
(15, 184, 208, 1, '2016-06-10 14:07:45', '2016-06-10 09:07:45'),
(16, 213, 143, 1, '2016-06-13 08:47:21', '2016-06-13 03:47:21'),
(17, 213, 184, 2, '2016-06-13 08:48:33', '2016-06-13 12:48:54');

-- --------------------------------------------------------

--
-- Table structure for table `tblGcAccount`
--

CREATE TABLE `tblGcAccount` (
  `GcAccountId` int(11) NOT NULL,
  `reference_number` varchar(255) DEFAULT NULL,
  `userId` int(11) NOT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `suggested_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblGcAccount`
--

INSERT INTO `tblGcAccount` (`GcAccountId`, `reference_number`, `userId`, `title`, `description`, `suggested_amount`, `status`) VALUES
(11, 'GCR#570f3fddd381b', 125, 'Cc', 'Dgvv', '70.00', 1),
(12, 'GCR#5714747ca7120', 134, 'tet', 'test', '10.00', 1),
(13, 'GCR#572061926d374', 134, 'test', 'bdjdjdn', '10.00', 1),
(14, 'GCR#572749864bb2d', 135, 'test', 'test', '10.00', 1),
(15, 'GCR#572749bf3eb7e', 134, 'new test', 'hdhdj', '10.00', 1),
(16, 'GCR#572749c24caa6', 135, 'testa', 'test', '10.00', 1),
(17, 'GCR#572992c3d0f1b', 125, 'abc group contribution', 'abc description', '105.00', 1),
(18, 'GCR#5729b79025ae2', 125, 'XYZ Group Contribution', 'XYZ Description', '102.00', 1),
(19, 'GCR#5729d4655953e', 134, 'me', 'hdufuv', '10.00', 1),
(20, 'GCR#572ae9669fe1a', 134, 'mine', 'hejd', '20.00', 1),
(21, 'GCR#57308a0b9e284', 135, 'teatet', 'gggg', '100.00', 1),
(22, 'GCR#57331046d37bf', 134, 'my group', 'vhhhd', '20.00', 1),
(23, 'GCR#57340d9d69e14', 135, 'group', 'test', '10.00', 1),
(24, 'GCR#57342bfb84e80', 145, 'Test Group', 'Tesring', '10.00', 1),
(25, 'GCR#5739f1d9e41ca', 131, 'Imali', 'Liz Burial', '100.00', 1),
(26, 'GCR#573ad3fc31ff1', 135, 'tedt', 'this', '100.00', 1),
(27, 'GCR#57500d4b982f8', 145, 'ABC', 'HIII', '10.00', 1),
(28, 'GCR#5750122bd77cf', 145, 'ABC', 'HIII', '10.00', 1),
(29, 'GCR#575018254c92f', 145, 'ABC', 'HIII', '10.00', 1),
(30, 'GCR#5750216398d9a', 145, 'ABC', 'HIII', '10.00', 1),
(31, 'GCR#575022020d926', 145, 'ABC', 'HIII', '10.00', 1),
(32, 'GCR#575023ad2ab0b', 145, 'ABC', 'HIII', '10.00', 1),
(33, 'GCR#575023ad2ca07', 145, 'ABC', 'HIII', '10.00', 1),
(34, 'GCR#5750244e12656', 145, 'ABC', 'HIII', '10.00', 1),
(35, 'GCR#575024536c7df', 145, 'ABC', 'HIII', '10.00', 1),
(36, 'GCR#575120e9994e0', 145, 'ABC', 'HIII', '10.00', 1),
(37, 'GCR#575120e999f51', 145, 'ABC', 'HIII', '10.00', 1),
(38, 'GCR#575120e99f814', 145, 'ABC', 'HIII', '10.00', 1),
(39, 'GCR#57512176c8358', 145, 'ABC', 'HIII', '10.00', 1),
(40, 'GCR#57512459e6aec', 145, 'ABC', 'HIII', '10.00', 1),
(41, 'GCR#575125a63bc41', 145, 'ABC', 'HIII', '10.00', 1),
(42, 'GCR#57564f9da0431', 145, 'test123', 'ndnjdj', '10.00', 1),
(43, 'GCR#575a5ea93a948', 184, 'school', 'sbhjkghj', '120.00', 1),
(44, 'GCR#575a67972d54c', 207, 'tesss', 'tyr', '10.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblGcRequests`
--

CREATE TABLE `tblGcRequests` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `friend_id` int(11) DEFAULT NULL,
  `gc_reference_number` varchar(250) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblGcRequests`
--

INSERT INTO `tblGcRequests` (`id`, `userId`, `friend_id`, `gc_reference_number`, `amount`, `status`, `updated`) VALUES
(10, 134, 125, 'GCR#572061926d374', '10.00', 4, '2016-04-27 15:53:17'),
(11, 134, 135, 'GCR#572061926d374', NULL, 1, '2016-05-02 21:38:16'),
(12, 131, 169, 'GCR#5739f1d9e41ca', '100.00', 4, '2016-05-19 07:42:42'),
(13, 145, 134, 'GCR#57342bfb84e80', NULL, 1, '2016-06-07 13:38:47'),
(14, 207, 204, 'GCR#575a67972d54c', '100.00', 4, '2016-06-10 16:12:39'),
(15, 184, 143, 'GCR#575a5ea93a948', NULL, 1, '2016-06-10 16:31:03');

-- --------------------------------------------------------

--
-- Table structure for table `tblIpool`
--

CREATE TABLE `tblIpool` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `uid` int(11) NOT NULL,
  `frequency` int(11) NOT NULL COMMENT '1 for Weekly, 2 for Monthly, 3 for Monthly',
  `max_people` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `closed` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblIpool`
--

INSERT INTO `tblIpool` (`id`, `title`, `amount`, `uid`, `frequency`, `max_people`, `start_date`, `closed`, `created`) VALUES
(1, 'abc title', '100', 125, 2, 12, '2017-05-10', 0, '2016-05-10 11:00:38'),
(7, 'Latest one', '10', 135, 1, 3, '2016-05-13', 0, '2016-05-13 10:45:12'),
(8, 'Ipool nauman', '100', 145, 0, 4, '1970-01-01', 0, '2016-05-13 15:52:14'),
(9, 'one', '100', 145, 0, 3, '2016-05-13', 0, '2016-05-13 15:54:42'),
(10, 'hi', '123', 145, 0, 3, '2016-04-13', 0, '2016-05-13 15:59:49'),
(11, 'fourth', '200', 145, 0, 5, '2017-05-13', 0, '2016-05-13 16:27:44'),
(12, 'fequency monthly', '299', 145, 2, 4, '2017-05-13', 0, '2016-05-13 16:29:02'),
(13, 'unique pool', '30', 134, 1, 6, '2016-05-13', 0, '2016-05-13 16:37:03'),
(14, 'Three', '10', 145, 1, 22, '2016-06-13', 0, '2016-05-13 17:12:02'),
(15, 'my pool', '40', 134, 2, 5, '2016-05-14', 0, '2016-05-13 17:48:26'),
(16, 'Latest', '10', 135, 1, 2, '2016-05-17', 0, '2016-05-17 15:39:14'),
(17, 'Nauman iPoll', '100', 125, 3, 10, '2016-05-18', 0, '2016-05-17 16:11:25'),
(18, 'Twt', '10', 135, 1, 2, '2016-05-18', 0, '2016-05-18 23:44:20'),
(19, 'y', 'y', 170, 1, 4, '2018-05-19', 0, '2016-05-19 17:46:32'),
(20, 'test', '40', 152, 0, 5, '2016-05-20', 0, '2016-05-19 20:14:32'),
(21, 'Imali', '100', 131, 0, 4, '2016-05-19', 0, '2016-05-19 22:06:31'),
(22, 'Sultan test', '10', 135, 1, 2, '2016-05-25', 0, '2016-05-24 19:33:03'),
(23, 'mbkt', '20', 152, 0, 5, '2016-05-26', 0, '2016-05-28 04:02:33'),
(24, 'mbk fund', '500', 182, 2, 5, '2016-06-12', 0, '2016-06-03 19:43:04'),
(25, 'hkuuk', '10', 145, 2, 4, '2016-08-06', 0, '2016-06-06 11:18:18'),
(26, 'Dummy', '1200', 184, 1, 3, '2016-06-06', 0, '2016-06-06 12:53:05'),
(27, 'yu', '12', 145, 1, 3, '2017-06-07', 0, '2016-06-07 11:58:40'),
(28, 'my', '1000', 184, 1, 5, '2016-06-08', 0, '2016-06-08 12:47:38'),
(29, 'tre', '10', 206, 1, 3, '2018-06-13', 0, '2016-06-13 09:14:52');

-- --------------------------------------------------------

--
-- Table structure for table `tblIpoolInvitation`
--

CREATE TABLE `tblIpoolInvitation` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL COMMENT 'invited users',
  `ipool_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `selected` int(11) NOT NULL COMMENT '0 for waiting ipool per month money,1 for accepted ipool per month money',
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblIpoolInvitation`
--

INSERT INTO `tblIpoolInvitation` (`id`, `uid`, `ipool_id`, `status`, `selected`, `datetime`) VALUES
(1, 134, 10, 1, 0, '2016-06-08 16:01:05'),
(2, 135, 10, 1, 0, '2016-06-08 16:01:05'),
(3, 213, 28, 2, 0, '2016-06-13 08:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `tblMarket`
--

CREATE TABLE `tblMarket` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `item_image` varchar(255) DEFAULT NULL,
  `gallery_images` text,
  `price` decimal(10,2) DEFAULT NULL,
  `contact_direct` int(11) DEFAULT NULL,
  `available` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblMerchantInvoices`
--

CREATE TABLE `tblMerchantInvoices` (
  `id` bigint(20) NOT NULL,
  `merchantId` bigint(20) NOT NULL,
  `details` text NOT NULL,
  `dated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `net_total` decimal(10,2) NOT NULL,
  `user_pin` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblMerchantInvoices`
--

INSERT INTO `tblMerchantInvoices` (`id`, `merchantId`, `details`, `dated`, `net_total`, `user_pin`, `status`) VALUES
(1, 1, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-02-25 06:19:38', '44.00', 3835918, 4),
(2, 4, '[{"item_name":"sgu","item_quantity":"4","unit_price":"10"}]', '2016-02-25 06:20:36', '40.00', 1242425, 1),
(3, 4, '[{"item_name":"sgu","item_quantity":"4","unit_price":"10"}]', '2016-02-25 06:20:45', '40.00', 3838918, 1),
(4, 4, '[{"item_name":"apple","item_quantity":"2","unit_price":"15"},{"item_name":"mango","item_quantity":"21","unit_price":"17"}]', '2016-02-25 10:24:15', '387.00', 3835918, 3),
(5, 4, '[{"item_name":"apple","item_quantity":"16","unit_price":"13"},{"item_name":"mango","item_quantity":"13","unit_price":"19"},{"item_name":"rice","item_quantity":"21","unit_price":"20"}]', '2016-02-25 11:57:41', '100.00', 3835918, 3),
(6, 4, '[{"item_name":"apple","item_quantity":"16","unit_price":"13"},{"item_name":"mango","item_quantity":"13","unit_price":"19"},{"item_name":"rice","item_quantity":"21","unit_price":"20"}]', '2016-02-25 11:58:32', '875.00', 1245786, 3),
(7, 4, '[{"item_name":"apple","item_quantity":"16","unit_price":"13"},{"item_name":"mango","item_quantity":"13","unit_price":"19"},{"item_name":"rice","item_quantity":"21","unit_price":"20"}]', '2016-02-25 11:58:35', '875.00', 1245786, 1),
(8, 4, '[{"item_name":"new items","item_quantity":"8","unit_price":"24"},{"item_name":"new item 2","item_quantity":"12","unit_price":"12"}]', '2016-02-26 06:39:37', '336.00', 3835918, 4),
(9, 4, '[{"item_name":"ett","item_quantity":"2","unit_price":"13"}]', '2016-02-26 13:26:24', '26.00', 3831918, 3),
(10, 4, '[{"item_name":"ett","item_quantity":"2","unit_price":"13"}]', '2016-02-26 13:26:35', '26.00', 3835918, 3),
(11, 4, '[{"item_name":"ett","item_quantity":"2","unit_price":"13"},{"item_name":"efhu","item_quantity":"6","unit_price":"12"},{"item_name":"ryuu","item_quantity":"12","unit_price":"7"}]', '2016-02-26 13:27:54', '182.00', 3835918, 4),
(12, 4, '[{"item_name":"dfg","item_quantity":"4","unit_price":"4"}]', '2016-02-26 13:34:54', '16.00', 3535918, 1),
(13, 4, '[{"item_name":"dfg","item_quantity":"4","unit_price":"4"},{"item_name":"dgu","item_quantity":"8","unit_price":"11"}]', '2016-02-26 13:35:10', '104.00', 3835918, 4),
(14, 4, '[{"item_name":"shock absorber","item_quantity":"1","unit_price":"1"}]', '2016-02-29 06:03:38', '1.00', 6327186, 4),
(15, 4, '[{"item_name":"lays","item_quantity":"8","unit_price":"20"},{"item_name":"tea\\n","item_quantity":"10","unit_price":"16"}]', '2016-02-29 06:25:51', '320.00', 3835918, 4),
(16, 4, '[{"item_name":"lays","item_quantity":"8","unit_price":"20"},{"item_name":"tea\\n","item_quantity":"10","unit_price":"16"},{"item_name":"pepsi","item_quantity":"16","unit_price":"90"}]', '2016-02-29 06:26:52', '1760.00', 7845963, 1),
(17, 4, '[{"item_name":"dew","item_quantity":"16","unit_price":"59"}]', '2016-02-29 06:34:51', '944.00', 3835918, 4),
(18, 4, '[{"item_name":"dew","item_quantity":"16","unit_price":"59"},{"item_name":"coke","item_quantity":"22","unit_price":"24"}]', '2016-02-29 06:35:15', '1472.00', 3835918, 4),
(19, 4, '[{"item_name":"miranda","item_quantity":"45","unit_price":"32"}]', '2016-02-29 06:40:29', '1440.00', 3835928, 1),
(20, 4, '[{"item_name":"miranda","item_quantity":"45","unit_price":"32"}]', '2016-02-29 06:40:39', '1440.00', 3835918, 4),
(21, 4, '[{"item_name":"best items","item_quantity":"31","unit_price":"5"}]', '2016-02-29 06:48:31', '155.00', 3835918, 4),
(22, 4, '[{"item_name":"best items","item_quantity":"31","unit_price":"5"},{"item_name":"rice","item_quantity":"3","unit_price":"60"}]', '2016-02-29 06:49:25', '335.00', 3835918, 4),
(23, 4, '[{"item_name":"best items","item_quantity":"31","unit_price":"5"},{"item_name":"rice","item_quantity":"3","unit_price":"60"},{"item_name":"asd","item_quantity":"11","unit_price":"17"}]', '2016-02-29 06:50:00', '522.00', 3835918, 4),
(24, 4, '[{"item_name":"apple","item_quantity":"24","unit_price":"17"}]', '2016-02-29 07:11:08', '408.00', 3831918, 1),
(25, 4, '[{"item_name":"apple","item_quantity":"24","unit_price":"17"},{"item_name":"orange","item_quantity":"22","unit_price":"29"}]', '2016-02-29 07:11:30', '1046.00', 3835918, 4),
(26, 4, '[{"item_name":"apple","item_quantity":"24","unit_price":"17"},{"item_name":"orange","item_quantity":"22","unit_price":"29"},{"item_name":"mango","item_quantity":"14","unit_price":"178"}]', '2016-02-29 07:12:02', '3538.00', 3835918, 4),
(27, 4, '[{"item_name":"ice cream","item_quantity":"17","unit_price":"158"}]', '2016-02-29 07:26:10', '2686.00', 3835918, 4),
(28, 4, '[{"item_name":"ice cream","item_quantity":"17","unit_price":"158"}]', '2016-02-29 07:26:16', '2686.00', 3835918, 4),
(29, 4, '[{"item_name":"ice cream","item_quantity":"17","unit_price":"158"},{"item_name":"burger","item_quantity":"30","unit_price":"37"}]', '2016-02-29 07:26:46', '3796.00', 3835918, 4),
(30, 4, '[{"item_name":"ice cream","item_quantity":"17","unit_price":"158"},{"item_name":"burger","item_quantity":"30","unit_price":"37"},{"item_name":"pizza","item_quantity":"1","unit_price":"378"}]', '2016-02-29 07:27:25', '4174.00', 3835918, 4),
(31, 4, '[{"item_name":"deghe","item_quantity":"20","unit_price":"8"}]', '2016-02-29 07:40:54', '160.00', 3835318, 1),
(32, 4, '[{"item_name":"deghe","item_quantity":"20","unit_price":"8"}]', '2016-02-29 07:41:00', '160.00', 3835918, 4),
(33, 4, '[{"item_name":"Brazilian hair","item_quantity":"1","unit_price":"200"}]', '2016-02-29 16:59:51', '200.00', 6327186, 4),
(34, 4, '[{"item_name":"vitamalt","item_quantity":"1","unit_price":"2"},{"item_name":"short bread","item_quantity":"1","unit_price":"3"}]', '2016-03-02 18:40:37', '5.00', 6327186, 4),
(35, 5, '[{"unit_price":"164","item_quantity":"999","item_name":"Gghn"}]', '2016-03-07 09:27:07', '163836.00', 2338922, 3),
(36, 5, '[{"unit_price":"74","item_quantity":"38","item_name":"Tezt"},{"unit_price":"74","item_quantity":"3","item_name":"Fhhh"},{"unit_price":"74","item_quantity":"3","item_name":"Fhhh"},{"unit_price":"74","item_quantity":"3","item_name":"Fhhh"}]', '2016-03-08 12:56:32', '3478.00', 1819151, 1),
(37, 5, '[{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"},{"unit_price":"80","item_quantity":"5","item_name":"Abc"}]', '2016-03-09 09:29:38', '4000.00', 2338922, 2),
(38, 5, '[{"unit_price":"58","item_quantity":"1","item_name":""}]', '2016-03-09 09:58:00', '58.00', 2338922, 1),
(39, 5, '[{"unit_price":"700","item_quantity":"1","item_name":"Laptop"},{"unit_price":"500","item_quantity":"2","item_name":"Mobile "}]', '2016-03-09 11:11:39', '1700.00', 2338922, 4),
(40, 5, '{"details":[["one","2","1"],["two","4","1"]]}', '2016-03-10 11:35:29', '6.00', 2338922, 1),
(41, 5, '{"details":[[{"unit_price":"2","item_quantity":"1","item_name":"e"}],[{"unit_price":"2","item_quantity":"1","item_name":"eeeee"}],[{"unit_price":"2","item_quantity":"1","item_name":"eeeeeeee"}]]}', '2016-03-10 12:38:47', '6.00', 2338922, 3),
(42, 7, '[{"item_name":"true religion jeans","item_quantity":"1","unit_price":"22000"}]', '2016-03-10 14:27:14', '22000.00', 5411214, 2),
(43, 7, '[{"item_name":"osita","item_quantity":"1","unit_price":"500"}]', '2016-03-10 18:45:17', '500.00', 5411214, 2),
(44, 5, '{"details":[[{"unit_price":"12","item_quantity":"1","item_name":"a"}]]}', '2016-03-11 05:07:11', '12.00', 2338922, 3),
(45, 5, '{"details":[[{"unit_price":"12","item_quantity":"1","item_name":"a"}]]}', '2016-03-11 05:07:16', '12.00', 2338922, 3),
(46, 5, '{"details":[[{"unit_price":"10","item_quantity":"1","item_name":""}],[{"unit_price":"10","item_quantity":"1","item_name":"2"}]]}', '2016-03-11 05:13:18', '20.00', 2338922, 1),
(47, 5, '{"details":[[{"unit_price":"12","item_quantity":"1","item_name":"test"}],[{"unit_price":"5","item_quantity":"2","item_name":"hiiii"}]]}', '2016-03-11 05:51:37', '22.00', 2338922, 1),
(48, 5, '{"details":[[{"unit_price":"2","item_quantity":"1","item_name":"teeee"}]]}', '2016-03-11 05:55:58', '2.00', 2338922, 1),
(49, 5, '{"details":[[{"unit_price":"2","item_quantity":"1","item_name":"w"}]]}', '2016-03-11 05:59:02', '2.00', 2338922, 1),
(50, 5, '{"details":[[{"unit_price":"1","item_quantity":"1","item_name":"hyhj"}],[{"unit_price":"5","item_quantity":"2","item_name":"233"}]]}', '2016-03-11 06:02:44', '11.00', 2338922, 3),
(51, 6, '[{"unit_price":"1","item_quantity":"1","item_name":"H"}]', '2016-03-11 06:04:53', '1.00', 2338944, 1),
(52, 5, '{"details":[[{"unit_price":"3","item_quantity":"1","item_name":"test"}]]}', '2016-03-11 06:07:03', '3.00', 2338944, 1),
(53, 5, '{"details":[[{"unit_price":"19","item_quantity":"1","item_name":"test"}],[{"unit_price":"5","item_quantity":"2","item_name":"sevf"}]]}', '2016-03-11 06:08:36', '29.00', 2338944, 1),
(66, 5, '[[{"unit_price":"12","item_quantity":"3","item_name":"test item"}]]', '2016-03-11 09:29:07', '36.00', 1829971, 2),
(67, 5, '[[{"unit_price":"12","item_quantity":"3","item_name":"test item"}]]', '2016-03-11 09:30:07', '36.00', 1829971, 3),
(68, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 09:31:31', '44.00', 2338922, 2),
(69, 5, '[[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]]', '2016-03-11 09:32:06', '44.00', 2338922, 2),
(70, 5, '[[{"unit_price":"12","item_quantity":"3","item_name":"test item"}]]', '2016-03-11 09:34:30', '36.00', 1829971, 3),
(71, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 09:39:52', '44.00', 2338922, 2),
(72, 5, '[{"unit_price":"5","item_quantity":"5","item_name":"bag"}]', '2016-03-11 09:45:49', '25.00', 2338922, 2),
(73, 5, '[{"unit_price":"50","item_quantity":"1","item_name":"push shops"}]', '2016-03-11 09:56:01', '50.00', 1829971, 3),
(74, 5, '[{"unit_price":"50","item_quantity":"1","item_name":"push shops"}]', '2016-03-11 09:57:01', '50.00', 1829971, 1),
(75, 5, '[{"unit_price":"50","item_quantity":"1","item_name":"push shops"}]', '2016-03-11 09:58:40', '50.00', 1829971, 2),
(76, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:03:18', '44.00', 1829971, 1),
(77, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:04:57', '44.00', 1829971, 1),
(78, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:05:40', '44.00', 1829971, 1),
(79, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:09:11', '44.00', 1829971, 1),
(80, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:09:44', '44.00', 1829971, 1),
(81, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:11:23', '44.00', 1829971, 1),
(82, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:12:21', '44.00', 1829971, 1),
(83, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:12:52', '44.00', 1829971, 1),
(84, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:32:19', '44.00', 1829971, 1),
(85, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:35:22', '44.00', 1829971, 1),
(86, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:37:32', '44.00', 1829971, 1),
(87, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:41:34', '44.00', 1829971, 3),
(88, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:45:35', '44.00', 1829971, 2),
(89, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:57:27', '44.00', 1829971, 2),
(90, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 10:58:26', '44.00', 1829971, 2),
(91, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 11:03:15', '44.00', 1829971, 2),
(92, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 11:06:22', '44.00', 1829971, 2),
(93, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 11:06:52', '44.00', 1829971, 3),
(94, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 11:14:31', '44.00', 1829971, 3),
(95, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-11 11:20:38', '44.00', 1829971, 2),
(96, 5, '[{"unit_price":"23","item_quantity":"1","item_name":"test this"}]', '2016-03-11 11:51:25', '23.00', 1829971, 4),
(97, 5, '[{"unit_price":"250","item_quantity":"1","item_name":"notify"}]', '2016-03-11 11:55:41', '250.00', 1829971, 4),
(98, 6, '[{"unit_price":"100","item_quantity":"3","item_name":"Sbd"},{"unit_price":"800","item_quantity":"1","item_name":"Fds"}]', '2016-03-15 10:47:19', '1100.00', 1829971, 3),
(99, 5, '[{"unit_price":"12","item_quantity":"2","item_name":"Test Item"}]', '2016-03-17 06:56:58', '24.00', 2338922, 1),
(100, 9, '[{"unit_price":"200","item_quantity":"1","item_name":"Wood n stood"}]', '2016-03-17 11:23:44', '200.00', 2338922, 2),
(101, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"suger"}]', '2016-03-17 11:28:07', '12.00', 2338922, 1),
(102, 6, '[{"unit_price":"800","item_quantity":"1","item_name":"Mobile"},{"unit_price":"1000","item_quantity":"1","item_name":"Laptop"}]', '2016-03-18 09:05:20', '1800.00', 1829971, 3),
(103, 6, '[{"unit_price":"400","item_quantity":"1","item_name":"Abc"}]', '2016-03-18 09:10:49', '400.00', 1829971, 3),
(104, 9, '[{"unit_price":"123","item_quantity":"1","item_name":"Test Wee"}]', '2016-03-18 10:00:14', '123.00', 2338922, 1),
(105, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"y"}]', '2016-03-18 10:01:07', '2.00', 2338922, 1),
(106, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"y"}]', '2016-03-18 10:01:22', '2.00', 2338922, 1),
(107, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"eee"}]', '2016-03-18 12:29:48', '2.00', 2338922, 1),
(108, 9, '[{"unit_price":"9","item_quantity":"1","item_name":"test uner"}]', '2016-03-21 06:42:24', '9.00', 2338922, 1),
(109, 9, '[{"unit_price":"9","item_quantity":"1","item_name":"test uner"}]', '2016-03-21 06:45:45', '9.00', 2338922, 1),
(110, 6, '[{"unit_price":"500","item_quantity":"1","item_name":"Abc"}]', '2016-03-21 06:50:14', '500.00', 2338922, 1),
(111, 5, '[{"unit_price":"500","item_quantity":"1","item_name":"Abc"}]', '2016-03-21 06:55:39', '500.00', 2338922, 1),
(112, 6, '[{"unit_price":"500","item_quantity":"1","item_name":"Abc"}]', '2016-03-21 07:00:03', '500.00', 1829971, 3),
(113, 5, '[{"unit_price":"500","item_quantity":"1","item_name":"Abc"}]', '2016-03-21 09:46:24', '500.00', 2338922, 1),
(114, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"jj"}]', '2016-03-21 10:04:20', '12.00', 2338922, 1),
(115, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"jj"}]', '2016-03-21 10:05:30', '12.00', 2338922, 1),
(116, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"jj"}]', '2016-03-21 10:06:02', '12.00', 2338922, 1),
(117, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"jj"}]', '2016-03-21 10:29:41', '12.00', 2338922, 1),
(118, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:31:55', '12.00', 2338922, 1),
(119, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:32:53', '12.00', 2338922, 1),
(120, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:40:36', '12.00', 2338922, 1),
(121, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:41:37', '12.00', 2338922, 1),
(122, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:42:02', '12.00', 2338922, 1),
(123, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:47:10', '12.00', 2338922, 1),
(124, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:51:11', '12.00', 2338922, 1),
(125, 5, '[{"item_name":"wgg","item_quantity":"2","unit_price":"15"},{"item_name":"dghj","item_quantity":"2","unit_price":"7"}]', '2016-03-21 10:55:51', '44.00', 1829971, 2),
(126, 9, '[{"unit_price":"12","item_quantity":"1","item_name":"we"}]', '2016-03-21 10:58:44', '12.00', 2338922, 1),
(127, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"q"}]', '2016-03-21 11:01:36', '1.00', 2338922, 1),
(128, 9, '[{"unit_price":"3","item_quantity":"1","item_name":"wer"}]', '2016-03-21 11:03:40', '3.00', 2338922, 1),
(129, 9, '[{"unit_price":"4","item_quantity":"1","item_name":"fhff"}]', '2016-03-21 11:04:12', '4.00', 2338922, 1),
(130, 9, '[{"unit_price":"5","item_quantity":"1","item_name":"dhfhng"}]', '2016-03-21 11:05:06', '5.00', 2338922, 1),
(131, 9, '[{"unit_price":"5","item_quantity":"1","item_name":"hy"}]', '2016-03-21 11:29:17', '5.00', 2338922, 1),
(132, 9, '[{"unit_price":"1234","item_quantity":"12","item_name":"uogiutuyfif"}]', '2016-03-21 11:33:54', '14808.00', 2338922, 1),
(133, 9, '[{"unit_price":"3","item_quantity":"1","item_name":"bfhfhf"}]', '2016-03-21 11:44:14', '3.00', 2338922, 1),
(134, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"rrr"}]', '2016-03-21 12:16:22', '2.00', 2338922, 1),
(135, 9, '[{"unit_price":"34","item_quantity":"1","item_name":"u"}]', '2016-03-21 12:17:26', '34.00', 2338922, 1),
(136, 9, '[{"unit_price":"3","item_quantity":"1","item_name":"w"}]', '2016-03-21 12:18:07', '3.00', 2338922, 1),
(137, 9, '[{"unit_price":"4","item_quantity":"1","item_name":"ghhhh"}]', '2016-03-22 05:12:37', '4.00', 2338922, 1),
(138, 9, '[{"unit_price":"4","item_quantity":"1","item_name":"sdf"}]', '2016-03-22 05:26:26', '4.00', 2338922, 1),
(139, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"ghh"}]', '2016-03-22 05:27:47', '2.00', 2338922, 1),
(140, 9, '[{"unit_price":"23","item_quantity":"1","item_name":"dgdgdg"}]', '2016-03-22 05:28:34', '23.00', 2338922, 1),
(141, 9, '[{"unit_price":"6","item_quantity":"1","item_name":"f"}]', '2016-03-22 05:30:27', '6.00', 2338922, 1),
(142, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"sd"}]', '2016-03-22 05:32:09', '2.00', 2338922, 1),
(143, 9, '[{"item_name":"test one","item_quantity":"3","unit_price":"1"}]', '2016-03-22 05:57:46', '3.00', 2338922, 1),
(144, 9, '[{"item_name":"test one","item_quantity":"3","unit_price":"1"}]', '2016-03-22 06:08:21', '3.00', 2338622, 1),
(145, 9, '[{"item_name":"test one","item_quantity":"3","unit_price":"1"}]', '2016-03-22 06:08:41', '3.00', 2338922, 1),
(146, 9, '[{"item_name":"ty","item_quantity":"1","unit_price":"1"}]', '2016-03-22 06:32:01', '1.00', 2338922, 1),
(147, 9, '[{"item_name":"ty","item_quantity":"1","unit_price":"1"}]', '2016-03-22 06:34:12', '1.00', 2338922, 1),
(148, 9, '[{"item_name":"ty","item_quantity":"1","unit_price":"1"}]', '2016-03-22 06:43:49', '1.00', 2338922, 1),
(149, 9, '[{"item_name":"ty","item_quantity":"1","unit_price":"1"}]', '2016-03-22 06:45:06', '1.00', 2338922, 1),
(150, 9, '[{"item_name":"ty","item_quantity":"1","unit_price":"1"}]', '2016-03-22 06:59:52', '1.00', 2338922, 2),
(151, 9, '[{"item_name":"ty","item_quantity":"1","unit_price":"1"}]', '2016-03-22 07:12:18', '1.00', 2338922, 2),
(152, 9, '[{"item_name":"ty","item_quantity":"1","unit_price":"1"}]', '2016-03-22 07:12:52', '1.00', 2338922, 3),
(153, 9, '[{"item_name":"trree","item_quantity":"1","unit_price":"1"}]', '2016-03-22 07:20:06', '1.00', 2338922, 1),
(154, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"hi"},{"unit_price":"2","item_quantity":"1","item_name":"jkkkkggh"},{"unit_price":"1","item_quantity":"1","item_name":"eere"}]', '2016-03-22 07:32:34', '4.00', 2338922, 1),
(155, 9, '[{"unit_price":"3","item_quantity":"1","item_name":"g"},{"unit_price":"5","item_quantity":"1","item_name":"geh"},{"unit_price":"3","item_quantity":"1","item_name":"jh"}]', '2016-03-22 07:38:48', '11.00', 2338922, 1),
(156, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"h"},{"unit_price":"5","item_quantity":"1","item_name":"kkk"},{"unit_price":"4","item_quantity":"1","item_name":"bbbb"}]', '2016-03-22 07:40:18', '10.00', 2338922, 1),
(157, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"sd"},{"unit_price":"3","item_quantity":"2","item_name":"gre"}]', '2016-03-22 07:42:22', '8.00', 2338922, 1),
(158, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"hd"},{"unit_price":"2","item_quantity":"1","item_name":"ghe"}]', '2016-03-22 07:44:52', '3.00', 2338922, 1),
(159, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"e"},{"unit_price":"2","item_quantity":"1","item_name":"d"}]', '2016-03-22 07:46:17', '4.00', 2338922, 1),
(160, 9, '[{"unit_price":"3","item_quantity":"1","item_name":"g"}]', '2016-03-22 07:50:32', '3.00', 2338922, 2),
(161, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"brush"},{"unit_price":"3","item_quantity":"1","item_name":"comb"},{"unit_price":"2","item_quantity":"2","item_name":"polish"},{"unit_price":"4","item_quantity":"1","item_name":"hair cream"}]', '2016-03-22 07:51:37', '13.00', 2338922, 2),
(162, 9, '[{"unit_price":"2","item_quantity":"1","item_name":"brush"},{"unit_price":"1","item_quantity":"1","item_name":"nail"},{"unit_price":"4","item_quantity":"1","item_name":"jeans"}]', '2016-03-22 07:54:30', '7.00', 2338922, 2),
(163, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"test items"}]', '2016-03-22 09:48:54', '1.00', 1829971, 2),
(164, 9, '[{"unit_price":"5","item_quantity":"2","item_name":"esde"}]', '2016-03-22 09:49:39', '10.00', 1829971, 2),
(165, 9, '[{"unit_price":"50","item_quantity":"2","item_name":"Abc"},{"unit_price":"80","item_quantity":"1","item_name":"Def"}]', '2016-03-22 10:15:05', '180.00', 2338922, 3),
(166, 9, '[{"item_name":"dg","item_quantity":"1","unit_price":"1"}]', '2016-03-22 10:17:04', '1.00', 2338922, 1),
(167, 6, '[{"unit_price":"800","item_quantity":"1","item_name":"summary invoice"}]', '2016-03-24 05:50:48', '800.00', 1829971, 3),
(168, 9, '[{"unit_price":"120","item_quantity":"12","item_name":"kdshkdm"}]', '2016-03-24 07:41:53', '1440.00', 2338922, 1),
(169, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:37:02', '1.00', 2338922, 1),
(170, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:37:41', '1.00', 2338922, 1),
(171, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:39:20', '1.00', 2338922, 1),
(172, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:41:16', '1.00', 2338922, 1),
(173, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:42:16', '1.00', 2338922, 1),
(174, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:48:41', '1.00', 2338922, 1),
(175, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:49:02', '1.00', 2338922, 1),
(176, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:50:40', '1.00', 2338922, 1),
(177, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:51:03', '1.00', 2338922, 1),
(178, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:52:08', '1.00', 2338922, 1),
(179, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:52:31', '1.00', 2338922, 1),
(180, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:58:06', '1.00', 2338922, 1),
(181, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 09:58:57', '1.00', 2338922, 1),
(182, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:00:18', '1.00', 2338922, 1),
(183, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:12:59', '1.00', 2338922, 1),
(184, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:18:29', '1.00', 2338922, 1),
(185, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:18:55', '1.00', 2338922, 1),
(186, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:19:12', '1.00', 2338922, 1),
(187, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:19:33', '1.00', 2338922, 1),
(188, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:20:42', '1.00', 2338922, 1),
(189, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:22:36', '1.00', 2338922, 1),
(190, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:23:35', '1.00', 2338922, 1),
(191, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:26:33', '1.00', 2338922, 1),
(192, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:42:36', '1.00', 2338922, 1),
(193, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:49:45', '1.00', 2338922, 1),
(194, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:51:20', '1.00', 2338922, 1),
(195, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:51:37', '1.00', 2338922, 1),
(196, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:52:10', '1.00', 2338922, 1),
(197, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:54:25', '1.00', 2338922, 1),
(198, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 10:55:18', '1.00', 2338922, 1),
(199, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:01:35', '1.00', 2338922, 1),
(200, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:02:21', '1.00', 2338922, 2),
(201, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:03:02', '1.00', 2338922, 2),
(202, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:03:55', '2.00', 2338922, 2),
(203, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:04:13', '2.00', 2338922, 2),
(204, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:08:35', '2.00', 9991827, 1),
(205, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:14:42', '2.00', 2338922, 1),
(206, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:14:56', '2.00', 9991827, 1),
(207, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:20:44', '2.00', 9991827, 1),
(208, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:23:26', '2.00', 9991827, 1),
(209, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:23:59', '2.00', 9991827, 1),
(210, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:25:33', '2.00', 9991827, 1),
(211, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:27:27', '2.00', 9991827, 1),
(212, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:28:09', '2.00', 9991827, 1),
(213, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:51:08', '2.00', 2338922, 2),
(214, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:51:22', '2.00', 9991827, 1),
(215, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:53:34', '2.00', 9991827, 1),
(216, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:53:52', '2.00', 9991827, 1),
(217, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:54:18', '2.00', 9991827, 1),
(218, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:56:24', '2.00', 9991827, 1),
(219, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 11:57:59', '2.00', 9991827, 1),
(220, 9, '[{"item_name":"hi","item_quantity":"1","unit_price":"1"},{"item_name":"w\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-24 12:00:25', '2.00', 9991827, 1),
(221, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"ge"}]', '2016-03-25 09:29:31', '1.00', 9729788, 1),
(222, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"ge"}]', '2016-03-25 09:30:01', '1.00', 9729788, 1),
(223, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"ge"}]', '2016-03-25 09:32:42', '1.00', 9729788, 1),
(224, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"ge"}]', '2016-03-25 09:41:16', '1.00', 9991827, 1),
(225, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"w"}]', '2016-03-25 09:43:07', '1.00', 9991827, 1),
(226, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"we"}]', '2016-03-25 11:00:51', '1.00', 9991827, 1),
(227, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"jgghjg"}]', '2016-03-25 11:01:22', '1.00', 9991827, 1),
(228, 9, '[{"unit_price":" 1","item_quantity":"1","item_name":"dgf"}]', '2016-03-25 11:02:23', '1.00', 9991827, 1),
(229, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"c"}]', '2016-03-25 11:03:27', '1.00', 9991827, 1),
(230, 9, '[{"unit_price":"1","item_quantity":"1","item_name":"d"}]', '2016-03-25 11:51:31', '1.00', 9991827, 1),
(231, 9, '[{"item_name":"test","item_quantity":"1","unit_price":"1"}]', '2016-03-28 05:42:57', '1.00', 9991827, 1),
(232, 9, '[{"item_name":"test","item_quantity":"1","unit_price":"1"}]', '2016-03-28 05:43:34', '1.00', 9991827, 2),
(233, 9, '[{"item_name":"test","item_quantity":"1","unit_price":"1"}]', '2016-03-28 05:44:44', '1.00', 9991827, 1),
(234, 9, '[{"item_name":"test","item_quantity":"1","unit_price":"1"},{"item_name":"g\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-28 05:45:21', '2.00', 9991827, 1),
(235, 9, '[{"item_name":"test","item_quantity":"1","unit_price":"1"},{"item_name":"g\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-28 05:47:47', '2.00', 9991827, 1),
(236, 9, '[{"item_name":"test","item_quantity":"1","unit_price":"1"},{"item_name":"g\\n","item_quantity":"1","unit_price":"1"}]', '2016-03-28 05:56:38', '2.00', 9991827, 1),
(237, 5, 'fg', '2016-03-28 06:43:45', '12.00', 9991827, 1),
(238, 5, '[{"Invoice_Summary":"as"}]', '2016-03-28 06:53:18', '21.00', 9991827, 1),
(239, 5, '[{"Invoice_Summary":"as"}]', '2016-03-28 06:56:54', '2.00', 9991827, 2),
(240, 5, '[{"Invoice_Summary":"as"}]', '2016-03-28 07:03:43', '3.00', 9991827, 3),
(241, 5, '[{"unit_price":"12","item_quantity":"1","item_name":"test"}]', '2016-03-28 07:23:21', '12.00', 1829971, 2),
(242, 5, '[{"unit_price":"10","item_quantity":"1","item_name":"ummery"}]', '2016-03-28 10:04:41', '10.00', 2338922, 2),
(243, 5, '[{"unit_price":"209","item_quantity":"1","item_name":"Sumary of invoice"}]', '2016-03-28 11:04:02', '209.00', 9991827, 2),
(244, 6, '[{"unit_price":"1020","item_quantity":"1","item_name":"abc"}]', '2016-03-29 05:56:35', '1020.00', 9991827, 2),
(245, 5, '[{"unit_price":"1","item_quantity":"1","item_name":"gee"}]', '2016-03-30 09:59:31', '1.00', 9991827, 1),
(246, 5, '[{"unit_price":"1","item_quantity":"1","item_name":"r"}]', '2016-03-30 10:00:05', '1.00', 9991827, 1),
(247, 5, '[{"unit_price":"100","item_quantity":"100","item_name":"malik"}]', '2016-03-30 10:02:12', '10000.00', 991827, 1),
(248, 5, '[{"unit_price":"100","item_quantity":"100","item_name":"malik"}]', '2016-03-30 10:02:23', '10000.00', 991827, 1),
(249, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"def"}]', '2016-03-30 10:02:56', '100.00', 9991827, 2),
(250, 5, '[{"unit_price":"100","item_quantity":"100","item_name":"malik"}]', '2016-03-30 10:03:29', '10000.00', 9991827, 1),
(251, 5, '[{"unit_price":"100","item_quantity":"100","item_name":"malik"}]', '2016-03-30 10:04:42', '10000.00', 9991827, 1),
(252, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"fghi"}]', '2016-03-30 10:08:37', '100.00', 9991827, 2),
(253, 5, '[{"unit_price":"123","item_quantity":"1","item_name":"rest"}]', '2016-03-30 10:10:04', '123.00', 1829971, 2),
(254, 6, '[{"unit_price":"50","item_quantity":"1","item_name":"tea"}]', '2016-03-30 12:01:47', '50.00', 9991827, 3),
(255, 9, '[{"item_name":"a","item_quantity":"1","unit_price":"1"}]', '2016-03-31 10:41:28', '1.00', 9991827, 3),
(256, 9, '[{"item_name":"a","item_quantity":"1","unit_price":"1"}]', '2016-03-31 10:53:33', '1.00', 9991827, 3),
(257, 9, '[{"item_name":"a","item_quantity":"1","unit_price":"1"}]', '2016-03-31 10:56:22', '1.00', 9991827, 3),
(258, 9, '[{"item_name":"a","item_quantity":"1","unit_price":"1"},{"item_name":"t","item_quantity":"1","unit_price":"1"}]', '2016-03-31 11:00:52', '2.00', 9991827, 3),
(259, 9, '[{"item_name":"a","item_quantity":"1","unit_price":"1"},{"item_name":"t","item_quantity":"1","unit_price":"1"},{"item_name":"h","item_quantity":"2","unit_price":"1"}]', '2016-03-31 11:07:50', '4.00', 9991827, 2),
(260, 6, '[{"unit_price":"20","item_quantity":"1","item_name":"coffee"}]', '2016-03-31 12:46:10', '20.00', 9991827, 2),
(261, 6, '[{"unit_price":"20","item_quantity":"1","item_name":"cup"}]', '2016-03-31 12:54:52', '20.00', 8385527, 2),
(262, 6, '[{"unit_price":"70","item_quantity":"1","item_name":"abc"}]', '2016-03-31 12:56:33', '70.00', 8385527, 2),
(263, 6, '[{"unit_price":"70","item_quantity":"1","item_name":"vdc"}]', '2016-03-31 12:59:11', '70.00', 8385527, 1),
(264, 10, '[{"item_name":"1 bag of rice","item_quantity":"1","unit_price":"15000"}]', '2016-04-03 02:21:37', '15000.00', 5179118, 1),
(265, 10, '[{"item_name":"bag of rice","item_quantity":"1","unit_price":"10000"}]', '2016-04-04 15:58:46', '10000.00', 8386844, 3),
(266, 10, '[{"item_name":"iPhone ","item_quantity":"1","unit_price":"750"}]', '2016-04-04 23:19:30', '750.00', 8386844, 2),
(267, 5, '[{"unit_price":"1","item_quantity":"1","item_name":"gh"}]', '2016-04-06 05:06:54', '1.00', 2338922, 3),
(268, 10, '[{"item_name":"pair shoe","item_quantity":"1","unit_price":"150000"},{"item_name":"jeans","item_quantity":"2","unit_price":"10000"}]', '2016-04-10 00:14:10', '170000.00', 8386844, 3),
(269, 9, '[{"item_name":"test","item_quantity":"2","unit_price":"2300"}]', '2016-04-14 06:43:58', '4600.00', 1829971, 3),
(270, 9, '[{"item_name":"test","item_quantity":"2","unit_price":"2300"}]', '2016-04-14 06:46:53', '4600.00', 1829971, 3),
(271, 9, '[{"item_name":"test","item_quantity":"2","unit_price":"2300"},{"item_name":"test2","item_quantity":"2","unit_price":"500"}]', '2016-04-14 06:47:30', '5600.00', 1829971, 2),
(272, 6, '[{"unit_price":"566","item_quantity":"2","item_name":"Books"}]', '2016-04-14 09:40:59', '1132.00', 1829971, 3),
(273, 6, '[{"unit_price":"566","item_quantity":"2","item_name":"Books"},{"unit_price":"400","item_quantity":"2","item_name":"Pen"}]', '2016-04-14 09:46:41', '1932.00', 2338922, 1),
(274, 6, '[{"unit_price":"400","item_quantity":"2","item_name":"Books"}]', '2016-04-14 09:50:54', '800.00', 2338922, 1),
(275, 6, '[{"unit_price":"400","item_quantity":"2","item_name":"Books"}]', '2016-04-14 09:53:25', '800.00', 2338922, 2),
(276, 6, '[{"unit_price":"300","item_quantity":"8","item_name":"Pencils"}]', '2016-04-14 09:55:00', '2400.00', 2338922, 1),
(277, 6, '[{"unit_price":"200","item_quantity":"1","item_name":"Chair"}]', '2016-04-14 09:58:36', '200.00', 2338922, 2),
(278, 9, '[{"item_name":"books","item_quantity":"1","unit_price":"100"}]', '2016-04-14 11:39:31', '100.00', 2338922, 1),
(279, 11, '[{"item_name":"rte","item_quantity":"1","unit_price":"25"}]', '2016-04-14 14:34:45', '25.00', 222, 1),
(280, 11, '[{"item_name":"gshhs","item_quantity":"1","unit_price":"25"}]', '2016-04-14 14:35:04', '25.00', 25, 1),
(281, 11, '[{"item_name":"gsgs","item_quantity":"1","unit_price":"25"}]', '2016-04-14 14:54:33', '25.00', 55, 1),
(282, 10, '[{"item_name":"pair of jeans","item_quantity":"1","unit_price":"10000"}]', '2016-04-15 15:04:07', '10000.00', 8386488, 1),
(283, 10, '[{"item_name":"shoes","item_quantity":"1","unit_price":"90"}]', '2016-04-15 19:00:22', '90.00', 8386844, 2),
(284, 6, '[{"item_name":"dvd","item_quantity":"2","unit_price":"100"}]', '2016-04-18 12:15:50', '200.00', 6785189, 2),
(285, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"abc test"}]', '2016-04-19 10:43:44', '100.00', 2338922, 2),
(286, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"ade"}]', '2016-04-19 10:47:17', '100.00', 2338922, 2),
(287, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"ade"},{"unit_price":"100","item_quantity":"1","item_name":"1829972"},{"unit_price":"100","item_quantity":"1","item_name":"gdfg"}]', '2016-04-19 10:50:44', '300.00', 1829972, 1),
(288, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"ade"},{"unit_price":"100","item_quantity":"1","item_name":"1829972"},{"unit_price":"100","item_quantity":"1","item_name":"gdfg"},{"unit_price":"180","item_quantity":"1","item_name":"fgfddd"}]', '2016-04-19 10:51:02', '480.00', 1829972, 1),
(289, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"ade"},{"unit_price":"100","item_quantity":"1","item_name":"1829972"},{"unit_price":"100","item_quantity":"1","item_name":"gdfg"},{"unit_price":"180","item_quantity":"1","item_name":"fgfddd"},{"unit_price":"100","item_quantity":"1","item_name":"fh"}]', '2016-04-19 10:51:25', '580.00', 2338922, 2),
(290, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"hgff"}]', '2016-04-19 10:54:07', '100.00', 2338922, 3),
(291, 6, '[{"unit_price":"100","item_quantity":"1","item_name":"ytref"}]', '2016-04-19 10:56:00', '100.00', 2338922, 2),
(292, 6, '[{"unit_price":"200","item_quantity":"1","item_name":"hffg"}]', '2016-04-19 10:57:09', '200.00', 2338922, 3),
(293, 6, '[{"item_name":"ink","item_quantity":"1","unit_price":"10"}]', '2016-04-20 11:27:59', '10.00', 6785189, 3),
(294, 6, '[{"item_name":"test item","item_quantity":"2","unit_price":"59"}]', '2016-04-20 12:16:04', '118.00', 1829971, 4),
(295, 6, '[{"item_name":"notebook","item_quantity":"1","unit_price":"50"}]', '2016-04-20 12:36:06', '50.00', 1829971, 3),
(298, 6, '[{"unit_price":"1000","item_quantity":"1","item_name":"summary"}]', '2016-04-22 15:25:41', '1000.00', 7444789, 3),
(304, 10, '[{"item_name":"jeans","item_quantity":"1","unit_price":"100"}]', '2016-04-26 15:26:39', '100.00', 8386488, 1),
(310, 10, '[{"item_name":"pair of jeans","item_quantity":"1","unit_price":"100"}]', '2016-05-05 01:37:56', '100.00', 8386844, 2),
(312, 6, '[{"unit_price":"10","item_quantity":"4","item_name":"dsfdsf"},{"unit_price":"20","item_quantity":"6","item_name":"coffee"}]', '2016-05-10 06:39:09', '160.00', 9199681, 2),
(324, 16, '[{"item_name":"books","item_quantity":"1","unit_price":"10"}]', '2016-05-11 12:55:10', '10.00', 6785189, 4),
(325, 17, '[{"item_name":"test","item_quantity":"20","unit_price":"24"}]', '2016-05-12 01:32:39', '480.00', 8386488, 1),
(326, 17, '[{"item_name":"test","item_quantity":"20","unit_price":"24"}]', '2016-05-12 01:33:12', '480.00', 8386844, 2),
(327, 16, '[{"item_name":"test","item_quantity":"1","unit_price":"10"}]', '2016-05-17 06:46:13', '10.00', 7444789, 3),
(328, 16, '[{"item_name":"yest","item_quantity":"1","unit_price":"10"},{"item_name":"test","item_quantity":"2","unit_price":"20"}]', '2016-05-17 06:49:06', '50.00', 7444789, 1),
(329, 19, '[{"item_name":"test","item_quantity":"3","unit_price":"100"}]', '2016-05-17 07:01:05', '300.00', 7444789, 2),
(330, 19, '[{"unit_price":"20","item_quantity":"1","item_name":"book"}]', '2016-05-18 06:26:45', '20.00', 7444789, 1),
(331, 19, '[{"unit_price":"20","item_quantity":"1","item_name":"pen"}]', '2016-05-18 06:29:24', '20.00', 6785189, 1),
(332, 19, '[{"item_name":"test","item_quantity":"2","unit_price":"10"}]', '2016-05-18 06:45:09', '20.00', 6785189, 2),
(333, 16, '[{"item_name":"pen","item_quantity":"1","unit_price":"20"}]', '2016-05-18 09:49:07', '20.00', 1829971, 2),
(334, 16, '[{"item_name":"book","item_quantity":"1","unit_price":"20"}]', '2016-05-18 09:57:06', '20.00', 1829971, 2),
(335, 10, '[{"item_name":"corolla","item_quantity":"2","unit_price":"200"}]', '2016-05-19 00:25:16', '400.00', 8386844, 2),
(336, 10, '[{"item_name":"invoice number 250","item_quantity":"1","unit_price":"500"}]', '2016-05-19 18:13:26', '500.00', 8386844, 2),
(337, 10, '[{"item_name":"jeans","item_quantity":"1","unit_price":"200"}]', '2016-05-21 17:39:54', '200.00', 6765287, 2),
(338, 10, '[{"item_name":"jeans","item_quantity":"1","unit_price":"200"}]', '2016-05-21 17:42:45', '200.00', 4684547, 2),
(339, 5, '[{"unit_price":"12","item_quantity":"1","item_name":"acc"}]', '2016-05-23 09:36:19', '12.00', 6489288, 1),
(340, 5, '[{"unit_price":"10","item_quantity":"1","item_name":"abs"}]', '2016-05-23 09:36:44', '10.00', 6489288, 3),
(341, 20, '[{"unit_price":"1","item_quantity":"1","item_name":"9683"}]', '2016-05-23 10:26:39', '1.00', 6489288, 3),
(342, 20, '[{"unit_price":"1","item_quantity":"1","item_name":"9683"}]', '2016-05-23 10:31:10', '1.00', 6489288, 2),
(343, 21, '[{"unit_price":"100","item_quantity":"1","item_name":"pair of jeans"}]', '2016-05-23 16:12:33', '100.00', 8686844, 1),
(344, 21, '[{"unit_price":"100","item_quantity":"1","item_name":"pair of jeans"},{"unit_price":"200","item_quantity":"1","item_name":"shirt"}]', '2016-05-23 16:13:02', '300.00', 8686844, 1),
(345, 21, '[{"unit_price":"100","item_quantity":"1","item_name":"pair of jeans"},{"unit_price":"200","item_quantity":"1","item_name":"shirt"},{"unit_price":"","item_quantity":"","item_name":""}]', '2016-05-23 16:13:15', '300.00', 8686844, 1),
(346, 22, '[{"unit_price":"100","item_quantity":"1","item_name":"jeans"}]', '2016-05-24 07:37:18', '100.00', 8386488, 1),
(347, 22, '[{"unit_price":"100","item_quantity":"1","item_name":"jeans"}]', '2016-05-24 07:37:23', '100.00', 8386488, 1),
(348, 23, '[{"unit_price":"100","item_quantity":"1","item_name":"pair of Versace jeans"}]', '2016-05-24 10:32:54', '100.00', 8386844, 2),
(349, 24, '[{"item_name":"as","item_quantity":"2","unit_price":"1"}]', '2016-05-24 12:03:06', '2.00', 7444789, 1),
(350, 24, '[{"item_name":"as","item_quantity":"2","unit_price":"1"}]', '2016-05-24 12:07:15', '2.00', 7444789, 2),
(351, 24, '[{"item_name":"1","item_quantity":"3","unit_price":"10"}]', '2016-05-24 12:15:08', '30.00', 7444789, 1),
(352, 20, '[{"unit_price":"1","item_quantity":"1","item_name":"tree"}]', '2016-05-25 05:37:00', '1.00', 6489288, 2),
(353, 20, '[{"unit_price":"10","item_quantity":"1","item_name":"testing "}]', '2016-05-25 05:38:48', '10.00', 6489288, 2),
(354, 20, '[{"unit_price":"12","item_quantity":"1","item_name":"he"},{"unit_price":"21","item_quantity":"1","item_name":"she"}]', '2016-05-25 05:41:39', '33.00', 6489288, 2),
(355, 20, '[{"unit_price":"12","item_quantity":"1","item_name":"testin"}]', '2016-05-25 05:43:56', '12.00', 6489288, 2),
(356, 20, '[{"unit_price":"10","item_quantity":"1","item_name":"treee"}]', '2016-05-25 05:45:06', '10.00', 6489288, 2),
(357, 20, '[{"unit_price":"1","item_quantity":"1","item_name":"reee"}]', '2016-05-25 07:23:28', '1.00', 6489288, 1),
(358, 20, '[{"unit_price":"15","item_quantity":"1","item_name":"second one"}]', '2016-05-25 07:51:49', '15.00', 6489288, 1),
(359, 20, '[{"unit_price":"10","item_quantity":"2","item_name":"test item"}]', '2016-05-25 11:24:06', '20.00', 7444789, 1),
(360, 20, '[{"unit_price":"30","item_quantity":"1","item_name":"testing"}]', '2016-05-25 11:25:14', '30.00', 7444789, 1),
(361, 20, '[{"unit_price":"40","item_quantity":"1","item_name":"testing test"}]', '2016-05-25 11:26:42', '40.00', 6489288, 2),
(362, 26, '[{"unit_price":"100","item_quantity":"1","item_name":"pair of jeans"}]', '2016-05-27 14:54:38', '100.00', 2147483647, 1),
(363, 26, '[{"unit_price":"2000","item_quantity":"1","item_name":"Versace shoes"}]', '2016-05-30 12:53:53', '2000.00', 2147483647, 1),
(364, 29, '[{"unit_price":"50","item_quantity":"5","item_name":"mascara"}]', '2016-05-30 20:20:20', '250.00', 2147483647, 1),
(365, 6, '[{"unit_price":"200","item_quantity":"1","item_name":"Mobile"}]', '2016-06-06 06:43:12', '200.00', 1829971, 1),
(366, 6, '[{"unit_price":"300","item_quantity":"1","item_name":"Watch"}]', '2016-06-06 06:46:20', '300.00', 1829971, 1),
(367, 20, '[{"unit_price":"100","item_quantity":"1","item_name":"yahoo mail "}]', '2016-06-06 10:58:00', '100.00', 6489288, 1),
(368, 20, '[{"unit_price":"20","item_quantity":"2","item_name":"yahoo mail 2"}]', '2016-06-06 11:01:23', '40.00', 6489288, 1),
(369, 20, '[{"unit_price":"100","item_quantity":"1","item_name":"Google "}]', '2016-06-06 11:05:02', '100.00', 6489288, 2),
(370, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail "}]', '2016-06-06 11:05:57', '1000.00', 6489288, 1),
(371, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:18', '1000.00', 6489822, 1),
(372, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:24', '1000.00', 6489822, 1),
(373, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:26', '1000.00', 6489822, 1),
(374, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:30', '1000.00', 6489822, 1),
(375, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:32', '1000.00', 6489822, 1),
(376, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:34', '1000.00', 6489822, 1),
(377, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:48', '1000.00', 6489822, 1),
(378, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"}]', '2016-06-06 11:13:51', '1000.00', 6489822, 1),
(379, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"},{"unit_price":"","item_quantity":"","item_name":""}]', '2016-06-06 11:14:06', '1000.00', 6489822, 1),
(380, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"hotmail"},{"unit_price":"","item_quantity":"","item_name":""}]', '2016-06-06 11:14:09', '1000.00', 6489822, 1),
(381, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"shjshe"}]', '2016-06-06 11:14:56', '1000.00', 6489288, 1),
(382, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"shehshhe"}]', '2016-06-06 11:18:11', '1000.00', 6489288, 1),
(383, 20, '[{"unit_price":"100","item_quantity":"10","item_name":"shehh"}]', '2016-06-06 11:27:02', '1000.00', 6489288, 3),
(384, 20, '[{"unit_price":"1000","item_quantity":"100","item_name":"hsjshk"}]', '2016-06-06 11:28:32', '100000.00', 6489288, 1),
(385, 6, '[{"unit_price":"1000","item_quantity":"2","item_name":"mobile"}]', '2016-06-06 11:35:37', '2000.00', 6489288, 1),
(386, 6, '[{"unit_price":"900","item_quantity":"7","item_name":"Fruits"}]', '2016-06-06 11:37:44', '6300.00', 1167882917, 1),
(387, 6, '[{"unit_price":"400","item_quantity":"2","item_name":"cars"}]', '2016-06-06 11:47:10', '800.00', 6489288, 1),
(388, 6, '[{"unit_price":"200","item_quantity":"4","item_name":"bottles"}]', '2016-06-06 11:48:44', '800.00', 6489288, 2),
(389, 20, '[{"unit_price":"10","item_quantity":"1","item_name":"test item"}]', '2016-06-06 11:50:24', '10.00', 1167882917, 1),
(390, 6, '[{"unit_price":"2000","item_quantity":"2","item_name":"bikes"}]', '2016-06-06 11:53:44', '4000.00', 6489288, 1),
(391, 6, '[{"unit_price":"2000","item_quantity":"2","item_name":"bikes"},{"unit_price":"100","item_quantity":"9","item_name":"flowers"}]', '2016-06-06 11:54:31', '4900.00', 6489288, 1),
(392, 20, '[{"unit_price":"1000","item_quantity":"2","item_name":"testing"}]', '2016-06-06 12:26:34', '2000.00', 1167882917, 1),
(393, 20, '[{"unit_price":"100","item_quantity":"3","item_name":"re testing"}]', '2016-06-06 12:27:28', '300.00', 1167882917, 1),
(394, 20, '[{"unit_price":"30","item_quantity":"2","item_name":"i test"}]', '2016-06-06 12:29:30', '60.00', 1167882917, 1),
(395, 6, '[{"unit_price":"23","item_quantity":"2","item_name":"parrots"}]', '2016-06-06 12:46:31', '46.00', 1167882917, 1),
(396, 6, '[{"unit_price":"23","item_quantity":"2","item_name":"parrots"},{"unit_price":"90","item_quantity":"2","item_name":"gggg"}]', '2016-06-06 12:54:08', '226.00', 1167882917, 1);
INSERT INTO `tblMerchantInvoices` (`id`, `merchantId`, `details`, `dated`, `net_total`, `user_pin`, `status`) VALUES
(397, 20, '[{"unit_price":"7","item_quantity":"1","item_name":"ab"}]', '2016-06-06 13:04:35', '7.00', 2147483647, 1),
(398, 20, '[{"unit_price":"8","item_quantity":"3","item_name":"uuuuu"}]', '2016-06-06 13:09:28', '24.00', 2147483647, 1),
(399, 6, '[{"unit_price":"122","item_quantity":"2","item_name":"irons"}]', '2016-06-07 04:12:07', '244.00', 1167882917, 1),
(400, 6, '[{"unit_price":"122","item_quantity":"4","item_name":"apples"}]', '2016-06-07 04:17:57', '488.00', 1167882917, 1),
(401, 6, '[{"unit_price":"22","item_quantity":"3","item_name":"pepsi"}]', '2016-06-07 04:24:27', '66.00', 1167882917, 1),
(402, 20, '[{"unit_price":"2","item_quantity":"1","item_name":"rewq"}]', '2016-06-07 05:21:23', '2.00', 6489288, 1),
(403, 20, '[{"unit_price":"20","item_quantity":"1","item_name":"treeee"}]', '2016-06-08 07:36:02', '20.00', 9999999, 2),
(404, 30, '[{"unit_price":"10","item_quantity":"01","item_name":"jsjd"}]', '2016-06-08 09:44:41', '10.00', 1215398277, 2),
(405, 6, '[{"unit_price":"12","item_quantity":"2","item_name":"goods"}]', '2016-06-09 04:43:40', '24.00', 1234567890, 2),
(406, 32, '[{"unit_price":"200","item_quantity":"1","item_name":"Versace shirt"}]', '2016-06-10 10:32:19', '200.00', 2147483647, 1),
(407, 32, '[{"unit_price":"50","item_quantity":"1","item_name":"pen"}]', '2016-06-10 10:54:39', '50.00', 2147483647, 1),
(408, 32, '[{"unit_price":"20","item_quantity":"1","item_name":"book"}]', '2016-06-10 10:56:23', '20.00', 2147483647, 1),
(409, 32, '[{"unit_price":"12","item_quantity":"1","item_name":"tres"}]', '2016-06-13 04:31:15', '12.00', 1234567890, 2),
(410, 20, '[{"unit_price":"86","item_quantity":"1","item_name":"tress"}]', '2016-06-13 04:34:15', '86.00', 1234567890, 2),
(411, 20, '[{"unit_price":"20","item_quantity":"1","item_name":"keychain"}]', '2016-06-13 09:38:38', '20.00', 2147483647, 1),
(412, 20, '[{"unit_price":"20","item_quantity":"1","item_name":"ddff"}]', '2016-06-13 09:40:39', '20.00', 2147483647, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblMerchantUsers`
--

CREATE TABLE `tblMerchantUsers` (
  `userId` bigint(20) NOT NULL,
  `merchant_title` varchar(100) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `userPIN` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `api_key` varchar(255) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `currency` varchar(255) DEFAULT NULL,
  `activation` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblMerchantUsers`
--

INSERT INTO `tblMerchantUsers` (`userId`, `merchant_title`, `firstName`, `lastName`, `phoneNumber`, `address`, `city`, `country`, `userPIN`, `email`, `userPassword`, `available`, `api_key`, `role_id`, `currency`, `activation`, `status`) VALUES
(4, 'Naveed', 'Arshad', 'naveed', '090078601', 'F5', 'Islamabad', '226', '1111111111', 'naveed@devdesks.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1, '5eb2b456c91027e581ad8b31728921d', 0, '8', '', 1),
(6, 'Al Jannat Mall', 'Khurram', 'Shehzad', '03438574521', 'F5 Islamabad', 'Islamabad', '226', '4335973', 'Khurram.Cp74@gmail.com', 'a837951a23f133a46c5f3d5d7c1ed6561d083e24', 1, '5b1ef35f9127602d3e6bb70a4ab62fb', 0, '8', '', 0),
(8, '', 'Obiora', 'Okoye', '08030998180', NULL, NULL, '226', '7742421', 'okoyechukwulozie@yahoo.com', 'eaf7c6894787be4f764f36d44669301798770040', 1, '89b130a075898822fc775b8f1faffcb', 0, '1', '', 0),
(9, 'Umer Cash n Cary', 'Umer', 'Afzal', '03335314617', 'Islamabad', 'Islamabad', '226', '9729788', 'umer@devdesks.com', '97fb9bacf141dd36d301425657b6cefad40115e9', 1, 'be514a4af3ff51418dfbfe34476be48', 0, '8', '', 0),
(11, 'Test merchant', 'Test', 'Testing', '555552464', 'Test', 'City', '226', '9117455', 'uvslicad@sharklasers.com', 'aa5eb1771328ea9a8d189d420678d824024bc2df', 1, '505ceeccba3381a7321f1cfc779e6e1', 0, '1', '', 0),
(12, '', 'Osita', 'Nwankwo', '9088843069', NULL, NULL, '226', '6715172', 'otbpictures12@gmail.com', 'ff3d3275e74dcbac2d970eea9ad779212cadda17', 1, 'c8e839345c9ebb7673e40fab061271f', 0, '1', '', 0),
(13, '', 'Osita', 'Nwankwo', '9088843069', NULL, NULL, '226', '1359257', 'nwankwo908@gmail.com', 'c5262fa6fb9e715e7d2cfaf3da63faacd55da1a9', 1, '1ca8823c7ea098f715c67e210961a5f', 0, '1', '', 0),
(14, 'Test', 'Sultan', 'Ali', '3335949626', 'Gf', 'Ff', '226', '7695394', 'sultan@devdesks.com', '657b1dcd67ebf015c9317c321a0ebc3df28b0da5', 1, '7a58be0fb68ca1b241306c3ce585936', 0, '8', '', 0),
(15, 'Nhb', 'Osita', 'Nwankwo', '9088843069', '385 monty ave', 'Union', '226', '7368529', 'nhbmediagroup@gmail.com', '6633098f33b49f36c2bdb0d14a00912bfb8172de', 1, '62c43fb7dc6d00236ff1d44b58d0112', 0, '1', '', 0),
(18, '', 'Umer', 'Khan', '3333333333', NULL, NULL, '226', '8377751', 'umerz@devdesks.com', 'e14dadd275597f81200f4e4059a036a9f23aa8a4', 1, '8a1b2a25bc6f7b269f5fb70bd2e3480', 0, '8', '', 0),
(19, 'Sultan llc', 'Test', 'Test', '3335949626', 'G13', 'Islamabad', '226', '3495833', 'sultan@ideagroupinc.net', '6a49e342fceb744168ee5c0cddacd48731cac311', 1, '95df6c9ec621875e089460f36a8f791', 0, '8', '', 0),
(20, 'NM cash n Carry', 'Nauman', 'Malik', '03335007726', 'F-5 1 Islamanad', 'Islamabad', '226', '2222222222', 'nouman.malik@devdesks.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1, '70c932f0f28137590e18dd1d4a9ca2b', 0, '8', '', 1),
(24, 'Sultan tech', 'Sultan', 'Khan', '3335949626', 'Ga', 'New york', '226', '2897975', 'alisultan482@gmail.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1, '1aa49d380276550eafdcd96e2dded5e', 0, '8', '', 1),
(25, '', 'Ugochukwu', 'Mbanugo', '9732890544', NULL, NULL, '226', '7885546336', 'umbanugo@gmail.com', '0195f71ccaf57fcdf2492de67e4cd69ece8cbd28', 1, '48672d54a48b293520fb228cec142098', 0, '1', '', 0),
(26, 'Ugo and Ugo motors', 'Ugochukwu', 'Mbanugo', '9732890544', '4000 Bordentown ave', 'Sayreville', '226', '3858727631', 'mbanugou@gmail.com', '42704f6c73679f11b3d8dee5a6b4a647de266c45', 1, '4d66570782497c2d94276a674d1e7aa6', 0, '8', '', 0),
(27, 'ABC Merchants', 'Nauman', 'Malik', '03335007726', 'F -5', 'Islamabad', '226', '6315813397', 'nauman541@yopmail.com', '08bf6ca3fcdcb44b72fd44694748fab054dab19f', 1, '2d301e5ea511776285f14d7247bbc4a1', 0, '2', '', 0),
(28, '', 'nm', 'mn', '1234', NULL, NULL, '226', '6543819454', 'nouman@yopmail.com', 'd8c41f8fa5403ecd97e1937281361fbe338c68cb', 1, '9d80706dacc8da3317784a3f7cde9d87', 0, '8', '', 0),
(29, 'beauty INC', 'Ijeoma', 'Mbanugo', '9082563708', '33 Bryant avenue', 'Bloomfield', '226', '8156949776', 'ijeluv1@gmail.com', '93a85790347628bbfa9c785dde4b7deee12f7a35', 1, 'bc45a5b32dbb2ce8416cd77112ac6c98', 0, '8', '', 0),
(30, 'sultan', 'sultan', 'ali', '555555555', 'isb', 'islamabad', '162', '2413562882', 'msn@yopmail.com', '6048a71fb8922dd264b5fd2be476ba489663168a', 1, '5d1231a728f115547f9bdf1f189d63d8', 0, '2', '', 0),
(31, 'My Shop', 'Marrium', 'Abid', '939874', 'Islamabad', 'Islamabad', '162', '1114153847', 'marrium@devdesks.com', '4d0442f812df9276c7e549fd22ea774448f4c85f', 1, 'e2a28ce6397ef91a9f0df80b0bcb78ff', 0, '2', '', 0),
(32, 'Ugo and Ugo motors', 'Ugochukwu', 'Mbanugo', '9732890544', '33 Bryant Ave', 'Bloomfield', '226', '6825673587', 'saosimeon@yahoo.com', '74c92134b13c3114b5d973512d082cd73722f969', 1, 'dd1e700d9b7875570d717021f21e734d', 0, '8', '', 0),
(33, 'Mr David Anaghara', 'David', 'Anaghara', '+2348033406123', '5 S. O. Williams Crescent Utako', 'Abuja', '156', '6841195695', 'dlaface@yahoo.com', '3aad3a2fb71589078ad72b3052043df52f7db55c', 1, 'f807c500cde22e5bf8d90b5c5d31f855', 0, '1', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblNotificationHistory`
--

CREATE TABLE `tblNotificationHistory` (
  `id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblNotifications`
--

CREATE TABLE `tblNotifications` (
  `notification_id` bigint(20) NOT NULL,
  `notification_type` varchar(100) NOT NULL,
  `from_user_id` bigint(20) NOT NULL,
  `to_user_id` bigint(20) NOT NULL,
  `notification_status` tinyint(4) NOT NULL DEFAULT '0',
  `notification_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblPromotionCoupons`
--

CREATE TABLE `tblPromotionCoupons` (
  `promotionCouponId` bigint(20) NOT NULL,
  `promotionId` bigint(20) NOT NULL,
  `couponCode` varchar(255) DEFAULT NULL,
  `couponPrize` float(10,2) DEFAULT '0.00',
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblPromotionCoupons`
--

INSERT INTO `tblPromotionCoupons` (`promotionCouponId`, `promotionId`, `couponCode`, `couponPrize`, `status`) VALUES
(1, 2, '23', 123.00, 0),
(2, 3, 'Car', 210000.00, 0),
(3, 4, '200', 1000.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblPromotions`
--

CREATE TABLE `tblPromotions` (
  `promotionId` bigint(20) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblPromotions`
--

INSERT INTO `tblPromotions` (`promotionId`, `title`, `description`, `image`, `status`, `created`) VALUES
(1, 'Coke', 'Sample description for coke promotion.', 'Desert.jpg', 0, '2016-06-01 16:21:15'),
(2, 'test promotion', 'this is about promotion1', 'Hydrangeas.jpg', 0, '2016-04-08 14:30:59'),
(3, 'Test Sweep', 'kjfdsfjh', 'coupan-200.png', 0, '2016-04-18 12:05:41'),
(4, 'Sale test', 'abscdhd', 'Chrysanthemum1.jpg', 0, '2016-04-28 10:48:16'),
(5, 'df', 'dfdf', 'Penguins.jpg', 0, '2016-06-03 16:38:01');

-- --------------------------------------------------------

--
-- Table structure for table `tblPromotionsAccount`
--

CREATE TABLE `tblPromotionsAccount` (
  `PromotionsAccountId` bigint(20) NOT NULL,
  `accountTitle` varchar(255) DEFAULT NULL,
  `accountNumber` varchar(255) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblPromotionsAccount`
--

INSERT INTO `tblPromotionsAccount` (`PromotionsAccountId`, `accountTitle`, `accountNumber`, `available`, `status`) VALUES
(1, 'Promotions Account', '12345678901', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblProviders`
--

CREATE TABLE `tblProviders` (
  `id` int(11) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblProviders`
--

INSERT INTO `tblProviders` (`id`, `type`, `name`, `status`) VALUES
(1, 1, 'Company A', 1),
(2, 2, 'Company B', 1),
(3, 3, 'C Company', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblProviderTypes`
--

CREATE TABLE `tblProviderTypes` (
  `id` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblProviderTypes`
--

INSERT INTO `tblProviderTypes` (`id`, `type`, `status`) VALUES
(1, 'Utility', 1),
(2, 'Telco', 1),
(3, 'Isp', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblrefund`
--

CREATE TABLE `tblrefund` (
  `id` bigint(20) NOT NULL,
  `invoice_no` bigint(20) NOT NULL,
  `uid` bigint(20) NOT NULL,
  `merchant_id` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `datetime` datetime NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblrefund`
--

INSERT INTO `tblrefund` (`id`, `invoice_no`, `uid`, `merchant_id`, `message`, `datetime`, `status`) VALUES
(1, 324, 134, 16, 'please refund me my cash ', '2016-05-11 17:58:21', 2),
(2, 164, 125, 9, 'refund my payment', '2016-05-12 12:00:14', 1),
(3, 284, 134, 6, 'yyu', '2016-05-12 14:17:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblRoles`
--

CREATE TABLE `tblRoles` (
  `role_id` bigint(20) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `role_create` tinyint(4) NOT NULL DEFAULT '0',
  `role_update` tinyint(4) NOT NULL DEFAULT '0',
  `role_delete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblRoles`
--

INSERT INTO `tblRoles` (`role_id`, `role_name`, `role_create`, `role_update`, `role_delete`) VALUES
(5, 'Project Manager', 1, 0, 0),
(7, 'adas', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblScratchCards`
--

CREATE TABLE `tblScratchCards` (
  `cardId` bigint(20) NOT NULL,
  `cardNumber` bigint(20) NOT NULL,
  `cardAmount` decimal(10,2) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblScratchCards`
--

INSERT INTO `tblScratchCards` (`cardId`, `cardNumber`, `cardAmount`, `available`) VALUES
(1, 1122334455667788, '5000.00', 1),
(2, 1122334455667799, '5000.00', 1),
(3, 987654321098765, '5000.00', 1),
(4, 1234567890123456, '100000.00', 1),
(5, 1122334455667700, '10000.00', 1),
(6, 8888888888888888, '10000.00', 1),
(7, 9998887776666888, '5000.00', 1),
(8, 999988887777, '5000.00', 1),
(9, 2222222222222222, '999.00', 1),
(10, 1, '22323.00', 1),
(11, 134, '0.00', 1),
(12, 23213, '99999999.99', 1),
(13, 56, '66666666.00', 1),
(14, 4569879879654329, '10000.00', 0),
(15, 7777777333445673, '2000000.00', 1),
(16, 111111111111111, '100000.00', 0),
(17, 1234567812345678, '10000.00', 0),
(18, 1234567812345679, '100.00', 0),
(19, 1234567912345679, '100.00', 0),
(20, 6321825431893211, '1000000.00', 1),
(21, 99, '100000.00', 0),
(22, 9999999999999900, '100000.00', 0),
(23, 5672319829567421, '10000.00', 0),
(24, 6666666666666667, '10000.00', 1),
(25, 1122334455667789, '100.00', 0),
(26, 9999999999999991, '10000.00', 0),
(27, 4333, '90000.00', 1),
(28, 1999999999999990, '90000.00', 0),
(29, 1122334455667111, '10000.00', 0),
(30, 1122334455667777, '10000.00', 0),
(31, 22222, '10000.00', 1),
(32, 3333333333333333, '10000.00', 1),
(33, 1000000000000000, '3000.00', 1),
(34, 111111111111122, '10000.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblStatus`
--

CREATE TABLE `tblStatus` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblStatus`
--

INSERT INTO `tblStatus` (`id`, `name`) VALUES
(1, 'pending'),
(2, 'approved'),
(3, 'reject'),
(4, 'Transaction Done');

-- --------------------------------------------------------

--
-- Table structure for table `tblTransactions`
--

CREATE TABLE `tblTransactions` (
  `transId` bigint(20) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `devId` text NOT NULL,
  `cr` decimal(10,2) NOT NULL,
  `dr` decimal(10,2) NOT NULL,
  `gps` text NOT NULL,
  `refId` bigint(20) NOT NULL,
  `type` varchar(50) NOT NULL,
  `mode` varchar(50) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `auth_code` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `transaction_code` varchar(100) DEFAULT NULL,
  `transaction_status` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblTransactions`
--

INSERT INTO `tblTransactions` (`transId`, `userId`, `devId`, `cr`, `dr`, `gps`, `refId`, `type`, `mode`, `transaction_id`, `auth_code`, `description`, `created`, `transaction_code`, `transaction_status`) VALUES
(251, 125, 'APA91bFsplc5lnq1Kr6T5Jh5iMUUjokKo8CoZYTuoGToC706-Z5F3r6RdcSYKXeaLSNLtKFzyrsyXgpzJAYafXuiGCQUpdE61blgiW-PjsoE7zLKk5iYa7V-HuRA3RYyKcabYccEtbjq', '10000.00', '0.00', '0', 5, 'AC', 'SC', '', '', '', '2016-03-11 09:26:07', NULL, NULL),
(500, 134, 'APA91bEs-zQYu8onMllR5aGoCUuIAN5Gmrq3NkCGac5M5DLrOyXNx1p9fIgoLS9b_33s7SS_4eMME_085WVST_yU211Zs-Q29RWxhVcg5s8CCbcJouNh7Rw1Jr_t6AhMPOkCDM2oy3lC', '50000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-05-10 12:53:03', NULL, NULL),
(600, 122, 'APA91bGnBj1Twpepo2xFzm7FTmwWDlRCNtoGTwkXR-9jlmTDvnprkoDosVCq4YJbMcTT0jWEnWAo2SxMmC-E5RLE9DfvgEz0qDSw5vRrS_ILLHpm-LhcbdhydRr2os8gGjF6IMas72G-', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 08:07:42', NULL, NULL),
(601, 123, 'APA91bGtjqDuChKDA4Si2B-dekKbKYu8k6lX-Zcaqx6sCZjyC7NRba9XaGjq_mdu9uGcdpPJgRcp7u1HoksjZ-2WchlWS9ICzaQiUpgENT31BO5MIFBey1GR9mWuttWcSFqUScj2fvrM', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 08:09:20', NULL, NULL),
(604, 124, 'APA91bHK01OJnke8lYZ_-xw3MN1XKPw15s9ft6gB0pMv4TbC-fvGcZzPDIhSuwC_jJN7UsT2Tcn8O-SVisA91wROzrhGsZm95ovRCJ0TPkxOLTsy429FfIubfRtc7F_TWUaXfBYxwAsa', '15000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 08:11:19', NULL, NULL),
(607, 126, 'APA91bHeNnNKrWWjo-foIT4Nn2vr7ZuUX4cLaRhx2Ihb1oPFyUqB3LIHv0pmqaMzYIRkRxBWgEjBv8RbfKSRLLIMiG1PZfrjxitrctht26t_KdD2HXNERkTvlSjzabCX5viX61JAR5_O', '15000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 08:42:46', NULL, NULL),
(608, 129, 'APA91bHt2gTt9YLy1Q1kxz4-AQXJrOgXMa4ABQg4dVPo-YyrLKZY5LWNPWy7gp0HaM96WJWpUv8Zj5N24PefNUqLU4GFMPUkrsod9kD5i6fIJ3x8s4_Zp3BkhAgvrutp_WyuGc-BAYJ4', '15000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 08:43:47', NULL, NULL),
(609, 132, 'APA91bG2TWlaJ6X1Zuv0BgqyXlBstdjsdZ4LtvU3L8AHjUhV1gnfZUqALzD65rQIA1fCgkC6JgICKMF41UE7axHvxAOWEizQfABvdTlTwEXbjtcabo6vW-wpEfZVoaIISF8jJ5-UE5mb', '15000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 08:53:39', NULL, NULL),
(610, 135, 'APA91bEhsPEuT-_w3B8psfHkKSg57jdd_KxxvxJqjbeMcSNxIjmwP4k6ZamnzffwWj4cOFs0rBp6q3fZgQS0ApFpwQYETEdYgc9tQb2sSqDerTdaLptCZ_uOvu_32doQne_16m-iWAK_', '15000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 08:54:37', NULL, NULL),
(612, 137, 'APA91bEs-zQYu8onMllR5aGoCUuIAN5Gmrq3NkCGac5M5DLrOyXNx1p9fIgoLS9b_33s7SS_4eMME_085WVST_yU211Zs-Q29RWxhVcg5s8CCbcJouNh7Rw1Jr_t6AhMPOkCDM2oy3lC', '15000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 09:10:58', NULL, NULL),
(614, 143, '123456', '50000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-05-10 12:11:51', NULL, NULL),
(615, 142, '123456', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 12:50:05', NULL, NULL),
(616, 141, '123456', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 12:51:38', NULL, NULL),
(617, 140, '123456', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 12:52:27', NULL, NULL),
(618, 139, '123456', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 12:53:21', NULL, NULL),
(619, 138, '123456', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 12:54:38', NULL, NULL),
(620, 131, '123456', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 12:57:01', NULL, NULL),
(621, 130, '123456', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-04-26 12:57:48', NULL, NULL),
(728, 145, 'd5dabd45fc6a14fbf624bfb2002286af808344d21f70ffd54148f4498a5ee468', '15000.00', '0.00', '0', 21, 'AC', 'SC', '', '', '', '2016-05-24 06:09:43', NULL, NULL),
(884, 1, '123456', '15000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-05-24 06:09:47', NULL, NULL),
(1038, 131, 'b337b17a29d50410bc5f1e09438626b2d3a5c187f1012a3e8a3fad3077aa6b72', '0.00', '1000.00', '0', 177, 'TR', 'TCP', '', '', 'Transfer Credit To Imali User', '2016-05-24 07:46:48', NULL, NULL),
(1039, 177, 'b337b17a29d50410bc5f1e09438626b2d3a5c187f1012a3e8a3fad3077aa6b72', '1000.00', '0.00', '0', 131, 'TR', 'TCP', '', '', 'Transfer Credit To Imali User', '2016-05-24 07:46:48', NULL, NULL),
(1040, 143, '0', '0.00', '10.00', '0', 1, 'TR', 'CF', '', '', '0', '2016-05-24 08:42:19', 'kVlS21WaSZ0P', NULL),
(1041, 143, '0', '10.00', '0.00', '0', 143, 'TR', 'CF', '', '', '0', '2016-05-24 08:42:19', 'kVlS21WaSZ0P', NULL),
(1042, 143, '0', '0.00', '20.00', '0', 1, 'TR', 'CF', '', '', '0', '2016-05-24 08:56:09', 'BiEr87pIzmHT', NULL),
(1043, 143, '0', '20.00', '0.00', '0', 143, 'TR', 'CF', '', '', '0', '2016-05-24 08:56:09', 'BiEr87pIzmHT', NULL),
(1044, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '0.00', '122.00', '0', 4, 'TR', 'CF', '', '', 'dgfgdfg', '2016-05-24 09:28:46', 'DurKiRJStuWh', NULL),
(1045, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '122.00', '0.00', '0', 125, 'TR', 'CF', '', '', 'dgfgdfg', '2016-05-24 09:28:46', 'DurKiRJStuWh', NULL),
(1046, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '0.00', '523.00', '0', 4, 'TR', 'CF', '', '', 'cvcxvc', '2016-05-24 09:29:01', '0e4b0WzWI0fq', NULL),
(1047, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '523.00', '0.00', '0', 125, 'TR', 'CF', '', '', 'cvcxvc', '2016-05-24 09:29:01', '0e4b0WzWI0fq', NULL),
(1048, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '0.00', '20.00', '0', 6, 'TR', 'CF', '', '', 'fggfg', '2016-05-24 09:40:30', 'cPRGSvYdZnQs', NULL),
(1049, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '20.00', '0.00', '0', 125, 'TR', 'CF', '', '', 'fggfg', '2016-05-24 09:40:30', 'cPRGSvYdZnQs', NULL),
(1050, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '0.00', '35.00', '0', 4, 'TR', 'CF', '', '', 'test', '2016-05-24 09:41:05', 'U6CtgLGrvF5k', NULL),
(1051, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '35.00', '0.00', '0', 125, 'TR', 'CF', '', '', 'test', '2016-05-24 09:41:05', 'U6CtgLGrvF5k', NULL),
(1052, 131, 'APA91bGRmtxVJC3xbVZvLHpHvfEcPlHlbaebe8Y3tq3CNJ1dSL5SpU-RsFkkjMXIW4z_RMSwG_zgMtUior4fAy83ICaToFlUa6irYE2ReRUnnBzdK2gEWq204tmMEbCI3mWSNtQvaIC1', '0.00', '5000.00', '0', 178, 'TR', 'TCP', '', '', '0', '2016-05-24 10:31:37', NULL, NULL),
(1053, 178, 'APA91bGRmtxVJC3xbVZvLHpHvfEcPlHlbaebe8Y3tq3CNJ1dSL5SpU-RsFkkjMXIW4z_RMSwG_zgMtUior4fAy83ICaToFlUa6irYE2ReRUnnBzdK2gEWq204tmMEbCI3mWSNtQvaIC1', '5000.00', '0.00', '0', 131, 'TR', 'TCP', '', '', '0', '2016-05-24 10:31:37', NULL, NULL),
(1054, 131, '0', '0.00', '100.00', '0', 23, 'TR', 'MIB', '', '', '0', '2016-05-24 10:33:30', '51452', NULL),
(1055, 131, '0', '100.00', '0.00', '0', 131, 'TR', 'MIB', '', '', '0', '2016-05-24 10:33:30', '51452', NULL),
(1056, 145, '0', '0.00', '12.00', '0', 2, 'TR', 'CF', '', '', 'vhhh', '2016-05-24 11:37:06', '3Jc6glx94AGy', NULL),
(1057, 145, '0', '12.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'vhhh', '2016-05-24 11:37:06', '3Jc6glx94AGy', NULL),
(1058, 135, '0', '0.00', '2.00', '0', 24, 'TR', 'MIB', '', '', '0', '2016-05-24 12:07:37', '7730', NULL),
(1059, 135, '0', '2.00', '0.00', '0', 135, 'TR', 'MIB', '', '', '0', '2016-05-24 12:07:38', '7730', NULL),
(1060, 145, '0', '0.00', '10.00', '0', 8, 'TR', 'CF', '', '', 'donation', '2016-05-24 12:24:45', 'a9R0OQKTrNXJ', NULL),
(1061, 145, '0', '10.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'donation', '2016-05-24 12:24:45', 'a9R0OQKTrNXJ', NULL),
(1062, 145, '0', '0.00', '5.00', '0', 2, 'TR', 'CF', '', '', 'ew', '2016-05-24 12:55:43', '1jS8aS5my3aK', NULL),
(1063, 145, '0', '5.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'ew', '2016-05-24 12:55:43', '1jS8aS5my3aK', NULL),
(1078, 143, '0', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-05-24 13:28:11', '6BvQB5vXaLxZ', NULL),
(1079, 143, '0', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-05-24 13:28:11', '6BvQB5vXaLxZ', NULL),
(1080, 145, '0', '0.00', '10.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:07', '82501', NULL),
(1081, 145, '0', '10.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:08', '82501', NULL),
(1082, 145, '0', '0.00', '12.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:10', '13612', NULL),
(1083, 145, '0', '12.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:10', '13612', NULL),
(1084, 145, '0', '0.00', '10.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:12', '12878', NULL),
(1085, 145, '0', '10.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:12', '12878', NULL),
(1086, 145, '0', '0.00', '1.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:15', '33483', NULL),
(1087, 145, '0', '1.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:15', '33483', NULL),
(1088, 145, '0', '0.00', '33.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:21', '80056', NULL),
(1089, 145, '0', '33.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:21', '80056', NULL),
(1090, 145, '0', '0.00', '1.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:26', '39659', NULL),
(1091, 145, '0', '1.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-05-25 05:53:26', '39659', NULL),
(1092, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '0.00', '10.00', '0', 4, 'TR', 'CF', '', '', 'grgrg', '2016-05-25 08:19:32', '6fgTmb8HcliL', NULL),
(1093, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '10.00', '0.00', '0', 125, 'TR', 'CF', '', '', 'grgrg', '2016-05-25 08:19:32', '6fgTmb8HcliL', NULL),
(1094, 145, '0', '0.00', '8.00', '0', 2, 'TR', 'CF', '', '', 'eight', '2016-05-25 08:21:09', 'YbFwXaVLbv7V', NULL),
(1095, 145, '0', '8.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'eight', '2016-05-25 08:21:09', 'YbFwXaVLbv7V', NULL),
(1096, 145, '0', '0.00', '40.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-05-25 11:26:57', '4551', NULL),
(1097, 145, '0', '40.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-05-25 11:26:57', '4551', NULL),
(1098, 145, '0', '0.00', '12.00', '0', 2, 'TR', 'CF', '', '', 'tes', '2016-05-25 11:29:21', 'L01AZ5WvsARP', NULL),
(1099, 145, '0', '12.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'tes', '2016-05-25 11:29:21', 'L01AZ5WvsARP', NULL),
(1100, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '0.00', '10.00', '0', 2, 'TR', 'CF', '', '', 'ala bala', '2016-05-26 10:01:22', 'nqiEBgWey4Cs', NULL),
(1101, 125, 'cdFPskAeqb8:APA91bFdbV4BmJ-oa6HGQIisJM9hVQCtqs3PmFCC6oa13MKDxN87VpnwGPiZK9WIegsPFbaK_PiaO42mNRFDyEq-k6g0Shgs1V_AuMYF81zZ8Jn8lHqCE9wDgma1_QQSN7JdC4Q5zdBS', '10.00', '0.00', '0', 125, 'TR', 'CF', '', '', 'ala bala', '2016-05-26 10:01:22', 'nqiEBgWey4Cs', NULL),
(1102, 145, '0', '0.00', '2.00', '0', 2, 'TR', 'CF', '', '', 'f', '2016-05-26 12:24:17', 'qlJJcDYtO5QA', NULL),
(1103, 145, '0', '2.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'f', '2016-05-26 12:24:17', 'qlJJcDYtO5QA', NULL),
(1104, 145, '0', '0.00', '6.00', '0', 15, 'TR', 'CF', '', '', 'accoted invites', '2016-05-26 12:26:04', 'qYN4kl2Nc0s6', NULL),
(1105, 145, '0', '6.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'accoted invites', '2016-05-26 12:26:04', 'qYN4kl2Nc0s6', NULL),
(1106, 180, 'b9daa87c035f8a12e0661b330b14600f47dd101fc76480ea43e135fa621a4580', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-05-27 05:49:16', NULL, NULL),
(1107, 180, 'b9daa87c035f8a12e0661b330b14600f47dd101fc76480ea43e135fa621a4580', '10000.00', '0.00', '0', 17, 'AC', 'SC', '', '', '', '2016-05-27 05:49:52', NULL, NULL),
(1108, 152, 'c323316920fd9a1ff01ad49bb2a0038beafe4a5e5cd567bc65ff5eb0dda42aac', '50.00', '0.00', '0', 0, 'AC', 'CC', '2259112691', '54BW5O', 'Balance Added via Credit Card', '2016-05-27 22:54:09', NULL, NULL),
(1109, 152, '0', '0.00', '10.00', '0', 4, 'TR', 'CHC', '', '', 'mine', '2016-05-27 22:55:23', 'RsMBC1GknMZ2', NULL),
(1110, 152, '0', '10.00', '0.00', '0', 152, 'TR', 'CHC', '', '', 'mine', '2016-05-27 22:55:23', 'RsMBC1GknMZ2', NULL),
(1111, 152, 'c323316920fd9a1ff01ad49bb2a0038beafe4a5e5cd567bc65ff5eb0dda42aac', '90.00', '0.00', '0', 0, 'AC', 'CC', '2259112777', 'EZLJK6', 'Balance Added via Credit Card', '2016-05-27 23:00:22', NULL, NULL),
(1112, 152, 'c323316920fd9a1ff01ad49bb2a0038beafe4a5e5cd567bc65ff5eb0dda42aac', '90.00', '0.00', '0', 0, 'AC', 'CC', '2259112896', '2QJGI6', 'Balance Added via Credit Card', '2016-05-27 23:07:28', NULL, NULL),
(1113, 152, 'c323316920fd9a1ff01ad49bb2a0038beafe4a5e5cd567bc65ff5eb0dda42aac', '99.00', '0.00', '0', 0, 'AC', 'CC', '2259112987', 'YJ0EKP', 'Balance Added via Credit Card', '2016-05-27 23:19:04', NULL, NULL),
(1114, 152, '0', '0.00', '20.00', '0', 3, 'TR', 'CHC', '', '', 'yeast', '2016-05-27 23:20:15', 'RMA6IIOrPgPE', NULL),
(1115, 152, '0', '20.00', '0.00', '0', 152, 'TR', 'CHC', '', '', 'yeast', '2016-05-27 23:20:15', 'RMA6IIOrPgPE', NULL),
(1116, 152, '0', '0.00', '50.00', '0', 4, 'TR', 'CHC', '', '', '', '2016-05-27 23:21:04', 'MbvhEX9Rf3fU', NULL),
(1117, 152, '0', '50.00', '0.00', '0', 152, 'TR', 'CHC', '', '', '', '2016-05-27 23:21:04', 'MbvhEX9Rf3fU', NULL),
(1118, 152, 'c323316920fd9a1ff01ad49bb2a0038beafe4a5e5cd567bc65ff5eb0dda42aac', '99.00', '0.00', '0', 0, 'AC', 'CC', '2259113745', '2K326S', 'Balance Added via Credit Card', '2016-05-28 01:06:39', NULL, NULL),
(1119, 152, '', '0.00', '200.00', '', 152, 'TR', 'CO', '', '', '', '2016-05-28 13:17:57', '40922715202905', NULL),
(1120, 152, '', '200.00', '0.00', '', 152, 'TR', 'CO', '', '', '', '2016-05-28 13:17:57', '40922715202905', NULL),
(1121, 125, '', '0.00', '6000.00', '', 125, 'TR', 'CO', '', '', '', '2016-06-03 06:42:39', '57746371268294', NULL),
(1122, 125, '', '6000.00', '0.00', '', 125, 'TR', 'CO', '', '', '', '2016-06-03 06:42:39', '57746371268294', NULL),
(1123, 125, 'fEmX-LBLd6c:APA91bHwAGUE89cEBkigGWwmnNMGIc6njdk5E3schEQLsf9XmPt3gOUW4tMMtGKcXYZMVTwTqduxeGJHMldsAurYTPR2qFLzhj_4bEPHflyvqdCvtTfULgdN_Nx0AhHXCsTdQbi5eWIW', '0.00', '500.00', '0', 1, 'TR', 'CHC', '', '', 'vzzz', '2016-06-03 06:57:34', 'Ukkc6Ge9dyxv', NULL),
(1124, 125, 'fEmX-LBLd6c:APA91bHwAGUE89cEBkigGWwmnNMGIc6njdk5E3schEQLsf9XmPt3gOUW4tMMtGKcXYZMVTwTqduxeGJHMldsAurYTPR2qFLzhj_4bEPHflyvqdCvtTfULgdN_Nx0AhHXCsTdQbi5eWIW', '500.00', '0.00', '0', 125, 'TR', 'CHC', '', '', 'vzzz', '2016-06-03 06:57:35', 'Ukkc6Ge9dyxv', NULL),
(1125, 125, 'fEmX-LBLd6c:APA91bHwAGUE89cEBkigGWwmnNMGIc6njdk5E3schEQLsf9XmPt3gOUW4tMMtGKcXYZMVTwTqduxeGJHMldsAurYTPR2qFLzhj_4bEPHflyvqdCvtTfULgdN_Nx0AhHXCsTdQbi5eWIW', '0.00', '1000.00', '0', 1, 'TR', 'CHC', '', '', 'Dhdh', '2016-06-03 07:00:10', '37KBd3R8e2dy', NULL),
(1126, 125, 'fEmX-LBLd6c:APA91bHwAGUE89cEBkigGWwmnNMGIc6njdk5E3schEQLsf9XmPt3gOUW4tMMtGKcXYZMVTwTqduxeGJHMldsAurYTPR2qFLzhj_4bEPHflyvqdCvtTfULgdN_Nx0AhHXCsTdQbi5eWIW', '1000.00', '0.00', '0', 125, 'TR', 'CHC', '', '', 'Dhdh', '2016-06-03 07:00:11', '37KBd3R8e2dy', NULL),
(1127, 125, 'fEmX-LBLd6c:APA91bHwAGUE89cEBkigGWwmnNMGIc6njdk5E3schEQLsf9XmPt3gOUW4tMMtGKcXYZMVTwTqduxeGJHMldsAurYTPR2qFLzhj_4bEPHflyvqdCvtTfULgdN_Nx0AhHXCsTdQbi5eWIW', '0.00', '500.00', '0', 1, 'TR', 'CHC', '', '', 'Hz vdb', '2016-06-03 07:12:29', '7n8hYNOrCtQ8', NULL),
(1128, 125, 'fEmX-LBLd6c:APA91bHwAGUE89cEBkigGWwmnNMGIc6njdk5E3schEQLsf9XmPt3gOUW4tMMtGKcXYZMVTwTqduxeGJHMldsAurYTPR2qFLzhj_4bEPHflyvqdCvtTfULgdN_Nx0AhHXCsTdQbi5eWIW', '500.00', '0.00', '0', 125, 'TR', 'CHC', '', '', 'Hz vdb', '2016-06-03 07:12:29', '7n8hYNOrCtQ8', NULL),
(1129, 125, '0', '0.00', '44.00', '0', 5, 'TR', 'MIB', '', '', '0', '2016-06-03 09:28:09', '95472', NULL),
(1130, 125, '0', '44.00', '0.00', '0', 125, 'TR', 'MIB', '', '', '0', '2016-06-03 09:28:09', '95472', NULL),
(1131, 125, '', '0.00', '80.00', '', 125, 'TR', 'CO', '', '', '', '2016-06-03 12:22:32', '15088608991354', NULL),
(1132, 125, '', '80.00', '0.00', '', 125, 'TR', 'CO', '', '', '', '2016-06-03 12:22:32', '15088608991354', NULL),
(1133, 182, 'a02fbcea7fc83356ef07cfab7e47f0dda17dfdbdf87f48d8ebf2001f35e241f1', '20.00', '0.00', '0', 0, 'AC', 'CC', '2259355338', 'VEWDTS', 'Balance Added via Credit Card', '2016-06-03 13:25:05', NULL, NULL),
(1134, 182, 'a02fbcea7fc83356ef07cfab7e47f0dda17dfdbdf87f48d8ebf2001f35e241f1', '90.00', '0.00', '0', 0, 'AC', 'CC', '2259355681', 'OKODPW', 'Balance Added via Credit Card', '2016-06-03 13:44:23', NULL, NULL),
(1135, 182, 'a02fbcea7fc83356ef07cfab7e47f0dda17dfdbdf87f48d8ebf2001f35e241f1', '0.00', '20.00', '0', 1, 'TR', 'DC', '', '', 'test', '2016-06-03 14:40:18', 'DCR#575196d2c5020', NULL),
(1136, 182, 'a02fbcea7fc83356ef07cfab7e47f0dda17dfdbdf87f48d8ebf2001f35e241f1', '20.00', '0.00', '0', 182, 'TR', 'DC', '', '', 'test', '2016-06-03 14:40:18', 'DCR#575196d2c5020', NULL),
(1137, 180, '', '0.00', '100.00', '', 180, 'TR', 'CO', '', '', '', '2016-06-03 14:45:35', '14646104602143', NULL),
(1138, 180, '', '100.00', '0.00', '', 180, 'TR', 'CO', '', '', '', '2016-06-03 14:45:35', '14646104602143', NULL),
(1139, 182, '', '0.00', '10.00', '', 182, 'TR', 'CO', '', '', '', '2016-06-03 14:47:03', '10565624530427', NULL),
(1140, 182, '', '10.00', '0.00', '', 182, 'TR', 'CO', '', '', '', '2016-06-03 14:47:03', '10565624530427', NULL),
(1141, 145, '0', '0.00', '38.00', '0', 2, 'TR', 'CF', '', '', 'tesrefg', '2016-06-06 05:57:59', 'dPkLZ2nzqs5W', NULL),
(1142, 145, '0', '38.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'tesrefg', '2016-06-06 05:57:59', 'dPkLZ2nzqs5W', NULL),
(1143, 145, '0', '0.00', '5.00', '0', 12, 'TR', 'CF', '', '', 'tree', '2016-06-06 05:58:38', 'TgyEjF4jDl4v', NULL),
(1144, 145, '0', '5.00', '0.00', '0', 145, 'TR', 'CF', '', '', 'tree', '2016-06-06 05:58:39', 'TgyEjF4jDl4v', NULL),
(1145, 145, '0', '0.00', '5.00', '0', 3, 'TR', 'CHC', '', '', 'vegef', '2016-06-06 05:59:11', '63eRFYuPa4pP', NULL),
(1146, 145, '0', '5.00', '0.00', '0', 145, 'TR', 'CHC', '', '', 'vegef', '2016-06-06 05:59:11', '63eRFYuPa4pP', NULL),
(1147, 145, '', '0.00', '12.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-06 09:52:11', '98219402260147', NULL),
(1148, 145, '', '12.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-06 09:52:11', '98219402260147', NULL),
(1149, 145, '0', '0.00', '100.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-06-06 11:05:18', '243', NULL),
(1150, 145, '0', '100.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-06-06 11:05:18', '243', NULL),
(1151, 145, '0', '0.00', '800.00', '0', 6, 'TR', 'MIB', '', '', '0', '2016-06-06 11:49:07', '21581', NULL),
(1152, 145, '0', '800.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-06-06 11:49:07', '21581', NULL),
(1153, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:44:52', 'OuhCRLTnJ6PB', NULL),
(1154, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:44:52', 'OuhCRLTnJ6PB', NULL),
(1155, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:44:59', 'SlF0ETxiAw0G', NULL),
(1156, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:44:59', 'SlF0ETxiAw0G', NULL),
(1157, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:45:05', 'r9y2AkiEyBP6', NULL),
(1158, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:45:05', 'r9y2AkiEyBP6', NULL),
(1159, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:26', 'A1WWe7wfpBwp', NULL),
(1160, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:26', 'A1WWe7wfpBwp', NULL),
(1161, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:30', '9Fh9xsAvCU4r', NULL),
(1162, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:31', '9Fh9xsAvCU4r', NULL),
(1163, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:32', '0jYyfdaCoyQ9', NULL),
(1164, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:32', '0jYyfdaCoyQ9', NULL),
(1165, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:34', 'DBWubR9aMKig', NULL),
(1166, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:49:34', 'DBWubR9aMKig', NULL),
(1167, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:53:43', 'tpPZ1Yhz7HPL', NULL),
(1168, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:53:43', 'tpPZ1Yhz7HPL', NULL),
(1169, 143, '123456789', '0.00', '10.00', '0', 1, 'TR', 'CHC', '', '', '0', '2016-06-06 12:53:46', 'ns6UCRsO4lH2', NULL),
(1170, 143, '123456789', '10.00', '0.00', '0', 143, 'TR', 'CHC', '', '', '0', '2016-06-06 12:53:46', 'ns6UCRsO4lH2', NULL),
(1171, 145, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', '300.00', '0.00', '0', 0, 'AC', 'CC', '2259458217', '45DY9J', 'Balance Added via Credit Card', '2016-06-07 03:48:27', NULL, NULL),
(1172, 145, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', '10000.00', '0.00', '0', 14, 'AC', 'SC', '', '', '', '2016-06-07 03:53:35', NULL, NULL),
(1173, 145, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', '0.00', '100.00', '0', 142, 'TR', 'TCP', '', '', '0', '2016-06-07 04:06:12', NULL, NULL),
(1174, 142, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', '100.00', '0.00', '0', 145, 'TR', 'TCP', '', '', '0', '2016-06-07 04:06:12', NULL, NULL),
(1175, 145, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', '0.00', '20.00', '0', 1, 'TR', 'DC', '', '', 'gdhhd', '2016-06-07 04:32:35', 'DCR#57564e63d2960', NULL),
(1176, 145, '40414c42558ae654671f6705cc5165a0ae9e319b0059ecb251284ddb16872634', '20.00', '0.00', '0', 145, 'TR', 'DC', '', '', 'gdhhd', '2016-06-07 04:32:35', 'DCR#57564e63d2960', NULL),
(1177, 145, '0', '0.00', '11.00', '0', 145, 'TR', 'CO', '', '', '0', '2016-06-07 05:53:59', '62603', NULL),
(1178, 145, '0', '11.00', '0.00', '0', 1, 'TR', 'CO', '', '', '0', '2016-06-07 05:53:59', '62603', NULL),
(1179, 145, '', '0.00', '10.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 05:57:56', '21609439016319', NULL),
(1180, 145, '', '10.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 05:57:56', '21609439016319', NULL),
(1181, 145, '0', '0.00', '22.00', '0', 145, 'TR', 'CO', '', '', '0', '2016-06-07 06:05:45', '83764', NULL),
(1182, 145, '0', '22.00', '0.00', '0', 1, 'TR', 'CO', '', '', '0', '2016-06-07 06:05:45', '83764', NULL),
(1183, 145, '', '0.00', '12.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 06:05:58', '88163216486573', NULL),
(1184, 145, '', '12.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 06:05:58', '88163216486573', NULL),
(1185, 145, '0', '0.00', '11.00', '0', 145, 'TR', 'CO', '', '', '0', '2016-06-07 06:07:24', '85393', NULL),
(1186, 145, '0', '11.00', '0.00', '0', 1, 'TR', 'CO', '', '', '0', '2016-06-07 06:07:24', '85393', NULL),
(1187, 145, '', '0.00', '2.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 06:07:35', '42269983510486', NULL),
(1188, 145, '', '2.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 06:07:35', '42269983510486', NULL),
(1189, 145, '', '0.00', '20.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 07:53:21', '77693313299678', NULL),
(1190, 145, '', '20.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 07:53:21', '77693313299678', NULL),
(1191, 145, '', '0.00', '23.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 07:55:43', '56245121490210', NULL),
(1192, 145, '', '23.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 07:55:43', '56245121490210', NULL),
(1193, 145, '', '0.00', '12.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 08:00:07', '47304133097641', NULL),
(1194, 145, '', '12.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 08:00:07', '47304133097641', NULL),
(1195, 145, '0', '0.00', '22.00', '0', 145, 'TR', 'CO', '', '', '0', '2016-06-07 08:02:09', '56090', NULL),
(1196, 145, '0', '22.00', '0.00', '0', 1, 'TR', 'CO', '', '', '0', '2016-06-07 08:02:09', '56090', NULL),
(1197, 145, '', '0.00', '34.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 08:02:26', '12777446126565', NULL),
(1198, 145, '', '34.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-07 08:02:26', '12777446126565', NULL),
(1199, 145, '0', '0.00', '9.00', '0', 2, 'TR', 'CHC', '', '', 'tre', '2016-06-07 08:09:22', 'WeJW311rIeVZ', NULL),
(1200, 145, '0', '9.00', '0.00', '0', 145, 'TR', 'CHC', '', '', 'tre', '2016-06-07 08:09:22', 'WeJW311rIeVZ', NULL),
(1201, 200, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '10000.00', '0.00', '0', 26, 'AC', 'SC', '', '', '', '2016-06-08 05:57:41', NULL, NULL),
(1202, 200, '', '0.00', '50.00', '', 200, 'TR', 'CO', '', '', '', '2016-06-08 05:57:56', '44994549048133', NULL),
(1203, 200, '', '50.00', '0.00', '', 200, 'TR', 'CO', '', '', '', '2016-06-08 05:57:56', '44994549048133', NULL),
(1204, 145, '', '0.00', '12.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-08 05:58:50', '16888349866494', NULL),
(1205, 145, '', '12.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-08 05:58:50', '16888349866494', NULL),
(1206, 200, '', '0.00', '50.00', '', 200, 'TR', 'CO', '', '', '', '2016-06-08 05:59:11', '84163732463493', NULL),
(1207, 200, '', '50.00', '0.00', '', 200, 'TR', 'CO', '', '', '', '2016-06-08 05:59:11', '84163732463493', NULL),
(1208, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '90000.00', '0.00', '0', 28, 'AC', 'SC', '', '', '', '2016-06-08 06:34:22', NULL, NULL),
(1209, 184, '', '0.00', '100.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:34:36', '81371466210111', NULL),
(1210, 184, '', '100.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:34:36', '81371466210111', NULL),
(1211, 184, '', '0.00', '100.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:35:47', '57537808311171', NULL),
(1212, 184, '', '100.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:35:47', '57537808311171', NULL),
(1213, 184, '', '0.00', '20.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:39:02', '46605216776952', NULL),
(1214, 184, '', '20.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:39:02', '46605216776952', NULL),
(1215, 184, '', '0.00', '10.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:40:20', '54235839280299', NULL),
(1216, 184, '', '10.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:40:20', '54235839280299', NULL),
(1217, 184, '', '0.00', '10.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:43:41', '73618978965096', NULL),
(1218, 184, '', '10.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:43:41', '73618978965096', NULL),
(1219, 184, '', '0.00', '10.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:48:26', '29389280853793', NULL),
(1220, 184, '', '10.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:48:26', '29389280853793', NULL),
(1221, 184, '', '0.00', '12.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:51:46', '87894455790519', NULL),
(1222, 184, '', '12.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:51:46', '87894455790519', NULL),
(1223, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:53:34', '79917304916307', NULL),
(1224, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:53:34', '79917304916307', NULL),
(1225, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:57:21', '54347177860327', NULL),
(1226, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:57:21', '54347177860327', NULL),
(1227, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:58:54', '82766508683562', NULL),
(1228, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 06:58:54', '82766508683562', NULL),
(1229, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:20:20', '98578266697004', NULL),
(1230, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:20:20', '98578266697004', NULL),
(1231, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:30:10', '55782233444042', NULL),
(1232, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:30:10', '55782233444042', NULL),
(1233, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:33:55', '28898043660447', NULL),
(1234, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:33:55', '28898043660447', NULL),
(1235, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:35:31', '44299227390438', NULL),
(1236, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:35:31', '44299227390438', NULL),
(1237, 184, '', '0.00', '12.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:35:56', '71234128484502', NULL),
(1238, 184, '', '12.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:35:56', '71234128484502', NULL),
(1239, 184, '', '0.00', '11.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:38:33', '68712062980048', NULL),
(1240, 184, '', '11.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:38:33', '68712062980048', NULL),
(1241, 184, '', '0.00', '12.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:38:46', '69183978587388', NULL),
(1242, 184, '', '12.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:38:46', '69183978587388', NULL),
(1243, 184, '', '0.00', '1000.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:51:18', '42710371678695', NULL),
(1244, 184, '', '1000.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 07:51:18', '42710371678695', NULL),
(1245, 145, '0', '0.00', '20.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-06-08 07:55:50', '13023', NULL),
(1246, 145, '0', '20.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-06-08 07:55:50', '13023', NULL),
(1247, 184, '', '0.00', '1000.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 08:02:14', '82407062710262', NULL),
(1248, 184, '', '1000.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 08:02:14', '82407062710262', NULL),
(1249, 184, '', '0.00', '200.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 08:51:14', '92160609308630', NULL),
(1250, 184, '', '200.00', '0.00', '', 184, 'TR', 'CO', '', '', '', '2016-06-08 08:51:14', '92160609308630', NULL),
(1251, 145, '0', '0.00', '8.00', '0', 2, 'TR', 'CHC', '', '', 'cherity', '2016-06-08 08:57:59', 'FsdyaWQjb13G', NULL),
(1252, 145, '0', '8.00', '0.00', '0', 145, 'TR', 'CHC', '', '', 'cherity', '2016-06-08 08:57:59', 'FsdyaWQjb13G', NULL),
(1253, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '0.00', '1000.00', '0', 5, 'TR', 'CHC', '', '', 'fdghfggh', '2016-06-08 09:02:42', 'eD4teq1T4mZX', NULL),
(1254, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '1000.00', '0.00', '0', 184, 'TR', 'CHC', '', '', 'fdghfggh', '2016-06-08 09:02:42', 'eD4teq1T4mZX', NULL),
(1255, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '0.00', '90.00', '0', 5, 'TR', 'CHC', '', '', 'fjhf', '2016-06-08 09:05:25', 'cLcvVSOYE6Hn', NULL),
(1256, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '90.00', '0.00', '0', 184, 'TR', 'CHC', '', '', 'fjhf', '2016-06-08 09:05:25', 'cLcvVSOYE6Hn', NULL),
(1257, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '0.00', '200.00', '0', 5, 'TR', 'CHC', '', '', 'fgbhg', '2016-06-08 09:10:54', 'Lv7YfLHLky15', NULL),
(1258, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '200.00', '0.00', '0', 184, 'TR', 'CHC', '', '', 'fgbhg', '2016-06-08 09:10:54', 'Lv7YfLHLky15', NULL),
(1259, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '0.00', '100.00', '0', 5, 'TR', 'CHC', '', '', 'fg', '2016-06-08 09:11:26', 'PGg2uqjlYtg4', NULL),
(1260, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '100.00', '0.00', '0', 184, 'TR', 'CHC', '', '', 'fg', '2016-06-08 09:11:26', 'PGg2uqjlYtg4', NULL),
(1261, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '0.00', '80.00', '0', 5, 'TR', 'CHC', '', '', 'ghgh', '2016-06-08 09:13:36', '0jcozlH7U91g', NULL),
(1262, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '80.00', '0.00', '0', 184, 'TR', 'CHC', '', '', 'ghgh', '2016-06-08 09:13:36', '0jcozlH7U91g', NULL),
(1263, 204, '9d3141c625193502d929dd463a2f4278c478007964e490c81daba8be4cbb0774', '10000.00', '0.00', '0', 29, 'AC', 'SC', '', '', '', '2016-06-08 09:43:08', NULL, NULL),
(1264, 204, '0', '0.00', '10.00', '0', 30, 'TR', 'MIB', '', '', '0', '2016-06-08 09:45:49', '37899', NULL),
(1265, 204, '0', '10.00', '0.00', '0', 204, 'TR', 'MIB', '', '', '0', '2016-06-08 09:45:49', '37899', NULL),
(1266, 204, '', '0.00', '12.00', '', 204, 'TR', 'CO', '', '', '', '2016-06-08 09:51:21', '17250768984667', NULL),
(1267, 204, '', '12.00', '0.00', '', 204, 'TR', 'CO', '', '', '', '2016-06-08 09:51:22', '17250768984667', NULL),
(1268, 145, '', '0.00', '12.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-08 09:54:37', '25523091261275', NULL),
(1269, 145, '', '12.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-08 09:54:37', '25523091261275', NULL),
(1270, 182, '', '0.00', '5.00', '', 182, 'TR', 'CO', '', '', '', '2016-06-08 19:22:47', '89165591523051', NULL),
(1271, 182, '', '5.00', '0.00', '', 182, 'TR', 'CO', '', '', '', '2016-06-08 19:22:48', '89165591523051', NULL),
(1272, 184, '0', '0.00', '24.00', '0', 6, 'TR', 'MIB', '', '', '0', '2016-06-09 04:44:22', '95807', NULL),
(1273, 184, '0', '24.00', '0.00', '0', 184, 'TR', 'MIB', '', '', '0', '2016-06-09 04:44:22', '95807', NULL),
(1274, 145, '', '0.00', '10.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-09 09:37:48', '56427615010179', NULL),
(1275, 145, '', '10.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-09 09:37:48', '56427615010179', NULL),
(1276, 145, '', '0.00', '10.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-09 09:38:09', '76601034514606', NULL),
(1277, 145, '', '10.00', '0.00', '', 145, 'TR', 'CO', '', '', '', '2016-06-09 09:38:09', '76601034514606', NULL),
(1278, 204, '0', '0.00', '100.00', '0', 44, 'TR', 'GC', '', '', '0', '2016-06-10 07:13:17', 'GCR#575a688dacefb', NULL),
(1279, 204, '0', '100.00', '0.00', '0', 204, 'TR', 'GC', '', '', '0', '2016-06-10 07:13:17', 'GCR#575a688dacefb', NULL),
(1280, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '0.00', '12.00', '0', 5, 'TR', 'CHC', '', '', 'fgfd', '2016-06-10 09:42:37', '6xiW13XyFp5f', NULL),
(1281, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '12.00', '0.00', '0', 184, 'TR', 'CHC', '', '', 'fgfd', '2016-06-10 09:42:37', '6xiW13XyFp5f', NULL),
(1282, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '0.00', '33.00', '0', 5, 'TR', 'CHC', '', '', 'dddd', '2016-06-10 09:43:16', 'gErqapKm8el6', NULL),
(1283, 184, 'cMAGuLg2p2k:APA91bGZ1DhZZxAm0jX0lpz40Al4FOjEFzI5hgkgPrC6e59DZgKAdQ691LhYldnhI0xAOSuuF0sIgdV-2kVhv6lpQqkP5WfSw5LtyQyv6vdot5-SM6SqDI7sgIVP-rboW32sVzH2yrUD', '33.00', '0.00', '0', 184, 'TR', 'CHC', '', '', 'dddd', '2016-06-10 09:43:16', 'gErqapKm8el6', NULL),
(1284, 206, '9d3141c625193502d929dd463a2f4278c478007964e490c81daba8be4cbb0774', '10000.00', '0.00', '0', 30, 'AC', 'SC', '', '', '', '2016-06-10 10:44:22', NULL, NULL),
(1285, 145, '0', '0.00', '12.00', '0', 32, 'TR', 'MIB', '', '', '0', '2016-06-13 04:33:19', '39444', NULL),
(1286, 145, '0', '12.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-06-13 04:33:19', '39444', NULL),
(1287, 145, '0', '0.00', '86.00', '0', 20, 'TR', 'MIB', '', '', '0', '2016-06-13 04:34:30', '65252', NULL),
(1288, 145, '0', '86.00', '0.00', '0', 145, 'TR', 'MIB', '', '', '0', '2016-06-13 04:34:30', '65252', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblUsers`
--

CREATE TABLE `tblUsers` (
  `userId` bigint(20) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `fb_id` varchar(255) NOT NULL,
  `phoneNumber` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `userPIN` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `userPassword` varchar(255) NOT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `api_key` varchar(255) NOT NULL,
  `user_type` int(11) NOT NULL,
  `currency` int(11) DEFAULT '8',
  `activation` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `agent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblUsers`
--

INSERT INTO `tblUsers` (`userId`, `firstName`, `lastName`, `fb_id`, `phoneNumber`, `address`, `city`, `country`, `userPIN`, `email`, `userPassword`, `available`, `api_key`, `user_type`, `currency`, `activation`, `status`, `agent_id`) VALUES
(125, 'Khurram', 'Shehzad', '228635874173371', '03438541418', 'F-5 Islamabad', 'Islamabad', '226', '1829971', 'Khurram@devdesks.com', 'd88fba4fd22ef960a5068f58b7fd42bdc7361c0c', 1, 'e1bc56a5b57aa8924d25b840bb422d6f', 2, 3, '', 0, NULL),
(130, 'Test', 'Testing', '', '2188463494494', NULL, 'Islamabad', '226', '8383844', 'test@mailinator.com', '5e443c003151351b220521f436b12a2ccdd82f6e', 1, 'd2047d8357afdaf9402682965a0a8410', 2, 8, '', 0, NULL),
(131, 'Jim', 'Honk', '10157065460300045', '6649181464', 'Address', 'Islamabad', '226', '8386844', 'uvslicad@sharklasers.com', 'e2aba1aa60461778182481103430b4420bedb62a', 1, 'd4fb6633ca48a4fc81f12a47d85cfe25', 2, 8, '', 0, NULL),
(132, 'Osita', 'Nwankwo', '', '9088843069', '1588 Edmund Terrance', 'Islamabad', '226', '1763684', 'nwankwo908@gmail.com', 'b5e96d31c0fa692e1db3e69eb2815ef873b93e8d', 1, 'd48601fadf10ec0bc11e3ab7d9601970', 2, 8, '', 0, NULL),
(135, 'Sultan', 'Shaukat', '1117287984990306', '3335943626', 'G.13', 'Islamabad', '226', '7444789', 'sultan@devdesks.com', 'c2c64287ee45fbb2727ec36b47256f77f17d977f', 1, '3c8ae35923068d8bcaffae3b64e12a87', 2, 8, '', 1, NULL),
(136, 'Hina', 'Kiran', '', '84353569501', NULL, 'Islamabad', '226', '2385959', 'hina@devdesks.com', '9531158d87c3f8269e8e181b9f337b634e6ed5af', 1, '3529ad6d6a4f7124557bd827ece5653d', 2, 8, '', 0, NULL),
(137, 'lee', 'brown', '', '065319450', 'street 123', 'Islamabad', '226', '9943795', 'lee.brown32145@gmail.com', '5abf5b53c561c1d8ffb28c08fa318dbf1ed10982', 1, '261ddfdcb87295fa44ccdec623cb289a', 2, 8, '', 0, NULL),
(138, 'Osita', 'Nwankwo', '', '9088843069', NULL, NULL, '226', '2411455', 'nhbmediagroup@gmail.com', 'db1e8b83ded5fd9e592f8e338f3e255c63cbeccf', 1, 'd6a4d595e820dcf4974f9c8d1d08f688', 2, 8, '', 0, NULL),
(139, 'Osita', 'Nwankwo', '', '9088843069', NULL, NULL, '226', '9168579', 'otbpictures12@gmail.com', 'eba45513a56ab4ec0f1c958995fa4a5cca29b8bc', 1, '381b3c9ee6e31fc94630e0366af7332e', 2, 1, '', 0, NULL),
(140, 'Osita', 'Nwankwo', '', '9088843069', NULL, NULL, '226', '8983595', 'nwankwo908@hotmail.com', '02eb9d91dbc3522d06fbb5130335da51479ba908', 1, 'bc2c752a7fe8649dd36b53ad39898bfc', 2, 1, '', 0, NULL),
(141, 'Adnan', 'Haider', '485242458328267', '046535356', 'Islamabad', 'Islamabad', '226', '9199681', 'adnan@devdesks.com', '1a33779d57abe280e7d0e9e8f7b60f5c8488411e', 1, '337ec3008b41cc32d038d80357fcfff3', 2, 8, '', 0, NULL),
(142, 'Sultan', 'Ali', '', '03335949626', 'G13', 'Islamabad', '226', '1177942', 'alisultan482@gmail.com', '792eed93e471be02b2efbef737e7c795c655a564', 1, '783b7dec7fc67c3cdcba1c20c9a0f28b', 2, 8, '', 0, NULL),
(143, 'Naveed', 'Arshad', '123456789', '090078601', 'sggf', 'dghh', '226', '1234567890', 'naveed@yopmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1, 'e95544476f2d71210e1e0f02a647f920', 2, 8, '', 1, NULL),
(144, 'dev', 'adnan', '', '923454825847', NULL, NULL, '226', '4741277', 'adi908242@gmail.com', '827ac818d422f57b93682a8478afa2d4f026513d', 1, '825399b89a56f4532957c8b8f6ea1da1', 2, 8, '', 0, NULL),
(145, 'Nauman', 'Malik', '227396064290683', '03335007726', 'F 5/1', 'Islamabad', '226', '1234567890', 'nouman.malik@devdesks.com', '7110eda4d09e062aa5e4a390b0a572ac0d2c0220', 1, 'c263f161f93bdcaf3031d982ffd3bef2', 2, 3, '', 1, NULL),
(149, 'Sultan', 'Ali', '', '3335949626', NULL, NULL, '226', '7716846', 'Mudasir.nazir@devdesks.com', '364f5a1d50a658ff06ade29daf12c25c21de114d', 1, '0763659832e4cc4bd6bb3a0649b63cc7', 2, 8, '', 0, NULL),
(150, 'Sultan', 'Ali', '', '03335949626', 'G13', 'Islamabad', '226', '7266222', 'Mudasir@devdesks.com', 'ae504127be4fdbd5a07adb43505b306ec4f867dc', 1, 'e2f970ab2322d92fb36d9375a2d015e6', 0, 8, '', 0, NULL),
(151, 'Obi', 'Abuchi', '', '4136246626', '123 chestnut place', 'redwood', '226', '9364324', 'hmbadugha@gmail.com', '9c2e79b57d0c4d70045d080062aa0918524ac0f7', 1, 'a859a2af66a1be7a8b7f87dfda13789a', 0, 1, '', 0, NULL),
(153, 'Ahsan', 'Ali', '', '3335949626', NULL, NULL, '226', '8356129', 'ahsan@devdesks.com', 'e9f8027c6d1ac0f16703285b89215342657016a9', 1, 'd8b1ade921d45aff9d4e4957af50e786', 0, 8, '', 0, NULL),
(154, 'D', 'D', '', '55', NULL, NULL, '226', '7363752', 'ddddd@gmail.com', '54a8331f9d80efa2e72b8779c304f61b135ddd63', 1, '59472198b73bad3d6b84cc3145f5a2d5', 0, 8, '', 0, NULL),
(161, 'zohaib92', 'Zohaib', '', '03440599039', 'Xyz', 'Rwp', '226', '1355622', 'Zohaib.idrees92@gmail.com', 'c9214c9a4616b87f9c660babcf4ee31bda3a7408', 1, '4036aecf99c1bd23f969d2d5f0fcb0cb', 0, 8, '', 0, NULL),
(162, 'Hina', 'Khan', '', '3335949626', NULL, NULL, '226', '1431611', 'hina@devdesks.vom', '6777ea49f3c51a1b5e7a4c8abd9684894a58613d', 1, '5020e7f61e31367b169b12f5706e4afd', 0, 8, '', 0, NULL),
(163, 'Manish', 'Balakrishnan', '', '8609902447', NULL, 'redwood', '226', '2919943', 'manish@imalimobile.com', '4746fa1e099ed970a1068be328a1a52176deff97', 1, '44897e3ac792f319ef8cf69c87ee5ad1', 0, 8, '', 0, NULL),
(164, 'Obi', 'Abuchi', '', '4136246626', NULL, NULL, '156', '4633455', 'test@gmail.com', '1414ca2bd77d49f8ac210d0f57f846e05e909c1a', 1, 'ee98fb256cd320c6d4c1e2e5f5b789b0', 0, 1, '', 0, NULL),
(165, 'Manish', 'Balakrishnan', '', '8609902447', '1028 Boilevard', 'west Hartford', '156', '6765287', 'manish@dshgsonic.com', '05fb0ccd6401a6e18e98a71818994070be7c1962', 1, '78ce4f551e58d42b971e1776eaa6358f', 0, 1, '', 0, NULL),
(167, 'Rocky', 'gray', '10157065460300045', '7189137501', '33 Bryant ace', 'Bloomfield', '156', '4684547', 'yoorokgray@gmail.com', 'b2734281b95f1e3c58e230cd3ef308a2dd9579cd', 1, '504fc39a07232c6528ca36c2ac84e274', 0, 1, '', 0, NULL),
(169, 'Ifeoma', 'Ndugba', '', '18622249296', '33 bryant ave', 'Bloomfield', '156', '5274654', 'Ifeoma_ndugba@yahoo.com', 'ce181f6d0b5b32926273312822c2a97fea6cefa4', 1, '86af252eee6c26eaa36b9c02090009b0', 0, 1, '', 0, NULL),
(171, 'Danial', 'azafar', '', '03335007726', 'abc', 'abc', '156', '8484592', 'danial@devdesks.com', '2b964fabc2770ec9d557a063b1ec09f0a282fe51', 1, 'f8c8058ba9600516c403f531e165b276', 0, 8, '', 0, NULL),
(172, 'abc', 'abc', '', '123456', 'hnb', 'hjj', '156', '4799649', 'naveed1@yopmail.com', '9a50c8a873a65b2b15d3b8d52b9dd9de0559916a', 1, '7af2a36d55951ab019a56912937cec14', 0, 8, '', 0, NULL),
(173, 'james', 'john', '', '38383993929', NULL, NULL, '156', '6372516', 'js963844@gmail.com', 'd7c487302da837857c0af3d9405e7b8e2ef85c3b', 1, '7594881fa6cf8ab921a97958d8f8ed57', 0, 1, '', 0, NULL),
(174, 'james', 'jo', '', '6377373838', 'islamabd', 'islamabd', '156', '4588117', 'yjames820@gmail.com', '07e65fb016f6f79d5310da3e83cec0c1aecd8720', 1, '95e41247e1e03d4829820a39edc6604d', 0, 1, '', 0, NULL),
(175, 'just', 'me', '', '3013524163', NULL, NULL, '156', '3879788', 'justme@yahoo.com', 'ae66c62c9cf7db0a9bd54637b28ebfe0b65402c1', 1, '6518d187ee130446d9803a923abb10e7', 0, 1, '', 0, NULL),
(176, 'nnagozie', 'ifebi', '', '2403554710', '10108 cascade falls ct', 'owings mills', '156', '8299523', 'nnagozy@yahoo.com', 'b6a97ee3df45a6cf0458940e458235edbbcf2552', 1, 'ae593a9158dceb7c898bc8de3345fbff', 0, 1, '', 0, NULL),
(177, 'Nnamdi', 'Celestine', '', '08037407228', '183 ijebu st', 'lagos', '156', '8621295', 'cele_cares@yahoo.ie', '0012e4f1dc0e5920644dc5eed874ce6baa7e25d7', 1, '1b8c5ba618c68194b95f6fbc199bbac8', 0, 1, '', 0, NULL),
(179, 'chinenye', 'Ilechukwu', '', '08023630972', NULL, NULL, '156', '1921327', 'chinenyeilechukwu@ymail.com', 'e5cb99e93e3a3839e702691c45d6cc28ffcabf9e', 1, '5ba88533e83a483fd05c3fdb8120870b', 0, 1, '1821167271', 0, NULL),
(180, 'Ugochukwu', 'Mbanugo', '', '9732890544', '33 Bryant ave', 'Bloomfield', '156', '4543465848', 'umbanugo@imalimobile.com', 'b895065ea78dc2bfc5c4ddb677d8f0c0ab250ccb', 1, 'e74c49763cb697d209bf793ca6ab06e6', 0, 1, '', 0, NULL),
(181, 'Manish', 'Balakrishnan', '', '8609902447', NULL, NULL, NULL, '1655928464', 'manishb01@gmail.com', '232a9db78e9e90eae13aefa2d2a2537749c7d81a', 1, 'e2d0f6ec3caa73d7e75c39a8138c940e', 0, 8, '', 0, NULL),
(182, 'Obi', 'Abuchi', '', '4136246626', '123 Main Street', 'Palo Alto', '226', '1234567890', 'hmbadugha@imalimobile.com', 'ff8dba29c5059915cdc37fc87f8a9d1835f70897', 1, 'c2d80ea90164acc3bf79660b08d1d1d4', 0, 8, '', 0, NULL),
(183, 'Ijeoma', 'Mbanugo', '', '9082563708', NULL, NULL, NULL, '4964956638', 'ijeluv1@gmail.com', '0311e1791c1fe92ecd3215fa0472bce6bf2b252d', 1, '1c028f6fc8bf9f95bc3705f208935a2e', 0, 8, '', 0, NULL),
(184, 'Malik', 'Qirtas', '', '0322101991', 'House 100', 'islamabad', '226', '1111111111', 'qirtas333@gmail.com', 'b74721f0cd0cd21b1ce967f427280881fe9132fb', 1, 'b93b421947cdd7f0b6d00133fd5f0623', 0, 8, '', 0, NULL),
(185, 'Nauman', 'Nauman', '', '12345678', 'Test address', 'Islamabad', 'Pakistan', '6427364919', 'nauman1234@yopmail.com', '65fbca9d1795e12f0553d37ea691faa157fa8700', 1, 'dd4a765547263b74bccb89eeb7e79cca', 0, 3, '', 0, NULL),
(186, 'Ali', 'khan', '', '58587585', NULL, NULL, NULL, '6719127897', 'sdvjvjhv@hgjhg.com', '8634355180f8ebd819b66dc59bf89118df5f8920', 1, '2c92b002412a5d3384775a6360f309bf', 0, 8, '', 0, NULL),
(187, 'nm', 'mn', '', '1234', 'bbjhjj,n mm', 'test', '226', '6239264932', 'nauman123@yopmail.com', '8ff43d7ca2cf43a742557f16567acee7c051e24e', 1, '333d324182d72a14fcc31a8e4806d371', 0, 3, '', 0, NULL),
(188, 'malik', 'malik', '', '12345', 'test', 'testing', '226', '6852641614', 'nm@yopmail.com', '51482b4f798f84addc996e559ea54571a72642b2', 1, '1f991ee9f6551f7ed6e94e8146be50d8', 0, 8, '', 0, NULL),
(189, 'Naveed', 'Arshad', '', '090078601', NULL, NULL, NULL, '8644327557', 'naveed@devdesks.com', '568138fb951d9f68253812af0f47cbacc3a79c42', 1, '2e92a7705f93c97ecf6e23827a2c0420', 0, 8, '', 0, NULL),
(190, 'Kirtas', 'Malik', '', '44548', NULL, NULL, NULL, '9562556329', 'kirtas@yopmail.com', '24bb65c70ba54237d984556989945405a9bd3677', 1, 'aeaca56a224d690a92f0b6efc0414fcc', 0, 8, '', 0, NULL),
(191, 'dfbjkd', 'fdsf', '', '5435', NULL, NULL, NULL, '3897448258', 'pk@yopmail.com', 'c3780f1bf13c6a9506ae61d45f775ef2a2a515c4', 1, 'f478f932b0fdea62f326fea7a66379c8', 0, 8, '', 0, NULL),
(192, 'gr', 'regr', '', '3454', NULL, NULL, NULL, '2423743398', 'pk1@yopmail.com', '8d7391e8d11a925e700a9b962c1638c081fe427d', 1, '7cb2071ade210fad846e284d98f5f8aa', 0, 8, '', 0, NULL),
(193, 'danish', 'khan', '', '6574657896', NULL, NULL, NULL, '4668328233', 'danish@yopmail.com', 'b1c26e9347ef547ff06845ca38cc443aedc4fa86', 1, 'dbd65018a8f089b79899775d95ad7d91', 0, 8, '', 0, NULL),
(195, 'Chuka', 'Obi', '', '08095187366', '226b etiosa street dolphin estate', 'ikoyi lagos', '156', '6833861958', 'Chukaobi@gmail.com', 'c709657c4d3b2f771c400a033001259ffc42f168', 1, 'b950d2f80096c303852c0e19fd03ade4', 0, 1, '', 0, NULL),
(196, 'uuu', 'mee', '', '12345', NULL, NULL, NULL, '3853284857', 'tree@yopmail.com', 'b8637b80556e92c03860bf2886e8be112c77974b', 1, '6737f49ce8d7d866c34d79e025de5342', 0, 8, '', 0, NULL),
(197, 'Naveed', 'Arshad', '', '090078601', 'A', 'Tex', '4', '2234827655', 'naveed2@yopmail.com', 'd3ec16d1868edc7da81be9fc203f819281c0e4ba', 1, '35b40a1ea14e926910537cbb9bfa68cd', 0, 3, '', 0, NULL),
(198, 'gfg', 'fdg', '', '435234', NULL, NULL, NULL, '6653183114', 's@yopmail.com', 'be98fbb5fef1a2a7ce8be317fcbce6b1912024c3', 1, '659b3070e2cf385a9b31dae1f244cd79', 0, 8, '', 0, NULL),
(199, 'gfdg', 'fdgfd', '', '54363', 'fdghdf', 'fgh', 'fgshgh', '8265226767', 'd@yopmail.com', 'c6105bb8d2c97ee05a22ca1f2760295d30145513', 1, '5f715cd967f313d3036ac7d3d649c531', 0, 3, '', 0, NULL),
(200, 'dfgbvjhg', 'gjig', '', '76786', 'fdgrf', 'rfgerw', 'fdhfg', '1466687913', 'b@yopmail.com', 'c60d81f35d8be966fa0437e43f1feff777c6e121', 1, 'bf9890e38b7c5d1dd4e8388b20edbab1', 0, 2, '', 0, NULL),
(201, 'gfdg', 'fdgdf', '', '5635', NULL, NULL, NULL, '4295649786', 'n@yopmail.com', '2bafcab69af7cc122e0243be50dbbfcc3da9c224', 1, '4361a7c68a75c6e48427230e212f14a5', 0, 8, '', 0, NULL),
(202, 'dsfg', 'saga', '', '57', 'hsdf', 'shh', 'United Kingdom', '4469446828', 't@yopmail.com', 'ae19e7d59819c9c43b9b60a464107ab34124efb9', 1, 'f60e5224a9ec09a07776898cf95db81c', 0, 3, '', 0, NULL),
(203, 'jhjk', 'hgk', '', '9867', NULL, NULL, NULL, '4331754182', 'g@yopmail.com', '8145abf752492c717f08f71ca1b9cf0f3d374e45', 1, 'e54b009693830fa1c3d5a68fffcdc1b2', 0, 8, '', 0, NULL),
(204, 'Sultan', 'Ali', '', '3335949626', NULL, NULL, NULL, '1215398277', 'test@yopmail.com', '5688506322eba14a54bfc74b39e6bcc5b76ab2f7', 1, 'f2ed2ee1a898455ce07d4e57313a8ffb', 0, 8, '', 0, NULL),
(205, 'gjkg', 'jkghjgh', '', '786578959', NULL, NULL, NULL, '7289823827', 'o@yopmail.com', '0581e7c94c284870d30659a6c0e449a9a7ab4ed1', 1, '574be32c900258608d0c67b7c2857c69', 0, 8, '', 0, NULL),
(206, 'Ugochukwu', 'Mbanugo', '', '9732890544', NULL, NULL, NULL, '2386396366', 'saosimeon@yahoo.com', '21fe8b6be0dde0b1a60bbee1b9061b41587722ab', 1, '037575d657684932de17b55b65d48342', 0, 8, '', 0, NULL),
(207, 'trst', 'user', '', '12345678', NULL, NULL, NULL, '1758328683', 'testuser@yopmail.com', 'b3ad32a369754ee7acf23c5370ccf5b401604a78', 1, 'd1602eb35c2f8369a77e8488cc8adce7', 0, 8, '', 0, NULL),
(208, 'gg', 'gjkgkj', '', '6969', NULL, NULL, NULL, '5413768718', 'l@yopmail.com', 'a64055ec6fda567b150c875bc42966fe9c409731', 1, 'ea5615a5568bc4c0b6296bdcd6fd89e5', 0, 8, '', 0, NULL),
(209, 'ghjk', 'jghjk', '', '875687', NULL, NULL, NULL, '9699673731', 'p@yopmail.com', '67857952cb9fedc88202f36aeed36ec79aee11a1', 1, '1b8766a9e208ed993bcdb2ab38a26b9f', 0, 8, '', 0, NULL),
(210, 'danish', 'khan', '\\"\\"', '576757', NULL, NULL, NULL, '6735728713', 'qq@yopmail.com', 'd8531a519b3d4dfebece0259f90b466a23efc57b', 1, '911b05508f60dbe77805ef4e8a4c0552', 0, 3, '', 0, NULL),
(211, 'ggiu', 'uiiu', '', '2346', NULL, NULL, NULL, '7728557252', 'r@hjfgjh.com', '23c1b78afe8d17b64e5572d7af014b17e6189b52', 1, '10eb39e5581c39d9d4309cec554ac799', 0, 8, '', 0, NULL),
(212, 'hgkjg', 'jkgkj', '', '698', NULL, NULL, NULL, '5374568649', 'yyy@yuy.com', 'f166048d6fb38c895db324b1e2d58e02d7ae9f81', 1, '12631e115703a6da1c24ba797b09c3fc', 0, 8, '', 0, NULL),
(213, 'hh', 'yhkjh', '', '767896', NULL, NULL, NULL, '1584934682', 'mm@yopmail.com', 'aef2a867acf487ce94256c4e3b5ca98cebed8e0a', 1, '5cac5140311038faab09f8b12ab33661', 0, 8, '', 0, NULL),
(214, 'jk', 'kj', '', '6777', NULL, NULL, NULL, '1689342495', 'll@yopmail.com', '69ad4cd0a2b696e74f920753dfc3439fe7ccef08', 1, '5c865e5f7c4ee04c315ac4b0080d0905', 0, 8, '', 0, NULL),
(215, 'David', 'Anaghara', '', '+2348033406123', NULL, NULL, NULL, '2959677429', 'dlaface@yahoo.com', 'ce1d36430e448692d03e0c7d82b92b6586d17505', 1, 'd5b517b430e66d71b3a8094439a77b1c', 0, 8, '', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblUtilityBills`
--

CREATE TABLE `tblUtilityBills` (
  `id` bigint(20) NOT NULL,
  `userId` bigint(20) DEFAULT NULL,
  `provider_id` bigint(20) DEFAULT NULL,
  `customer_id` varchar(255) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `dated` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `paid` tinyint(1) DEFAULT NULL,
  `transaction_track_code` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblUtilityBills`
--

INSERT INTO `tblUtilityBills` (`id`, `userId`, `provider_id`, `customer_id`, `comments`, `amount`, `dated`, `paid`, `transaction_track_code`, `status`) VALUES
(9, 125, 2, '12345678', NULL, '20.00', '2016-03-18 11:00:48', 1, '37458', 1),
(10, 125, 2, '12345678', NULL, '200.00', '2016-03-18 11:04:06', 1, '33864', 1),
(11, 125, 1, '789456123', NULL, '100.00', '2016-03-18 11:07:24', 1, '89593', 1),
(12, 125, 1, 'Dghhcc', NULL, '71.00', '2016-03-18 11:13:14', 1, '26156', 1),
(15, 131, 1, 'gfdr', NULL, NULL, '2016-04-04 15:44:11', 0, NULL, 1),
(16, 125, 1, 'Gjggg', NULL, '10.00', '2016-04-05 06:08:32', 1, '13382', 1),
(17, 125, 2, 'Hgg', NULL, '10.00', '2016-04-05 06:08:53', 1, '5157', 1),
(23, 134, 2, 'heeje', NULL, NULL, '2016-04-14 12:55:13', 0, NULL, 1),
(25, 131, 1, 'rnwapko', NULL, NULL, '2016-04-19 17:50:42', 0, NULL, 1),
(26, 131, 1, 'rnwapko', NULL, NULL, '2016-04-19 17:50:44', 0, NULL, 1),
(27, 131, 1, '3346543356', NULL, NULL, '2016-04-19 17:52:53', 0, NULL, 1),
(28, 134, 2, '18384999', NULL, '200.00', '2016-04-28 04:51:03', 1, '27410', 1),
(29, 134, 2, '18384999', NULL, '2.00', '2016-04-28 04:53:19', 1, '66394', 1),
(30, 134, 1, '4456', NULL, '20.00', '2016-04-28 04:59:20', 1, '26751', 1),
(31, 134, 1, '4456', NULL, '2.00', '2016-04-28 05:00:02', 1, '46896', 1),
(32, 135, 2, '6785189', NULL, '10.00', '2016-05-02 12:40:53', 1, '89904', 1),
(33, 134, 2, '3335949626', NULL, '100.00', '2016-05-17 08:57:02', 1, '99748', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblUtilityBillsAccount`
--

CREATE TABLE `tblUtilityBillsAccount` (
  `id` bigint(20) NOT NULL,
  `accountTitle` varchar(255) DEFAULT NULL,
  `accountNumber` varchar(255) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblUtilityBillsAccount`
--

INSERT INTO `tblUtilityBillsAccount` (`id`, `accountTitle`, `accountNumber`, `available`) VALUES
(1, 'Utility Bills', '090078601', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblUtilityCompanies`
--

CREATE TABLE `tblUtilityCompanies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `account_title` varchar(255) DEFAULT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblUtilityCompanies`
--

INSERT INTO `tblUtilityCompanies` (`id`, `name`, `phone`, `account_title`, `account_number`, `status`) VALUES
(1, 'Xyz Company', '00821234567889', 'Xyz Company Account', '0021296058', 1),
(2, 'Abc Company', '00823445432217', 'Abc Company Account', '0121254605', 1);

-- --------------------------------------------------------

--
-- Table structure for table `usertype`
--

CREATE TABLE `usertype` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usertype`
--

INSERT INTO `usertype` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Merchant'),
(4, 'Agent');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_branches`
--
ALTER TABLE `bank_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `base_currency`
--
ALTER TABLE `base_currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manual_transactions`
--
ALTER TABLE `manual_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblAccountsPayable`
--
ALTER TABLE `tblAccountsPayable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblAdmins`
--
ALTER TABLE `tblAdmins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tblAgents`
--
ALTER TABLE `tblAgents`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `tblBankAccounts`
--
ALTER TABLE `tblBankAccounts`
  ADD PRIMARY KEY (`bankAccountId`,`userId`,`bankId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `bankId` (`bankId`);

--
-- Indexes for table `tblBankTransfer`
--
ALTER TABLE `tblBankTransfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblCashInHand`
--
ALTER TABLE `tblCashInHand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `tblCashInHandMerchant`
--
ALTER TABLE `tblCashInHandMerchant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `tblCashoutPin`
--
ALTER TABLE `tblCashoutPin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblCashTransfer`
--
ALTER TABLE `tblCashTransfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblCharges`
--
ALTER TABLE `tblCharges`
  ADD PRIMARY KEY (`charges_id`);

--
-- Indexes for table `tblCharityAccount`
--
ALTER TABLE `tblCharityAccount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblCharityInvitations`
--
ALTER TABLE `tblCharityInvitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblCrowdfund`
--
ALTER TABLE `tblCrowdfund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblCrowdfundInvitations`
--
ALTER TABLE `tblCrowdfundInvitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblDcAccount`
--
ALTER TABLE `tblDcAccount`
  ADD PRIMARY KEY (`DcAccountId`);

--
-- Indexes for table `tblDcSubscriptions`
--
ALTER TABLE `tblDcSubscriptions`
  ADD PRIMARY KEY (`subId`);

--
-- Indexes for table `tblDepositAccounts`
--
ALTER TABLE `tblDepositAccounts`
  ADD PRIMARY KEY (`depositAccountId`);

--
-- Indexes for table `tbldepositSlip`
--
ALTER TABLE `tbldepositSlip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblDevices`
--
ALTER TABLE `tblDevices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblExchangeRates`
--
ALTER TABLE `tblExchangeRates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblFbToken`
--
ALTER TABLE `tblFbToken`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblFriends`
--
ALTER TABLE `tblFriends`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblGcAccount`
--
ALTER TABLE `tblGcAccount`
  ADD PRIMARY KEY (`GcAccountId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `tblGcRequests`
--
ALTER TABLE `tblGcRequests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblIpool`
--
ALTER TABLE `tblIpool`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblIpoolInvitation`
--
ALTER TABLE `tblIpoolInvitation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblMarket`
--
ALTER TABLE `tblMarket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblMerchantInvoices`
--
ALTER TABLE `tblMerchantInvoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblMerchantUsers`
--
ALTER TABLE `tblMerchantUsers`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `tblNotificationHistory`
--
ALTER TABLE `tblNotificationHistory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblNotifications`
--
ALTER TABLE `tblNotifications`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `tblPromotionCoupons`
--
ALTER TABLE `tblPromotionCoupons`
  ADD PRIMARY KEY (`promotionCouponId`);

--
-- Indexes for table `tblPromotions`
--
ALTER TABLE `tblPromotions`
  ADD PRIMARY KEY (`promotionId`);

--
-- Indexes for table `tblPromotionsAccount`
--
ALTER TABLE `tblPromotionsAccount`
  ADD PRIMARY KEY (`PromotionsAccountId`);

--
-- Indexes for table `tblProviders`
--
ALTER TABLE `tblProviders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblProviderTypes`
--
ALTER TABLE `tblProviderTypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblrefund`
--
ALTER TABLE `tblrefund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblRoles`
--
ALTER TABLE `tblRoles`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `tblScratchCards`
--
ALTER TABLE `tblScratchCards`
  ADD PRIMARY KEY (`cardId`);

--
-- Indexes for table `tblStatus`
--
ALTER TABLE `tblStatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblTransactions`
--
ALTER TABLE `tblTransactions`
  ADD PRIMARY KEY (`transId`);

--
-- Indexes for table `tblUsers`
--
ALTER TABLE `tblUsers`
  ADD PRIMARY KEY (`userId`);

--
-- Indexes for table `tblUtilityBills`
--
ALTER TABLE `tblUtilityBills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblUtilityBillsAccount`
--
ALTER TABLE `tblUtilityBillsAccount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblUtilityCompanies`
--
ALTER TABLE `tblUtilityCompanies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usertype`
--
ALTER TABLE `usertype`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `bank_branches`
--
ALTER TABLE `bank_branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `base_currency`
--
ALTER TABLE `base_currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `manual_transactions`
--
ALTER TABLE `manual_transactions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tblAccountsPayable`
--
ALTER TABLE `tblAccountsPayable`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblAdmins`
--
ALTER TABLE `tblAdmins`
  MODIFY `admin_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tblAgents`
--
ALTER TABLE `tblAgents`
  MODIFY `userId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- AUTO_INCREMENT for table `tblBankAccounts`
--
ALTER TABLE `tblBankAccounts`
  MODIFY `bankAccountId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `tblBankTransfer`
--
ALTER TABLE `tblBankTransfer`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tblCashInHand`
--
ALTER TABLE `tblCashInHand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tblCashInHandMerchant`
--
ALTER TABLE `tblCashInHandMerchant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblCashoutPin`
--
ALTER TABLE `tblCashoutPin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `tblCashTransfer`
--
ALTER TABLE `tblCashTransfer`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tblCharges`
--
ALTER TABLE `tblCharges`
  MODIFY `charges_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tblCharityAccount`
--
ALTER TABLE `tblCharityAccount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tblCharityInvitations`
--
ALTER TABLE `tblCharityInvitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tblCrowdfund`
--
ALTER TABLE `tblCrowdfund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tblCrowdfundInvitations`
--
ALTER TABLE `tblCrowdfundInvitations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT for table `tblDcAccount`
--
ALTER TABLE `tblDcAccount`
  MODIFY `DcAccountId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblDcSubscriptions`
--
ALTER TABLE `tblDcSubscriptions`
  MODIFY `subId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tblDepositAccounts`
--
ALTER TABLE `tblDepositAccounts`
  MODIFY `depositAccountId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;
--
-- AUTO_INCREMENT for table `tbldepositSlip`
--
ALTER TABLE `tbldepositSlip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `tblDevices`
--
ALTER TABLE `tblDevices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `tblExchangeRates`
--
ALTER TABLE `tblExchangeRates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tblFbToken`
--
ALTER TABLE `tblFbToken`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tblFriends`
--
ALTER TABLE `tblFriends`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tblGcAccount`
--
ALTER TABLE `tblGcAccount`
  MODIFY `GcAccountId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tblGcRequests`
--
ALTER TABLE `tblGcRequests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tblIpool`
--
ALTER TABLE `tblIpool`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tblIpoolInvitation`
--
ALTER TABLE `tblIpoolInvitation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblMarket`
--
ALTER TABLE `tblMarket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblMerchantInvoices`
--
ALTER TABLE `tblMerchantInvoices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=413;
--
-- AUTO_INCREMENT for table `tblMerchantUsers`
--
ALTER TABLE `tblMerchantUsers`
  MODIFY `userId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tblNotificationHistory`
--
ALTER TABLE `tblNotificationHistory`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblNotifications`
--
ALTER TABLE `tblNotifications`
  MODIFY `notification_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblPromotionCoupons`
--
ALTER TABLE `tblPromotionCoupons`
  MODIFY `promotionCouponId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblPromotions`
--
ALTER TABLE `tblPromotions`
  MODIFY `promotionId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tblPromotionsAccount`
--
ALTER TABLE `tblPromotionsAccount`
  MODIFY `PromotionsAccountId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblProviders`
--
ALTER TABLE `tblProviders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblProviderTypes`
--
ALTER TABLE `tblProviderTypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblrefund`
--
ALTER TABLE `tblrefund`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblRoles`
--
ALTER TABLE `tblRoles`
  MODIFY `role_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tblScratchCards`
--
ALTER TABLE `tblScratchCards`
  MODIFY `cardId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `tblStatus`
--
ALTER TABLE `tblStatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblTransactions`
--
ALTER TABLE `tblTransactions`
  MODIFY `transId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1289;
--
-- AUTO_INCREMENT for table `tblUsers`
--
ALTER TABLE `tblUsers`
  MODIFY `userId` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;
--
-- AUTO_INCREMENT for table `tblUtilityBills`
--
ALTER TABLE `tblUtilityBills`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tblUtilityBillsAccount`
--
ALTER TABLE `tblUtilityBillsAccount`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblUtilityCompanies`
--
ALTER TABLE `tblUtilityCompanies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usertype`
--
ALTER TABLE `usertype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
